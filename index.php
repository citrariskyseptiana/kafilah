<?php

session_start();

require 'app/api/vendor/autoload.php';

/* --- System --- */
require 'app/api/systems/gump-validation/gump.class.php';
require 'systems/domain.php';
require 'systems/systems.php';
require 'app/api/systems/functions.php';
require 'app/api/systems/nested/Nested.php';
if (file_exists('app/api/vendor/cahkampung/landa-php/src/LandaPhp.php')) {
    require 'app/api/vendor/cahkampung/landa-php/src/LandaPhp.php';
}

$config = [
    'displayErrorDetails'               => config('DISPLAY_ERROR'),
    'determineRouteBeforeAppMiddleware' => true,
];

$app = new \Slim\App(["settings" => $config]);

require 'app/api/systems/dependencies.php';
require 'systems/middleware.php';

/** route to php file */
$file = getUrlFile();
require $file;

$app->run();
