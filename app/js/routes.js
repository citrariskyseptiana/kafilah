angular.module("app").config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider", "$breadcrumbProvider",
    function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
        $urlRouterProvider.otherwise("/dashboard");
        $ocLazyLoadProvider.config({
            debug: false
        });
        $breadcrumbProvider.setOptions({
            prefixStateName: "app.main",
            includeAbstract: false,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
        });

        var currTime = $.now();

        $stateProvider.state("app", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Root",
                skip: true
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ],
            }
        }).state("app.main", {
            url: "/dashboard",
            cache:false,
            templateUrl: "tpl/dashboard/dashboard.html?t=" + currTime,
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/dashboard/dashboard.js?t=" + currTime]
                            });
                        });
                    }
                ]
            }
        }).state("app.generator", {
            url: "/generator",
            templateUrl: "tpl/generator/index.html",
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js?t=" + currTime]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/generator/index.js?t=" + currTime]
                            });
                        });
                    }
                ]
            }
        }).state("pengguna", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?t=" + currTime,
            ncyBreadcrumb: {
                label: "User Login"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("pengguna.akses", {
          cache:false,
            url: "/hak-akses",
            templateUrl: "tpl/m_akses/index.html?t=" + currTime,
            ncyBreadcrumb: {
                label: "Hak Akses"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_akses/index.js?t=" + currTime]
                        });
                    }
                ]
            }
        }).state("pengguna.user", {
            url: "/user",
            templateUrl: "tpl/m_user/index.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/index.js?t=" + currTime]
                        });
                    }
                ]
            }
        }).state("pengguna.profil", {
            url: "/profil",
            templateUrl: "tpl/m_user/profile.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Profil Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/profile.js?t=" + currTime]
                        });
                    }
                ]
            }
        })

        // State Master
        .state("master", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?t=" + currTime,
            cache: false,
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("master.kategori", {
            url: "/kategori-produk",
            templateUrl: "tpl/m_kategori_produk/kategori.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Kategori"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_kategori_produk/kategori.js?t=" + currTime]
                        });
                    }
                ]
            }
        }).state("master.produk", {
            url: "/produk",
            templateUrl: "tpl/m_produk/produk-index.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Produk"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['angularFileUpload']).then(() => {
							return $ocLazyLoad.load({
								files: ["tpl/m_produk/produk.js?t=" + currTime]
							});
						});
                    }
                ]
            }
        })
        .state("master.promo", {
            url: "/promo",
            templateUrl: "tpl/m_promo/promo.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Promo"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['naif.base64']).then(() => {
            						return $ocLazyLoad.load({
            							files: ["tpl/m_promo/promo.js?t=" + currTime]
            						});
          						});
                    }
                ]
            }
        })
        .state("master.testimoni", {
            url: "/testimoni",
            templateUrl: "tpl/m_testimoni/testimoni.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Testimoni"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load({
							files: ["tpl/m_testimoni/testimoni.js?t=" + currTime]
						});
                    }
                ]
            }
        })
        .state("master.slider", {
            url: "/slider",
            templateUrl: "tpl/m_slider/slider.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Slider"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['naif.base64']).then(() => {
            						return $ocLazyLoad.load({
            							files: ["tpl/m_slider/slider.js?t=" + currTime]
            						});
          						});
                    }
                ]
            }
        })
        .state("master.big_order", {
            url: "/big-order",
            templateUrl: "tpl/m_big_order/big.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Big Order"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load({
							files: ["tpl/m_big_order/big.js?t=" + currTime]
						});
                    }
                ]
            }
        })
        .state("master.member", {
            url: "/member",
            templateUrl: "tpl/m_member/member.html?t=" + currTime,
            cache: false,
            params: {
              obj: null
            },
            ncyBreadcrumb: {
                label: "Master Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_member/member.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("master.member_profil", {
            url: "/member-profil",
            templateUrl: "tpl/m_member/member-profile.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Profil Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_member/member-profile.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("master.pengajuan", {
            url: "/pengajuan",
            templateUrl: "tpl/m_pengajuan/pengajuan.html?t=" + currTime,
            cache: false,
            params: {
              obj: null
            },
            ncyBreadcrumb: {
                label: "Master Pengajuan Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_pengajuan/pengajuan.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("master.reseller", {
            url: "/reseller",
            templateUrl: "tpl/m_reseller/reseller.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Reseller"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_reseller/reseller.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.bonus", {
            url: "/bonus",
            templateUrl: "tpl/m_bonus/bonus.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Setting Bonus"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_bonus/bonus.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.tree", {
            url: "/tree",
            templateUrl: "tpl/m_pohon_jaringan/tree.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Pohon Jaringan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_pohon_jaringan/tree.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.forum", {
            url: "/forum",
            templateUrl: "tpl/m_forum/forum.html?t=" + currTime,
            cache: false,
            params: {
                obj: null
            },
            ncyBreadcrumb: {
                label: "Forum Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_forum/forum.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.forum_custom", {
            url: "/forum/:judul",
            templateUrl: "tpl/m_forum/forum.html?t=" + currTime,
            cache: false,
            controller: function ($stateParams) {
                $stateParams.judul
            },
            ncyBreadcrumb: {
                label: "Forum Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_forum/forum.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.faq", {
            url: "/faq",
            templateUrl: "tpl/m_faq/faq.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Frequently Asked Question"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_faq/faq.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
		.state("master.artikel", {
            url: "/artikel",
            templateUrl: "tpl/m_artikel/artikel.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Master Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_artikel/artikel.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.afiliasi_produk", {
            url: "/afiliasi-produk",
            templateUrl: "tpl/m_afiliasi/afiliasi_produk.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Link Afiliasi Produk"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_afiliasi/afiliasi_produk.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.afiliasi_member", {
            url: "/afiliasi-member",
            templateUrl: "tpl/m_afiliasi/afiliasi_member.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Link Afiliasi Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_afiliasi/afiliasi_member.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("master.setting", {
            url: "/setting",
            templateUrl: "tpl/m_setting/setting.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Setting Web"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_setting/setting.js?t=" + currTime]
                        });
                        });
                    }
                ]
            }
        })
        .state("master.komplain", {
            url: "/komplain",
            templateUrl: "tpl/m_komplain/komplain.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Komplain Pelanggan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_komplain/komplain.js?t=" + currTime]
                        });
                        });
                    }
                ]
            }
        })
        .state("master.sentral_cargo", {
            url: "/sentral_cargo",
            templateUrl: "tpl/m_sentral_cargo/sentral_cargo.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "sentral_cargo"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                          files: ["tpl/m_sentral_cargo/sentral_cargo.js?t=" + currTime]
                        });
                        });
                    }
                ]
            }
        })
		// State Transaksi
        .state("transaksi", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Transaksi"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        })
        .state("transaksi.keranjang", {
            url: "/keranjang",
            templateUrl: "tpl/t_keranjang/keranjang.html?t=" + currTime,
            cache: false,
            params: {
              obj: null
            },
            ncyBreadcrumb: {
                label: "Keranjang Belanja"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_keranjang/keranjang.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("transaksi.histori_pesanan", {
            url: "/histori-pesanan",
            templateUrl: "tpl/t_histori_pesanan/histori_pesanan.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Riwayat Pesanan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_histori_pesanan/histori_pesanan.js?t=" + currTime]
                        });
                    }
                ]
            }
        })
        .state("transaksi.konfirmasi_pembayaran", {
            url: "/konfirmasi-pembayaran",
            templateUrl: "tpl/t_konfirmasi_pembayaran/konfirmasi_pembayaran.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Konfirmasi Pembayaran"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
			                  return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ["tpl/t_konfirmasi_pembayaran/konfirmasi_pembayaran.js?t=" + currTime]
                          });
                        });
                    }
                ]
            }
        })
        .state("transaksi.approval_pembayaran", {
            url: "/approval-pembayaran",
            templateUrl: "tpl/t_approval_pembayaran/approval_pembayaran.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Approval Pembayaran"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_approval_pembayaran/approval_pembayaran.js?t=" + currTime]
                        });
                        });
                    }
                ]
            }
        })
        .state("transaksi.big_order", {
            url: "/pemesanan-big-order",
            templateUrl: "tpl/t_keranjang_big/keranjang_big.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Big Disc Order"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_keranjang_big/keranjang_big.js?t=" + currTime]
                        });
                        });
                    }
                ]
            }
        })
		.state("transaksi.mutasi", {
            url: "/mutasi",
            templateUrl: "tpl/t_mutasi/mutasi.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Mutasi Level Member"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/t_mutasi/mutasi.js?t=" + currTime]
                        });
                        });
                    }
                ]
            }
        })
    // Note : Transaksi Arisan diubah jadi Tabungan
		.state("transaksi.arisan", {
            url: "/arisan",
            templateUrl: "tpl/t_arisan/arisan.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Tabungan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
						return $ocLazyLoad.load(['naif.base64']).then(() => {
							return $ocLazyLoad.load({
								files: ["tpl/t_arisan/arisan.js?t=" + currTime]
							});
                        });
                    }
                ]
            }
        })

        .state("transaksi.arisan_pj", {
            url           : "/arisan-pj",
            templateUrl   : "tpl/t_arisan_pj/arisan_pj.html?t=" + currTime,
            cache         : false,
            ncyBreadcrumb : {
                label: "PJ Arisan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad", function($ocLazyLoad) {
        					return $ocLazyLoad.load({
        						files: ["tpl/t_arisan_pj/arisan_pj.js?t=" + currTime]
        					});
                }]
            }
        })

        .state("transaksi.arisan_list", {
            url           : "/arisan-list",
            templateUrl   : "tpl/t_arisan_list/arisan_list.html?t=" + currTime,
            cache         : false,
            ncyBreadcrumb : {
                label: "List Arisan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad", function($ocLazyLoad) {
        					return $ocLazyLoad.load({
        						files: ["tpl/t_arisan_list/arisan_list.js?t=" + currTime]
        					});
                }]
            }
        })

        // State Laporan
        .state("laporan", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        })
        .state("laporan.pesanan", {
            url: "/laporan-pesanan",
            templateUrl: "tpl/l_pesanan/l_pesanan.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Pesanan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_pesanan/l_pesanan.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.detail_komisi", {
            url: "/detail-komisi",
            templateUrl: "tpl/l_detail_komisi/detail_komisi.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Detail Komisi"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_detail_komisi/detail_komisi.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
		.state("laporan.poin_reseller", {
            url: "/laporan-poin",
            templateUrl: "tpl/l_poin_reseller/l_poin.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Poin Reseller"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_poin_reseller/l_poin.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.bonus_pc", {
            url: "/laporan-pc",
            templateUrl: "tpl/l_bonus_pc/bonus_pc.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Personal Commission"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_bonus_pc/bonus_pc.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.bonus_rc", {
            url: "/laporan-rc",
            templateUrl: "tpl/l_bonus_rc/bonus_rc.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Recruitment Commission"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_bonus_rc/bonus_rc.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.bonus_mr", {
            url: "/laporan-mr",
            templateUrl: "tpl/l_bonus_mr/bonus_mr.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Monthly Reward"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_bonus_mr/bonus_mr.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.bonus_mcm", {
            url: "/laporan-mcm",
            templateUrl: "tpl/l_bonus_mcm/bonus_mcm.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Member Commission Manager"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_bonus_mcm/bonus_mcm.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.bonus_mcs", {
            url: "/laporan-mcs",
            templateUrl: "tpl/l_bonus_mcs/bonus_mcs.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Member Commission Supervisor"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_bonus_mcs/bonus_mcs.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.tutup_bulan", {
            url: "/laporan-tutup-bulan",
            templateUrl: "tpl/l_tutup_bulan/tutup_bulan.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Tutup Bulan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_tutup_bulan/tutup_bulan.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        // Note : Arisan dirubah jadi tabungan
        .state("laporan.arisan", {
            url: "/laporan-arisan",
            templateUrl: "tpl/l_arisan/l_arisan.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Tabungan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_arisan/l_arisan.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
        .state("laporan.mutasi_moota", {
            url: "/laporan-mutasi-moota",
            templateUrl: "tpl/l_mutasi_moota/mutasi_moota.html?t=" + currTime,
            cache: false,
            ncyBreadcrumb: {
                label: "Laporan Mutasi Moota"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                      return $ocLazyLoad.load(['daterangepicker']).then(() => {
                        return $ocLazyLoad.load({
                            files: ["tpl/l_mutasi_moota/mutasi_moota.js?t=" + currTime]
                        });
                      });
                    }
                ]
            }
        })
		// State PAGE
        .state("page", {
            abstract: true,
            templateUrl: "tpl/common/layouts/blank.html",
            cache: false,
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ]
            }
        }).state("page.login", {
            url: "/login",
            templateUrl: "tpl/common/pages/login.html?t=" + currTime,
            cache: false,
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/login.js?t=" + currTime]
                        });
                    }
                ]
            }
        }).state("page.forgot", {
            url: "/forgot-password",
            templateUrl: "tpl/site/forgot.html?t=" + currTime,
            cache: false,
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/forgot.js?t=" + currTime]
                        });
                    }
                ]
            }
        }).state("page.404", {
            url: "/404",
            templateUrl: "tpl/common/pages/404.html"
        }).state("page.500", {
            url: "/500",
            templateUrl: "tpl/common/pages/500.html"
        });

        function authenticate($q, UserService, $state, $transitions, $location, $rootScope) {
            var deferred = $q.defer();
            if (UserService.isAuth()) {
                deferred.resolve();
                var fromState = $state;
                var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator"];
                $transitions.onStart({}, function($transition$) {
                    var toState = $transition$.$to();
                    if ($rootScope.user.akses[toState.name.replace(".", "_")] || globalmenu.indexOf(toState.name)) {} else {
                        $state.target("page.500")
                    }
                });
            } else {
                $location.path("/login");
            }
            return deferred.promise;
        }
    }
]);
