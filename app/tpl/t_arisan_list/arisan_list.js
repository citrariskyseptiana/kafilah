app.controller("arisanListCtrl", function($scope, Data, toaster, $state) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "t_arisan_pj";
  $scope.formtittle = "";
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;

  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/indexList", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };

  $scope.update = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form       = form;
  };
  $scope.view = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = true;
    $scope.formtittle = "Lihat Data : " + form.kode;
    $scope.form       = form;
    $scope.hitungTotal(form);
  };

  $scope.cancel = function() {
    $scope.is_edit    = false;
    $scope.is_view    = false;
    $scope.is_create  = false;
    $scope.callServer(tableStateRef);
  };

  $scope.hitungTotal = function(peserta) {
    console.log('pepes', peserta);
    var detail  = peserta.detail;
    var total   = 0;
    console.log('stand up');
    angular.forEach(detail, function(val, key) {
      val.sub_total = (parseInt(val.harga) - parseInt(val.diskon)) * parseInt(val.jumlah);
      total += val.sub_total;
    });

    $scope.form.total = total;
    console.log('hitung total');
  }

  $scope.setPemenang = function(peserta){
    if( !confirm(`Apakah Anda ingin menetapkan "` + peserta.nama + `" sebagai Pemenang? Dan melanjutkan ke proses Pemesanan Produk?`) )
      return;

    //Simpan daftar pesanan di SESSION
    Data.post(control_link + "/setPemenang", peserta).then(function(result) {
      if(result.status_code == 200){
        peserta.status_arisan = 'Selesai';
        $state.go('transaksi.keranjang', {obj: peserta });
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });
  }

});
