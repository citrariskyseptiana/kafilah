app.controller("tutupbulanCtrl", function($scope, Data, toaster) {
  var control_link = "l_tutup_bulan";
  $scope.laporan = [];

  var tableStateRef = {};
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.loading = false;
  $scope.stPeriode = new Date();
  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
      tableStateRef = tableState;
      $scope.isLoading = true;
      var offset = tableState.pagination.start || 0;
      var limit = tableState.pagination.number || 10;
      var param = {
          offset: offset,
          limit: limit
      };
      if (tableState.sort.predicate) {
          param["sort"] = tableState.sort.predicate;
          param["order"] = tableState.sort.reverse;
      }
      if (tableState.search.predicateObject) {
          param["filter"] = tableState.search.predicateObject;
      }

      if($scope.stPeriode != undefined){
        if(param['filter'] == undefined){
          param['filter'] = {};
        }
        param["filter"]['periode'] = $scope.stPeriode;
      }

      Data.get(control_link+"/index", param).then(function(response) {
          $scope.displayed = response.data.list;
          tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
      });
      $scope.isLoading = false;
  };

  $scope.setPeriode = function(date){
    $scope.stPeriode = new Date(date);
    $scope.callServer(tableStateRef);
  }

  $scope.create = function(form) {
      $scope.is_edit = true;
      $scope.is_view = false;
      $scope.formtittle = "Form Tambah Data";
      $scope.form = {};
      $scope.form.akses = {};
  };
  $scope.goToIndex = function() {
      $scope.is_edit = false;
      $scope.callServer(tableStateRef);
  };
  $scope.tampilkan = function(row) {
      $scope.is_edit = true;
      $scope.is_view = true;

      $scope.filter.periode = new Date(row.bulan);
      $scope.view($scope.filter);
  };

  $scope.reset_filter = function() {
    $scope.filter = {};
    $scope.is_view = false;
    $scope.loading = false;
    $scope.filter.periode = "";
  }

  $scope.reset_filter();

  $scope.viewData = function(level){
    $scope.laporan_view     = $scope.laporan[level];
    $scope.totalKomisi_view = $scope.totalKomisi[level];
    $scope.level            = level;
  };

  $scope.view = function(filter) {
    filter.tipe_laporan = 'laporan_manual';
    console.log("filter", filter);
    // return;
    Data.get(control_link + "/laporan", filter).then(function(result) {
      if (result.status_code == 200) {
        $scope.is_view        = true;
        $scope.laporan        = result.data.laporan;
        $scope.totalKomisi    = result.data.totalKomisi;
        $scope.totalPenjualan = result.data.totalPenjualan;
        $scope.periode        = new Date($scope.filter.periode);

        // Inisialisasi data table
        $scope.viewData('QPA');
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };

  $scope.print = function() {
    var printContents = document.getElementById('print-area').innerHTML;
    var popupWin = window.open('', '_blank', 'width=1000,height=700');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
        <link rel="stylesheet" type="text/css" href="./tpl/common/print-style.css" />
        </head>
        <body onload="window.print()">` + printContents
        + `</body></html>`);
    popupWin.document.close();
  }

  $scope.export = function(){
    console.log("like");
    var blob = new Blob([document.getElementById('excel-area').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Laporan-Tutup-Bulan.xls");
  }
});
