app.controller("boCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_big_order";
  $scope.formtittle = "";
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;
  $scope.listBulk = [{
      jenis_diskon: "gratis barang",
      min_beli: 0,
      max_beli: 0,
  }];

  Data.get(control_link + "/getBulk").then(function(result) {
      if (result.status_code == 200 && result.data.length > 0) {
          $scope.listBulk = result.data;
          $scope.hitungDiskonBulk();
      } else {
          $scope.listBulk = [{
              jenis_diskon: "gratis barang",
              min_beli: 0,
              max_beli: 0,
          }];
      }
  });

  $scope.getProduk = function(cari) {
      if (cari.toString().length > 2) {
          var param = {
              nama: cari
          };
          Data.get("m_produk/getProduk", param).then(function(result) {
              $scope.listProdukGratis = result.data;
          });
      }
  }

  /** Operasi Detail Bulk */
  $scope.addBulk = function() {
      var newDet = {
          jenis_diskon: "gratis barang",
          min_beli: 0,
          max_beli: 0,
      };
      $scope.listBulk.push(newDet);
  };

  $scope.remBulk = function(row) {
      if (confirm("Apa Anda yakin akan MENGHAPUS item ini ?")) {
          $scope.listBulk.splice($scope.listBulk.indexOf(row), 1);
      }
  };

  $scope.save = function() {
    $scope.loading = true;
    Data.post(control_link+"/save", $scope.listBulk).then(function(result) {
      if(result.status_code == 200){
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
      $scope.loading = false;
    });
  }

});
