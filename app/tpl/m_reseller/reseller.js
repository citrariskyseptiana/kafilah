app.controller("resellerCtrl", function($scope, Data, toaster, $stateParams) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_reseller";
  $scope.formtittle = "";
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;
  $scope.is_pengajuan = false;
  /**
   * End inialisasi
   */

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(result) {
        $scope.form.kode = result.data;
    });
  }

  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };
  $scope.create = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.is_create = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form = {};
    $scope.status = 'menunggu persetujuan';
    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.formtittle = "Edit Data : " + form.nik;
    $scope.form = form;
    $scope.form.tanggal_lahir = new Date(form.tanggal_lahir);
  };

  $scope.view = function(form) {
    $scope.is_edit = true;
    $scope.is_view = true;
    $scope.formtittle = "Lihat Data : " + form.nik;
    $scope.form = form;
    $scope.form.tanggal_lahir = new Date(form.tanggal_lahir);
  };

  $scope.save = function(form) {
    $scope.loading = true;
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop("error","Terjadi Kesalahan",setErrorMessage(result.errors));
      }
      $scope.loading = false;
    });
  };
  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.is_pengajuan = false;
    $scope.callServer(tableStateRef);
  };
  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  // Get Provinsi, kota, kec, desa
  Data.get("site/getProvinsi").then(function(result) {
    $scope.listProvinsi = result.data;
  });

  $scope.cariKota = function(provinsi){
    var param = {
      provinsi: provinsi.id
    };
    Data.get("site/getKota", param).then(function(result) {
      $scope.listKota = result.data;
    });
  };

  $scope.cariKecamatan = function(kota){
    var param = {
      kota: kota.id
    };
    Data.get("site/getKecamatan", param).then(function(result) {
      $scope.listKecamatan = result.data;
    });
  };

  $scope.cariDesa = function(kecamatan){
    var param = {
      kecamatan: kecamatan.id
    };
    Data.get("site/getDesa", param).then(function(result) {
      $scope.listDesa = result.data;
    });
  };

});
