app.controller("faqCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link  = "m_faq";
  $scope.formtittle = "";
  $scope.displayed  = [];
  $scope.form       = {};
  $scope.is_edit    = false;
  $scope.is_view    = false;
  $scope.is_create  = false;
  $scope.loading    = false;

  $scope.getIndex = function(key=""){
    var params = {
      filter : key
    }
    Data.get(control_link + "/index", params).then(function(response) {
      $scope.displayed = [];
      angular.forEach(response.data, function(val, key) {
        $scope.displayed[key] = val;
        $scope.displayed[key].isCollapsed = false;
      });

      console.log("$scope.displayed", $scope.displayed);
    });
  };

  $scope.getIndex();

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  $scope.create = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.is_create = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form = {};
    $scope.listDetail=[]
    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.formtittle = "Edit Data : " + form.pertanyaan;
    $scope.form = form;

  };

  $scope.view = function(form) {
    $scope.is_edit = true;
    $scope.is_view = true;
    $scope.formtittle = "Lihat Data : " + form.pertanyaan;
    $scope.form = form;
  };

  $scope.save = function(form) {
    $scope.loading = true;
    var form = {
      data: form
    }
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
      $scope.loading = false;
    });
  };
  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.getIndex();
  };
  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
});
