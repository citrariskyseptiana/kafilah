app.controller("artikelCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_artikel";
  $scope.formtittle = "";
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;

  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };

  $scope.create = function(form) {
    $scope.is_edit      = true;
    $scope.is_view      = false;
    $scope.is_create    = true;
    $scope.formtittle   = "Buat Artikel";
    $scope.form         = {};
    $scope.form.tanggal_publish = new Date();
  };

  $scope.update = function(form) {
    $scope.is_edit      = true;
    $scope.is_view      = false;
    $scope.formtittle   = "Edit Artikel : " + form.judul;
    $scope.form         = form;
    $scope.form.tanggal_publish = new Date(form.tanggal_publish);
  };

  $scope.view = function(form) {
    $scope.is_edit      = true;
    $scope.is_view      = true;
    $scope.formtittle   = "Lihat Artikel : " + form.judul;
    $scope.form         = form;
    $scope.form.tanggal_publish = new Date(form.tanggal_publish);
  };

  $scope.save = function(form) {
    $scope.loading = true;

    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
      $scope.loading = false;
    });
  };

  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.callServer(tableStateRef);
  };

  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.slugify = function(input){
    if (!input)
        return;

    // make lower case and trim
    var slug = input.toLowerCase().trim();
    // replace invalid chars with spaces
    slug = slug.replace(/[^a-z0-9\s-]/g, ' ');
    // replace multiple spaces or hyphens with a single hyphen
    slug = slug.replace(/[\s-]+/g, '-');
    $scope.form.url = slug;
  };

  $scope.opened = {};
  $scope.toggle = function($event, elemId) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened[elemId] = !$scope.opened[elemId];
  };
});