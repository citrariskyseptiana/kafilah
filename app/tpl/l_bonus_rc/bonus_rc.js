app.controller("lrcCtrl", function($scope, Data, toaster) {
  var control_link = "l_bonus_rc";

  $scope.reset_filter = function() {
    $scope.filter = {};
    $scope.is_view = false;
    $scope.loading = false;
    $scope.filter.periode = "";
  }

  $scope.reset_filter();

  $scope.getMember = function(cari, tipe) {
    if (cari.toString().length > 2) {
      var param = {
        nama: cari,
        tipe_member: tipe
      };
      Data.get("m_member/getMember", param).then(function(result) {
        if (tipe == 'Member') $scope.listMember = result.data;
        if (tipe == 'Reseller') $scope.listReseller = result.data;
      });
    }
  };

  $scope.view = function(filter) {
    Data.post(control_link + "/laporan", filter).then(function(result) {
      if (result.status_code == 200) {
        $scope.is_view = true;
        $scope.laporan = result.data.laporan;
        $scope.totalKomisi = result.data.totalKomisi;
        $scope.periode = new Date($scope.filter.periode);
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };

  $scope.print = function() {
    var printContents = document.getElementById('print-area').innerHTML;
    var popupWin = window.open('', '_blank', 'width=1000,height=700');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
        <link rel="stylesheet" type="text/css" href="./tpl/common/print-style.css" />
        </head>
        <body onload="window.print()">` + printContents
        + `</body></html>`);
    popupWin.document.close();
  }

  $scope.export = function(){
    var blob = new Blob([document.getElementById('print-area').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Laporan-Recruitment-Commission.xls");
  }
});
