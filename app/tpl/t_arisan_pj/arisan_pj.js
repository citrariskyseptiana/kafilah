app.controller("arisan_pjCtrl", function($scope, Data, toaster, $state) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "t_arisan_pj";
  $scope.formtittle = "";
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;

  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };

  $scope.getDetail = function(id) {
    Data.get(control_link + "/view?t_arisan_id=" + id).then(function(response) {
      $scope.listPeserta = response.data;
      angular.forEach($scope.listPeserta, function(val, key) {
        $scope.hitungTotal(val);
      });
    });
  };

  /* Operasi detail peserta */
  $scope.listPeserta = [];
  // $scope.listPeserta = [
  // { nama    : "",
  //   no_hp   : "",
  //   jenis_pesanan   :"",
  //   detail  : [{}],
  // }];
  $scope.addPeserta = function() {
    var newDet = {
      nama    : "",
      no_hp   : "",
      jenis_pesanan   :"",
      detail  : [{}],
    };
    $scope.listPeserta.unshift(newDet);
  };
  $scope.removePeserta = function(val, paramindex) {
    if ( confirm("Apakah Anda ingin menghapus Customer ini dari daftar Peserta Arisan?") ) {
      val.splice(paramindex, 1);
    }
  };

  /* End of Operasi detail peserta */

  $scope.listDetail = [{}];
  $scope.addDetail = function(val) {

    var comArr = eval(val);
    var newDet = {
      harga:0,
      jumlah:0,
    };
    val.detail.unshift(newDet);
  };
  $scope.removeDetail = function(val, paramindex) {
    if ( confirm("Apakah Anda ingin menghapus produk ini?") ) {
      val.splice(paramindex, 1);
    }
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  $scope.getProduk = function(cari, jenis_pesanan) {
    if(cari.toString().length > 2){

      if(jenis_pesanan =="" || jenis_pesanan==undefined ){
        toaster.pop("error", "Peringatan!", "Anda harus mengisi Jenis Barang!");
        return;
      }

      var param = {
        nama          : cari,
        jenis_pesanan : jenis_pesanan,
        promo         : 'aktif'
      };

      Data.get("m_produk/getProduk", param).then(function(result) {
        $scope.listProduk = result.data;
      });
    }
  }

  $scope.setProduk = function(peserta, data, index) {
    var is_ok         = true;
    var indexPeserta  = $scope.listPeserta.indexOf(peserta);

    angular.forEach(peserta.detail, function(val, key) {
      if(key != index && data.id == val.m_produk_id.id){
        toaster.pop("error", "Peringatan!", "Produk sudah ada dalam list!");
        $scope.listPeserta[indexPeserta].detail.splice( index, 1);
        is_ok = false;
        $scope.listProduk = [];
        return;
      }
    });

    if(is_ok){
      $scope.listPeserta[indexPeserta].detail[index].harga  = data.harga_jual;
      $scope.listPeserta[indexPeserta].detail[index].jumlah = 1;
      $scope.hitungTotal($scope.listPeserta[indexPeserta]);
    }

    $scope.listProduk = [];
  }

  $scope.hitungTotal = function(peserta) {
    console.log('pepes', peserta);
    var detail  = peserta.detail;
    var total   = 0;
    console.log('stand up');
    var index   = $scope.listPeserta.indexOf(peserta);
    console.log('kollll');
    angular.forEach(detail, function(val, key) {
      val.sub_total = (parseInt(val.harga) - parseInt(val.diskon)) * parseInt(val.jumlah);
      total += val.sub_total;
    });

    $scope.listPeserta[index].total = total;
    console.log('hitung total');
  }

  $scope.create = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = false;
    $scope.is_create  = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form       = {};
    $scope.listPeserta = [];
    $scope.form.progres = 'Berjalan';
    $scope.form.jenis_pesanan = 'non-po';
    $scope.form.tanggal_mulai = new Date();
    $scope.form.pembuat_nama = $scope.user.nama;
    $scope.form.pembuat_no_hp = $scope.user.no_hp;

    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form       = form;
    $scope.form.tanggal_mulai = new Date($scope.form.tanggal_mulai);
    if($scope.form.progres == 'Selesai'){
      $scope.form.tanggal_selesai = new Date($scope.form.tanggal_selesai);
    }
    $scope.getDetail(form.id);
  };
  $scope.view = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = true;
    $scope.formtittle = "Lihat Data : " + form.kode;
    $scope.form       = form;
    $scope.form.tanggal_mulai = new Date($scope.form.tanggal_mulai);
    if($scope.form.progres == 'Selesai'){
      $scope.form.tanggal_selesai = new Date($scope.form.tanggal_selesai);
    }
    $scope.getDetail(form.id);
  };

  $scope.save = function(form) {
    $scope.loading    = true;
    var form = {
      data    : form,
      detail  : $scope.listPeserta
    }
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
      $scope.loading = false;
    });
  };

  $scope.cancel = function() {
    $scope.is_edit    = false;
    $scope.is_view    = false;
    $scope.is_create  = false;
    $scope.callServer(tableStateRef);
  };

  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.setPemenang = function(peserta){
    if( !confirm(`Apakah Anda ingin menetapkan "` + peserta.nama + `" sebagai Pemenang? Dan melanjutkan ke proses Pemesanan Produk?`) )
      return;

    // Simpan daftar pesanan di SESSION
    Data.post(control_link + "/prosesArisan", peserta).then(function(result) {
      if(result.status_code == 200){
        $scope.getDetail($scope.form.id);
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });

    // Simpan daftar pesanan di SESSION
    // Data.post(control_link + "/setPemenang", peserta).then(function(result) {
    //   if(result.status_code == 200){
    //     peserta.status_arisan = 'Selesai';
    //     $state.go('transaksi.keranjang', {obj: peserta });
    //   } else {
    //     toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
    //   }
    // });
  }

  /* Modal Peserta */
  $scope.openPeserta = function(val){
    var form = {
      data    : $scope.form
    };
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        $scope.form.id            = result.data.id;
        $scope.formPeserta        = {};
        $scope.formPeserta.status_arisan = 'Pending';
        $scope.indexPeserta       = undefined;
        console.log($scope.formPeserta);
        $(val).modal('toggle');
        $(val).modal('hide');
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });
  };

  $scope.editPeserta = function(val, indexPeserta){
    $scope.indexPeserta = indexPeserta;
    $scope.formPeserta  = val;
    $('#modalPeserta').modal('toggle');
  };

  $scope.simpanPeserta = function(val, indexPeserta){
    var params = {
      form    : $scope.form,
      peserta : val,
    };
    console.log("idx ", indexPeserta);
    console.log("simpan", val);
    Data.post(control_link + "/savePeserta", params).then(function(result) {
      if(result.status_code == 200){
        if(indexPeserta == undefined){
          console.log("dunangis", $scope.listPeserta);
          console.log('baruuu ccasdfasdkf');
          var newDet    = result.data.peserta;
          newDet.detail = [];
          console.log('+ det', newDet);
          $scope.listPeserta.push(newDet);
          console.log('unsift');
        } else {
          console.log('ediiittt');
          $scope.listPeserta[indexPeserta] = result.data.peserta;
        }

        $scope.hitungTotal(result.data.peserta);
        $('#modalPeserta').modal('toggle');
        $('#modalPeserta').modal('hide');
        console.log('tertutup');
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });
  };

  $scope.hapusPeserta = function(formPeserta, cariIndex){
    var konfirmasi = `Apakah Anda ingin menghapus peserta ini?`;
    if( !confirm(konfirmasi) ){
      return;
    }

    var params = {
      form    : $scope.form,
      peserta : formPeserta
    };
    console.log('idx', cariIndex);
    Data.post(control_link + "/hapusPeserta", params).then(function(result) {
      if(result.status_code == 200){
        $scope.listPeserta.splice(cariIndex, 1);
        $scope.hitungTotal(result.data.peserta);
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });
  }

  /* Modal Peserta - END */

  /* Modal Produk */
  $scope.openDetail = function(peserta, index){
    $scope.formPeserta        = peserta;
    $scope.indexProduk        = index;

    if( $scope.formPeserta.detail != undefined && $scope.formPeserta.detail != [] ){
      console.log($scope.formPeserta.detail);
      if($scope.formPeserta.detail[index] == undefined){
        $scope.formProduk = {};
        console.log('kosong yo');
        console.log('index ? ', index);
      } else {

        console.log('ada yo', $scope.formPeserta.detail[index]);
        $scope.formProduk = $scope.formPeserta.detail[index];
      }
    } else {
      console.log('kosong yo last');
      $scope.formProduk = {};
    }

    $('#modalProduk').modal('toggle');
  };

  $scope.setSubProduk = function() {
    $scope.formProduk.harga   = $scope.formProduk.m_produk_id.harga_jual == undefined ? 0 : $scope.formProduk.m_produk_id.harga_jual;
    $scope.formProduk.diskon  = $scope.formProduk.m_produk_id.diskon == undefined ? 0 : $scope.formProduk.m_produk_id.diskon;
    $scope.formProduk.jumlah  = 1;
  }

  $scope.hitungSub = function() {
    var sub_total = parseInt($scope.formProduk.jumlah) * (parseInt($scope.formProduk.harga) - parseInt($scope.formProduk.diskon));
    $scope.formProduk.sub_total = sub_total;
  }

  $scope.simpanProduk = function(formPeserta, formProduk, index){
    var params = {
      form    : $scope.form,
      peserta : formPeserta,
      produk  : formProduk,
    };

    Data.post(control_link + "/saveProduk", params).then(function(result) {
      if(result.status_code == 200){
        // set id detailnya
        formProduk.id = result.data.formProduk.id;

        // Temukan urutan peserta
        var cariIndex = undefined;
        angular.forEach($scope.listPeserta, function(valPeserta, indexPeserta) {
          if(valPeserta.id == formPeserta.id){
            cariIndex = indexPeserta;
          }
        });
        console.log("aftersave cr idx");
        if(index == undefined){
          console.log("before save new 1");
          if(($scope.listPeserta[cariIndex].detail) == undefined){
            $scope.listPeserta[cariIndex].detail = [];
          }
          // Jika tambah barang baru
          $scope.listPeserta[cariIndex].detail.unshift(formProduk);
          console.log("aftersave bru");
        } else {
          // Jika barang udah ada
          console.log("aftersave edit");
          $scope.listPeserta[cariIndex].detail[index] = formProduk;
        }

        console.log("aftersave togle");
        $('#modalProduk').modal('toggle');
        $scope.hitungTotal(formPeserta);
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });
  };

  $scope.hapusProduk = function(formPeserta, formProduk, indexProduk){
    var konfirmasi = `Apakah Anda ingin menghapus Produk `+
    formProduk.m_produk_id.nama
    +` dari daftar?`;
    if( !confirm(konfirmasi) ){
      return;
    }

    var params = {
      form    : $scope.form,
      peserta : formPeserta,
      produk  : formProduk,
    };

    Data.post(control_link + "/hapusProduk", params).then(function(result) {
      if(result.status_code == 200){
        // Temukan urutan peserta
        var cariIndex = undefined;
        angular.forEach($scope.listPeserta, function(valPeserta, indexPeserta) {
          if(valPeserta.id == formPeserta.id){
            cariIndex = indexPeserta;
          }
        });

        $scope.listPeserta[cariIndex].detail.splice(indexProduk, 1);
        $scope.hitungTotal($scope.listPeserta[cariIndex]);
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
    });
  }
  /* Modal Produk - END */

  // New arisan
  $scope.getMember = function(cari, tipe) {
    if (cari.toString().length > 2) {
      var param = {
        nama: cari,
        tipe_member: tipe
      };
      Data.get("m_member/getMember", param).then(function(result) {
        if (tipe == 'Member') $scope.listMember = result.data;
        if (tipe == 'Reseller') $scope.listReseller = result.data;
      });
    }
  };
  // New arisan - END

});
