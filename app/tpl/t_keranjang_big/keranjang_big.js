app.controller("keranjangBigCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link    = "t_keranjang";
  $scope.formtittle   = "";
  $scope.displayed    = [];
  $scope.listPesanan  = [];
  $scope.form         = {};

  $scope.is_edit      = false;
  $scope.is_view      = false;
  $scope.is_create    = false;
  $scope.loading      = false;
  $scope.is_checkout  = false;
  /**
   * End inialisasi
   */

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(result) {
      $scope.form.kode = result.data;
    });
  }
  Data.get(control_link + "/getKota").then(function(result) {
    $scope.listKotaAsal = result.data;
  });

  Data.get(control_link + "/getKotaSC").then(function(result) {
    $scope.listKotaSC = result.data;
  });

  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 20;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.jumlah_po = response.data.jumlah_po;
      $scope.jumlah_non_po = response.data.jumlah_non_po;
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };
  $scope.create = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.is_create = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form = {};
    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form = form;
  };
  $scope.view = function(form) {
    $scope.is_edit = true;
    $scope.is_view = true;
    $scope.formtittle = "Lihat Data : " + form.kode;
    $scope.form = form;
  };
  $scope.save = function(form, detail) {
    if(form.reseller_id == undefined){
      if ( !confirm("Apakah Anda yakin pesanan ini tanpa perantara reseller?")) {
        return;
      }
    }

    $scope.loading = true;
    var param = {
      form: form,
      detail: detail
    };
    Data.post(control_link + "/save", param).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cetakNota(result.data.id);
        $scope.cancel();
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
      $scope.loading = false;
    });
  };

  $scope.cetakNota = function(id){
      window.open(
          "api/" + control_link + "/cetakPesanan?id=" + id,
          "_blank",
          "width=1000,height=700"
      );
    }

  $scope.cancel = function() {
    $scope.is_edit      = false;
    $scope.is_view      = false;
    $scope.is_create    = false;
    $scope.is_checkout  = false;
    $scope.listPesanan  = [];
    $scope.form         = {};

    $scope.callServer(tableStateRef);
  };
  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.getReseller = function(cari) {
    if (cari.toString().length > 2) {
      var param = {
        nama: cari
      };
      Data.get(control_link + "/getReseller", param).then(function(result) {
        $scope.listReseller = result.data;
      });
    }
  };

  $scope.tambahPesanan = function(form) {
    form.jumlah_pesan += 1;
    if (form.jumlah_pesan > form.stok && form.jenis == "non-po") {
      form.jumlah_pesan = form.stok;
    }
  };
  $scope.kurangiPesanan = function(form) {
    form.jumlah_pesan -= 1;
    if (form.jumlah_pesan < 0) {
      form.jumlah_pesan = 0;
    }
  };
  $scope.isiKeranjang = function(form) {
    if ( form.jumlah_pesan == 0 ){
      form.jumlah_pesan = 1;
    }
    // if (confirm("Apa Anda ingin menambahkan produk ini ke Keranjang Belanja?") && form.jumlah_pesan > 0) {
    Data.post(control_link + "/isiKeranjang", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Produk telah ditambahkan ke keranjang.");
        $scope.callServer(tableStateRef);
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };
  $scope.hapusKeranjang = function(form) {
    if (confirm("Apa Anda ingin membatalkan produk ini dari Keranjang Belanja?")) {
      Data.post(control_link + "/hapusKeranjang", form).then(function(result) {
        if (result.status_code == 200) {
          toaster.pop("success", "Berhasil", "Produk telah dikeluarkan ke keranjang.");
          $scope.callServer(tableStateRef);
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    }
  };
  $scope.checkout = function(tipe) {
    var bigOrder = {};

    if (confirm("Apa Anda ingin melanjutkan ke proses Checkout?")) {
      /* Pemeriksaan Syarat bonus big order  */
      angular.forEach($scope.listBigOrder, function(val, key) {
        if( val.id == $scope.form.m_big_order ){
          bigOrder = val;
        }
      });

      console.log("$scope.form.m_big_order.id", $scope.form.m_big_order);
      console.log("bigOrder", bigOrder);
      /* End Pemeriksaan Syarat bonus big order  */

      Data.get(control_link + "/checkout", {
        tipe: tipe
      }).then(function(result) {
        if (result.status_code == 200) {
          $scope.listPesanan      = result.data.listPesanan;
          $scope.form.kode_unik   = result.data.kode_unik;
          $scope.hitungTotal();

          console.log($scope.form);

          /* Pemeriksaan Syarat bonus big order  */
          if( bigOrder.jenis_bulk == 'Rupiah' ){
            // Jika total harga pembelian masuk rentang syarat dapat bonus
            if( (bigOrder.min_beli <= $scope.form.total && bigOrder.max_beli >= $scope.form.total) || $scope.form.total >= bigOrder.max_beli ){

              console.log('masuk syarat');
              // Bonus Barang --> hitung total baru di diskon :)
              if( bigOrder.jenis_diskon == 'diskon' ){
                angular.forEach($scope.listPesanan, function(val, key) {
                  $scope.listPesanan[key].diskon_big_order = bigOrder.diskon;
                });
              } else if( bigOrder.jenis_diskon == 'gratis barang' ) {
                var barang_baru = {
                  id              : bigOrder.m_produk_gratis.id,
                  kode            : bigOrder.m_produk_gratis.kode,
                  nama            : bigOrder.m_produk_gratis.nama,
                  m_kategori_id   : bigOrder.m_produk_gratis.m_kategori_id,
                  berat           : bigOrder.m_produk_gratis.berat,
                  satuan          : bigOrder.m_produk_gratis.satuan,
                  stok            : bigOrder.m_produk_gratis.stok,
                  harga_jual      : 0,
                  poin            : bigOrder.m_produk_gratis.poin,
                  jenis           : 'gratis',
                  jumlah_pesan    : bigOrder.jumlah,
                };

                $scope.listPesanan.push( barang_baru );
              }

              $scope.hitungTotal();

            /*
            * Jika total harga pembelian belum masuk rentang syarat
            * beri alert dan kembalikan ke halaman pilih produk
            */
            } else {
              swal("Perhatian", `Jumlah pesanan Anda, belum memenuhi syarat Big Order, silakan lanjutkan belanja!`,
              "error");

              $scope.is_checkout = false;
              return;
            }
          }

          if( bigOrder.jenis_bulk == 'Pcs' ){
            console.log("PCS");
            // Pemeriksaan syarat
            if( (bigOrder.min_beli <= $scope.form.total_barang && bigOrder.max_beli >= $scope.form.total_barang) ){
              console.log("Masuk sayarat pcs");
              // Bonus Barang --> hitung total baru di diskon :)
              if( bigOrder.jenis_diskon == 'diskon' ){
                console.log("diskon");
                angular.forEach($scope.listPesanan, function(val, key) {
                  $scope.listPesanan[key].diskon_big_order = bigOrder.diskon;
                });

                console.log("diskon nih bg order masuk gak ", $scope.listPesanan);


              } else if( bigOrder.jenis_diskon == 'gratis barang' ) {
                console.log("gts brg");
                console.log("bigOrder.m_produk_gratis.id", bigOrder.m_produk_gratis.id);
                var barang_baru = {
                  id              : bigOrder.m_produk_gratis.id,
                  kode            : bigOrder.m_produk_gratis.kode,
                  nama            : bigOrder.m_produk_gratis.nama,
                  m_kategori_id   : bigOrder.m_produk_gratis.m_kategori_id,
                  berat           : bigOrder.m_produk_gratis.berat,
                  satuan          : bigOrder.m_produk_gratis.satuan,
                  stok            : bigOrder.m_produk_gratis.stok,
                  harga_jual      : 0,
                  poin            : bigOrder.m_produk_gratis.poin,
                  jenis           : 'gratis',
                  jumlah_pesan    : bigOrder.jumlah,
                };

                $scope.listPesanan.push( barang_baru );
                console.log("msh grts brg ", $scope.listPesanan);
              }

              $scope.hitungTotal();

            } else {
              swal("Perhatian", `Jumlah pesanan Anda, belum memenuhi syarat Big Order, silakan lanjutkan belanja!`, "error");

              $scope.is_checkout = false;
              return;
            }
            // End Pemeriksaan syarat
          }
          /* End Pemeriksaan Syarat bonus big order  */

          $scope.getKode();
          $scope.is_checkout            = true;
          $scope.formTitle              = "Checkout";
          $scope.form.tipe_produk       = tipe;
          $scope.form.kota_asal_id      =  result.data.kota_asal;
          $scope.form.kecamatan_asal    = result.data.kec_asal;
          $scope.form.kota_asal_sc      =  result.data.kota_asal_sc;
          console.log("is_chekout", true);

          if(result.data.alert != undefined && result.data.alert.length > 0){
            swal("Perhatian", `Jumlah pesanan Anda, melebihi jumlah stok. Sistem telah menyesuaikan jumlah pesanan untuk produk berikut : \n`
            + result.data.alert,
            "error");
          }

        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    }
  };

  $scope.hitungTotal = function() {
    var total_berat = total_harga = total_diskon = total_barang = 0;
    angular.forEach($scope.listPesanan, function(val, key) {
      if (val.diskon_big_order > 0) {
        var disc = (parseInt(val.diskon_big_order) / 100) * (parseInt(val.harga_jual) * parseInt(val.jumlah_pesan));
        $scope.listPesanan[key].diskon_big_order_rp = disc;
        $scope.listPesanan[key].sub_total = (parseInt(val.harga_jual) * parseInt(val.jumlah_pesan)) - disc;
      } else {
        $scope.listPesanan[key].diskon_big_order_rp = 0;
        $scope.listPesanan[key].sub_total = parseInt(val.harga_jual) * parseInt(val.jumlah_pesan);
      }

      $scope.listPesanan[key].sub_berat = parseInt(val.berat) * parseInt(val.jumlah_pesan);
      total_berat   += $scope.listPesanan[key].sub_berat;
      total_harga   += $scope.listPesanan[key].sub_total;
      total_diskon  += $scope.listPesanan[key].diskon_big_order_rp;
      total_barang  += val.jumlah_pesan;
    });

    $scope.form.total_berat   = total_berat;
    $scope.form.total         = total_harga;
    $scope.form.total_diskon  = total_diskon;
    $scope.form.total_barang  = total_barang;
  };

  $scope.getReseller = function(cari) {
    if (cari.toString().length > 2) {
      var param = {
        nama: cari,
        tipe_member: "Reseller"
      };
      Data.get("m_member/getMember", param).then(function(result) {
        $scope.listReseller = result.data;
      });
    }
  };

  $scope.getMember = function(cari) {
    if (cari.toString().length > 2) {
      var param = {
        nama: cari,
        tipe_member: "Member"
      };
      Data.get("m_member/getMember", param).then(function(result) {
        $scope.listMember = result.data;
      });
    }
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(result) {
      $scope.form.kode = result.data;
    });
  }

  // Get Provinsi, kota, kec, desa
  Data.get("site/getProvinsi").then(function(result) {
    $scope.listProvinsi = result.data;
  });
  $scope.cariKota = function(provinsi) {
    var param = {
      provinsi: provinsi.id
    };
    Data.get("site/getKota", param).then(function(result) {
      $scope.listKota = result.data;
    });
  };
  $scope.cariKecamatan = function(kota) {
    var param = {
      kota: kota.id
    };
    Data.get("site/getKecamatan", param).then(function(result) {
      $scope.listKecamatan = result.data;
    });
  };
  $scope.cariDesa = function(kecamatan) {
    var param = {
      kecamatan: kecamatan.id
    };
    Data.get("site/getDesa", param).then(function(result) {
      $scope.listDesa = result.data;
    });
  };

  $scope.view_dakota      = false;
  $scope.view_raja_ongkir = false;
  $scope.view_ekspedisi_lain = false;

  $scope.getPaketKota = function(form, pilihan) {
    $scope.form.kecamatan_asal = undefined;
    $scope.getPaket(form, pilihan);
  }

  $scope.getPaketKecamatan = function(form, pilihan) {
    $scope.getPaket(form, pilihan);
  }

  $scope.getPaket = function(form, tipe) {
    // Reset Keterangan & Biaya Ongkir
    $scope.form.ongkir = undefined;
    $scope.form.catatan = '';

    console.log("get paket", form);
    if(form.w_kota == undefined || form.w_kecamatan == undefined){
      toaster.pop("error", "Peringatan", "Field Kabupaten/Kota/Kecamatan harus diisi!");
      $scope.form.ekspedisi="";
      return;
    }

    // $scope.form.kota_asal = $scope.form.kota_asal_id;
    if (tipe == 'ekspedisi') {
      $scope.form.kota_asal = $scope.form.kota_asal_id;
      $scope.cariKecamatan($scope.form.kota_asal);
    }

    if( form.ekspedisi == 'lainnya' ) {
      $scope.view_dakota          = false;
      $scope.view_scargo          = false;
      $scope.view_raja_ongkir     = false;
      $scope.view_ekspedisi_lain  = false;
      console.log("lainnya", $scope.view_dakota);
    } else if( form.ekspedisi == 'scargo' ) {
      $scope.form.kota_asal = $scope.form.kota_asal_sc;
      var param = {
        destination : form.w_kecamatan,
        weight      : form.total_berat,
        courier     : form.ekspedisi,
      };
      if (form.kota_asal != undefined) {
        param.kota_asal = form.kota_asal
      }


      Data.get(control_link + "/cekOngkirSC", param).then(function(result) {
        if(result.status_code == 200){
          $scope.listPaket            = result.data;
          $scope.view_scargo          = true;
          $scope.view_raja_ongkir     = false;
          $scope.view_dakota          = false;
          $scope.view_ekspedisi_lain  = false;

        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    } else if( form.ekspedisi == 'dakota' ) {
      var param = { form : form };
      Data.get(control_link + "/cekOngkirDakota", param).then(function(result) {
        if(result.status_code == 200){
          $scope.view_dakota          = true;
          $scope.view_scargo          = false;
          $scope.view_raja_ongkir     = false;
          $scope.view_ekspedisi_lain  = false;

          $scope.dakota_price     = result.data.origin_price;
          $scope.hitung_ongkir    = result.data.hitung_ongkir;

          $scope.form.ongkir = parseInt( $scope.hitung_ongkir );

          // Catatan + Keterangan Paket :D
          if($scope.form.catatan_temp == undefined && $scope.form.catatan != undefined)
            $scope.form.catatan_temp = $scope.form.catatan;

          if($scope.form.catatan_temp == undefined && $scope.form.catatan == undefined)
            $scope.form.catatan_temp = "";

          $scope.form.catatan = "";
          $scope.form.catatan = $scope.form.catatan_temp;

          $scope.form.catatan += ` - Dikirim dengan ekspedisi Dakota`;

          $scope.hitungGrandTotal();
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });

    } else {
      // if(form.total_berat > 30000){
      //   swal("Perhatian", `Total berat barang melebihi kapasitas perhitungan ongkir oleh sistem (Max: 30000 gram). Silakan isi field biaya Ongkir secara manual.`,
      //   "error");
      //   return;
      // }

      var param = {
        destination         : form.w_kota.ro_kota_id,
        subdistrict_tujuan  : form.w_kecamatan.ro_subdistrict_id != undefined ? form.w_kecamatan.ro_subdistrict_id : NULL,
        weight              : form.total_berat,
        courier             : form.ekspedisi,
      };

      if (form.kota_asal != undefined) {
        param.kota_asal = form.kota_asal.ro_kota_id
      }

      if(form.kecamatan_asal != undefined && form.kecamatan_asal.ro_subdistrict_id != undefined) {
        param.subdistrict_asal = form.kecamatan_asal.ro_subdistrict_id;
      }

      Data.get(control_link + "/cekOngkir", param).then(function(result) {
        if(result.status_code == 200){
          $scope.listPaket            = result.data.result[0];
          $scope.catatanKirim         = result.data.keterangan;
          $scope.view_raja_ongkir     = true;
          $scope.view_dakota          = false;
          $scope.view_ekspedisi_lain  = false;
          $scope.view_scargo          = false;
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    }

  };

  $scope.setOngkir = function(harga, index){
    $scope.form.ongkir = parseInt(harga);

    angular.forEach($scope.listPaket.costs, function(val, key) {
      val.btn="";
    });
    $scope.listPaket.costs[index].btn = 'btn-success';

    // Catatan + Keterangan Paket :D
    if($scope.form.catatan_temp == undefined && $scope.form.catatan != undefined)
      $scope.form.catatan_temp = $scope.form.catatan;

    if($scope.form.catatan_temp == undefined && $scope.form.catatan == undefined)
      $scope.form.catatan_temp = "";

    $scope.form.catatan = "";
    $scope.form.catatan = $scope.form.catatan_temp;

    $scope.form.catatan += ` - Dikirim dengan ekspedisi ` + $scope.form.ekspedisi
    + ` paket : ` +`(`+$scope.listPaket.costs[index].service +`) ` + $scope.listPaket.costs[index].description + ' - ' + $scope.catatanKirim;

    // Grand total : total + ongkir
    $scope.hitungGrandTotal();
  };

  $scope.setOngkirSC = function(harga, index){
    $scope.form.ongkir = parseInt(harga);

    angular.forEach($scope.listPaket, function(val, key) {
      val.btn="";
    });
    $scope.listPaket[index].btn = 'btn-success';

    // Catatan + Keterangan Paket :D
    if($scope.form.catatan_temp == undefined && $scope.form.catatan != undefined)
      $scope.form.catatan_temp = $scope.form.catatan;

    if($scope.form.catatan_temp == undefined && $scope.form.catatan == undefined)
      $scope.form.catatan_temp = "";

    $scope.form.catatan = "";
    $scope.form.catatan = $scope.form.catatan_temp;

    $scope.form.catatan += ` - Dikirim dengan ekspedisi Sentral Cargo paket : ` +`(`+$scope.listPaket[index].PackageCategory +`) dengan Transportasi ` + $scope.listPaket[index].TransportType;

    // Grand total : total + ongkir
    $scope.hitungGrandTotal();
  };

  $scope.hitungGrandTotal = function(){
    $scope.form.grand_total = parseInt($scope.form.ongkir) + parseInt($scope.form.total);
    // var str     = $scope.form.grand_total.toString();
    // var length  = str.length;
    // $scope.form.grand_total = str.slice(0, (str.length-3)) + $scope.form.kode_unik;
  }
  $scope.remDetPesanan = function(row) {
    var params    = row;
    var index     = $scope.listPesanan.indexOf(row);
    params.jenis  = $scope.form.tipe_produk;

    if (confirm("Apa Anda ingin membatalkan produk ini dari Keranjang Belanja?")) {
      Data.post(control_link + "/hapusKeranjang", params).then(function(result) {
        if (result.status_code == 200) {
          toaster.pop("success", "Berhasil", "Produk telah dikeluarkan dari keranjang.");
          $scope.listPesanan.splice(index, 1);
          $scope.hitungTotal();
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    }
  };

  $scope.cekOverStok = function(row){
    if($scope.form.tipe_produk == "non-po" && row.jumlah_pesan > row.stok){
      toaster.pop("error", "Terjadi Kesalahan", "Jumlah Pesanan tidak boleh melebihi stok!");
      row.jumlah_pesan = row.stok;
    }
    $scope.hitungTotal();
  };

  Data.get("m_big_order/getBulk").then(function(result) {
    if (result.status_code == 200 && result.data.length > 0) {
        $scope.listBigOrder = result.data;
    }
  });

});
