app.controller("treeCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_pohon_jaringan";

  Data.get("m_pohon_jaringan/index").then(function(result) {
      if (result.status_code == 200) {
          $scope.qpaTree = result.data;
          // console.log($scope.list);
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
  });

  $scope.basicTree = [{
        name: "Node 1",
        children: [{
          name: "Node 1.1",
          children: [{
            name: "Node 1.1.1"
          }, {
            name: "Node 1.1.2"
          }]
        }]
      },
      {
        name: "Node 2",
        children: [{
          name: "Node 2.1"
        }, {
          name: "Node 2.2"
        }]
      }
    ];

});
