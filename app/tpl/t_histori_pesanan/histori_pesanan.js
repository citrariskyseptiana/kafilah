app.controller("rpesananCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link        = "t_histori_pesanan";
  $scope.formtittle       = "";
  $scope.displayed        = [];
  $scope.form             = {};
  $scope.is_edit          = false;
  $scope.is_view          = false;
  $scope.is_create        = false;
  $scope.loading          = false;
  $scope.produk_tambahan  = undefined;
  /**
   * End inialisasi
   */

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(result) {
      $scope.form.kode = result.data;
    });
  }

  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;

      angular.forEach($scope.displayed, function(val, key) {
        $scope.displayed[key].tanggal = new Date(val.tanggal);
      });

      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };
  $scope.create = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.is_create = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form = {};
    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form = form;
    $scope.getDetailPesanan(form);
  };
  $scope.view = function(form) {
    $scope.is_edit = true;
    $scope.is_view = true;
    $scope.formtittle = "Lihat Data : " + form.kode;
    $scope.form = form;
    $scope.getDetailPesanan(form);
  };
  $scope.getDetailPesanan = function(form) {
    Data.get(control_link + "/getDetailPesanan",{id:form.id}).then(function(result) {
      if(result.status_code == 200){
        $scope.listPesanan      = result.data.list;
        $scope.form.total_berat = result.data.total_berat;
        $scope.hitungTotal();
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };
  $scope.cetakNota = function(id){
    window.open(
        "api/t_keranjang/cetakPesanan?id=" + id, "_blank",
        "width=1000,height=700"
    );
  };

  $scope.cetakAlamat = function(form){
      window.open(
          "api/t_keranjang/cetakAlamat?id=" + form.id, "_blank",
          "width=1000,height=700"
      );
    }

  $scope.save = function(form) {
    $scope.loading = true;
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop("error","Terjadi Kesalahan", setErrorMessage(result.errors));
      }
      $scope.loading = false;
    });
  };
  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.is_edit_pesanan = false;
    $scope.callServer(tableStateRef);
  };
  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
        Data.post(control_link + "/saveStatus", row).then(function(result) {
          if(result.status_code == 200){
            toaster.pop("success", "Berhasil", "Pesanan berhasil dihapus.");
            $scope.displayed.splice($scope.displayed.indexOf(row), 1);
          }
        });
    }
  };
  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.getPaket = function(form) {
    if(form.w_kota == undefined){
      toaster.pop("error", "Peringatan", "Field Kabupaten/Kota harus diisi!");
      $scope.form.ekspedisi="";
      return;
    }

    if(form.total_berat > 30000){
      swal("Perhatian", `Total berat barang melebihi kapasitas perhitungan ongkir oleh sistem (Max: 30000 gram). Silakan isi field biaya Ongkir secara manual.`,
      "error");
      return;
    }

    var param = {
      destination : form.w_kota.ro_kota_id,
      weight      : form.total_berat,
      courier     : form.ekspedisi,
    };

    Data.get("t_keranjang/cekOngkir", param).then(function(result) {
      if(result.status_code == 200){
        $scope.listPaket = result.data[0];
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };
  $scope.setOngkir = function(harga, index){
    $scope.form.ongkir = parseInt(harga);

    angular.forEach($scope.listPaket.costs, function(val, key) {
      val.btn="";
    });
    $scope.listPaket.costs[index].btn = 'btn-success';
  };
  $scope.getReseller = function(cari, tipe) {
    if (cari.toString().length > 2) {
      var param = {
        nama: cari,
        tipe_member: "Reseller"
      };
      Data.get("m_member/getMember", param).then(function(result) {
        $scope.listReseller = result.data;
      });
    }
  };
  $scope.updateStatus = function(row, value){
    if(value == 'Sudah Sampai'){
      if( !confirm(`Apakah Anda ingin mengkonfirmasi bahwa Penjualan : `
         + row.kode + `, sudah terkirim & diterima pelanggan?`) ){
        return;
      } else {
        value = 'Selesai';
      }
    } else {
      if( !confirm(`Apakah Anda ingin mengubah status Penjualan : `
         + row.kode + `, menjadi "` + value + `"?`) ){
        return;
      }
    }

    var param = {
      form  : row,
      value : value
    };
    var index = $scope.displayed.indexOf(row);
    Data.post(control_link + "/updateStatus", param).then(function(result) {
      if(result.status_code == 200){
        toaster.pop("success", "Berhasil", "Status Berhasil Diupdate!");
        $scope.displayed[index].status = value;
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  }

  $scope.is_edit_pesanan = false;
  $scope.setEditPesanan = function(){
    $scope.is_edit_pesanan = true;
  };

  $scope.remDetPesanan = function(row) {
    var params    = row;
    // var index     = $scope.listPesanan.indexOf(row);
    // params.jenis  = $scope.form.tipe_produk;

    if (confirm("Apa Anda ingin membatalkan produk ini dari Keranjang Belanja?")) {
      Data.post(control_link + "/hapusKeranjang", params).then(function(result) {
        if (result.status_code == 200) {
          // toaster.pop("success", "Berhasil", "Produk telah dikeluarkan dari keranjang.");
          // $scope.listPesanan.splice(index, 1);
          // $scope.hitungTotal();
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    }
  };

  $scope.hitungTotal = function() {
    var total_berat = total_harga = total_diskon = total_poin_penjualan = 0;
    angular.forEach($scope.listPesanan, function(val, key) {
      // jika PO maka periksa stok nya
      if( ($scope.form.jenis_pesanan == 'non-po') && (val.jumlah > val.stok_temp) ){
        swal("Perhatian", `Jumlah pesanan Anda, melebihi jumlah stok.`, "error");
        $scope.listPesanan[key].jumlah = val.stok_temp;
      }

      // Hitung berat
      $scope.listPesanan[key].total_berat = parseInt(val.m_produk_berat) * parseInt(val.jumlah);
      total_berat += $scope.listPesanan[key].total_berat;

      // Hitung harga hanya untuk yang berjenis detail / tidak gratis
      if(val.jenis =='detail'){
        $scope.listPesanan[key].sub_total = (parseInt(val.harga) * parseInt(val.jumlah)) - parseInt(val.diskon);
        total_harga += $scope.listPesanan[key].sub_total;

        total_diskon += $scope.listPesanan[key].diskon;
      }

      // hitung poin penjualan
      total_poin_penjualan += ( parseInt(val.poin) * parseInt(val.jumlah));
      console.log(total_poin_penjualan);
    });

    $scope.form.total         = total_harga;
    $scope.form.total_berat   = total_berat;
    $scope.form.total_diskon  = total_diskon;
    $scope.form.poin_penjualan = total_poin_penjualan;
  };

  $scope.updateStok = function(row) {
    var params            = row;
    params.jenis_pesanan  = $scope.form.jenis_pesanan;

    // if($scope.form.jenis_pesanan == 'non-po'){
      Data.post(control_link + "/updateStok", params).then(function(result) {
        if (result.status_code == 200) {
          $scope.getDetailPesanan($scope.form);
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    // }
  };

  $scope.updateDetail = function(row) {
    var params            = row;
    params.jenis_pesanan  = $scope.form.jenis_pesanan;

    if( confirm("Apakah Anda ingin menghapus item '"+ row.m_produk_nama+ "' dari daftar pesanan?") ){
      Data.post(control_link + "/updateDetail", params).then(function(result) {
        if (result.status_code == 200) {
          $scope.getDetailPesanan($scope.form);
        } else {
          toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
      });
    }
  };

  $scope.getProdukTambahan = function(cari) {
    if (cari.toString().length > 2) {
      var param = {
        nama    : cari,
        jenis   : $scope.form.jenis_pesanan,
      };
      Data.get(control_link + "/getProdukTambahan", param).then(function(result) {
        $scope.listProdukTambahan = result.data;
      });
    }
  };

  $scope.addProduk = function( value ) {
    value.jenis   = 'detail';
    value.jumlah  = 1;
    value.diskon  = 0;
    value.jenis_pesanan   = $scope.form.jenis_pesanan;
    value.t_penjualan_id  = $scope.form.id;

    $scope.listPesanan.push( value );
    $scope.hitungTotal();

    Data.post(control_link + "/saveProdukTambahan", value).then(function(result) {
      if (result.status_code == 200) {
        $scope.produk_tambahan = undefined;
        $scope.getDetailPesanan($scope.form);
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  }

});
