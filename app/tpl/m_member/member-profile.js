app.controller("mprofileCtrl", function($scope, Data, toaster, $stateParams, $state) {
    var control_link = "m_member";
    Data.get(control_link + "/profile").then(function(result) {
        var form = result.data;
        $scope.formtittle = "Update Profil Member : " + form.nik;
        $scope.form = form;
        $scope.form.tanggal_lahir = new Date(form.tanggal_lahir);
    });
    $scope.save = function(form) {
        $scope.loading = true;
        Data.post(control_link + "/save", form).then(function(result) {
            if (result.status_code == 200) {
                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                $state.reload()
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
            $scope.loading = false;
        });
    };
    // Get Provinsi, kota, kec, desa
    Data.get("site/getProvinsi").then(function(result) {
        $scope.listProvinsi = result.data;
    });
    $scope.cariKota = function(provinsi) {
        var param = {
            provinsi: provinsi.id
        };
        Data.get("site/getKota", param).then(function(result) {
            $scope.listKota = result.data;
        });
    };
    $scope.cariKecamatan = function(kota) {
        var param = {
            kota: kota.id
        };
        Data.get("site/getKecamatan", param).then(function(result) {
            $scope.listKecamatan = result.data;
        });
    };
    $scope.cariDesa = function(kecamatan) {
        var param = {
            kecamatan: kecamatan.id
        };
        Data.get("site/getDesa", param).then(function(result) {
            $scope.listDesa = result.data;
        });
    };
});
