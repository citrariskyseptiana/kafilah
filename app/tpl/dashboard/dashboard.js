angular.module('app').controller('dashboardCtrl', function ($scope, Data, $stateParams, $state, UserService, $location) {
    var user = UserService.getUser();
    if (user === null) {
        $location.path('/login');
    }

    console.log($scope)

    // Jika User Admin
    if ($scope.user != undefined && $scope.user.tipe_member == 'Admin') {
        // Mendapatkan notifikasi
        $scope.getNotifLevelUp = function (filter_tanggal) {
            console.log("getNotifLevelUp");
            var params = {
                tanggal: filter_tanggal == undefined ? new Date() : filter_tanggal,
            };
            Data.get("site/getNotifLevelUp", params).then(function (result) {
                if (result.status_code == 200) {
                    $scope.listSQPA = result.data.listSQPA;
                    $scope.listMQPA = result.data.listMQPA;
                    $scope.listStatusQPA = result.data.listStatusQPA;
                    $scope.listNewQPA = result.data.listNewQPA;
                    $scope.listKonfirmasiBayar = result.data.listKonfirmasiBayar;
                    $scope.apj_labels = result.data.lapAPJ.label;
                    $scope.apj_data = result.data.lapAPJ.data;

                    $scope.totalPBayar = 0;
                    angular.forEach($scope.listKonfirmasiBayar, function (val, key) {
                        $scope.totalPBayar += val.grand_total;
                    });

                }
            });

        }
        // end function
        $scope.getNotifLevelUp();

        $scope.setujui = function (form) {
            $state.go('master.member', {obj: form});
        };

        // $scope.labels1 = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', "Ags", "Sep", "Okt", "Nov", "Des"];
        // $scope.data1 = [90, 23, 22, 78, 36, 44, 38, 60, 90, 30, 34, 51];
    }

    // Jika User Member
    if ($scope.user != undefined && $scope.user.tipe_member == "Member") {

        // function Get Laporan Member
        $scope.laporanMember = function (filter_tahun) {

            const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
            const endOfMonth = moment().endOf('month').format('YYYY-MM-DD hh:mm');

            var filter = {
                periode: {startDate: startOfMonth, endDate: endOfMonth},
                member: $scope.user,
                status: "Menunggu Pembayaran",
                tahun: filter_tahun == undefined ? new Date() : filter_tahun,
            };
            // Get Laporan Pesanan

            Data.get("l_pesanan/laporan", filter).then(function (result) {
                if (result.status_code == 200) {
                    $scope.is_view = true;
                    $scope.laporan = result.data.laporan;
                    $scope.periode_start = new Date(startOfMonth);
                    $scope.periode_end = new Date(endOfMonth);

                    $scope.totalBayar = 0;
                    angular.forEach($scope.laporan, function (val, key) {
                        $scope.totalBayar += val.grand_total;
                    });

                } else {
                    toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                }
            });

            // Get Laporan Poin
            Data.post("l_poin_reseller/laporan_per_tahun", filter).then(function (result) {
                if (result.status_code == 200) {
                    $scope.is_view = true;
                    $scope.laporan_poin = result.data.laporan;
                    $scope.lp_total_kom = result.data.total_komisi;
                    $scope.lp_total_pj = result.data.total_penjualan;
                    $scope.lp_total_poin = result.data.total_poin;
                    $scope.pajak = result.data.pajak;
                    $scope.periode_start = new Date(startOfMonth);
                    $scope.periode_end = new Date(endOfMonth);
                } else {
                    toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                }
            });

            // Get laporan Personal Commission
            var filter_pc = {
                periode: new Date(),
                member: $scope.user,
                tahun: filter_tahun == undefined ? new Date() : filter_tahun,
            };
            Data.post("l_bonus_pc/laporan", filter_pc).then(function (result) {
                if (result.status_code == 200) {
                    $scope.is_view = true;
                    $scope.laporan_pc = result.data.laporan;
                    $scope.totalKomisi = result.data.totalKomisi;
                    $scope.periode_pc = new Date(filter_pc.tahun);
                } else {
                    toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                }
            });

            // Laporan MR
            if ($scope.user.m_roles_id == 3 || $scope.user.m_roles_id == 4) {
                var filter_mr = {
                    periode: new Date(),
                    member: $scope.user,
                };
                Data.post("/l_bonus_mr/laporan", filter_mr).then(function (result) {
                    if (result.status_code == 200) {
                        $scope.is_view = true;
                        $scope.laporan_mr = result.data.laporan;
                        $scope.komisi_mr = result.data.totalKomisi;
                        $scope.periode_mr = new Date();
                    } else {
                        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                    }
                });

                Data.post("/l_bonus_mcs/laporan", filter_mr).then(function (result) {
                    if (result.status_code == 200) {
                        $scope.is_view = true;
                        $scope.laporan_mcs = result.data.laporan;
                        $scope.komisi_mcs = result.data.totalKomisi;
                        $scope.periode_mcs = new Date();
                    } else {
                        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                    }
                });
            }

            // Laporan MCM
            if ($scope.user.m_roles_id == 4) {
                var filter_mcm = {
                    periode: new Date(),
                    member: $scope.user,
                };
                Data.post("/l_bonus_mcm/laporan", filter_mr).then(function (result) {
                    if (result.status_code == 200) {
                        $scope.is_view = true;
                        $scope.laporan_mcm = result.data.laporan.listMQPA;
                        $scope.komisi_mcm = result.data.totalKomisi;
                        $scope.periode_mcm = new Date();
                    } else {
                        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                    }
                });
            }

            // Laporan RC
            if ($scope.user.m_roles_id == 2) {
                var filter_rc = {
                    periode: new Date(),
                    member: $scope.user,
                    tahun: filter_tahun == undefined ? new Date() : filter_tahun,
                };
                Data.post("/l_bonus_rc/laporan", filter_rc).then(function (result) {
                    if (result.status_code == 200) {
                        $scope.is_view = true;
                        $scope.laporan_rc = result.data.laporan;
                        $scope.komisi_rc = result.data.totalKomisi;
                        $scope.periode_rc = new Date();
                    } else {
                        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                    }
                });
            }

            // Pohon Jaringan
            Data.get("m_pohon_jaringan/index").then(function (result) {
                if (result.status_code == 200) {
                    $scope.qpaTree = result.data;
                    // console.log($scope.list);
                } else {
                    toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                }
            });
        }
        // End - function Get Laporan Member

        $scope.laporanMember();
    }

    // Get data member
    Data.get("/m_member/profile").then(function (result) {
        if (result.status_code == 200) {
            $scope.data_member = result.data;
        } else {
            toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
        }
    });

    $scope.printLaporan = function () {
        var printContents = document.getElementById('print_area').innerHTML;
        var popupWin = window.open('', '_blank', 'width=1000,height=500');
        popupWin.document.open();
        popupWin.document.write(`
			<html>
				<head>
					<style>
					.float-left {float: left!important;}
						.text-right{ text-align:right}
						.text-left{ text-align:left}
						.text-center{ text-align:center}
						table{
					    width:100%;
							border-collapse: collapse;
							display: table;
							border-spacing: 2px;
							border-color: grey;
							margin-top:10px;
							margin-bottom:10px;
						}
						table.myFormat tr th {
							text-align: center;
							font-size: 11px;
							padding:2px;
						}

						table.myFormat tr td {
							font-size: 11px;
							padding:2px;
						}
						.borderless td, .borderless th {
							border: none;
						}
						.table-bordered, .table-bordered td, .table-bordered th {
					    border: 1px solid #c8ced3;
						}
					</style>
				</head>
				<body onload="window.print()">`
                + printContents +
                `</body>
			</html>
			`);
        popupWin.document.close();
    }

    $scope.print_laporan = function (data, pajak) {
        console.log('pajak ', pajak);
        console.log('data print', data);
        $scope.tanggal_cetak = new Date();
        $scope.komisiDetail_bulan = data.bulan;

        $scope.komisiDetail = [];
        $scope.komisiDetail_total_cash = 0;
        $scope.komisiDetail_total_komisi = 0;
        $scope.komisiDetail_total_pajak = 0;
        $scope.komisiDetail_total_net = 0;

        // Insert MCM
        if (parseInt($scope.user.m_roles_id) == 4 && parseInt(data.mcm) > 0) {
            var pajak_nominal = (parseInt(data.mcm) * (parseInt(pajak) / 100)).toFixed(2);
            var net = (parseInt(data.mcm) - pajak_nominal).toFixed(2);

            $scope.insertDetail(
                    new Date(),
                    '',
                    '',
                    '',
                    '',
                    0,
                    "MCM",
                    parseInt(data.mcm),
                    0,
                    pajak_nominal,
                    net
                    );

            $scope.komisiDetail_total_komisi += parseInt(data.mcm);
            $scope.komisiDetail_total_pajak += parseFloat(pajak_nominal);
            $scope.komisiDetail_total_net += parseFloat(net);
        }

        // Insert MCS
        if (parseInt($scope.user.m_roles_id) == 4 || parseInt($scope.user.m_roles_id) == 3 && parseInt(data.mcs) > 0) {
            var pajak_nominal = (parseInt(data.mcs) * (parseInt(pajak) / 100)).toFixed(2);
            var net = (parseInt(data.mcs) - pajak_nominal).toFixed(2);

            $scope.insertDetail(
                    new Date(),
                    '',
                    '',
                    '',
                    '',
                    0,
                    "MCS",
                    parseInt(data.mcs),
                    0,
                    pajak_nominal,
                    net
                    );

            $scope.komisiDetail_total_komisi += parseInt(data.mcs);
            $scope.komisiDetail_total_pajak += parseFloat(pajak_nominal);
            $scope.komisiDetail_total_net += parseFloat(net);
        }

        // Insert MR
        if (parseInt($scope.user.m_roles_id) == 4 || parseInt($scope.user.m_roles_id) == 3 && parseInt(data.mr) > 0) {
            var pajak_nominal = (parseInt(data.mr) * (parseInt(pajak) / 100)).toFixed(2);
            var net = (parseInt(data.mr) - pajak_nominal).toFixed(2);

            $scope.insertDetail(
                    new Date(),
                    '',
                    '',
                    '',
                    '',
                    0,
                    "MR",
                    parseInt(data.mr),
                    0,
                    pajak_nominal,
                    net
                    );

            $scope.komisiDetail_total_komisi += parseInt(data.mr);
            $scope.komisiDetail_total_pajak += parseFloat(pajak_nominal);
            $scope.komisiDetail_total_net += parseFloat(net);
        }

        // Insert RC
        if (parseInt($scope.user.m_roles_id) == 2 && parseInt(data.rc) > 0) {
            var pajak_nominal = (parseInt(data.rc) * (parseInt(pajak) / 100)).toFixed(2);
            var net = (parseInt(data.rc) - pajak_nominal).toFixed(2);

            $scope.insertDetail(
                    new Date(),
                    '',
                    '',
                    '',
                    '',
                    0,
                    "RC",
                    parseInt(data.rc),
                    0,
                    pajak_nominal,
                    net
                    );

            $scope.komisiDetail_total_komisi += parseInt(data.rc);
            $scope.komisiDetail_total_pajak += parseFloat(pajak_nominal);
            $scope.komisiDetail_total_net += parseFloat(net);
        }

        // Insert PC
        if (parseInt(data.pc) > 0) {
            console.log(data);
            angular.forEach(data.pc_data.list_t_penjualan, function (val, key) {
                var pc = (parseInt(val.total) * parseInt(data.pc_data.komisi_persen) / 100).toFixed(2);
                var pajak_nominal = (parseInt(pc) * (parseInt(pajak) / 100)).toFixed(2);
                var net = (parseInt(pc) - pajak_nominal).toFixed(2);
                $scope.insertDetail(
                        val.tanggal,
                        '',
                        val.kode,
                        val.nama_penerima,
                        0,
                        val.total,
                        "PC",
                        pc,
                        0,
                        pajak_nominal,
                        net
                        );

                $scope.komisiDetail_total_cash += parseInt(val.total);
                $scope.komisiDetail_total_komisi += parseFloat(pc);
                $scope.komisiDetail_total_pajak += parseFloat(pajak_nominal);
                $scope.komisiDetail_total_net += parseFloat(net);
            });

        }
    };

    $scope.insertDetail = function (
            tanggal,
            no_kp,
            no_invoice,
            nama_cust,
            no_su,
            cash,
            keterangan,
            komisi,
            cwh,
            pajak,
            net
            ) {

        var data = {
            "tanggal": tanggal,
            "no_kp": no_kp,
            "no_invoice": no_invoice,
            "nama_cust": nama_cust,
            "no_su": no_su,
            "cash": cash,
            "keterangan": keterangan,
            "komisi": komisi,
            "cwh": cwh,
            "pajak": pajak,
            "net": net,
        };
        $scope.komisiDetail.push(data);
    }

    $scope.test = function () {
        console.log("ok")
    }

    $scope.deleteNotif = function (trans_tipe) {
        console.log("ok")
        Data.post('/site/delete_notification', {trans_tipe: trans_tipe}).then(function (response) {
            console.log(response)
        });
//        $state.go(state)
    }

});

/**
 Unused Code
 */

// var apSeries = [];
// angular.forEach(result.data.lapPenjualan.data, function(val, key) {
// 	apSeries.push( val );
// });
//
// console.log("apSeries", apSeries[0]);
//
// $scope.myConfig = {
// 	"type":"line",
// 	"scaleY": {
// 		"short" : true,
// 	},
// 	"scale-x"  : {
// 		"labels" : ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agt", "Sep", "Okt", "Nov", "Des"]
// 	},
// 	"legend" : {
// 		"layout":"1x5", //row x column
// 		"x":"20%",
// 		"y":"2%",
// 	},
// 	"series" : [
// 		{
// 			"values"			: apSeries[0],
// 			"text": 'QPA',
// 		},
// 		{
// 			"values"			: apSeries[1],
// 			"text": 'SQPA',
// 		},
// 		{
// 			"values"			: apSeries[2],
// 			"text": 'MQPA',
// 		}
// 	]
// };
// // Chart Analisa Penjualan
// $scope.lpj_data				= result.data.lapPenjualan.data;
// $scope.lpj_series			= result.data.lapPenjualan.series;
// $scope.lpj_labels			= result.data.lapPenjualan.label;
// $scope.lpj_options 		= { legend: { display: true } }; // missing
