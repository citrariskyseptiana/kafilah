app.controller("lmootaCtrl", function($scope, Data, toaster) {
  var control_link = "l_mutasi_moota";

  $scope.reset_filter = function() {
    $scope.filter = {};
    $scope.is_view = false;
    $scope.loading = false;
    $scope.periode_start = undefined;
    $scope.periode_end = undefined;

    $scope.filter.periode = {
      endDate: moment(),
      startDate: moment()
    };
  }

  $scope.reset_filter();

  $scope.view = function(filter) {
    filter.export = 0;
    filter.periode.startDate  = moment(filter.periode.startDate).format('YYYY-MM-DD HH:mm:00');
    filter.periode.endDate    = moment(filter.periode.endDate).format('YYYY-MM-DD HH:mm:00');

    Data.post(control_link + "/laporan", filter).then(function(result) {
      if (result.status_code == 200) {
        $scope.is_view        = true;
        $scope.laporan        = result.data.laporan;
        $scope.grand_total    = result.data.grand_total;
        $scope.periode_start  = new Date($scope.filter.periode.startDate);
        $scope.periode_end    = new Date($scope.filter.periode.endDate);
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };

  $scope.print = function() {
    var printContents = document.getElementById('print-area').innerHTML;
    var popupWin = window.open('', '_blank', 'width=1000,height=700');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
        <link rel="stylesheet" type="text/css" href="./tpl/common/print-style.css" />
        </head>
        <body onload="window.print()">` + printContents
        + `</body></html>`);
    popupWin.document.close();
  }

  $scope.export = function(filter){
    // filter.export     = 1;
    // filter.periode.startDate  = moment(filter.periode.startDate).format('YYYY-MM-DD HH:mm:00');
    // filter.periode.endDate    = moment(filter.periode.endDate).format('YYYY-MM-DD HH:mm:00');
    //
    // window.open("api/" + control_link + "/laporan?" + $.param(filter), "_blank");

    var blob = new Blob([document.getElementById('print-area').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Laporan-Arisan.xls");
  }
});
