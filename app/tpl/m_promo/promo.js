app.controller("mpromoCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_promo";
  $scope.formtittle = "";
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;

  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };
  $scope.getDetail = function(id) {
    Data.get(control_link + "/view?m_promo_id=" + id).then(function(response) {
      $scope.listDetail = response.data;

      angular.forEach($scope.listDetail, function(val, key) {
        $scope.listDetail[key].harga_asli = val.m_produk_id.harga_jual;
      });
      $scope.hitungDiskon();
      // console.log("$scope.listDetail", $scope.listDetail);
    });
  };
  $scope.listDetail = [{}];
  $scope.addDetail = function(val) {
    var comArr = eval(val);
    var newDet = {
      harga_asli:0,
      harga_diskon:0,
      diskon:0,
      diskon_nominal:0,
    };
    val.unshift(newDet);
  };
  $scope.removeDetail = function(val, paramindex) {
    var comArr = eval(val);
    if (comArr.length > 1) {
      val.splice(paramindex, 1);
    } else {
      alert("Something gone wrong");
    }
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  // Inialisasi cari produk
  Data.get("m_produk/getProduk").then(function(result) {
    $scope.listProduk = result.data;
  });

  $scope.getProduk = function(cari) {
    if(cari.toString().length > 2){
      var param = {
        nama : cari
      };

      Data.get("m_produk/getProduk", param).then(function(result) {
        $scope.listProduk = result.data;
      });
    }
  }

  $scope.setProduk = function(data, index) {
    var is_ok = true;
    angular.forEach($scope.listDetail, function(val, key) {
      if(key != index && data.id == val.m_produk_id.id){
        toaster.pop("error", "Peringatan!", "Produk sudah ada dalam list!");
        $scope.listDetail.splice($scope.listDetail.indexOf(data), 1);
        is_ok = false;
        return;
      }
    });

    if(is_ok){
      $scope.listDetail[index].harga_asli = data.harga_jual;
      $scope.listDetail[index].diskon = 0;
      $scope.listDetail[index].diskon_nominal = 0;
      $scope.listDetail[index].harga_diskon = data.harga_jual;
      $scope.hitungDiskon();
    }
  }

  $scope.hitungDiskon = function() {
    angular.forEach($scope.listDetail, function(val, key) {
      var diskon_nominal = (parseFloat(val.diskon)/100 * parseInt(val.harga_asli));
      $scope.listDetail[key].harga_diskon = parseInt(val.harga_asli) - diskon_nominal;
      $scope.listDetail[key].diskon_nominal = diskon_nominal;
    });
  }

  $scope.setDiskon = function(row, tipe){
    if(tipe == 'persen'){
      if(row.diskon > 100){
        toaster.pop("error", "Peringatan!", "Persentase diskon lebih besar 100!");
        return;
      }
      row.diskon_nominal = (parseFloat(row.diskon)/100 * parseInt(row.harga_asli));
    } else if(tipe == 'nominal') {
      if(row.diskon_nominal > row.harga_asli){
        toaster.pop("error", "Peringatan!", "Nominal diskon lebih besar dari harga produk!");
        return;
      }
      row.diskon = parseFloat(row.diskon_nominal) / parseInt(row.harga_asli) * 100;
    }

    $scope.hitungDiskon();
  }

  $scope.create = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.is_create = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form = {};
    $scope.listDetail=[]
    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form = form;
    $scope.getDetail(form.id);
  };
  $scope.view = function(form) {
    $scope.is_edit = true;
    $scope.is_view = true;
    $scope.formtittle = "Lihat Data : " + form.kode;
    $scope.form = form;
    $scope.getDetail(form.id);
  };
  $scope.save = function(form) {
    $scope.loading = true;
    var form = {
      data: form,
      detail: $scope.listDetail
    }
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop(
          "error",
          "Terjadi Kesalahan",
          setErrorMessage(result.errors)
        );
      }
      $scope.loading = false;
    });
  };
  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.callServer(tableStateRef);
  };
  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.aktifkan = function(row, status) {
    var is_proses = false;

    if(status == "aktif"){
      var param = {
        id: row.id,
        is_used: 1
      };
      if (confirm("Apa Anda ingin Mengaktifkan Promo ini ?")) {
        is_proses = true;
      }
    } else if(status == "non-aktif"){
      var param = {
        id: row.id,
        is_used: 0
      };

      if (confirm("Apa Anda ingin Me Non-Aktifkan Promo ini ?")) {
        is_proses = true;
      }
    }

    if(is_proses == true){
      Data.post(control_link + "/setPromo", param).then(function(result) {
        if(result.status_code == 200){
          toaster.pop("success", "Berhasil", "Operasi sukses!");
          $scope.callServer(tableStateRef);
        }
      });
    }

  };
});
