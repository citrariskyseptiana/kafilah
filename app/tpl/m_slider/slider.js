app.controller("msliderCtrl", function($scope, Data, toaster, $state) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_slider";

  $scope.loading = false;

  Data.get(control_link + "/index").then(function(result) {
    if(result.status_code == 200){
      $scope.listDetail = result.data.list;
    }
  });

  $scope.listDetail = [{}];
  $scope.addDetail = function(val) {
    var comArr = eval(val);
    var newDet = {
      harga_asli:0,
      harga_diskon:0,
      diskon:0,
      diskon_nominal:0,
    };
    val.unshift(newDet);
  };

  $scope.removeDetail = function(val, paramindex) {
    var konfirm = `Apakah Anda ingin menghapus item ini?`;
    if(!confirm(konfirm)){
      return;
    }

    val.splice(paramindex, 1);
    $scope.save();
  };

  $scope.save = function() {
    $scope.loading = true;
    var form = {
      detail: $scope.listDetail
    }
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
      $scope.loading = false;
    });
  };
  $scope.cancel = function() {
    $state.reload();
  };

  $scope.aktifkan = function(row, status) {
    var is_proses = false;

    if(status == "aktif"){
      var param = {
        id: row.id,
        is_used: 1
      };
      if (confirm("Apa Anda ingin Mengaktifkan Promo ini ?")) {
        is_proses = true;
      }
    } else if(status == "non-aktif"){
      var param = {
        id: row.id,
        is_used: 0
      };

      if (confirm("Apa Anda ingin Me Non-Aktifkan Promo ini ?")) {
        is_proses = true;
      }
    }

    if(is_proses == true){
      Data.post(control_link + "/setPromo", param).then(function(result) {
        if(result.status_code == 200){
          toaster.pop("success", "Berhasil", "Operasi sukses!");
          $scope.callServer(tableStateRef);
        }
      });
    }

  };
});
