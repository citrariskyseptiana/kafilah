angular.module('app').controller('settingCtrl', function($scope, Data, $state, UserService, $location, toaster) {
	var control_link = "m_setting";

	Data.get(control_link + "/index").then(function(result) {
		$scope.form 						= result.data.form;
		$scope.listSettingBank 	= result.data.listSettingBank;
		$scope.cariKecamatan($scope.form.kota_asal);
	});

	$scope.cariKecamatan = function(kota) {
    var param = {
      kota: kota.id
    };
    Data.get("site/getKecamatan", param).then(function(result) {
      $scope.listKecamatan = result.data;
    });
  };

	// Get daftar bank
	Data.get("t_approval_pembayaran/getBank").then(function(result) {
		$scope.listBank = result.data;
	});

	Data.get(control_link + "/getKota").then(function(result) {
		$scope.listKotaAsal = result.data;
	});

	Data.get(control_link + "/getKotaSC").then(function(result) {
		$scope.listKotaSC = result.data;
	});

	$scope.save = function(form) {
			$scope.loading = true;
			var params = {
				form: form,
				listSettingBank: $scope.listSettingBank,
			}
			Data.post(control_link + "/save", params).then(function(result) {
					if (result.status_code == 200) {
							toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
							$state.reload();
					} else {
							toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
					}
					$scope.loading = false;
			});
	};

	$scope.setMaintenance = function(form) {
			var params = {
				form: form,
			}

			Data.post(control_link + "/setMaintenance", params).then(function(result) {
				var pesan = 'Mode Maintenance telah diaktifkan.';
				if(form.is_maintenance == 0){
					var pesan = 'Mode Maintenance telah di-non-aktifkan.';
				}

				if (result.status_code == 200) {
						toaster.pop("success", "Berhasil", pesan);

				} else {
						toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
				}

			});
	};

	$scope.listSettingBank = [{}];
	console.log("aSADASDAS", $scope.listSettingBank );
  $scope.addBank = function(val) {
		console.log("woii",val);
		return;
    var comArr = eval(val);
    var newDet = {
      atas_nama:"",
    };
		if(val == undefined){
			$scope.listSettingBank = [{}];
		} else {
			val.push(newDet);
		}
  };
  $scope.remBank = function(val, paramindex) {
		if( !confirm("Apakah Anda yakin ingin menghapus item ini?") )
			return;

    var comArr = eval(val);
    val.splice(paramindex, 1);
  };

});
