app.controller("apembayaranCtrl", function($scope, Data, toaster, $uibModal) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "t_approval_pembayaran";
  $scope.formtittle = "";
  $scope.displayed = [];
  $scope.form = {};
  $scope.is_edit = false;
  $scope.is_view = false;
  $scope.is_create = false;
  $scope.loading = false;

  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  $scope.getPesanan = function(cari) {
    if(cari.toString().length > 2){
      var param = {
        kode : cari
      };

      Data.get(control_link + "/getPesanan", param).then(function(result) {
        $scope.listPesanan = result.data;
      });
    }
  }

  // Get daftar bank
  Data.get(control_link + "/getBank").then(function(result) {
    $scope.listBank = result.data;
  });

  $scope.setPesanan = function(value) {
    if(value == undefined) return;
    $scope.form.grand_total = parseInt(value.total)+parseInt(value.ongkir);
    $scope.form.t_penjualan_tanggal = new Date(value.tanggal);
  };

  $scope.create = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.is_create = true;
    $scope.formtittle = "Form Tambah Data";
    $scope.form = {};
    $scope.form.tanggal_transfer = new Date();
    $scope.getKode();
  };
  $scope.update = function(form) {
    $scope.is_edit = true;
    $scope.is_view = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form = form;
    $scope.form.tanggal_transfer = new Date(form.tanggal_transfer);
  };
  $scope.view = function(form) {
    $scope.is_edit = true;
    $scope.is_view = true;
    $scope.formtittle = "Lihat Data : " + form.kode;
    $scope.form = form;
    $scope.form.tanggal_transfer = new Date(form.tanggal_transfer);
  };
  $scope.save = function(form) {
    $scope.loading = true;
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop(
          "error",
          "Terjadi Kesalahan",
          setErrorMessage(result.errors)
        );
      }
      $scope.loading = false;
    });
  };
  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.callServer(tableStateRef);
  };
  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };
  $scope.konfirmasi = function(form, status) {
    var param = {
      status: status,
      form: form
    };

      if (status == 'Rejected') {
          var pesan = "MENOLAK";
          Data.post(control_link + "/konfirmasi", param).then(function(result) {
              if (result.status_code == 200) {
                  toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                  $scope.cancel();
              } else {
                  toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
              }
          });
      } else if (status == 'Approved') {
          var pesan = "MENYETUJUI";

          if (confirm("Apa Anda ingin " + pesan + " pembayaran ini?")) {
              Data.post(control_link + "/konfirmasi", param).then(function(result) {
                  if (result.status_code == 200) {
                      toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                      $scope.cancel();
                  } else {
                      toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                  }
              });
          }
      }
  };

});

// Modal Detail Batch Item
app.controller('batchCtrl', function ($scope, Data, $uibModalInstance, toaster, form) {
    $scope.params = form;
    console.log('params : ', $scope.params)

    $scope.close = function () {
      event.preventDefault();
      var satuan_terkecil_id=0;
      $uibModalInstance.close( { "data" : $scope.params } );
    };


});
