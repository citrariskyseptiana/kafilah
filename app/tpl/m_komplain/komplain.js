app.controller("komplainCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var tableStateRef2;
  var control_link  = "m_komplain";
  $scope.formtittle = "";
  $scope.displayed  = [];
  $scope.listReply  = [];
  $scope.replyParams= {};
  $scope.form       = {};
  $scope.is_edit    = false;
  $scope.is_view_diskusi = false;
  $scope.is_view    = false;
  $scope.is_create  = false;
  $scope.loading    = false;

  /**
   * End inialisasi
   */
  $scope.callServer = function callServer(tableState) {
    tableStateRef = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset: offset,
      limit: limit
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/index", param).then(function(response) {
      $scope.displayed = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
    $scope.isLoading = false;
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  $scope.create = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = false;
    $scope.is_create  = true;
    $scope.formtittle = "Post Baru";
    $scope.form       = {};
    $scope.form.tipe  = "post";
    $scope.listDetail = []
    $scope.getKode();
  };

  $scope.update = function(form) {
    $scope.is_edit    = true;
    $scope.is_view    = false;
    $scope.formtittle = "Edit Data : " + form.kode;
    $scope.form       = form;
  };

  $scope.view_diskusi = function(form) {
    $scope.is_view_diskusi    = true;
    $scope.formtittle = form.judul;
    $scope.form       = form;
  };

  $scope.save = function(form) {
    $scope.loading = true;
    var form = {
      data: form,
      detail: $scope.listDetail
    }
    Data.post(control_link + "/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
        $scope.cancel();
      } else {
        toaster.pop("error","Terjadi Kesalahan", setErrorMessage(result.errors));
      }
      $scope.loading = false;
    });
  };

  $scope.cancel = function() {
    $scope.is_edit = false;
    $scope.is_view_diskusi = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.callServer(tableStateRef);
  };

  $scope.trash = function(row) {
    if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
      row.is_deleted = 1;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.restore = function(row) {
    if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
      row.is_deleted = 0;
      Data.post(control_link + "/saveStatus", row).then(function(result) {
        $scope.displayed.splice($scope.displayed.indexOf(row), 1);
      });
    }
  };

  $scope.setReplyParams = function(form, m_komplain_id) {
    $scope.replyParams            = {};
    $scope.replyParams            = form;
    // $scope.replyParams.reply      = `<blockquote>`+form.isi+`</blockquote><br>`;
    $scope.replyParams.m_komplain_id = m_komplain_id;
    $scope.is_reply               = true;
  };

  $scope.reply = function() {

    Data.post(control_link + "/saveReply", $scope.replyParams).then(function(result) {
      if(result.status_code == 200){
        $scope.callServer2(tableStateRef2);
        $scope.is_reply = false;

        toaster.pop("success","Berhasil", "Balasan berhasil disimpan.");
      } else {
        toaster.pop("error","Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };

  // Untuk daftar reply
  $scope.callServer2 = function callServer2(tableState) {
    tableStateRef2 = tableState;
    $scope.isLoading = true;
    var offset = tableState.pagination.start || 0;
    var limit = tableState.pagination.number || 10;
    var param = {
      offset      : offset,
      limit       : limit,
      m_komplain_id  : $scope.form.id,
    };
    if (tableState.sort.predicate) {
      param["sort"] = tableState.sort.predicate;
      param["order"] = tableState.sort.reverse;
    }
    if (tableState.search.predicateObject) {
      param["filter"] = tableState.search.predicateObject;
    }
    Data.get(control_link + "/listReply", param).then(function(response) {
      $scope.listReply = response.data.list;
      tableState.pagination.numberOfPages = Math.ceil(
        response.data.totalItems / limit
      );
    });
  };

  $scope.close = function(form) {
    if ( !confirm("Apakah Anda ingin menutup diskusi ini?")) return;

    form.is_closed = 1;
    Data.post(control_link + "/close", form).then(function(result) {
      if(result.status_code = 200){
        toaster.pop("success","Berhasil", "Balasan berhasil disimpan.");
        $scope.view_diskusi(form);
      } else {
        toaster.pop("error","Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };

});
