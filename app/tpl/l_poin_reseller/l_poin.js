app.controller("lpoinCtrl", function($scope, Data, toaster) {
  var control_link = "l_poin_reseller";

  $scope.reset_filter = function() {
    $scope.filter = {};
    $scope.is_view = false;
    $scope.loading = false;
    $scope.periode_start = undefined;
    $scope.periode_end = undefined;

    $scope.filter.periode = {
      locale: {
        format: 'DD-MMM-YYYY'
      },
      endDate: moment(),
      startDate: moment()
    };
  }

  $scope.reset_filter();

  $scope.view = function(filter) {
    Data.post(control_link + "/laporan", filter).then(function(result) {
      if (result.status_code == 200) {
        $scope.is_view        = true;
        $scope.laporan        = result.data.laporan;
        $scope.total_poin     = result.data.total_poin;
        $scope.total_penjualan= result.data.total_penjualan;
        $scope.periode_start  = new Date($scope.filter.periode.startDate);
        $scope.periode_end    = new Date($scope.filter.periode.endDate);
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
    });
  };

  $scope.print = function() {
    var printContents = document.getElementById('print-area').innerHTML;
    var popupWin = window.open('', '_blank', 'width=1000,height=700');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
        <link rel="stylesheet" type="text/css" href="./tpl/common/print-style.css" />
        </head>
        <body onload="window.print()">` + printContents
        + `</body></html>`);
    popupWin.document.close();
  }

  $scope.export = function(){
    var blob = new Blob([document.getElementById('print-area').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Laporan-Poin-Reseller.xls");
  }
});
