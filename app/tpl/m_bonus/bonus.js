app.controller("mbonusCtrl", function($scope, Data, toaster) {
  /**
   * Inialisasi
   */
  var tableStateRef;
  var control_link = "m_bonus";
  $scope.formtittle = "";
  $scope.loading = false;

  // Get Level QPA
  Data.get(control_link + "/getLevel").then(function(response) {
    $scope.listLevel = response.data.list;
  });


  Data.get(control_link + "/index").then(function(response) {
    if(response.status_code == 200){
      $scope.listDetail=[];
      // Grouping detail berdasarkan levelnya
      angular.forEach(response.data, function(val, key) {
        if($scope.listDetail[key] == undefined){
          $scope.listDetail[key]=[];
        }
        $scope.listDetail[key] = val;
      });

    } else {
      toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(response.errors) );
    }
  });

  $scope.getDetail = function(id) {
    Data.get(control_link + "/view?m_promo_id=" + id).then(function(response) {
      $scope.listDetail = response.data;

      angular.forEach($scope.listDetail, function(val, key) {
        $scope.listDetail[key].harga_asli = val.m_produk_id.harga_jual;
      });
      $scope.hitungDiskon();
    });
  };

  $scope.addDetail = function(m_level, index) {
    var newDet = {
      m_level_id:m_level,
      basic_com_persen:0,
      basic_com_nominal:0,
      min:0,
      max:0,
      add_com_persen:0,
      add_com_nominal:0,
    };

    if($scope.listDetail[m_level.id] == undefined){
      $scope.listDetail[m_level.id] = [];
    }

    $scope.listDetail[m_level.id].splice((index+1), 0, newDet);
  };
  $scope.removeDetail = function(val, paramindex) {
    if(confirm("Apakah Anda yakin akan menghapus item ini?"))
      val.splice(paramindex, 1);
  };

  $scope.getKode = function() {
    Data.get(control_link + "/kode").then(function(response) {
      $scope.form.kode = response.data;
    });
  };

  $scope.setProduk = function(data, index) {
    var is_ok = true;
    angular.forEach($scope.listDetail, function(val, key) {
      if(key != index && data.id == val.m_produk_id.id){
        toaster.pop("error", "Peringatan!", "Produk sudah ada dalam list!");
        $scope.listDetail.splice($scope.listDetail.indexOf(data), 1);
        is_ok = false;
        return;
      }
    });

    if(is_ok){
      $scope.listDetail[index].harga_asli = data.harga_jual;
      $scope.listDetail[index].diskon = 0;
      $scope.listDetail[index].diskon_nominal = 0;
      $scope.listDetail[index].harga_diskon = data.harga_jual;
      $scope.hitungDiskon();
    }
  }

  $scope.save = function() {
    $scope.loading = true;
    var params = $scope.listDetail;

    Data.post(control_link + "/save", params).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
      } else {
        toaster.pop( "error", "Terjadi Kesalahan", setErrorMessage(result.errors) );
      }
      $scope.loading = false;
    });
  };

});
