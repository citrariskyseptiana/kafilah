app.controller("mprodukCtrl", function($scope, Data, toaster, FileUploader) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    var control_link = "m_produk";
    $scope.displayed = [];

    $scope.formtittle = "";
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function(response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
        $scope.isLoading = false;
    };
    $scope.getKode = function() {
        Data.get(control_link + "/kode").then(function(result) {
            $scope.form.kode = result.data;
        });
    };

    $scope.getProduk = function(cari) {
        if (cari.toString().length > 2) {
            var param = {
                nama: cari
            };
            Data.get(control_link + "/getProduk", param).then(function(result) {
                $scope.listProdukGratis = result.data;
            });
        }
    }

    $scope.create = function(form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.satuan = "PCS";
        $scope.getKode();
    };
    $scope.update = function(form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.kode;
        $scope.form = form;
        $scope.getFoto(form.id);
        $scope.cariKecamatan($scope.form.kota_asal);
    };
    $scope.view = function(form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.kode;
        $scope.form = form;
        $scope.getFoto(form.id);
        $scope.cariKecamatan($scope.form.kota_asal);
    };
    $scope.save = function(is_final) {
        var params = {
          form: $scope.form,
        };

        $scope.loading = true;
        Data.post(control_link + "/save", params).then(function(result) {
          if (result.status_code == 200) {
              $scope.form = result.data.form;
              toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
              $scope.cancel();
              $scope.is_create = false;
              $scope.update(result.data.form);
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function() {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function(row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post(control_link + "/saveStatus", row).then(function(result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.restore = function(row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post(control_link + "/saveStatus", row).then(function(result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

  	$scope.cariKecamatan = function(kota) {
      var param = {
        kota: kota.id
      };
      Data.get("site/getKecamatan", param).then(function(result) {
        $scope.listKecamatan = result.data;
      });
    };

    Data.get("m_kategori_produk/kategori").then(function(response) {
        $scope.listKategori = response.data;
    });
    /* Upload Foto */
    $scope.getFoto = function() {
        var id = id;
        Data.get(control_link + "/getFoto", id).then(function(response) {
            $scope.listFoto = response.data;
        });
    };
    $scope.remFoto = function(val) {
        Data.post(control_link + "/hapusFoto", val).then(function(response) {
            if (response.status_code == 200) {
                toaster.pop("success", "Berhasil", "Foto berhasil dihapus");
                $scope.listFoto.splice($scope.listFoto.indexOf(val), 1);
            }
        });
    };
    $scope.setPrimary = function(val) {
        Data.post(control_link + "/setPrimary", val).then(function(response) {
            if (response.status_code == 200) {
                toaster.pop("success", "Berhasil", "Foto berhasil di set sebagai primary!");
                $scope.getFoto($scope.form.id);
            }
        });
    };
    var uploader = ($scope.uploader = new FileUploader({
        url: Data.base + control_link + "/upload",
        formData: [],
        removeAfterUpload: true
    }));
    $scope.uploadFile = function(form) {
        if ($scope.form.id == undefined) {
            $scope.save(0);
        } else {
            $scope.uploader.uploadAll();
        }
    };
    uploader.filters.push({
        name: "fileFilter",
        fn: function(item) {
            var type = "|" + item.type.slice(item.type.lastIndexOf("/") + 1) + "|";
            var x = "|jpg|JPG|png|PNG|jpeg|".indexOf(type) !== -1;
            if (!x) {
                toaster.pop("error", "Jenis File tidak sesuai");
            }
            return x;
        }
    });
    uploader.filters.push({
        name: "sizeFilter",
        fn: function(item) {
            var xz = item.size < 2097152;
            if (!xz) {
                toaster.pop("error", "Ukuran gambar tidak boleh lebih dari 2 MB");
            }
            return xz;
        }
    });
    uploader.onSuccessItem = function(fileItem, response) {
        if (response.status_code == 200) {
            $scope.getFoto(response.data.m_produk_id);
        }
    };
    uploader.onBeforeUploadItem = function(item) {
        item.formData.push({
            id: $scope.form.id
        });
    };

    // Fungsi untuk membuat alias
    $scope.slugify = function(input){
      if (!input)
          return;

      // make lower case and trim
      var slug = input.toLowerCase().trim();
      // replace invalid chars with spaces
      slug = slug.replace(/[^a-z0-9\s-]/g, ' ');
      // replace multiple spaces or hyphens with a single hyphen
      slug = slug.replace(/[\s-]+/g, '-');
      $scope.form.url = slug;
    };

    $scope.setBestSeller = function(row) {
      var pesan = row.is_best_seller == 0 ? "Produk Best Seller?" : "Produk Biasa?";
      if( !confirm(`Apakah Anda ingin menandai produk "`+ row.nama + `" sebagai ` + pesan ) ){
        return;
      }

      Data.post(control_link + "/setBestSeller", row).then(function(response) {
          if (response.status_code == 200) {
            if(response.data.is_best_seller == 1){
              toaster.pop("success", "Berhasil", "Produk telah ditandai sebagai best seller!");
            } else {
              toaster.pop("success", "Berhasil", "Produk telah ditandai sebagai produk biasa!");
            }
            $scope.cancel();
          } else {
            toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
          }
      });
    }
    Data.get(control_link + "/getKota").then(function(result) {
        $scope.listKotaAsal = result.data;
    });

    Data.get(control_link + "/getKotaSC").then(function(result) {
        $scope.listKotaSC = result.data;
    });
});
