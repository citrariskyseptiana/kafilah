app.controller("forumCtrl", function ($scope, Data, toaster, $stateParams, $state) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    var tableStateRef2;
    var control_link = "m_forum";
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.listReply = [];
    $scope.replyParams = {};
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view_diskusi = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    $scope.base_url = window.location.href;

    Data.get(control_link + "/getUrl").then(function (response) {
        $scope.site_url = response.data;
    })

    if ($stateParams.judul != undefined) {
        var form = {
            url: $stateParams.judul
        }

        Data.get(control_link + "/index-first", form).then(function (response) {
            $scope.datafirst = response.data.list;
            console.log($scope.datafirst[response.data.kategori])
            $scope.is_view_diskusi = true;
            $scope.formtittle = $scope.datafirst[response.data.kategori][0].judul;
            $scope.form = $scope.datafirst[response.data.kategori][0];
        })
    } else {
        Data.get(control_link + "/index-first").then(function (response) {
            // $scope.form.kode = response.data;
            if (response.kategori == undefined) {
                $scope.datafirst = response.data;
            }
        });
    }

    //delete notification
    Data.post('/site/delete_notification', {trans_tipe: 'm_forum'}).then(function (response) {
        console.log(response)
    });
    //delete notification

    /**
     * End inialisasi
     */
   $scope.sorting = {};
   $scope.refreshPage = function(){
     $scope.callServer(tableStateRef);
   };
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 20;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }

        if (param["filter"] == undefined)
            param["filter"] = {};

        if (param["sort"] == undefined)
            param["sort"] = {};

        param["filter"]['kategori'] = $scope.pilihanKategori;
        param["sort"]['tanggal']    = $scope.sorting.tanggal;
        param["sort"]['komentar']   = $scope.sorting.komentar;

        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.getKode = function () {
        Data.get(control_link + "/kode").then(function (response) {
            $scope.form.kode = response.data;
        });
    };

    $scope.viewList = true;
    $scope.viewKategori = false;
    $scope.pilihKategori = function (kategori) {
        $scope.viewKategori = false;
        $scope.viewList = true;
        $scope.pilihanKategori = kategori;
        $scope.callServer(tableStateRef);
    }

    $scope.listKategori = function () {
        $scope.viewKategori = true;
        $scope.viewList = false;
        $scope.pilihanKategori = undefined;
    }

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Post Baru";
        $scope.form = {};
        $scope.form.tipe = "post";
        $scope.listDetail = []
        $scope.getKode();
    };

    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.kode;
        $scope.form = form;
        $scope.form.share_link = $scope.site_url + 'index.html#!/forum/' + $scope.form.url;
    };

    $scope.view_diskusi = function (form) {
        $scope.is_view_diskusi = true;
        $scope.formtittle = form.judul;
        $scope.form = form;
    };

    $scope.indexFirst = function (form) {
        Data.get(control_link + "/index-first").then(function (response) {
            // $scope.form.kode = response.data;
            if (response.kategori == undefined) {
                $scope.datafirst = response.data;
            }
        });
    }

    $scope.save = function (form) {
        $scope.loading = true;
        var form = {
            data: form,
            detail: $scope.listDetail
        }
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                $scope.cancel();
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
            $scope.loading = false;
        });
    };

    $scope.copy = function (url) {
        console.log("halo");
        if (url != undefined) {
            toaster.pop("success", "Berhasil", "Berhasil mengcopy link")
        }
    }

    $scope.cancel = function () {
        if ($stateParams.judul != undefined) {
            $state.go('master.forum');
        } else {
            $scope.is_edit = false;
            $scope.is_view_diskusi = false;
            $scope.is_view = false;
            $scope.is_create = false;
            $scope.callServer(tableStateRef);
            $scope.indexFirst();
        }
    };

    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post(control_link + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                $scope.indexFirst()
            });
        }
    };

    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post(control_link + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                $scope.indexFirst()
            });
        }
    };

    $scope.isiUrl = function (form) {
        Data.get(control_link + "/isiurl").then(function (result) {
            if (result.status_code == 200) {
                console.log(result.data);
            }
        })
    }

    $scope.slugify = function(input){
        function truncate(str, no_words) {
            return str.split(" ").splice(0,no_words).join(" ");
        }

        if (!input)
        return;

        var judul = truncate(input, 5);

        // make lower case and trim
        var slug = judul.toLowerCase().trim();
        // replace invalid chars with spaces
        slug = slug.replace(/[^a-z0-9\s-]/g, ' ');
        // replace multiple spaces or hyphens with a single hyphen
        slug = slug.replace(/[\s-]+/g, '-');
        $scope.form.url = slug;
        $scope.form.share_link = $scope.site_url + 'index.html#!/forum/' + slug;
      };

    $scope.setReplyParams = function (form, m_forum_id) {
        $scope.replyParams = {};
        $scope.replyParams = form;
        // $scope.replyParams.reply      = `<blockquote>`+form.isi+`</blockquote><br>`;
        $scope.replyParams.m_forum_id = m_forum_id;
        $scope.is_reply = true;
    };

    $scope.reply = function () {

        Data.post(control_link + "/saveReply", $scope.replyParams).then(function (result) {
            if (result.status_code == 200) {
                $scope.callServer2(tableStateRef2);
                $scope.is_reply = false;

                toaster.pop("success", "Berhasil", "Balasan berhasil disimpan.");
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
        });
    };

    // Untuk daftar reply
    $scope.callServer2 = function callServer2(tableState) {
        tableStateRef2 = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit,
            m_forum_id: $scope.form.id,
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/listReply", param).then(function (response) {
            $scope.listReply = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
    };
});
