<!DOCTYPE html>
<html lang="en" ng-app="app">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta http-equiv="Cache-control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <link rel="shortcut icon" href="img/favicon.png"/>
        <title>Kafila Web</title>
        <!-- Main styles for this application -->
        <link href="css/style.min.css" rel="stylesheet"/>
        <link href="css/custom.css" rel="stylesheet"/>
        <link href="library/ui-bootstrap4/dist/ui-bootstrap-csp.css" rel="stylesheet"/>
        <link href="library/angularjs-toaster/toaster.min.css" rel="stylesheet"/>
        <link href="library/angular-ui-select/dist/select.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="library/angular-tree-widget/dist/angular-tree-widget.min.css">
        <link href="library/textAngular-master/textAngular.css" rel="stylesheet"/>
    </head>
    <body class="header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden" ng-controller="AppCtrl">
        <toaster-container toaster-options="{'body-output-type': 'trustedHtml'}"></toaster-container>
        <!-- User Interface -->
        <ui-view class="app"></ui-view>
        <!-- Bootstrap and necessary plugins -->
        <script src="library/jquery/jquery.min.js"></script>
        <script src="library/popper.js/popper.min.js"></script>
        <script src="library/bootstrap/bootstrap.min.js"></script>
        <!-- AngularJS -->
        <script src="library/angular/angular.min.js"></script>
        <!-- AngularJS plugins -->
        <script src="library/angular-animate/angular-animate.min.js"></script>
        <script src="library/angular-sanitize/angular-sanitize.min.js"></script>
        <script src="library/@uirouter/angularjs/release/angular-ui-router.min.js"></script>
        <script src="library/oclazyload/dist/ocLazyLoad.min.js"></script>
        <script src="library/angular-breadcrumb/dist/angular-breadcrumb.min.js"></script>
        <script src="library/angular-loading-bar/build/loading-bar.min.js"></script>
        <script src="library/angular-ui-select/dist/select.min.js"></script>
        <script src="library/angular-local-storage/dist/angular-local-storage.min.js"></script>
        <script src="library/angularjs-toaster/toaster.min.js"></script>
        <script src="library/ui-bootstrap4/dist/ui-bootstrap-tpls.js"></script>
        <script src="library/smart-table.min.js"></script>
        <script src="library/FileSaver.js"></script>
        <script src="library/angular-password-strength/ng-password-strength.min.js"></script>
        <script src="library/angular-button-spinner/angular-button-spinner.min.js"></script>
        <script src="library/moment/moment.js"></script>
        <script src="library/angular-locale/angular-locale_id-id.js"></script>
        <script src="library/angular-input-masks/angular-input-masks-dependencies.js"></script>
        <script src="library/angular-input-masks/angular-input-masks.js"></script>
        <script src="library/sweetalert/sweetalert.min.js"></script>
        <script src="library/ckeditor/ckeditor.js"></script>
        <script src="library/clipboard/dist/clipboard.min.js"></script>
        <script src="library/ngclipboard/dist/ngclipboard.min.js"></script>
        <!-- Angular text editor -->
        <script src="library/textAngular-master/textAngular-sanitize.js"></script>
        <script src="library/textAngular-master/textAngular-rangy.min.js"></script>
        <script src="library/textAngular-master/textAngular.js"></script>
        <script src="library/textAngular-master/textAngularSetup.js"></script>
        <!-- Angular tree  -->
        <script type="text/javascript" src="library/angular-tree-widget/dist/angular-tree-widget.min.js"></script>
        <script type="text/javascript" src="library/angular-tree-widget/dist/angular-recursion.js"></script>
        <!-- Angular chart - zingchart -->
        <script type="text/javascript" src="library/zingchart-angularjs.js"></script>
        <script type="text/javascript" src="library/zingchart.min.js"></script>

        <!-- AngularJS CoreUI App scripts -->
        <script src="js/app.js?t=21903809"></script>
        <script src="js/httpInterceptor.js"></script>
        <script src="js/lazyload.js"></script>
        <script src="js/routes.js?t=234892347"></script>
        <script src="js/directives.js?t=32479093"></script>
        <script src="js/config.js"></script>
        <script src="js/controllers.js"></script>
        <script type="text/javascript">
            function setErrorMessage(n){var e="";return angular.forEach(n,function(n,o){e=e+" - "+n+"<br>"}),e}$(document).ready(function(){$("body").on("keypress",".angka",function(n){var e=n.which?n.which:event.keyCode;return!(e>31&&(48>e||e>57)&&45!=e&&46!=e)}),$("body").on("focus",".angka",function(){0==$(this).val()&&$(this).val("")}),$("body").on("blur",".angka",function(){""==$(this).val()&&$(this).val(0)}),$("input").keypress(function(n){13==n.keyCode&&n.preventDefault()})});
        </script>
    </body>
</html>
