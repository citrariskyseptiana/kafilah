<?php
$app->add(function ($request, $response, $next) {
    /**
     * Get route name
     */
    $route = $request->getAttribute('route');
    $uri = $request->getUri()->getPath();

    $routeName = '';
    if ($route !== null) {
        $routeName = $route->getName();
    }
    /**
     * Set Global route
     */
    $publicRoutesArray = array(
        'login',
        'session',
        'logout',
        'resetPassword',
        'publicFrontend',
    );
    /**
     * Check session
     */
    if ((!isset($_SESSION['user']['id']) || !isset($_SESSION['user']['m_roles_id']) || !isset($_SESSION['user']['akses'])) && !in_array($routeName, $publicRoutesArray)) {

      if( strpos($uri, "public") !== false ){
        $view = $this->view->fetch('public/404.html', [
            "base_url"      => config('SITE_URL') . "views/public",
            "site_url"      => config('SITE_URL') . "public",
            "listKategori"  => get_kategori_produk(),
        ]);
        echo $view;
        die;
      } else {
        return unauthorizedResponse($response, ['Mohon maaf, anda tidak mempunyai akses']);
      }
    } else if((isset($_SESSION['user']['id']) || isset($_SESSION['user']['m_roles_id']) || !isset($_SESSION['user']['akses'])) && !in_array($routeName, $publicRoutesArray)){
      if( strpos($uri, "public") !== false ){
        $view = $this->view->fetch('public/404.html', [
            "base_url"      => config('SITE_URL') . "views/public",
            "site_url"      => config('SITE_URL') . "public",
            "listKategori"  => get_kategori_produk(),
        ]);
        echo $view;
        die;
      }
    }
    /**
     * Return if isset session
     */
    return $next($request, $response);
});
