<?php

// Fungsi untuk print array
function pd($data = []) {
    echo "<pre>";
    print_r($data);
    die;
}

function insertNotif($trans_tipe, $keterangan, $member_id) {
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $member_id = is_array($member_id) ? $member_id : [$member_id];

    $member_id = array_unique($member_id);

    $query = "INSERT INTO m_notif(trans_tipe, keterangan, m_member_id) VALUES";
    foreach ($member_id as $key => $value) {
        $query .= "('$trans_tipe', '$keterangan', $value),";
    }

    $query = substr_replace($query, ";", -1);
//    pd($query);
    $models = $db->run($query);
    return $models;
}

function getNotif() {
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $user = $_SESSION['user']['id'];

    $models = $db->select("trans_tipe, COUNT(id) as notification")->from("m_notif")->where("m_member_id", "=", $user)->groupBy("trans_tipe")->findAll();

//    pd($models);

    $arr = [];
    foreach ($models as $key => $value) {
        $arr[$value->trans_tipe] = $value->notification;
    }

    return $arr;
}

function deleteNotif($trans_tipe, $member_id) {
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $db->delete("m_notif", ["trans_tipe" => $trans_tipe, "m_member_id" => $member_id]);
}

function createNode($member_id, $reff_id) {
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    // Ambil parent id
    $parent = $db->find("select * from m_jaringan where m_member_id = '" . $reff_id . "'");
    $parentId = isset($parent->id) ? $parent->id : 0;
    // Tentukan tree_id
    if ($parentId > 0) {
        $tree_id = $parent->tree_id;
    } else {
        $tree = $db->find("select * from m_jaringan order by tree_id DESC");
        $lastTree = isset($tree->tree_id) ? (int) $tree->tree_id : 0;
        $tree_id = $lastTree + 1;
    }
    // simpan jaringan
    $pdo = new PDO('mysql:host=' . $config['db']['DB_HOST'] . ';dbname=' . $config['db']['DB_NAME'], $config['db']['DB_USER'], $config['db']['DB_PASS']);
    $jaringan = new NS\NestedSet($pdo, "m_jaringan", "id", "parent_id", "tree_id", "lft", "rgt");
    $child_id = $jaringan->createChild($parentId, ["m_member_id" => $member_id], $tree_id);
}

function hapusNode($member_id) {
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    $pdo = new PDO('mysql:host=' . $config['db']['DB_HOST'] . ';dbname=' . $config['db']['DB_NAME'], $config['db']['DB_USER'], $config['db']['DB_PASS']);
    $jaringan = new NS\NestedSet($pdo, "m_jaringan", "id", "parent_id", "tree_id", "lft", "rgt");

    $node = $db->find("select * from m_jaringan where m_member_id = '" . $member_id . "'");
    if (isset($node->id)) {
        $jaringan->deleteNode($node->id);
    }
}

function urlParsing($string) {
    $arrDash = array("--", "---", "----", "-----");
    $string = strtolower(trim($string));
    // $string  = strtr($string, normalizeChars());
    $string = preg_replace('/[^a-zA-Z0-9 -.]/', '', $string);
    $string = str_replace(" ", "-", $string);
    $string = str_replace("&", "", $string);
    $string = str_replace(array("'", "\"", "&quot;"), "", $string);
    $string = str_replace($arrDash, "-", $string);
    return str_replace($arrDash, "-", $string);
}

function dataSortID($table = "") {
    if ($table != "") {
        $db = config('DB');
        $database = new Cahkampung\Landadb($db['db']);

        $model = $database->select("*")
                        ->from($table)->findAll();

        $result = [];
        if (!empty($model)) {
            foreach ($model as $key => $value) {
                $result[$value->id] = (array) $value;
            }
        }

        return $result;
    }
}

function dataSortID2($table = []) {
    $result = [];
    if (!empty($table)) {
        foreach ($table as $key => $value) {
            $value = (array) $value;
            $result[$value['id']] = (array) $value;
        }

        return $result;
    }
}

function sendMail($subjek, $nama, $email, $isi) {
    $mail = new PHPMailer\PHPMailer\PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'noreplyinfosystems@gmail.com';
    $mail->Password = 'bismillah2018';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('Admin Kafilah');
    $mail->addAddress($email, $nama);
    $mail->isHTML(true);

    $mail->Subject = $subjek;
    $mail->Body = $isi;

    if (!$mail->send()) {
        return [
            'status' => false,
            'error' => $mail->ErrorInfo,
        ];
    } else {
        return [
            'status' => true,
        ];
    }

    return [
        'status' => false,
        'error' => 'Email SMP Belum di setting',
    ];
}

function randomPassword($length, $characters) {
    $symbols = array();
    $passwords = array();
    $used_symbols = '';
    $pass = '';

    // an array of different character types
    $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
    $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $symbols["numbers"] = '1234567890';
    $symbols["special_symbols"] = '!?~@#-_+<>[]{}';

    $characters = explode(",", $characters);
    foreach ($characters as $key => $value) {
        $used_symbols .= $symbols[$value];
    }
    $symbols_length = strlen($used_symbols) - 1;

    $pass = '';
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $symbols_length);
        $pass .= $used_symbols[$n];
    }

    return $pass;
}

function safePassword($string) {
    $iv = substr(hash('sha256', "landasystems"), 0, 16);
    $encryptedText = openssl_encrypt($string, "AES-256-CBC", "encryptionhash", 0, $iv);
    return $encryptedText;
}

function cekKeranjang($form = []) {
    $jumlah_pesan = 0;
    if (!empty($form)) {
        $jenis = $form['jenis'];
        $produk_id = $form['id'];
        if (!empty($_SESSION['user']['keranjang'][$jenis][$produk_id])) {
            $jumlah_pesan = $_SESSION['user']['keranjang'][$jenis][$produk_id];
        }
    }

    return $jumlah_pesan;
}

;

function jumlahPesanan() {
    $po = $non_po = 0;
    if (!empty($_SESSION['user']['keranjang']["po"]) && is_array($_SESSION['user']['keranjang']["po"])) {
        $po = sizeof($_SESSION['user']['keranjang']["po"]);
    }
    if (!empty($_SESSION['user']['keranjang']["non-po"]) && is_array($_SESSION['user']['keranjang']["non-po"])) {

        $non_po = sizeof($_SESSION['user']['keranjang']["non-po"]);
    }

    return ["po" => $po, "non_po" => $non_po];
}

;

function nBetween($varToCheck, $high, $low) {
    if ($varToCheck < $low) {
        return false;
    }

    if ($varToCheck > $high) {
        return false;
    }

    return true;
}

function isBigOrder($listBigOrder, $produk) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    $result = [];
    if (!empty($listBigOrder) && !empty($produk)) {
        foreach ($listBigOrder as $key => $value) {
            $nominal = 0;
            $nominal = $produk->jumlah_pesan * $produk->harga_jual;

            $inBigOrder = nBetween($nominal, $value["max_beli"], $value["min_beli"]);

            if ($value["m_produk_id"] == $produk->id && $inBigOrder) {

                if ($value["jenis_diskon"] == 'gratis barang') {
                    $m_produk_gratis = $db->find("SELECT * FROM m_produk WHERE id={$value['m_produk_gratis_id']} AND is_deleted=0");

                    if (!empty($m_produk_gratis)) {
                        $m_produk_gratis->harga_jual = 0;
                        $m_produk_gratis->jumlah_pesan = $value['jumlah'];
                    }
                    $result = [
                        'jenis_diskon' => $value['jenis_diskon'],
                        'm_produk_gratis' => $m_produk_gratis,
                    ];
                } else if ($value['jenis_diskon'] == 'diskon') {
                    $result = [
                        'jenis_diskon' => $value['jenis_diskon'],
                        'diskon' => $value['diskon'],
                    ];
                }
            }
        }
    }
    return $result;
}

function costRajaongkir($form_params) {
    if (empty($form_params)) {
        return [];
    }

    $client = new \GuzzleHttp\Client(['headers' => [
            'key' => 'b3830bd7b7f1aadeaa07f89ac4892bfc',
            "content-type" => "multipart/form-data",
    ]]);

    $cost = $client->request('POST', 'https://api.rajaongkir.com/starter/cost', [
        "form_params" => $form_params,
    ]);

    $model = $cost->getBody()->getContents();

    return json_decode($model, true);
}

function costRajaongkirPRO($form_params) {
    if (empty($form_params)) {
        return [];
    }

    $client = new \GuzzleHttp\Client(['headers'                                                                                             => [
            'key' => '917c5e2481d806185b444d32366bf782',
            "content-type" => "multipart/form-data",
    ]]);

    $cost = $client->request('POST', 'https://pro.rajaongkir.com/api/cost', [
        "form_params" => $form_params,
    ]);

    $model = $cost->getBody()->getContents();

    return json_decode($model, true);
}

function costDakota($params) {
    if (empty($params)) {
        return [];
    }

    $client = new \GuzzleHttp\Client();
    $arr = "ak=" . $params['asal_kota'] . "&tpr=" . $params['tujuan_provinsi'] . "&tko=" . $params['tujuan_kota'] . "&tke=" . $params['tujuan_kecamatan'];

    $cost = $client->request('GET', 'https://www.dakotacargo.co.id/api/dbs/price/?' . $arr);
    $model = $cost->getBody()->getContents();
    return json_decode($model, true);
}

function getAllBank() {
    $result = [];

    $banks = json_decode(config("BANKS"), true);

    if (empty($banks))
        return $result;

    foreach ($banks as $key => $value) {
        $result[$value['code']] = $value;
    }

    return $result;
}

/*
 * Mendapatkan Setting Komisi / Bonus dan mengurutkannya
 * berdasarkan Level Jabatan dan Tipe Komisinya
 */

function getCommissionSetting() {
    $result = [];
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    $model = $db->select("*")->from("m_bonus")
                    ->orderBy("m_level_id, level_member")->findAll();

    if (empty($model))
        return $result;

    foreach ($model as $key => $value) {
        $result[$value->m_level_id][$value->tipe][] = (array) $value;
    }

    return $result;
}

/*
 * Mengumpulkan value dari hasil array of array / object
 * dan mengembalikan hasilnya sebagai string concat
 */

function valueConcate($array = [], $field = "") {
    $result = "";
    $temp = [];

    if (empty($array) || $field == "")
        return $result;

    foreach ($array as $key => $value) {
        $val = (array) $value;
        if (trim($val[$field]) != "") {
            $temp[] = $val[$field];
        }
    }

    $result = implode(",", $temp);

    return $result;
}

function getQuaQPA() {
    $result = [];
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    $allQPA = $db->select("
      m_member.id,
      m_member.reff_id,
      m_level.nama as level,
      m_histori_level.status_qpa,
      m_histori_level.m_level_id
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->andWhere("m_member.status", "=", "Aktif")
            ->andWhere("m_member.tipe_member", "=", "Member")
            ->andWhere("m_level.id", "=", "1")
            ->andWhere("m_histori_level.status_qpa", "=", "Qualified")
            ->findAll();

    $temp = [];
    foreach ($allQPA as $key => $value) {

    }
}

function personalCommission($data = []) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    date_default_timezone_set("Asia/Jakarta");

    // Perhitungan SUM Penjualan Non PO
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $db->select("
    //     m_member.id,
    //     m_member.kode,
    //     m_member.nama,
    //     m_member.no_hp,
    //     m_member.reff_id,
    //     m_level.nama as level,
    //     m_histori_level.status_qpa,
    //     m_histori_level.m_level_id,
    //     SUM(t_penjualan.poin_penjualan) as poin_penjualan,
    //     SUM(t_penjualan.total) total,
    //     MONTH(t_penjualan.tanggal_lunas) as bulan
    //   ");
    // } else {
    //     $db->select("
    //     m_member.id,
    //     m_member.kode,
    //     m_member.nama,
    //     m_member.no_hp,
    //     m_member.reff_id,
    //     m_level.nama as level,
    //     m_histori_level.status_qpa,
    //     m_histori_level.m_level_id,
    //     SUM(t_penjualan.poin_penjualan) as poin_penjualan,
    //     SUM(t_penjualan.total) total
    //   ");
    // }
    //
    // $db->from("m_member")
    //     ->join("LEFT JOIN", "t_penjualan",
    //         "m_member.id = t_penjualan.m_member_id AND
    //         t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
    //         t_penjualan.is_deleted =0")
    //     ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
    //     ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
    //     ->andWhere("m_member.status", "=", "Aktif")
    //     ->andWhere("t_penjualan.jenis_pesanan", "=", "non-po")
    //     ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
    //     ->andWhere("m_member.tipe_member", "=", "Member");
    //
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $start = date("Y-01-01", strtotime($data["tahun"]));
    //     $end = date("Y-12-31", strtotime($data["tahun"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    //
    // } else if (isset($data["periode"]) && !empty($data["periode"])) {
    //     $start = date("Y-m-01", strtotime($data["periode"]));
    //     $end = date("Y-m-t", strtotime($data["periode"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    // }
    //
    //
    // if ($_SESSION['user']['tipe_member'] == "Member") {
    //     $db->andWhere("t_penjualan.m_member_id", "=", $_SESSION['user']['id']);
    // }
    //
    // if (isset($data["member"]) && !empty($data["member"])) {
    //     $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
    // }
    //
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $db->groupBy("t_penjualan.m_member_id, MONTH(t_penjualan.tanggal_lunas)");
    // } else {
    //     $db->groupBy("t_penjualan.m_member_id");
    // }
    // $laporan = $db->findAll();
    // End of - Perhitungan SUM Penjualan Non PO
    $laporan = [];

    // Perhitungan SUM Penjualan PO
    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $db->select("
        m_member.id,
        m_member.kode,
        m_member.nama,
        m_member.no_hp,
        m_member.reff_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        m_histori_level.m_level_id,
        SUM(t_penjualan.poin_penjualan) as poin_penjualan,
        SUM(t_penjualan.total) total,
        MONTH(t_penjualan.tanggal_lunas) as bulan
      ");
    } else {
        $db->select("
        m_member.id,
        m_member.kode,
        m_member.nama,
        m_member.no_hp,
        m_member.reff_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        m_histori_level.m_level_id,
        SUM(t_penjualan.poin_penjualan) as poin_penjualan,
        SUM(t_penjualan.total) total
      ");
    }

    $db->from("m_member")
            ->join("LEFT JOIN", "t_penjualan", "m_member.id = t_penjualan.m_member_id AND
            t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
            t_penjualan.is_deleted =0")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->andWhere("m_member.status", "=", "Aktif")
            // ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
            ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $start = date("Y-01-01", strtotime($data["tahun"]));
        $end = date("Y-12-31", strtotime($data["tahun"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    } else if (isset($data["periode"]) && !empty($data["periode"])) {
        $start = date("Y-m-01", strtotime($data["periode"]));
        $end = date("Y-m-t", strtotime($data["periode"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    }


    if ($_SESSION['user']['tipe_member'] == "Member") {
        $db->andWhere("t_penjualan.m_member_id", "=", $_SESSION['user']['id']);
    }

    if (isset($data["member"]) && !empty($data["member"])) {
        $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
    }

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $db->groupBy("t_penjualan.m_member_id, MONTH(t_penjualan.tanggal_kirim)");
    } else {
        $db->groupBy("t_penjualan.m_member_id");
    }
    $laporan2 = $db->findAll();
    // End of - Perhitungan SUM Penjualan PO

    $laporanMerge = [];
    foreach ($laporan as $key => $value) {
        if (empty($laporanMerge[$value->id])) {
            $laporanMerge[$value->id] = $value;
        } else {
            $laporanMerge[$value->id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->id]->total += $value->total;
        }
    }

    foreach ($laporan2 as $key => $value) {
        if (empty($laporanMerge[$value->id])) {
            $laporanMerge[$value->id] = $value;
        } else {
            $laporanMerge[$value->id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->id]->total += $value->total;
        }
    }

    $laporan_pc = array_values($laporanMerge);

    // Get list ID Penjualan Non PO
    // $db->select("
    // t_penjualan.*,
    // m_member.nama as m_member_nama,
    // m_member.id as m_member_id,
    // m_reseller.nama as m_reseller_nama,
    // t_penjualan.id as t_penjualan_id,
    // MONTH(t_penjualan.tanggal_lunas) as bulan,
    // m_histori_level.m_level_id,
    // m_histori_level.status_qpa
    // ");
    // $db->from("t_penjualan")
    //     ->join("LEFT JOIN", "m_member",
    //         "m_member.id = t_penjualan.m_member_id AND
    //         t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
    //         t_penjualan.is_deleted =0")
    //     ->join("LEFT JOIN", "m_member as m_reseller", "t_penjualan.reseller_id = m_reseller.id")
    //     ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
    //     ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
    //     ->andWhere("m_member.status", "=", "Aktif")
    //     ->andWhere("t_penjualan.jenis_pesanan", "=", "non-po")
    //     ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
    //     ->andWhere("m_member.tipe_member", "=", "Member");
    //
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $start = date("Y-01-01", strtotime($data["tahun"]));
    //     $end = date("Y-12-31", strtotime($data["tahun"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    //
    // } else if (isset($data["periode"]) && !empty($data["periode"])) {
    //     $start = date("Y-m-01", strtotime($data["periode"]));
    //     $end = date("Y-m-t", strtotime($data["periode"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    // }
    //
    // if (isset($data["member"]) && !empty($data["member"])) {
    //     $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
    // }
    //
    // $db->orderBy('t_penjualan.id ASC');
    // $get_t_penjualan = $db->findAll();
    // End of - Get list ID Penjualan Non PO
    $get_t_penjualan = [];

    // Get list ID Penjualan PO
    $db->select("
    t_penjualan.*,
    m_member.nama as m_member_nama,
    m_member.id as m_member_id,
    m_reseller.nama as m_reseller_nama,
    t_penjualan.id as t_penjualan_id,
    MONTH(t_penjualan.tanggal_lunas) as bulan,
    m_histori_level.m_level_id,
    m_histori_level.status_qpa
    ");
    $db->from("t_penjualan")
            ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id AND
            t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
            t_penjualan.is_deleted =0")
            ->join("LEFT JOIN", "m_member as m_reseller", "t_penjualan.reseller_id = m_reseller.id")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->andWhere("m_member.status", "=", "Aktif")
            // ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
            ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $start = date("Y-01-01", strtotime($data["tahun"]));
        $end = date("Y-12-31", strtotime($data["tahun"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    } else if (isset($data["periode"]) && !empty($data["periode"])) {
        $start = date("Y-m-01", strtotime($data["periode"]));
        $end = date("Y-m-t", strtotime($data["periode"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    }

    if (isset($data["member"]) && !empty($data["member"])) {
        $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
    }

    $db->orderBy("t_penjualan.id ASC");
    $get_t_penjualan2 = $db->findAll();
    // End of - Get list ID Penjualan PO

    $get_t_penjualan_pc = array_merge($get_t_penjualan, $get_t_penjualan2);

    // Get setting pajak
    $pajak = $db->select("pajak")->from("m_setting")->find();
    $pajak = !empty($pajak) ? $pajak->pajak : 0;

    $penjualan_by_id = [];
    foreach ($get_t_penjualan_pc as $key => $value) {
        $arr_data = (array) $value;
        $arr_data['pajak'] = $pajak;
        $penjualan_by_id[$value->m_member_id][$value->bulan][] = $arr_data;
    }

    $comSetting = getCommissionSetting();
    $totalKomisi = 0;

    foreach ($laporan_pc as $key => $value) {
        // Inisialisasi dengan 0
        $laporan_pc[$key]->komisi_persen = 0;

        // Ambil setting Personal Commission
        if (isset($comSetting[$value->m_level_id]['PC'])) {
            $thisCom = $comSetting[$value->m_level_id]['PC'];

            // Ambil Additional Commission
            foreach ($thisCom as $Comkey => $Comval) {

                if ($value->m_level_id == 1) { // JIKA QPA
                    // Ambil Basic Commission unt
                    if (($Comval['level_qpa'] == $value->status_qpa)) {
                        $laporan_pc[$key]->komisi_persen = isset($Comval['basic_com_persen']) ? $Comval['basic_com_persen'] : 0;
                    }
                    // Membandingkan poin apakah masuk rentang
                    $isBetween = nBetween($value->poin_penjualan, $Comval['max'], $Comval['min']);
                    if (($Comval['level_qpa'] == $value->status_qpa) && $isBetween) {
                        $laporan_pc[$key]->komisi_persen += isset($Comval['add_com_persen']) ? $Comval['add_com_persen'] : 0;
                    }

                    // Jika max poin is unlimited
                    if (($Comval['max'] == 0) && ($value->poin_penjualan > $Comval['min']) && ($Comval['level_qpa'] == $value->status_qpa)) {
                        $laporan_pc[$key]->komisi_persen += isset($Comval['add_com_persen']) ? $Comval['add_com_persen'] : 0;
                    }
                } else { // MQPA
                    $laporan_pc[$key]->komisi_persen = ($laporan[$key]->komisi_persen == 0) && isset($Comval['basic_com_persen']) ? $Comval['basic_com_persen'] : $laporan[$key]->komisi_persen;

                    // Membandingkan poin apakah masuk rentang
                    if (nBetween($value->poin_penjualan, $Comval['max'], $Comval['min'])) {
                        $laporan_pc[$key]->komisi_persen += isset($Comval['add_com_persen']) ? $Comval['add_com_persen'] : 0;
                    }

                    // Jika max poin is unlimited
                    if (($Comval['max'] == 0) && ($value->poin_penjualan >= $Comval['min'])) {
                        $laporan_pc[$key]->komisi_persen += isset($Comval['add_com_persen']) ? $Comval['add_com_persen'] : 0;
                    }
                }
            }
        }
        // Perhitungan Personal Commission
        $laporan_pc[$key]->komisi_nominal = 0;
        if ($laporan_pc[$key]->komisi_persen > 0) {
            $laporan_pc[$key]->komisi_nominal = ($laporan_pc[$key]->komisi_persen / 100) * $laporan_pc[$key]->total;

            if (!isset($data["tahun"]) && empty($data["tahun"])) {
                $bulan = date("m", strtotime($data['periode']));
                $laporan_pc[$key]->list_t_penjualan = isset($penjualan_by_id[$value->id][(int) $bulan]) ? $penjualan_by_id[$value->id][(int) $bulan] : [];
            }
        }

        $totalKomisi += $laporan_pc[$key]->komisi_nominal;
    }

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $lap_temp = sort_array_by_key($laporan_pc, "bulan");

        $result = [];
        $idx = 0;
        for ($i = 1; $i <= 12; $i++) {
            $month = $i < 10 ? '0' . $i : $i;
            if (isset($lap_temp[$i])) {
                $result[$idx] = $lap_temp[$i];
                $result[$idx]['list_t_penjualan'] = isset($penjualan_by_id[$result[$idx]['id']][$i]) ? $penjualan_by_id[$result[$idx]['id']][$i] : [];
            } else {
                $result[$idx]['komisi_nominal'] = 0;
                $result[$idx]['total'] = 0;
            }
            $result[$idx]['bulan'] = date("Y-$month-01", strtotime($data["tahun"]));

            $idx++;
        }

        $laporan_pc = $result;
    }

    return [
        'laporan' => $laporan_pc,
        'totalKomisi' => $totalKomisi,
        'pajak' => $pajak,
    ];
}

function sort_array_by_key($array = [], $field = '') {
    if (empty($array) || empty($field))
        return [];

    $result = [];
    foreach ($array as $key => $value) {
        $data = (array) $value;

        $result[$data[$field]] = $data;
    }

    return $result;
}

function monthlyRewardCommission($data = []) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    date_default_timezone_set("Asia/Jakarta");

    // Dapatkan jumlah bawahan beserta levelnya
    $db->select("
    m_member.reff_id,
    COUNT(m_level.id) as jumlah,
    m_level.id as m_level_id,
    m_level.nama as level,
    m_histori_level.status_qpa
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->andWhere("m_member.status", "=", "Aktif")
            ->andWhere("m_member.reff_id", ">", 0)
            ->andWhere("m_member.tipe_member", "=", "Member")
            ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->groupBy("m_member.reff_id, m_level.id, m_histori_level.status_qpa");

    if (isset($data["member"]) && !empty($data["member"])) {
        $db->andWhere("m_member.reff_id", "=", $data["member"]['id']);
    }

    $listMember = $db->findAll();

    if (empty($listMember)) {
        return [
            'laporan' => [],
            'totalKomisi' => 0,
        ];
    }

    // Dpatkan list reff id
    $listReffID = [];
    foreach ($listMember as $key => $value) {
        if (!isset($listReffID[$value->reff_id]))
            $listReffID[$value->reff_id] = $value->reff_id;
    }

    $listReffID = implode(",", $listReffID);

    $db->select("
    m_member.id,
    m_member.nama,
    m_member.kode,
    m_level.nama as level,
    m_histori_level.status_qpa,
    m_histori_level.m_level_id
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id"); // Pilih QPA only

    if ($_SESSION['user']["tipe_member"] == 'Member') {
        $db->andWhere("m_member.id", "=", $_SESSION['user']['id']);
    } else {
        $db->customWhere("m_member.id IN($listReffID)");
    }

    $db->customWhere('m_histori_level.tgl_selesai IS NULL', "AND");
    $getListReff = $db->findAll();

    $listReff = [];
    foreach ($getListReff as $key => $value) {
        $listReff[$value->id] = $value;
    }

    // Urutkan data anak buah berdasarkan reff id, level id, dan status_qpa
    $jumlahMember = [];
    foreach ($listMember as $key => $value) {
        $jumlahMember[$value->reff_id][$value->m_level_id][$value->status_qpa] = $value->jumlah;
    }

    $comSetting = getCommissionSetting();

    $rc[2] = $comSetting[2]['MR']; // Commission RC SQPA Only
    $rc[3] = $comSetting[3]['MR']; // Commission RC MQPA Only

    $totalKomisi = 0;

    foreach ($listReff as $key => $value) {
        $monthly_reward = 0;
        // Setting sesuai level nya
        $bonus = $rc[$value->m_level_id];
        // Jumlah member sesuai level dan status
        $members = $jumlahMember[$value->id];

        if (isset($bonus) && !empty($bonus)) {
            foreach ($bonus as $bkey => $bval) {
                $thisLevel = $bval['m_level_id_member'];
                $thisStatus = $bval['level_member'];

                if (isset($members[$thisLevel][$thisStatus])) {

                    $thisJumlah = $members[$thisLevel][$thisStatus];
                    $masuk = nBetween($thisJumlah, $bval['max'], $bval['min']);

                    if ($bval['max'] > 0 && $masuk) {
                        $monthly_reward = $bval['basic_com_nominal'];
                    } else if ($bval['max'] == 0 && $thisJumlah >= $bval['min']) {
                        $monthly_reward = $bval['basic_com_nominal'];
                    }
                }
            }
        }

        $listReff[$key]->monthly_reward = $monthly_reward;
        $listReff[$key]->members = $members;
        $totalKomisi += $monthly_reward;
    }

    // Get setting pajak
    $pajak = $db->select("pajak")->from("m_setting")->find();
    $pajak = !empty($pajak) ? $pajak->pajak : 0;

    return [
        'laporan' => $listReff,
        'totalKomisi' => $totalKomisi,
        'pajak' => $pajak,
    ];
}

function recruitmentCommission($data = []) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    date_default_timezone_set("Asia/Jakarta");

    // Dapatkan penjualan para QPA Non PO
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $db->select("
    //     m_member.id as m_member_id,
    //     m_member.kode,
    //     m_member.nama,
    //     m_member.no_hp,
    //     m_member.reff_id,
    //     m_level.nama as level,
    //     m_histori_level.status_qpa,
    //     m_histori_level.m_level_id,
    //     SUM(t_penjualan.poin_penjualan) as poin_penjualan,
    //     SUM(t_penjualan.total) total,
    //     MONTH(t_penjualan.tanggal_lunas) as bulan
    // ");
    // } else {
    //     $db->select("
    //     m_member.id as m_member_id,
    //     m_member.kode,
    //     m_member.nama,
    //     m_member.no_hp,
    //     m_member.reff_id,
    //     m_level.nama as level,
    //     m_histori_level.status_qpa,
    //     m_histori_level.m_level_id,
    //     SUM(t_penjualan.poin_penjualan) as poin_penjualan,
    //     SUM(t_penjualan.total) total,
    //     MONTH(t_penjualan.tanggal_lunas) as bulan
    // ");
    // }
    // $db->from("m_member")
    //     ->join("LEFT JOIN", "t_penjualan",
    //         "m_member.id = t_penjualan.m_member_id AND
    //         t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
    //         t_penjualan.is_deleted =0")
    //     ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
    //     ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id AND m_histori_level.m_level_id = 1")// Pilih QPA only
    //     ->andWhere("m_member.status", "=", "Aktif")
    //     ->andWhere("t_penjualan.jenis_pesanan", "=", "non-po")
    //     ->customWhere("m_member.reff_id IS NOT NULL", "AND")
    //     ->andWhere("m_member.reff_id", ">", "0")
    //     ->andWhere("m_member.tipe_member", "=", "Member");
    //
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $start = date("Y-01-01", strtotime($data["tahun"]));
    //     $end = date("Y-12-31", strtotime($data["tahun"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    // } else {
    //     if (isset($data["periode"]) && !empty($data["periode"])) {
    //         $start = date("Y-m-01", strtotime($data["periode"]));
    //         $end = date("Y-m-t", strtotime($data["periode"]));
    //
    //         $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //         $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    //     }
    // }
    //
    // if (isset($data["member"]) && !empty($data["member"])) {
    //     $db->andWhere("m_member.reff_id", "=", $data["member"]['id']);
    // }
    //
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $db->groupBy("t_penjualan.m_member_id, MONTH(t_penjualan.tanggal_lunas)");
    // } else {
    //     $db->groupBy("t_penjualan.m_member_id, MONTH(t_penjualan.tanggal_lunas)");
    // }
    // $laporan = $db->findAll();
    // Dapatkan penjualan para QPA Non PO - END
    $laporan = [];

    // Dapatkan penjualan para QPA PO
    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $db->select("
        m_member.id as m_member_id,
        m_member.kode,
        m_member.nama,
        m_member.no_hp,
        m_member.reff_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        m_histori_level.m_level_id,
        SUM(t_penjualan.poin_penjualan) as poin_penjualan,
        SUM(t_penjualan.total) total,
        MONTH(t_penjualan.tanggal_lunas) as bulan
    ");
    } else {
        $db->select("
        m_member.id as m_member_id,
        m_member.kode,
        m_member.nama,
        m_member.no_hp,
        m_member.reff_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        m_histori_level.m_level_id,
        SUM(t_penjualan.poin_penjualan) as poin_penjualan,
        SUM(t_penjualan.total) total,
        MONTH(t_penjualan.tanggal_lunas) as bulan
    ");
    }
    $db->from("m_member")
            ->join("LEFT JOIN", "t_penjualan", "m_member.id = t_penjualan.m_member_id AND
            t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
            t_penjualan.is_deleted =0")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id AND m_histori_level.m_level_id = 1")// Pilih QPA only
            ->andWhere("m_member.status", "=", "Aktif")
            // ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
            ->customWhere("m_member.reff_id IS NOT NULL", "AND")
            ->andWhere("m_member.reff_id", ">", "0")
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $start = date("Y-01-01", strtotime($data["tahun"]));
        $end = date("Y-12-31", strtotime($data["tahun"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    } else {
        if (isset($data["periode"]) && !empty($data["periode"])) {
            $start = date("Y-m-01", strtotime($data["periode"]));
            $end = date("Y-m-t", strtotime($data["periode"]));

            $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
            $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
        }
    }

    if (isset($data["member"]) && !empty($data["member"])) {
        $db->andWhere("m_member.reff_id", "=", $data["member"]['id']);
    }

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $db->groupBy("t_penjualan.m_member_id, MONTH(t_penjualan.tanggal_lunas)");
    } else {
        $db->groupBy("t_penjualan.m_member_id, MONTH(t_penjualan.tanggal_lunas)");
    }
    $laporan2 = $db->findAll();

    $laporanMerge = [];
    foreach ($laporan as $key => $value) {
        if (empty($laporanMerge[$value->m_member_id])) {
            $laporanMerge[$value->m_member_id] = $value;
        } else {
            $laporanMerge[$value->m_member_id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->m_member_id]->total += $value->total;
        }
    }

    foreach ($laporan2 as $key => $value) {
        if (empty($laporanMerge[$value->m_member_id])) {
            $laporanMerge[$value->m_member_id] = $value;
        } else {
            $laporanMerge[$value->m_member_id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->m_member_id]->total += $value->total;
        }
    }

    $laporan_rc = array_values($laporanMerge);
    // $laporan_rc = array_merge($laporan, $laporan2);
    // Get list Penjualan Para QPA Non PO
    // $db->select("
    // t_penjualan.*,
    // m_member.nama as m_member_nama,
    // m_member.id as m_member_id,
    // t_penjualan.id as t_penjualan_id,
    // MONTH(t_penjualan.tanggal_lunas) as bulan,
    // m_histori_level.m_level_id,
    // m_histori_level.status_qpa
    // ");
    // $db->from("t_penjualan")
    //     ->join("LEFT JOIN", "m_member",
    //         "m_member.id = t_penjualan.m_member_id AND
    //         t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
    //         t_penjualan.is_deleted =0")
    //     ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
    //     ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id AND m_histori_level.m_level_id = 1")// Pilih QPA only
    //     ->andWhere("m_member.status", "=", "Aktif")
    //     ->andWhere("t_penjualan.jenis_pesanan", "=", "non-po")
    //     ->customWhere("m_member.reff_id IS NOT NULL", "AND")
    //     ->andWhere("m_member.reff_id", ">", "0")
    //     ->andWhere("m_member.tipe_member", "=", "Member");
    //
    // if (isset($data["tahun"]) && !empty($data["tahun"])) {
    //     $start = date("Y-01-01", strtotime($data["tahun"]));
    //     $end = date("Y-12-31", strtotime($data["tahun"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    // } else {
    //     if (isset($data["periode"]) && !empty($data["periode"])) {
    //         $start = date("Y-m-01", strtotime($data["periode"]));
    //         $end = date("Y-m-t", strtotime($data["periode"]));
    //
    //         $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //         $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    //     }
    // }
    //
    // if (isset($data["member"]) && !empty($data["member"])) {
    //     $db->andWhere("m_member.reff_id", "=", $data["member"]['id']);
    // }
    //
    // $get_t_penjualan = $db->findAll();
    // End of - Get list Penjualan Para QPA Non PO
    $get_t_penjualan = [];

    // Get list Penjualan Para QPA PO
    $db->select("
    t_penjualan.*,
    m_member.nama as m_member_nama,
    m_member.id as m_member_id,
    t_penjualan.id as t_penjualan_id,
    MONTH(t_penjualan.tanggal_kirim) as bulan,
    m_histori_level.m_level_id,
    m_histori_level.status_qpa
    ");
    $db->from("t_penjualan")
            ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id AND
            t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
            t_penjualan.is_deleted =0")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id AND m_histori_level.m_level_id = 1")// Pilih QPA only
            ->andWhere("m_member.status", "=", "Aktif")
            ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
            ->customWhere("m_member.reff_id IS NOT NULL", "AND")
            ->andWhere("m_member.reff_id", ">", "0")
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $start = date("Y-01-01", strtotime($data["tahun"]));
        $end = date("Y-12-31", strtotime($data["tahun"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    } else {
        if (isset($data["periode"]) && !empty($data["periode"])) {
            $start = date("Y-m-01", strtotime($data["periode"]));
            $end = date("Y-m-t", strtotime($data["periode"]));

            $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
            $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
        }
    }

    if (isset($data["member"]) && !empty($data["member"])) {
        $db->andWhere("m_member.reff_id", "=", $data["member"]['id']);
    }

    $get_t_penjualan2 = $db->findAll();
    // End of - Get list Penjualan Para QPA PO

    $get_t_penjualan_rc = array_merge($get_t_penjualan, $get_t_penjualan2);

    // Get setting pajak
    $pajak = $db->select("pajak")->from("m_setting")->find();
    $pajak = !empty($pajak) ? $pajak->pajak : 0;

    $penjualan_by_id = [];
    foreach ($get_t_penjualan_rc as $key => $value) {
        $arr_data = (array) $value;
        $arr_data['pajak'] = $pajak;
        $penjualan_by_id[$value->m_member_id][] = $arr_data;
    }



    // Jika laporan penjulan kosong maka lakukan early return
    if (empty($laporan_rc)) {

        if (isset($data["tahun"]) && !empty($data["tahun"])) {
            $finalResult = [];
            $idx = 0;

            for ($i = 1; $i <= 12; $i++) {
                $month = $i < 10 ? '0' . $i : $i;
                if (isset($lap_temp[$i])) {
                    $finalResult[$idx] = $lap_temp[$i];
                } else {
                    $finalResult[$idx]['komisi_qpa_b'] = 0;
                    $finalResult[$idx]['komisi_qpa_q'] = 0;
                    $finalResult[$idx]['total'] = 0;
                }
                $finalResult[$idx]['bulan'] = date("Y-$month-01", strtotime($data["tahun"]));

                $idx++;
            }

            $result = $finalResult;
        }

        return [
            'laporan' => $result,
            'totalKomisi' => 0,
        ];
    }

    // Get RC Setting
    $comSetting = getCommissionSetting();
    $rc = $comSetting[1]['RC']; // Commission RC QPA Only
    $salesReff = [];
    $totalKomisi = 0;

    // Urutkan berdasarkan level member
    $setting_rc = [];
    foreach ($rc as $key => $value) {
        $setting_rc[$value["level_member"]] = $value;
    }

    // Mengelompokkan hasil penjualan berdasarkan reff_id & menghitung bonus RC nya
    foreach ($laporan_rc as $key => $value) {
        if (!isset($salesReff[$value->reff_id])) {
            $salesReff[$value->reff_id]['total'] = 0;
            $salesReff[$value->reff_id]['poin_penjualan'] = 0;
            $salesReff[$value->reff_id]['qpa_b'] = 0;
            $salesReff[$value->reff_id]['qpa_q'] = 0;
            $salesReff[$value->reff_id]['komisi_qpa_b'] = 0;
            $salesReff[$value->reff_id]['komisi_qpa_q'] = 0;
            $salesReff[$value->reff_id]['persen_komisi_qpa_b'] = 0;
            $salesReff[$value->reff_id]['persen_komisi_qpa_q'] = 0;
        }

        $salesReff[$value->reff_id]['total'] += $value->total;
        $salesReff[$value->reff_id]['poin_penjualan'] += $value->poin_penjualan;

        // Jika member = QPA Beginner
        if ($value->status_qpa == "Beginner") {
            $komisi = 0;
            $komisi = ($setting_rc["Beginner"]['basic_com_persen'] / 100) * $value->total;

            $salesReff[$value->reff_id]['qpa_b'] += 1;
            $salesReff[$value->reff_id]['komisi_qpa_b'] += $komisi;
            $totalKomisi += $komisi;
            $salesReff[$value->reff_id]['persen_komisi_qpa_b'] = ($setting_rc["Beginner"]['basic_com_persen'] / 100);

            if (isset($penjualan_by_id[$value->m_member_id])) {
                foreach ($penjualan_by_id[$value->m_member_id] as $pjkey => $pjvalue) {
                    $salesReff[$value->reff_id]['list_penjualan_qpa_b'][] = $pjvalue;
                }
            }
        } else {
            $salesReff[$value->reff_id]['list_penjualan_qpa_b'][] = null;
        }

        // Jika member = QPA Qualified
        if ($value->status_qpa == "Qualified") {
            $komisi = 0;
            $komisi = ($setting_rc["Qualified"]['basic_com_persen'] / 100) * $value->total;

            $salesReff[$value->reff_id]['qpa_q'] += 1;
            $salesReff[$value->reff_id]['komisi_qpa_q'] += $komisi;
            $totalKomisi += $komisi;
            $salesReff[$value->reff_id]['persen_komisi_qpa_q'] = ($setting_rc["Qualified"]['basic_com_persen'] / 100);

            if (isset($penjualan_by_id[$value->m_member_id])) {
                foreach ($penjualan_by_id[$value->m_member_id] as $pjkey => $pjvalue) {
                    $salesReff[$value->reff_id]['list_penjualan_qpa_q'][] = $pjvalue;
                }
            }
        } else {
            $salesReff[$value->reff_id]['list_penjualan_qpa_q'][] = null;
        }
    }

    // Get nama dan kode Reff ID
    $listReff = valueConcate($laporan_rc, 'reff_id');
    $db->select("
      m_member.id,
      m_member.nama,
      m_member.kode,
      m_level.nama as level,
      m_histori_level.status_qpa,
      m_histori_level.m_level_id
      ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id"); // Pilih QPA only
    if (isset($listReff) && !empty($listReff)) {
        $db->customWhere("m_member.id IN($listReff)");
    }
    $listReff = $db->findAll();

    $result = [];
    foreach ($listReff as $key => $value) {
        $data = $salesReff[$value->id];
        $result[$key] = (array) $value;
        $result[$key]['total'] = $data['total'];
        $result[$key]['poin_penjualan'] = $data['poin_penjualan'];
        $result[$key]['qpa_b'] = $data['qpa_b'];
        $result[$key]['qpa_q'] = $data['qpa_q'];
        $result[$key]['komisi_qpa_b'] = $data['komisi_qpa_b'];
        $result[$key]['komisi_qpa_q'] = $data['komisi_qpa_q'];
        $result[$key]['persen_komisi_qpa_b'] = $data['persen_komisi_qpa_b'];
        $result[$key]['persen_komisi_qpa_q'] = $data['persen_komisi_qpa_q'];
        $result[$key]['list_penjualan_qpa_b'] = isset($data['list_penjualan_qpa_b']) ? $data['list_penjualan_qpa_b'] : 0 ;
        $result[$key]['list_penjualan_qpa_q'] = isset($data['list_penjualan_qpa_q']) ? $data['list_penjualan_qpa_q'] : 0 ;
    }

    if (isset($data["tahun"]) && !empty($data["tahun"])) {
        $lap_temp = sort_array_by_key($result, "bulan");
        $finalResult = [];
        $idx = 0;

        for ($i = 1; $i <= 12; $i++) {
            $month = $i < 10 ? '0' . $i : $i;
            if (isset($lap_temp[$i])) {
                $finalResult[$idx] = $lap_temp[$i];
            } else {
                $finalResult[$idx]['komisi_qpa_b'] = 0;
                $finalResult[$idx]['komisi_qpa_q'] = 0;
                $finalResult[$idx]['total'] = 0;
            }
            $finalResult[$idx]['bulan'] = date("Y-$month-01", strtotime($data["tahun"]));

            $idx++;
        }

        $result = $finalResult;
    }

    // Get setting pajak
    $pajak = $db->select("pajak")->from("m_setting")->find();
    $pajak = !empty($pajak) ? $pajak->pajak : 0;

    return [
        'laporan' => $result,
        'totalKomisi' => $totalKomisi,
        'pajak' => $pajak,
    ];
}

function memberSupervisorCommission($data = []) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    date_default_timezone_set("Asia/Jakarta");

    // Dapatkan jumlah bawahan beserta levelnya
    $db->select("
      m_member.reff_id,
      COUNT(m_level.id) as jumlah,
      m_level.id as m_level_id,
      m_level.nama as level,
      m_histori_level.status_qpa
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->andWhere("m_member.status", "=", "Aktif")
            ->andWhere("m_member.reff_id", ">", 0)
            ->andWhere("m_member.tipe_member", "=", "Member")
            ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->groupBy("m_member.reff_id, m_level.id, m_histori_level.status_qpa");

    if (isset($data["member"]) && !empty($data["member"])) {
        $db->andWhere("m_member.reff_id", "=", $data["member"]['id']);
    }

    $listMember = $db->findAll();

    if (empty($listMember)) {
        return [
            'laporan' => [],
            'totalKomisi' => 0,
        ];
    }
    $totalKomisi = 0;

    // Dpatkan list reff id
    $listReffID = [];
    foreach ($listMember as $key => $value) {
        if (!isset($listReffID[$value->reff_id]))
            $listReffID[$value->reff_id] = $value->reff_id;
    }

    $listReffID = implode(",", $listReffID);

    /*
     * Dapatkan total Penjualan NON-PO bawahan
     */
    // $db->select("
    //   m_member.reff_id,
    //   SUM(t_penjualan.poin_penjualan) as poin_penjualan,
    //   SUM(t_penjualan.total) total
    // ")
    //     ->from("m_member")
    //     ->join("LEFT JOIN", "t_penjualan",
    //         "m_member.id = t_penjualan.m_member_id AND
    //   t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
    //   t_penjualan.jenis_pesanan = 'non-po' AND
    //   t_penjualan.is_deleted = 0
    //   ")
    //     ->andWhere("m_member.status", "=", "Aktif")
    //     ->customWhere("m_member.reff_id IN($listReffID)", "AND")
    //     ->andWhere("m_member.tipe_member", "=", "Member");
    //
    // if (isset($data["periode"]) && !empty($data["periode"])) {
    //     $start = date("Y-m-01", strtotime($data["periode"]));
    //     $end = date("Y-m-t", strtotime($data["periode"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    // }
    //
    // $db->groupBy("m_member.reff_id");
    // $getPenjualan = $db->findAll();
    // Dapatkan total Penjualan NON-PO bawahan
    $getPenjualan = [];

    /*
     * Dapatkan total Penjualan PO bawahan
     */
    $db->select("
      m_member.reff_id,
      SUM(t_penjualan.poin_penjualan) as poin_penjualan,
      SUM(t_penjualan.total) total
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "t_penjualan", "m_member.id = t_penjualan.m_member_id AND
      t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
      t_penjualan.is_deleted = 0
      ")
            // t_penjualan.jenis_pesanan = 'po' AND
            ->andWhere("m_member.status", "=", "Aktif")
            ->customWhere("m_member.reff_id IN($listReffID)", "AND")
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["periode"]) && !empty($data["periode"])) {
        $start = date("Y-m-01", strtotime($data["periode"]));
        $end = date("Y-m-t", strtotime($data["periode"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    }

    $db->groupBy("m_member.reff_id");
    $getPenjualan2 = $db->findAll();
    // Dapatkan total Penjualan NON-PO bawahan

    $laporanMerge = [];
    foreach ($getPenjualan as $key => $value) {
        if (empty($laporanMerge[$value->reff_id])) {
            $laporanMerge[$value->reff_id] = $value;
        } else {
            $laporanMerge[$value->reff_id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->reff_id]->total += $value->total;
        }
    }

    foreach ($getPenjualan2 as $key => $value) {
        if (empty($laporanMerge[$value->reff_id])) {
            $laporanMerge[$value->reff_id] = $value;
        } else {
            $laporanMerge[$value->reff_id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->reff_id]->total += $value->total;
        }
    }

    $penjualan = array_values($laporanMerge);
    // $penjualan = sort_array_by_key($penjualan, "reff_id");
    // Get data Atasan
    $db->select("
      m_member.id,
      m_member.nama,
      m_member.kode,
      m_level.nama as level,
      m_histori_level.status_qpa,
      m_histori_level.m_level_id
      ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id");

    if ($_SESSION['user']["tipe_member"] == 'Member') {
        $db->andWhere("m_member.id", "=", $_SESSION['user']['id']);
        // $db->customWhere("m_member.id IN($listReffID)")
    } else {
        $db->customWhere("m_member.id IN($listReffID)");
    }

    $db->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->customWhere("m_histori_level.m_level_id IN(2,3)", 'AND'); // Hanya MQPA dan SQPA
    $listReff = $db->findAll();

    // Urutkan data anak buah berdasarkan reff id, level id, dan status_qpa
    $jumlahMember = [];
    foreach ($listMember as $key => $value) {
        $jumlahMember[$value->reff_id][$value->m_level_id][$value->status_qpa] = $value->jumlah;
    }

    $comSetting = getCommissionSetting();
    $mcs[3] = $comSetting[3]['MCS']; // Commission MCS MQPA
    $mcs[2] = $comSetting[2]['MCS']; // Commission MCS SQPA

    foreach ($listReff as $key => $value) {
        $mcs_reward = 0;

        // Setting sesuai level nya
        $bonus = $mcs[$value->m_level_id];

        // Jumlah member bawahan sesuai level dan status
        $members = $jumlahMember[$value->id];

        $totalKomisi = $totalPoin = 0;
        foreach ($bonus as $bkey => $bval) {
            /*
             * Inisialisasi default MCM,
             * yaitu mcm reward dengan syarat terkecil
             */
            if ($mcs_reward == 0 && $bval['min'] == 0) {
                $mcs_reward = $bval['basic_com_persen'];
            }
            /* end */

            $thisLevel = $bval['m_level_id_member'];
            $thisStatus = $bval['level_member'];

            if (isset($members[$thisLevel][$thisStatus])) {

                $thisJumlah = $members[$thisLevel][$thisStatus];
                $masuk = nBetween($thisJumlah, $bval['max'], $bval['min']);

                if ($bval['max'] > 0 && $masuk) {
                    $mcs_reward = $bval['basic_com_persen'];
                } else if ($bval['max'] == 0 && $thisJumlah >= $bval['min']) {
                    $mcs_reward = $bval['basic_com_persen'];
                }
            }
        }

        /* Hitung komisi dari total penjualan bawahan */
        $listReff[$key]->mcs_reward_nominal = 0;
        if (isset($penjualan[$value->id])) {
            $listReff[$key]->total_penjualan = $penjualan[$value->id]['total'];
            $listReff[$key]->total_poin_penjualan = $penjualan[$value->id]['poin_penjualan'];

            $komisi = $penjualan[$value->id]['total'] * ($mcs_reward / 100);
            $listReff[$key]->mcs_reward_nominal = $komisi;
            $totalKomisi += $komisi;
        } else {
            $listReff[$key]->total_poin_penjualan = 0;
        }

        $listReff[$key]->mcs_reward_persen = $mcs_reward;
        $listReff[$key]->members = $members;
    }

    return [
        'laporan' => $listReff,
        'totalKomisi' => $totalKomisi,
    ];
}

function memberManagerCommission($data = []) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    date_default_timezone_set("Asia/Jakarta");

    // dapatkan pendapatan semua QPA - NON-PO
    // $db->select("
    //   m_member.id as m_member_id,
    //   SUM(t_penjualan.poin_penjualan) as poin_penjualan,
    //   SUM(t_penjualan.total) total
    // ")
    //     ->from("m_member")
    //     ->join("LEFT JOIN", "m_histori_level", "
    //   m_histori_level.id = m_member.m_histori_level_id
    //   AND m_histori_level.tgl_selesai IS NULL ")
    //     ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
    //     ->join("LEFT JOIN", "t_penjualan",
    //         "m_member.id = t_penjualan.m_member_id AND
    //   t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
    //   t_penjualan.is_deleted = 0
    //   ")
    //     ->andWhere("m_member.status", "=", "Aktif")
    //     ->andWhere("t_penjualan.jenis_pesanan", "=", "non-po")
    //     ->andWhere("m_histori_level.m_level_id", "=", 1)
    //     ->andWhere("m_member.tipe_member", "=", "Member");
    //
    // if (isset($data["periode"]) && !empty($data["periode"])) {
    //     $start = date("Y-m-01", strtotime($data["periode"]));
    //     $end = date("Y-m-t", strtotime($data["periode"]));
    //
    //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
    //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    // }
    //
    // $db->groupBy("t_penjualan.m_member_id");
    // $getPendapatan = $db->findAll();
    // dapatkan pendapatan semua QPA - NON-PO - END
    $getPendapatan = [];

    // dapatkan pendapatan semua QPA - PO
    $db->select("
      m_member.id as m_member_id,
      SUM(t_penjualan.poin_penjualan) as poin_penjualan,
      SUM(t_penjualan.total) total
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "
      m_histori_level.id = m_member.m_histori_level_id
      AND m_histori_level.tgl_selesai IS NULL ")
            ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
            ->join("LEFT JOIN", "t_penjualan", "m_member.id = t_penjualan.m_member_id AND
      t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
      t_penjualan.is_deleted = 0
      ")
            ->andWhere("m_member.status", "=", "Aktif")
            // ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
            ->andWhere("m_histori_level.m_level_id", "=", 1)
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["periode"]) && !empty($data["periode"])) {
        $start = date("Y-m-01", strtotime($data["periode"]));
        $end = date("Y-m-t", strtotime($data["periode"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    }

    $db->groupBy("t_penjualan.m_member_id");
    $getPendapatan2 = $db->findAll();
    // dapatkan pendapatan semua QPA - PO - END

    $laporanMerge = [];
    foreach ($getPendapatan as $key => $value) {
        if (empty($laporanMerge[$value->m_member_id])) {
            $laporanMerge[$value->m_member_id] = $value;
        } else {
            $laporanMerge[$value->m_member_id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->m_member_id]->total += $value->total;
        }
    }

    foreach ($getPendapatan2 as $key => $value) {
        if (empty($laporanMerge[$value->m_member_id])) {
            $laporanMerge[$value->m_member_id] = $value;
        } else {
            $laporanMerge[$value->m_member_id]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->m_member_id]->total += $value->total;
        }
    }

    // $penjualan = sort_array_by_key($db->findAll(), 'm_member_id');
    $penjualan = $laporanMerge;

    // get setting mcm
    $comSetting = getCommissionSetting();

    if (isset($comSetting[3]['MCM'])) {
        $mcm_setting[3] = $comSetting[3]['MCM']; // Commission MCM MQPA Only
    } else {
        return [
            'laporan' => [],
            'totalKomisi' => []
        ];
    }

    /*
     * ~ Periksa apakah member root ber level MQPA
     * ~ Jika ya, dapatkan direct child nya yang levelnya SQPA
     * ~ Jika ada ( Child status SQPA ), kumpulkan semua direct child nya yang levelnya QPA
     * ~ hitung pendapatan para QPA tsb, dan tentukan MCM
     */
    $pohonJaringan = get_pohon_jaringan();

    $laporanMCM = [
        'listMQPA' => [],
        'totalKomisi' => 0,
    ];
    foreach ($pohonJaringan as $key => $value) {
        $indexM = $totalPenjualan = $totalPoin = 0;
        $listQPA = [];
        $listQPAID = [];

        if ($value['level'] == 'MQPA' && !empty($value['children'])) {

            foreach ($value['children'] as $skey => $sval) {
                if ($sval['level'] == 'SQPA' && !empty($sval['children'])) {

                    foreach ($sval['children'] as $qkey => $qval) {
                        if ($qval['level'] == 'QPA' && $qval['status'] == 'Qualified') {
                            $totalPenjualan += isset($penjualan[$qval['m_member_id']]['total']) ? $penjualan[$qval['m_member_id']]['total'] : 0;
                            $totalPoin += isset($penjualan[$qval['m_member_id']]['poin_penjualan']) ? $penjualan[$qval['m_member_id']]['poin_penjualan'] : 0;
                            $listQPA['Qualified'][] = $qval;
                            $listQPAID[] = $qval['m_member_id'];
                        } else if ($qval['level'] == 'QPA' && $qval['status'] == 'Beginner') {
                            $totalPenjualan += isset($penjualan[$qval['m_member_id']]['total']) ? $penjualan[$qval['m_member_id']]['total'] : 0;
                            $totalPoin += isset($penjualan[$qval['m_member_id']]['poin_penjualan']) ? $penjualan[$qval['m_member_id']]['poin_penjualan'] : 0;
                            $listQPA['Beginner'][] = $qval;
                            $listQPAID[] = $qval['m_member_id'];
                        }
                    }
                }
            }
            unset($value['children']);
            $laporanMCM['listMQPA'][$indexM]['data'] = $value;
            $laporanMCM['listMQPA'][$indexM]['listQPA'] = $listQPA;
            $laporanMCM['listMQPA'][$indexM]['listQPAID'] = $listQPAID;
            $laporanMCM['listMQPA'][$indexM]['total_penjualan'] = $totalPenjualan;
            $laporanMCM['listMQPA'][$indexM]['total_poin'] = $totalPoin;

            // Perhitungan bonus MCM
            $mcm_reward = 0;
            // Jumlah QPA Beginner dan Qualified
            $qpa_b = isset($laporanMCM['listMQPA'][$indexM]['listQPA']['Beginner']) ? sizeof($laporanMCM['listMQPA'][$indexM]['listQPA']['Beginner']) : 0;
            $qpa_q = isset($laporanMCM['listMQPA'][$indexM]['listQPA']['Qualified']) ? sizeof($laporanMCM['listMQPA'][$indexM]['listQPA']['Qualified']) : 0;

            /*
             * Inisialisasi default MCM,
             * yaitu mcm reward dengan syarat terkecil
             */
            $mcm_reward = $mcm_setting[3][0]['basic_com_persen'];

            foreach ($mcm_setting[3] as $mkey => $mval) {
                if ($mval['m_level_id_member'] == 1 && $mval['level_member'] == 'Qualified') {
                    $masuk = nBetween($qpa_q, $mval['max'], $mval['min']);

                    if ($mval['max'] > 0 && $masuk) {
                        $mcm_reward = $mval['basic_com_persen'];
                    } else if ($mval['max'] == 0 && $qpa_q >= $mval['min']) {
                        $mcm_reward = $mval['basic_com_persen'];
                    }
                }
            }

            $laporanMCM['listMQPA'][$indexM]['komisi_persen'] = $mcm_reward;
            $laporanMCM['listMQPA'][$indexM]['komisi_nominal'] = $laporanMCM['listMQPA'][$indexM]['total_penjualan'] * ($mcm_reward / 100);
            $laporanMCM['totalKomisi'] += $laporanMCM['listMQPA'][$indexM]['komisi_nominal'];

            if (!empty($listQPAID)) {
                $listQPAID = implode(',', $listQPAID);
                // Get list ID Penjualan - NON-PO
                // $db->select("
                //   t_penjualan.*,
                //   m_member.nama as m_member_nama,
                //   m_member.id as m_member_id,
                //   t_penjualan.id as t_penjualan_id,
                //   MONTH(t_penjualan.tanggal_lunas) as bulan,
                //   m_histori_level.m_level_id,
                //   m_histori_level.status_qpa
                //   ");
                // $db->from("t_penjualan")
                //     ->join("LEFT JOIN", "m_member",
                //         "m_member.id = t_penjualan.m_member_id AND
                //         t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas') AND
                //         t_penjualan.is_deleted =0
                //         ")
                //     ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                //     ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
                //     ->andWhere("m_member.status", "=", "Aktif")
                //     ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
                //     ->andWhere("m_member.tipe_member", "=", "Member");
                //
                // if (isset($data["tahun"]) && !empty($data["tahun"])) {
                //     $start = date("Y-01-01", strtotime($data["tahun"]));
                //     $end = date("Y-12-31", strtotime($data["tahun"]));
                //
                //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
                //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
                //
                // } else if (isset($data["periode"]) && !empty($data["periode"])) {
                //     $start = date("Y-m-01", strtotime($data["periode"]));
                //     $end = date("Y-m-t", strtotime($data["periode"]));
                //
                //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
                //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
                // }
                //
                // $db->customWhere("t_penjualan.m_member_id IN (" . $listQPAID . ")", "AND");
                // $get_t_penjualan = $db->findAll();
                // Get list ID Penjualan - NON-PO - END
                $get_t_penjualan = [];

                // Get list ID Penjualan - PO
                $db->select("
                  t_penjualan.*,
                  m_member.nama as m_member_nama,
                  m_member.id as m_member_id,
                  t_penjualan.id as t_penjualan_id,
                  MONTH(t_penjualan.tanggal_lunas) as bulan,
                  m_histori_level.m_level_id,
                  m_histori_level.status_qpa
                  ");
                $db->from("t_penjualan")
                        ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id AND
                        t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
                        t_penjualan.is_deleted =0
                        ")
                        ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                        ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
                        ->andWhere("m_member.status", "=", "Aktif")
                        // ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
                        ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
                        ->andWhere("m_member.tipe_member", "=", "Member");

                if (isset($data["tahun"]) && !empty($data["tahun"])) {
                    $start = date("Y-01-01", strtotime($data["tahun"]));
                    $end = date("Y-12-31", strtotime($data["tahun"]));

                    $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
                    $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
                } else if (isset($data["periode"]) && !empty($data["periode"])) {
                    $start = date("Y-m-01", strtotime($data["periode"]));
                    $end = date("Y-m-t", strtotime($data["periode"]));

                    $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
                    $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
                }

                $db->customWhere("t_penjualan.m_member_id IN (" . $listQPAID . ")", "AND");
                $get_t_penjualan2 = $db->findAll();
                // Get list ID Penjualan - PO - END

                $all_penjualan = array_merge($get_t_penjualan, $get_t_penjualan2);

                $laporanMCM['listMQPA'][$indexM]['list_t_penjualan'] = $all_penjualan;
            }

            $indexM++;
        }
    }

    return [
        'laporan' => $laporanMCM,
        'totalKomisi' => $laporanMCM['totalKomisi'],
    ];
}

function buildTree($elements, $parentId = 0, $listMember = []) {

    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {

            $children = buildTree($elements, $element['id'], $listMember);
            if ($children) {
                $element['children'] = $children;
            }
            // tambah member
            $member_id = $element['m_member_id'];

            $title = '-';
            if (isset($listMember[$member_id])) {
                $title = $listMember[$member_id]['nama'] . " (" . $listMember[$member_id]['level'] . " - " . $listMember[$member_id]['status_qpa'] . ")";
                $element['level'] = $listMember[$member_id]['level'];
                $element['kode'] = $listMember[$member_id]['kode'];
                $element['status'] = $listMember[$member_id]['status_qpa'];
            }
            $element['name'] = $title;

            $branch[] = $element;
            unset($elements[$element['id']]);
        }
    }
    return $branch;
}

function get_images($html) {
    preg_match_all('@src="([^"]+)"@', $html, $match);
    $src = array_pop($match);
    return $src;
}

function is_base64($base64) {
    $data = $base64;
    $data = str_replace(" ", "+", $data);
    $data = str_replace(":=", "==", $data);

    list($type, $data) = explode(';', $data);
    list(, $data) = explode(',', $data);
    list($header, $ext) = explode("/", $type);

    if (isset($data) and base64_decode($data)) {
        return true;
    } else {
        return false;
    }
}

function base64toImg($base64, $path, $name = null) {
    $data = $base64;
    $data = str_replace(" ", "+", $data);
    $data = str_replace(":=", "==", $data);

    if (!empty($data) and is_base64($data)) {
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        list($header, $ext) = explode("/", $type);

        $data = base64_decode($data);
        if (empty($name)) {
            $name = date("ymdis");
        }

        $allowExt = array('PNG', 'JPG', 'jpg', 'png', 'JPEG', 'jpeg');
        if (in_array($ext, $allowExt)) {
            $fileName = $name . '.' . $ext;
            file_put_contents($path . $fileName, $data);
            return [
                'status' => true,
                'data' => $fileName,
            ];
        } else {
            return [
                'status' => false,
                'data' => ['ekstensi gambar harus JPG atau PNG'],
            ];
        }
    } else {
        return [
            'status' => false,
            'data' => $data,
        ];
    }
}

function createPagination($table_name, $_adjacents, $_limit, $_page, $_url) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    /* How many records you want to show in a single page. */
    $limit = $_limit;

    /* How may adjacent page links should be shown on each side of the current page link. */
    $adjacents = $_adjacents;

    /* Get total number of records */
    if ($table_name == 'm_artikel') {
        $tgl = date("Y-m-d");
        $row = $db->find("SELECT COUNT(id) 'total_rows'
                        FROM $table_name
                        WHERE is_deleted=0
                        AND is_publish=1
                        AND tanggal_publish <= '$tgl'
                        ");
    } else {
        $row = $db->find("SELECT COUNT(id) 'total_rows' FROM $table_name WHERE is_deleted=0");
    }

    $total_rows = $row->total_rows;

    /* Get the total number of pages. */
    $total_pages = ceil($total_rows / $limit);

    $page = $_page;
    $offset = $limit * ($page - 1);

    if ($table_name == 'm_artikel') {
        $tgl = date("Y-m-d");
        $resultData = $db->findAll("SELECT m_artikel.*,
        m_member.nama created_by
        FROM $table_name
        JOIN m_member ON m_member.id = m_artikel.created_by
        WHERE m_artikel.is_deleted=0
        AND is_publish=1
        AND tanggal_publish <= '$tgl'
        ORDER BY m_artikel.id DESC
        LIMIT $offset, $limit");
    } else if ($table_name == 'm_produk') {
        $resultData = $db->select("m_produk.*,
            m_produk_img.foto,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon
        ")
                ->from("m_produk")
                ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
                ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
                ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
                ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
                ->where("m_produk.is_deleted", "=", 0)
                ->offset($offset)
                ->limit($limit)
                ->findAll();
        // $db->findAll("SELECT * FROM $table_name WHERE is_deleted=0 ORDER BY id DESC LIMIT $offset, $limit");
    }

    //Checking if the adjacent plus current page number is less than the total page number.
    //If small then page link start showing from page 1 to upto last page.
    if ($total_pages <= (1 + ($adjacents * 2))) {
        $start = 1;
        $end = $total_pages;
    } else {
        if (($page - $adjacents) > 1) {                   //Checking if the current page minus adjacent is greateer than one.
            if (($page + $adjacents) < $total_pages) {  //Checking if current page plus adjacents is less than total pages.
                $start = ($page - $adjacents);         //If true, then we will substract and add adjacent from and to the current page number
                $end = ($page + $adjacents);         //to get the range of the page numbers which will be display in the pagination.
            } else {                                   //If current page plus adjacents is greater than total pages.
                $start = ($total_pages - (1 + ($adjacents * 2)));  //then the page range will start from total pages minus 1+($adjacents*2)
                $end = $total_pages;                           //and the end will be the last page number that is total pages number.
            }
        } else {                                       //If the current page minus adjacent is less than one.
            $start = 1;                                //then start will be start from page number 1
            $end = (1 + ($adjacents * 2));             //and end will be the (1+($adjacents * 2)).
        }
    }
    //If you want to display all page links in the pagination then
    //uncomment the following two lines
    //and comment out the whole if condition just above it.
    /* $start = 1;
      $end = $total_pages; */

    // -- Pagination Bootstrap -- //
    if ($total_pages > 1) {
        $pagination = '';

        // Link of the first page
        $pagination .= "<ul class='pagination pagination-sm justify-content-center'>
								<li class='page-item " . ($page <= 1 ? 'disabled' : '') . "'>
									<a class='page-link' href=" . $_url . "?page=1" . ">&lt;&lt;</a>
								</li>";

        // Link of the previous page
        $pagination .= "<li class='page-item " . ($page <= 1 ? 'disabled' : '') . "'>
								<a class='page-link' href=" . $_url . "?page=" . ($page > 1 ? $page - 1 : 1) . ">&lt;</a>
							</li>";

        // Links of the pages with page number
        for ($i = $start; $i <= $end; $i++) {
            $pagination .= "<li class='page-item " . ($i == $page ? 'active' : '') . "'>
									<a class='page-link' href=" . $_url . "?page=" . $i . ">" . $i . "</a>
								</li>";
        }

        // Link of the next page
        $pagination .= "<li class='page-item " . ($page >= $total_pages ? 'disabled' : '') . "'>
								<a class='page-link' href=" . $_url . "?page=" . ($page < $total_pages ? $page + 1 : $total_pages) . ">&gt;</a>
							</li>";

        // Link of the last page
        $pagination .= "<li class='page-item " . ($page >= $total_pages ? 'disabled' : '') . "'>
								<a class='page-link' href=" . $_url . "?page=" . $total_pages . ">&gt;&gt;</a>
							</li>
						</ul>";
    }
    // -- End of Pagination Bootstrap -- //

    return [
        'resultData' => $resultData,
        'pagination' => $pagination,
    ];
}

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $text = strip_tags($text);
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }

    return $text;
}

function get_kategori_produk() {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    // Get Kategori Produk
    $kategori = $db->select("*")
            ->from("m_kategori_produk")
            ->where("is_deleted", '=', 0)
            ->findAll();

    return $kategori;
}

function get_some_article($type = "", $limit) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    $defaultImg = config("SITE_IMG") . "no-image.png";

    if ($type == 'populer') {
        $type_filter = "views";
    } else {
        $type_filter = "id";
    }

    // gET Preview Artikel
    $artikel = $db->select("
    m_artikel.*,
    m_member.nama created_by
  ")
            ->from("m_artikel")
            ->join("LEFT JOIN", "m_member", "m_member.id = m_artikel.created_by")
            ->where("m_artikel.is_deleted", "=", 0)
            ->andWhere("m_artikel.is_publish", "=", 1)
            ->limit($limit)
            ->orderBy("$type_filter DESC")
            ->findAll();

    foreach ($artikel as $key => $value) {
        $firstImg = get_images($value->artikel);
        $artikel[$key]->firstImg = isset($firstImg[0]) ? $firstImg[0] : $defaultImg;
        $artikel[$key]->url = config("SITE_URL") . "public/artikel/" . $value->url;
        $artikel[$key]->deskripsi = isset($value->deskripsi) ? $value->deskripsi : limit_text($value->artikel, 20);
    }

    return $artikel;
}

function siapkan_produk($contohProduk = [], $tipe = "all") {
    if (empty($contohProduk))
        return [];

    $defaultImg = config("SITE_IMG") . "no-image.png";
    foreach ($contohProduk as $key => $value) {
        if (isset($value->kategori_id)) {
            $contohProduk[$key]->m_kategori_id = [
                "id" => $value->kategori_id,
                "kode" => $value->kategori_kode,
                "nama" => $value->kategori,
            ];
        }

        // Inisialisasi gambar produk
        if (empty($value->foto)) {
            $contohProduk[$key]->foto = $defaultImg;
        } else {
            if ($tipe == 'promo') {
                $contohProduk[$key]->foto = config("SITE_IMG") . "/promo/$value->foto";
            } else {
                $contohProduk[$key]->foto = config("SITE_IMG") . "/produk/$value->id/$value->foto";
            }
        }

        // Diskon Promo
        if (!empty($value->diskon)) {
            $contohProduk[$key]->harga_coret = $value->harga_jual;
            $contohProduk[$key]->harga_jual = $value->harga_jual - ($value->harga_jual * ($value->diskon / 100));
        }
    }

    return $contohProduk;
}

function session_customer() {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    $session = [];

    // Jika Link afiliasi ada
    if (isset($_SESSION['public']['afiliasi'])) {
        $session['afiliasi'] = $_SESSION['public']['afiliasi'];
        $session['afiliasi_det'] = $db->select("*")
                ->from("m_member")
                ->where("m_member.kode", "=", $session['afiliasi'])
                ->find();
    } else {
        $session['afiliasi'] = $session['afiliasi_det'] = NULL;
    }

    $session['keranjang'] = [
        'po' => isset($_SESSION['public']['keranjang']['po']) ? $_SESSION['public']['keranjang']['po'] : [],
        'non-po' => isset($_SESSION['public']['keranjang']['non-po']) ? $_SESSION['public']['keranjang']['non-po'] : [],
    ];

    $total_pesanan['po'] = isset($_SESSION['public']['keranjang']['po']) ? array_sum($_SESSION['public']['keranjang']['po']) : 0;
    $total_pesanan['non-po'] = isset($_SESSION['public']['keranjang']['non-po']) ? array_sum($_SESSION['public']['keranjang']['non-po']) : 0;

    $session['jumlah_pesan'] = [
        'po' => $total_pesanan['po'],
        'non-po' => $total_pesanan['non-po'],
    ];

    // Setting web
    $setting = $db->select("*")->from("m_setting")->find();
    if (!empty($setting)) {
        $setting->seo_image = isset($setting->seo_image) ? config("SITE_IMG") . $setting->seo_image : config("SITE_IMG") . "no-image.png";
        $banks = getAllBank();
        $setting->bank = isset($banks[$setting->bank]['name']) ? $banks[$setting->bank]['name'] : "";

        if (!empty($session['afiliasi_det'])) {
            $setting->no_telp = $session['afiliasi_det']->no_hp;
            $setting->email = $session['afiliasi_det']->email;
        }
    }

    $session['setting'] = !empty($setting) ? $setting : [];

    if (!isset($_SESSION['public']['customer']['id']))
        return $session;

    $session['customer'] = isset($_SESSION['public']['customer']['id']) ? $_SESSION['public']['customer'] : [];
    return $session;
}

function get_pohon_jaringan() {
    $db = config('DB');

    $pdo = new PDO('mysql:host=' . $db['db']['DB_HOST'] . ';dbname=' . $db['db']['DB_NAME'], $db['db']['DB_USER'], $db['db']['DB_PASS']);
    $jaringan = new NS\NestedSet($pdo, "m_jaringan", "id", "parent_id", "tree_id", "lft", "rgt");

    $db = new Cahkampung\Landadb($db['db']);

    /*
     * jika user admin maka Temukan para parent
     * jika member maka mulai dari dia sebagai tree root
     * */
    $db->select("*")
            ->from("m_jaringan");

    if ($_SESSION['user']['tipe_member'] == 'Member') {
        $db->where("m_member_id", "=", $_SESSION['user']['id']);
    } else if ($_SESSION['user']['tipe_member'] == 'Admin') {
        $db->where("parent_id", "=", 0);
    }

    $listParent = $db->findAll();

    $listMember = $db->select("
      m_member.id,
      m_member.nama,
      m_member.kode,
      m_member.reff_id,
      m_level.nama as level,
      m_histori_level.status_qpa,
      m_histori_level.m_level_id
      ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->customWhere('m_histori_level.m_member_id IS NOT NULL', "AND")
            ->orderBy("reff_id DESC")
            ->findAll();

    $member_by_id = dataSortID2($listMember);

    error_reporting(0);
    $result = [];
    foreach ($listParent as $key => $value) {

        $root = $member_by_id[$value->m_member_id];
        $temp_result = [
            'id' => $value->id,
            'name' => $root['nama'] . " (" . $root['level'] . " - " . $root['status_qpa'] . ")",
            'm_member_id' => $value->m_member_id,
            'kode' => $root['kode'],
            'level' => $root['level'],
            'status' => $root['status_qpa'],
        ];
        $desc = $jaringan->getDescendants($value->id);

        if (!empty($desc)) {
            // Sorting descendant
            if (sizeof($desc) > 1) {
                $no = 0;
                foreach ($desc as $k => $v) {
                    $ids[$k] = $v['id'];
                    $no++;
                }
                array_multisort($ids, SORT_ASC, $desc);
            }

            $tree = buildTree((array) $desc, $value->id, $member_by_id);

            foreach ($tree as $key => $value) {
                $temp_result['children'][] = $value;
            }
        }

        $result[] = $temp_result;
    }

    return $result;
}

function custom_youtube_iframe($htmlContent) {
    if (empty($htmlContent)) {
        return "";
    }

    // read all image tags into an array
    preg_match_all('/(<img .*?>)/', $htmlContent, $imgTags);


    for ($i = 0; $i < count($imgTags[0]); $i++) {
        // get the source string
        preg_match('/ta-insert-video="([^"]+)/i', $imgTags[0][$i], $imgage);

        if (!empty($imgage[0])) {

            // remove opening 'src=' tag, can`t get the regex right
            $origImageSrc = [
                'str' => $imgTags[0][$i],
                'embed_link' => str_ireplace('ta-insert-video="', '', $imgage[0]),
            ];


            $iframe = '<iframe width="500px" height="350px" src="' . $origImageSrc['embed_link'] . '"
        allowfullscreen="true" frameborder="0">
        </iframe>';

            $htmlContent = str_replace($origImageSrc['str'], $iframe, $htmlContent);
        }
    }

    return $htmlContent;
}

// Fungsi ini Gak dipakai lagi
function gen_kode_penjualan() {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    $saved = $db->select("kode_unik")
            ->from("t_penjualan")
            ->customWhere("status NOT IN('Lunas', 'Proses Pengiriman', 'Selesai')")
            ->findAll();

    $list = [];
    foreach ($saved as $key => $value) {
        $list[] = $value->kode_unik;
    }

    $kode_rand = 0;
    while (in_array($kode_rand, $list) || $kode_rand == 0) {
        $kode_rand = rand(100, 999);
    }

    return $kode_rand;
}

function gen_kode_member() {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    $cekKode = $db->select("kode")
            ->from("m_member")
            ->orderBy("kode DESC")
            ->where("tipe_member", "=", "Member")
            ->find();
    if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
    } else {
        $kode_terakhir = '';
    }
    $kode_cust = (!empty($kode_terakhir)) ? (substr($kode_terakhir, -5) + 1) : 1;
    $kodeCust = substr('00000' . $kode_cust, strlen($kode_cust));
    $kodeCust = 'MKD' . date("Y") . $kodeCust;

    return $kodeCust;
}

// Get Supervisor POIN
function getSupervisorPoin($data = []) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    // dapatkan pendapatan semua QPA
    $db->select("
      m_member.id as m_member_id,
      SUM(t_penjualan.poin_penjualan) as poin_penjualan,
      SUM(t_penjualan.total) total
    ")
            ->from("m_member")
            ->join("LEFT JOIN", "m_histori_level", "
      m_histori_level.id = m_member.m_histori_level_id
      AND m_histori_level.tgl_selesai IS NULL ")
            ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
            ->join("LEFT JOIN", "t_penjualan", "m_member.id = t_penjualan.m_member_id AND
      t_penjualan.status IN('Selesai', 'Proses Pengiriman') AND
      t_penjualan.is_deleted = 0
      ")
            ->andWhere("m_member.status", "=", "Aktif")
            // ->andWhere("m_histori_level.m_level_id", "=", 1)
            ->andWhere("m_member.tipe_member", "=", "Member");

    if (isset($data["periode"]) && !empty($data["periode"])) {
        $start = date("Y-m-01", strtotime($data["periode"]));
        $end = date("Y-m-t", strtotime($data["periode"]));

        $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
    }

    $db->groupBy("t_penjualan.m_member_id");
    $penjualan = sort_array_by_key($db->findAll(), 'm_member_id');

    $jaringan = $db->select('*')->from("m_jaringan")->findAll();
    $jaringan_by_id = sort_array_by_key($jaringan, 'id');

    $result = [];
    foreach ($jaringan as $key => $value) {
        if (!isset($result[$value->parent_id]['poin'])) {
            $result[$value->parent_id]['poin'] = 0;
        }

        $result[$value->parent_id]['poin'] += $penjualan[$value->m_member_id]['poin_penjualan'];
    }

    $final_res = [];
    foreach ($jaringan_by_id as $key => $value) {
        if (isset($result[$key]['poin'])) {
            $final_res[$value['m_member_id']] = $result[$key]['poin'];
        }
    }

    return $final_res;
}

function genKodePesanan() {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);

    $cekKode = $db->select("kode")
            ->from("t_penjualan")
            ->orderBy("id DESC")
            ->find();

    if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
    } else {
        $kode_terakhir = 0;
    }
    $kode_cust = (substr($kode_terakhir, -5) + 1);
    $kodeCust = substr('00000' . $kode_cust, strlen($kode_cust));
    $kodeCust = 'SPN' . date("Ym") . $kodeCust;

    return $kodeCust;
}

/*
 * Fungsi generate tagihan dengan dilengkapi kode Unik
 * params : pesanan/dp, nominal
 */

function gen_kode_unik($tipe = 'pesanan', $nominal = 0) {
    $db = config('DB');
    $db = new Cahkampung\Landadb($db['db']);
    $ulangi = TRUE;

    if ($tipe == 'dp') {
        while ($ulangi == TRUE) {
            $tempNominal = substr($nominal, 0, -3) . rand(100, 999);
            $saved = $db->select("tagihan_dp")
                    ->from("t_penjualan")
                    ->customWhere("status NOT IN('Pembayaran DP', 'Lunas', 'Proses Pengiriman', 'Selesai')")
                    ->andWhere("t_penjualan.tagihan_dp", "=", $tempNominal)
                    ->andWhere("t_penjualan.t_moota_id_dp", "=", 0)
                    ->find();

            if (empty($saved)) {
                $ulangi = FALSE;
            }
        }
    } else {
        while ($ulangi == TRUE) {
            $tempNominal = substr($nominal, 0, -3) . rand(100, 999);
            $saved = $db->select("tagihan_pesanan")
                    ->from("t_penjualan")
                    ->customWhere("status NOT IN('Lunas', 'Proses Pengiriman', 'Selesai')")
                    ->andWhere("t_penjualan.tagihan_pesanan", "=", $tempNominal)
                    ->andWhere("t_penjualan.t_moota_id_pesanan", "=", 0)
                    ->find();

            if (empty($saved)) {
                $ulangi = FALSE;
            }
        }
    }

    return $tempNominal;
}

function saveBase64($base64, $path, $custom_name = null) {

    if (isset($base64['base64'])) {
        $extension = substr($base64['filename'], strrpos($base64['filename'], ".") + 1);

        if (!empty($custom_name)) {
            $nama = $custom_name . "." . $extension;
        } else {
            $nama = $base64['filename'];
        }

        $file = base64_decode($base64['base64']);
        file_put_contents($path . '/' . $nama, $file);

        return [
            'fileName' => $nama,
            'filePath' => $path . '/' . $nama,
        ];
    } else {
        return [
            'fileName' => '',
            'filePath' => '',
        ];
    }
}

function getTarifSentralCargo($asal, $tujuan, $berat) {
    $client = new \GuzzleHttp\Client();
    $arr = "$asal/$tujuan/$berat";

    $cost = $client->request('GET', 'https://api.sentralcargo.co.id/api/districtpricelist/' . $arr);
    $model = $cost->getBody()->getContents();

    return $model;
}

function getErrorDesc($exception) {
    $trace = $exception->getTrace();
    $error = "[EXCEPTION] Message: " . $exception->getMessage() . " \n\nFile:" . $trace[2]['file'] . " \n\nline:" . $trace[1]['line'] . ", " . $trace[2]['line'];

    return $error;
}
