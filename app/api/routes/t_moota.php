<?php

function validasi($data, $custom = array())
{
    $validasi = array(
             "kode"     => "required",
             "isi"      => "required",
             // "kategori" => "required",
            );

    if( isset($data['tipe']) && $data['tipe'] == 'post' ){
      $validasi['judul'] = 'required';
    }

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/t_moota/cekMutation", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $getToken         = $db->find("SELECT moota_token FROM m_setting");
      $mootaToken       = !empty($getToken->moota_token) ? $getToken->moota_token: '';

      if($mootaToken != $data["apikey"]){
        return successResponse($response, [
          'message'   => 'Woops incorrect!',
          'data'      => $data,
          ] );
      }

      $file_path        = __DIR__ . "";
      $file_path        = substr($file_path, 0, strpos($file_path, "routes")) . "file/testFile-".strtotime("now").".txt";

      file_put_contents($file_path, file_get_contents('php://input'));

      // Get List Bank yg terdaftar di Moota
      $client = new \GuzzleHttp\Client(['headers' => [
          'Authorization' => 'Bearer ' . $mootaToken,
          "Accept"        => 'application/json',
      ]]);

      $getBank   = $client->request('GET', 'https://app.moota.co/api/v1/bank');

      $getBank   = $getBank->getBody()->getContents();
      $getBank   = json_decode($getBank, true);

      $listBank = [];
      if( !empty($getBank) ){
        foreach ($getBank['data'] as $key => $value) {
          $listBank[ $value['bank_id'] ] = $value;
        }
      }
      // Get List Bank yg terdaftar di Moota - END

      // Get Notifikasi Moota - berisi daftar mutasi
      $notifications = json_decode( file_get_contents("php://input"), true );
      // $notifications = json_decode( file_get_contents($file_path), true );

      if(!is_array($notifications)) {
          $notifications = json_decode( $notifications, true);
      }

      $listMutasi = [];
      if( sizeof($notifications) > 0 ) {
          foreach( $notifications as $notification) {
            // Simpan ke table histori
            if( isset($notification['id']) ){
              unset($notification['id']);
            }
            $notification['date']       = date('Y-m-d');
            $notification['atas_nama']  = isset($listBank[ $notification['bank_id'] ]) ? $listBank[ $notification['bank_id'] ]['atas_nama'] : "";

            $insert = $db->insert("t_histori_moota", $notification);

            // Tampung balance mutasi yg bertipe CR / Credit
            if($notification['type'] == "CR"){
              $listMutasi[] = $insert;
            }

          }
      }

      // Update data Penjualan berdasarkan Nominal Credit
      if( sizeof($listMutasi) > 0 ){
        foreach ($listMutasi as $key => $value) {
          /*Periksa DP - END*/
          $tagihanDP = $db->find("SELECT * FROM t_penjualan
              WHERE t_moota_id_dp = 0
              AND tagihan_dp = {$value->amount}
              AND status NOT IN ('Pembayaran DP', 'Lunas', 'Proses Pengiriman', 'Selesai')
            ");

          if(!empty($tagihanDP)){
            if(($tagihanDP->terbayar+$value->amount) >= ($tagihanDP->tagihan_dp + $tagihanDP->tagihan_pesanan)){
              $updateDP = $db->update('t_penjualan', [
                'status'        => 'Lunas',
                'terbayar'      => ($tagihanDP->terbayar + $value->amount),
                't_moota_id_dp' => $value->id,
                'tanggal_lunas' => date("Y-m-d"),
              ], [
                'id'            => $tagihanDP->id
              ]);
            } else {
              $updateDP = $db->update('t_penjualan', [
                'status'        => 'Pembayaran DP',
                'terbayar'      => ($tagihanDP->terbayar + $value->amount),
                't_moota_id_dp' => $value->id
              ], [
                'id'            => $tagihanDP->id
              ]);
            }

            /*Periksa DP - END*/
          } else {
            /*Periksa Pesanan */
            $tagihanPesan = $db->find("SELECT * FROM t_penjualan
                WHERE t_moota_id_pesanan = 0
                AND tagihan_pesanan = {$value->amount}
                AND status NOT IN ('Lunas', 'Proses Pengiriman', 'Selesai')
              ");

            if( !empty($tagihanPesan) && ($tagihanPesan->tagihan_pesanan == $value->amount) ){
              // Jika Nominal Moota =>
              if( ($tagihanPesan->terbayar + $value->amount) >= ($tagihanPesan->tagihan_dp + $tagihanPesan->tagihan_pesanan) ){
                $updatePesanan = $db->update('t_penjualan', [
                  'status'              => 'Lunas',
                  'terbayar'            => ($tagihanPesan->terbayar + $value->amount),
                  't_moota_id_pesanan'  => $value->id,
                  'tanggal_lunas'       => date("Y-m-d"),
                ], [
                  'id' => $tagihanPesan->id
                ]);
              } else {
                $updatePesanan = $db->update('t_penjualan', [
                  'terbayar'            => ($tagihanPesan->terbayar + $value->amount),
                  't_moota_id_pesanan'  => $value->id
                ], [
                  'id' => $tagihanPesan->id
                ]);
              }
            }
            /*Periksa Pesanan - END*/
          }
        }

      }

      return successResponse($response, $listMutasi );
      // return successResponse($response, $notifications );
    } catch (Exception $e) {
        return unprocessResponse($response, $e);
    }
})->setName("publicFrontend");

$app->get("/t_moota/sendMutation", function ($request, $response) {
    $db       = $this->db;
    $params   = $request->getParams();

    try {
      $getToken         = $db->find("SELECT moota_token FROM m_setting");
      $mootaToken       = !empty($getToken->moota_token) ? $getToken->moota_token: '';

      $client = new \GuzzleHttp\Client(['headers' => [
          'Authorization' => 'Bearer ' . $mootaToken,
          "Accept"        => 'application/json',
      ]]);

      // Get List Bank yg terdaftar di Moota
      $getBank   = $client->request('GET', 'https://app.moota.co/api/v1/bank');

      $getBank   = $getBank->getBody()->getContents();
      $getBank   = json_decode($getBank, true);

      $listBank = [];
      if( !empty($getBank) ){
        foreach ($getBank['data'] as $key => $value) {
          $listBank[ $value['bank_id'] ] = $value;
        }
      }

      return successResponse($response, []);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
})->setName("publicFrontend");
