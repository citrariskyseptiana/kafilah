<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        "kategori" => "required",
        "judul" => "required",
        "url" => "required",
        "artikel" => "required",
        "tanggal_publish" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_artikel/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
            m_artikel.*,
            m_member.nama as pembuat
            ")
            ->from("m_artikel")
            ->join("LEFT JOIN", "m_member", "m_member.id = m_artikel.created_by")
            ->where("m_artikel.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $db->orderBy("id DESC");

    $models = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

$app->post("/m_artikel/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "api")) . "img/artikel/";

    $validasi = validasi($data);
    if ($validasi !== true)
        return unprocessResponse($response, $validasi);

    try {
        $data["tanggal_publish"] = date("Y-m-d", strtotime($data["tanggal_publish"]));
//        print_r($data);
//        die;

        // Proses upload gambar ke server
        $images = get_images($data["artikel"]);

        foreach ($images as $key => $value) {
//            print_r($images);
//            die;
            if (is_base64($value)) {
                $img_file_name = base64toImg($value, $file_path);
                $img_url = config("SITE_IMG") . 'artikel/' . $img_file_name['data'];
                $data['artikel'] = str_replace($value, $img_url, $data['artikel']);
            }
        }
        // End Proses upload gambar ke server
        // Cek apakah url alias unik
        $db->select("url")
                ->from("m_artikel")
                ->where('url', '=', $data['url'])
                ->andwhere('is_deleted', '=', 0);
        if (isset($data['id'])) {
            $db->andwhere('id', '<>', $data['id']);
        }
        $periksa_alias = $db->find();

        if ($periksa_alias)
            return unprocessResponse($response, ['URL artikel sudah dipakai, silakan gunakan URL yg baru.']);
        // End of Cek apakah url alias unik

        if (isset($data["id"])) {
            $model = $db->update("m_artikel", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("m_artikel", $data);
        }

        //insert notif
        $model_member = $db->select("id")->from("m_member")->where("status", "=", "aktif")->findAll();
        $arr_member = [];
        foreach ($model_member as $key => $value) {
            $arr_member[] = $value->id;
        }
        insertNotif('m_artikel', 'Ada artikel baru', $arr_member);
        //end insert notif

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
/**
 * Save status m promo
 */
$app->post("/m_artikel/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        // pd($data);
        $model = $db->update("m_artikel", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
