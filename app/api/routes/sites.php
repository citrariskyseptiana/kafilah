<?php

$app->get('/', function ($request, $responsep) {
    $db = $this->db;

    $jaringan = $this->jaringan;
    // $jaringan->deleteNode(2);
    /**
     * Tambah parent (MQPA)
     */
    // $root_id  = $jaringan->createRoot(["m_member_id" => 1], NULL);

    /**
     * Tambah Child
     */
    // $child_id = $jaringan->createChild(3, ["m_member_id" => "4"], 1);

    /**
     * Get parent
     */
    // $parent = $jaringan->getParent(6);
    // echo json_encode($parent);

    /**
     * Get descendant
     */
    $desc = $jaringan->getDescendants(16);
    pd($desc);
    $tree = buildTree((array) $desc, 8, ['0' => 1]);
    // $a = array_values($tree);
    print_r($tree);
    // echo json_encode($desc);
    // Get Child
    // $desc = $jaringan->getChildren(8);
    // echo json_encode($desc);

    /**
     * Get Tree Leaf
     */
    // $tree = $jaringan->getTreeLeafs();
    // echo json_encode($tree);

    /*  */
    // $tree = $jaringan->getTreeLeafs();
    // echo json_encode($tree);
});

$app->get('/site/notification', function ($request, $response) {
    return successResponse($response, getNotif());
});

$app->post('/site/delete_notification', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

//    pd($params);

    deleteNotif($params['trans_tipe'], $_SESSION['user']['id']);
    return successResponse($response, []);
});

$app->get('/site/rajaongkir', function ($request, $responsep) {

    $client = new \GuzzleHttp\Client(['headers' => [
            'key' => 'b3830bd7b7f1aadeaa07f89ac4892bfc',
            "content-type" => "multipart/form-data",
    ]]);
    $province = $client->request('GET', 'https://api.rajaongkir.com/starter/province?id=12');
    // $response = $client->request('GET', 'https://api.rajaongkir.com/starter/city');
    $cost = $client->request('POST', 'https://api.rajaongkir.com/starter/cost', [
        "form_params" => [
            "origin" => 501,
            "destination" => 114,
            "weight" => 1700,
            "courier" => "jne",
        ]
    ]);
    $model = $cost->getBody()->getContents();

    echo $model;
    die;
    $prov = json_decode($model, FALSE);

    // $db = $this->db;
})->setName("logout");
/**
 * Ambil session user
 */
$app->get('/site/session', function ($request, $response) {
    if (isset($_SESSION['user']['m_roles_id'])) {
        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['undefined']);
})->setName('session');
/**
 * Proses login
 */
$app->post('/site/login', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    $username = isset($params['username']) ? $params['username'] : '';
    $password = isset($params['password']) ? $params['password'] : '';
    /**
     * Login Admin
     */
    $sql->select("m_member.*, m_roles.akses")
            ->from("m_member")
            ->leftJoin("m_roles", "m_roles.id = m_member.m_roles_id")
            ->where("username", "=", $username)
            ->andWhere("m_member.status", "=", "aktif")
            ->andWhere("m_member.is_deleted", "=", 0)
            ->andWhere("password", "=", sha1($password));
    $model = $sql->find();
    /**
     * Simpan user ke dalam session
     */
    if (isset($model->id)) {
        $_SESSION['user']['id']           = $model->id;
        $_SESSION['user']['username']     = $model->username;
        $_SESSION['user']['nama']         = $model->nama;
        $_SESSION['user']['no_hp']        = $model->no_hp;
        $_SESSION['user']['reff_id']      = $model->reff_id;
        $_SESSION['user']['m_roles_id']   = $model->m_roles_id;
        $_SESSION['user']['tipe_member']  = $model->tipe_member;
        $_SESSION['user']['akses']        = json_decode($model->akses);
        $_SESSION['user']['akses_produk'] = !empty($model->hak_akses_produk) ? json_decode($model->hak_akses_produk) : [];
        $_SESSION['user']['notification'] = getNotif();
        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['Authentication Systems gagal, username atau password Anda salah.']);
})->setName('login');
/**
 * Hapus semua session
 */
$app->get('/site/logout', function ($request, $response) {
    session_destroy();
    return successResponse($response, []);
})->setName('logout');

$app->get('/site/tes', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();
    die;
    $new_dir = __DIR__;
    $new_dir = substr($new_dir, 0, strpos($new_dir, "routes")) . "file/";

    $myfile = fopen($new_dir . "kecamatan.csv", "r") or die("Unable to open file!");

    $listDakota = [];
    while (!feof($myfile)) {
        $csv = fgetcsv($myfile);
        $prov = str_replace("<SP>", " ", $csv[0]);
        $kota = str_replace("<SP>", " ", $csv[1]);
        $kec = str_replace("<SP>", " ", $csv[2]);

        $listDakota[$prov][$kota][] = $kec;
    }

    foreach ($listDakota as $kprov => $kota) {
        $prov_prm = [
            'provinsi' => $kprov
        ];
        $input_prov = $db->insert("dakota_provinsi", $prov_prm);

        foreach ($kota as $key_kota => $kecamatan) {
            $kot_prm = [
                'dakota_provinsi_id' => $input_prov->id,
                'kota' => $key_kota,
            ];
            $input_kota = $db->insert("dakota_kota", $kot_prm);

            foreach ($kecamatan as $key => $value) {
                $kec_prm = [
                    'dakota_kota_id' => $input_kota->id,
                    'kecamatan' => $value,
                ];
                $input_kec = $db->insert("dakota_kecamatan", $kec_prm);
            }
        }
    }

    pd($listDakota);

    fclose($myfile);
    die;
    if (is_dir($new_dir)) {
        echo ("$new_dir is a directory");
    } else {
        echo ("$new_dir is not a directory");
    }
    die;
})->setName('logout');

/** API GET Prov, Kota, Kec, Desa */
$app->get('/site/getProvinsi', function ($request, $response) {
    $db = $this->db;
    $listProvinsi = $db->findAll("SELECT * FROM w_provinsi WHERE is_deleted = 0 ");

    return successResponse($response, $listProvinsi);
});

$app->get('/site/getKota', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $listKota = $db->findAll("SELECT * FROM w_kota WHERE is_deleted = 0 AND provinsi_id={$params['provinsi']}");
    return successResponse($response, $listKota);
});

$app->get('/site/getKecamatan', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $listKecamatan = $db->findAll("SELECT * FROM w_kecamatan WHERE is_deleted = 0 AND kota_id={$params['kota']}");
    return successResponse($response, $listKecamatan);
});

$app->get('/site/getDesa', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $listDesa = $db->findAll("SELECT * FROM w_desa WHERE is_deleted = 0 AND kecamatan_id={$params['kecamatan']}");
    return successResponse($response, $listDesa);
});

$app->post('/site/resetPassword', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $email = isset($params['email']) ? $params['email'] : '';
    if (!empty($email)) {
        $db->select("id, email, nama, username")
                ->from("m_member")
                ->where("email", "=", $email);
        $model = $db->find();
        if (isset($model->id)) {
            /**
             * Generate password
             */
            $getPassword = randomPassword(8, "lower_case,upper_case,numbers");
            $data["password"] = sha1($getPassword);
            /**
             * Update password
             */
            $db->update("m_member", ["password" => $data["password"]], ["id" => $model->id]);
            $view = $this->view->fetch('template_email/emailPasswordReset.html', [
                'model' => $model,
                'newPassword' => $getPassword,
            ]);
            sendMail("Permintaan Reset Password", $model->nama, $model->email, $view);
            return successResponse($response, []);
        } else {
            return unprocessResponse($response, ['Email yang anda masukkan tidak terdaftar pada system']);
        }
    }
    return unprocessResponse($response, ['Email yang anda masukkan tidak terdaftar pada system']);
})->setName('resetPassword');

$app->get('/site/setNotifLevelUp', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    try {
        /* Get Rekom SQPA
         * Dapatkan QPA Qualified yg sudah join >1 thn
         */
        $listQQPA = $db->select("
        m_member.id,
        m_member.nama,
        m_member.kode,
        m_level.id as m_level_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        FROM_UNIXTIME(m_member.approved_at) as tgl,
        DATEDIFF( now(), FROM_UNIXTIME(m_member.approved_at) ) as days
        ")
                ->from("m_member")
                ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
                ->customWhere("m_histori_level.tgl_selesai IS NULL OR m_histori_level.tgl_selesai > now()")
                ->andWhere("m_histori_level.tgl_mulai", "<=", "now()")
                ->customWhere("DATEDIFF( now(), FROM_UNIXTIME(m_member.approved_at) ) > 365", "AND")
                ->andWhere("m_histori_level.m_level_id", "=", 1)
                ->andWhere("m_histori_level.status_qpa", "=", "Qualified")
                ->findAll();

        // Dapatkan list iD nya
        $listIDQQPA = valueConcate($listQQPA, "id");

        /*
         * Dapatkan child dari user di atas yg statusnya QPA qualified dan hitung jumlahnya
         */
        $rekomSQPA = [];
        if (!empty($listIDQQPA)) {
            $jumlahQQPA = $db->findAll("SELECT
        COUNT(m_member.id) AS jumlah_qqpa,
        m_member.reff_id
        FROM m_member
        LEFT JOIN m_histori_level ON m_histori_level.id = m_member.m_histori_level_id
        LEFT JOIN m_level ON m_level.id = m_histori_level.m_level_id
        WHERE (m_histori_level.tgl_selesai IS NULL OR m_histori_level.tgl_selesai > now() )
        AND m_histori_level.tgl_mulai <= 'now()'
        AND m_histori_level.m_level_id = 1
        AND m_histori_level.status_qpa = 'Qualified'
        AND m_member.reff_id IN($listIDQQPA)
        GROUP BY m_member.reff_id
        HAVING COUNT(m_member.id) >= 4
        ");

            $listQQPA = dataSortID2($listQQPA);

            foreach ($jumlahQQPA as $key => $value) {
                if (isset($listQQPA[$value->reff_id])) {
                    $rekomSQPA[] = $listQQPA[$value->reff_id];

                    $params = $listQQPA[$value->reff_id];
                    $data = [
                        "m_member_id" => $params['id'],
                        "m_level_id_lama" => $params['m_level_id'],
                        "m_level_id_baru" => 2,
                        "tanggal" => date("Y-m-d"),
                        "tipe_perubahan" => 'level',
                    ];
                    $insertNotif = $db->insert("notif_level", $data);
                }
            }
        }

        // END - Get Rekom SQPA

        /* Get Rekom MQPA
         * Dapatkan SQPA Qualified yg sudah join >1 thn
         */
        $listQ_SQPA = $db->select("
        m_member.id,
        m_member.nama,
        m_member.kode,
        m_level.id as m_level_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        FROM_UNIXTIME(m_member.approved_at) as tgl,
        DATEDIFF( now(), FROM_UNIXTIME(m_member.approved_at) ) as days
        ")
                ->from("m_member")
                ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
                ->customWhere("m_histori_level.tgl_selesai IS NULL OR m_histori_level.tgl_selesai > now()")
                ->andWhere("m_histori_level.tgl_mulai", "<=", "now()")
                ->customWhere("DATEDIFF( now(), FROM_UNIXTIME(m_member.approved_at) ) > 365", "AND")
                ->andWhere("m_histori_level.m_level_id", "=", 2)
                ->andWhere("m_histori_level.status_qpa", "=", "Qualified")
                ->findAll();

        // Dapatkan list iD nya
        $listIDQ_SQPA = valueConcate($listQ_SQPA, "id");

        /*
         * Dapatkan child dari user di atas yg statusnya SQPA qualified dan hitung jumlahnya
         */
        $rekomMQPA = [];
        if (!empty($listIDQ_SQPA)) {
            $jumlahQ_SQPA = $db->findAll("SELECT
        COUNT(m_member.id) AS jumlah_q_sqpa,
        m_member.reff_id
        FROM m_member
        LEFT JOIN m_histori_level ON m_histori_level.id = m_member.m_histori_level_id
        LEFT JOIN m_level ON m_level.id = m_histori_level.m_level_id
        WHERE (m_histori_level.tgl_selesai IS NULL OR m_histori_level.tgl_selesai > now() )
        AND m_histori_level.tgl_mulai <= 'now()'
        AND m_histori_level.m_level_id = 2
        AND m_histori_level.status_qpa = 'Qualified'
        AND m_member.reff_id IN($listIDQ_SQPA)
        GROUP BY m_member.reff_id
        HAVING COUNT(m_member.id) >= 2
        ");

            $listQ_SQPA = dataSortID2($listQ_SQPA);

            foreach ($jumlahQ_SQPA as $key => $value) {
                if (isset($listQ_SQPA[$value->reff_id])) {
                    $rekomMQPA[] = $listQ_SQPA[$value->reff_id];

                    $params = $listQ_SQPA[$value->reff_id];
                    $data = [
                        "m_member_id" => $params['id'],
                        "m_level_id_lama" => $params['m_level_id'],
                        "m_level_id_baru" => 3,
                        "tanggal" => date("Y-m-d"),
                        "tipe_perubahan" => "level",
                    ];
                    $insertNotif = $db->insert("notif_level", $data);
                }
            }
        }
        // END - Get Rekom MQPA

        return successResponse($response, [
            'listSQPA' => $rekomSQPA,
            'listMQPA' => $rekomMQPA,
        ]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server" . $e]);
    }
});

$app->get('/site/getNotifLevelUp', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    try {
        $listSQPA = $db->select("m_member.id, m_member.kode, m_member.nama")
                ->from("notif_level")
                ->join("LEFT JOIN", "m_member", "m_member.id = notif_level.m_member_id")
                ->where("notif_level.is_deleted", "=", 0)
                ->andWhere("notif_level.m_level_id_baru", "=", 2)
                ->andWhere("MONTH(notif_level.tanggal)", "=", date("m"))
                ->andWhere("notif_level.tipe_perubahan", "=", 'level')
                ->findAll();

        $listMQPA = $db->select("m_member.id, m_member.kode, m_member.nama")
                ->from("notif_level")
                ->join("LEFT JOIN", "m_member", "m_member.id = notif_level.m_member_id")
                ->where("notif_level.is_deleted", "=", 0)
                ->andWhere("notif_level.m_level_id_baru", "=", 3)
                ->andWhere("MONTH(notif_level.tanggal)", "=", date("m"))
                ->andWhere("notif_level.tipe_perubahan", "=", 'level')
                ->findAll();

        $listStatusQPA = $db->select("
          m_member.id,
          m_member.kode,
          m_member.nama,
          m_level.nama as level,
          m_level.id as m_level_id,
          notif_level.status_qpa_lama,
          notif_level.status_qpa_baru
      ")
                ->from("notif_level")
                ->join("LEFT JOIN", "m_member", "m_member.id = notif_level.m_member_id")
                ->join("LEFT JOIN", "m_level", "notif_level.m_level_id_lama = m_level.id")
                ->where("notif_level.is_deleted", "=", 0)
                ->andWhere("MONTH(notif_level.tanggal)", "=", date("m"))
                ->andWhere("notif_level.tipe_perubahan", "=", 'status_qpa')
                ->orderBy("m_level.id")
                ->findAll();

        // List QPA yg Menunggu persetujuan
        $db->select("
      m_member.*,
      m_level.id level_id,
      m_level.no_urut level_urut,
      m_level.nama level_nama
      ")
                ->from("m_member")
                ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
                ->where("tipe_member", "=", "Member")
                ->andWhere("m_member.status", "=", 'menunggu persetujuan')
                ->andWhere("m_member.is_deleted", "=", 0);
        $listNewQPA = $db->findAll();

        if (!empty($listNewQPA)) {
            $banks = getAllBank();
            // Get All Provinsi, Kota, Kec, Desa
            $listDesa = dataSortID('w_desa');
            $listKecamatan = dataSortID('w_kecamatan');
            $listKota = dataSortID('w_kota');
            $listProvinsi = dataSortID('w_provinsi');

            foreach ($listNewQPA as $key => $value) {
                $listNewQPA[$key]->bank = isset($banks[$listNewQPA[$key]->bank]) ? $banks[$listNewQPA[$key]->bank] : NULL;
                $listNewQPA[$key]->reff_id = $db->find("SELECT id, nama FROM m_member WHERE id ={$value->reff_id}");
                $listNewQPA[$key]->w_desa = isset($listDesa[$value->w_desa_id]) ? $listDesa[$value->w_desa_id] : NULL;
                $listNewQPA[$key]->w_kecamatan = isset($listKecamatan[$value->w_desa['kecamatan_id']]) ? $listKecamatan[$value->w_desa['kecamatan_id']] : NULL;
                $listNewQPA[$key]->w_kota = isset($listKota[$value->w_kecamatan['kota_id']]) ? $listKota[$value->w_kecamatan['kota_id']] : NULL;
                $listNewQPA[$key]->w_provinsi = isset($listProvinsi[$value->w_kota['provinsi_id']]) ? $listProvinsi[$value->w_kota['provinsi_id']] : NULL;

                // Inisilisasi link
                $listNewQPA[$key]->link_ktp = isset($value->file_ktp) ? $value->file_ktp : null;
                $listNewQPA[$key]->link_npwp = isset($value->file_npwp) ? $value->file_npwp : null;
                $listNewQPA[$key]->link_tabungan = isset($value->file_tabungan) ? $value->file_tabungan : null;
                unset($listNewQPA[$key]->file_ktp);
                unset($listNewQPA[$key]->file_npwp);
                unset($listNewQPA[$key]->file_tabungan);
                unset($listNewQPA[$key]->password);
            }
        }

        $db->select("
              t_konfirmasi_pembayaran.*,
              t_penjualan.kode as t_penjualan_kode,
              t_penjualan.m_member_id as m_member_id,
              t_penjualan.tanggal as t_penjualan_tanggal,
              t_penjualan.no_resi,
              (t_penjualan.total+t_penjualan.ongkir) as grand_total
              ")
                ->from("t_konfirmasi_pembayaran")
                ->join("LEFT JOIN", "t_penjualan", "t_konfirmasi_pembayaran.t_penjualan_id = t_penjualan.id")
                ->where("t_konfirmasi_pembayaran.is_deleted", "=", 0)
                ->andWhere("t_konfirmasi_pembayaran.status", "=", "Pending");
        $listKonfirmasiBayar = $db->findAll();

        // Laporan penjualan per level per quartal
        // $db->select("
        //     MONTH(t_penjualan.tanggal) bulan,
        //     m_level.id as m_level_id,
        //     m_level.nama,
        //     SUM(t_penjualan.total+t_penjualan.ongkir) as grand_total
        //   ")
        // ->from("t_penjualan")
        // ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id")
        // ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
        // ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
        // ->where("t_penjualan.is_deleted", "=", 0)
        // ->customWhere("t_penjualan.status IN ('Proses Pengiriman', 'Selesai')", "AND")
        // ->andWhere("m_member.status", "=", "Aktif")
        // ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
        // ->andWhere("m_member.tipe_member", "=", "Member");
        //
    // $start  = date("Y-01-01");
        // $end    = date("Y-12-31");
        //
    // $db->andWhere("t_penjualan.tanggal", ">=", $start);
        // $db->andWhere("t_penjualan.tanggal", "<=", $end);
        //
    // $db->groupBy('m_histori_level.id, MONTH(t_penjualan.tanggal)');
        //
    // $laporanPenjualan = $db->findAll();
        // $lpj_label  = ['Januari - Maret', 'April - Juni', 'Juli - September', 'Oktober - Desember'];
        // $lpj_series = ['QPA', 'SQPA', 'MQPA'];
        //
    // $lpj_data = [
        //   0 => [0,0,0,0,0,0,0,0,0,0,0,0],
        //   1 => [0,0,0,0,0,0,0,0,0,0,0,0],
        //   2 => [0,0,0,0,0,0,0,0,0,0,0,0],
        // ];
        //
    // foreach ($laporanPenjualan as $key => $value) {
        //   // if( in_array($value->bulan, [1,2,3]) ){
        //   //   $index = 0;
        //   // } else if( in_array($value->bulan, [4,5,6]) ) {
        //   //   $index = 1;
        //   // } else if( in_array($value->bulan, [7,8,9]) ) {
        //   //   $index = 2;
        //   // } else if( in_array($value->bulan, [10,11,12]) ) {
        //   //   $index = 3;
        //   // }
        //
    //   $lpj_data[ ($value->m_level_id-1) ][$value->bulan] = isset($value->grand_total) ? $value->grand_total : 0;
        // }
        //
    // $lapPenjualan = [
        //   'label'     => $lpj_label,
        //   'series'    => $lpj_series,
        //   'data'      => $lpj_data,
        // ];
        // End ~ Laporan penjualan per level per quartal
        // Laporan 10 Produk terlaris
        $db->select("
        t_penjualan_det.m_produk_id as m_produk_id,
        m_produk.nama as m_produk_nama,
        SUM(t_penjualan_det.jumlah) as jumlah
      ")
                ->from("t_penjualan_det")
                ->join("LEFT JOIN", "t_penjualan", "t_penjualan.id = t_penjualan_det.t_penjualan_id")
                ->join("LEFT JOIN", "m_produk", "m_produk.id = t_penjualan_det.m_produk_id")
                ->where("t_penjualan.is_deleted", "=", 0)
                ->customWhere("t_penjualan.status IN ('Proses Pengiriman', 'Selesai', 'Lunas')", "AND");

        if (isset($params['tanggal']) && !empty($params['tanggal'])) {
            $start = date("Y-m-01", strtotime($params['tanggal']));
            $end = date("Y-m-t", strtotime($params['tanggal']));

            $db->andWhere("t_penjualan.tanggal", ">=", $start);
            $db->andWhere("t_penjualan.tanggal", "<=", $end);
        }

        if ($_SESSION['user']['tipe_member'] == 'Member') {
            $db->andWhere("t_penjualan.m_member_id", "=", $_SESSION['user']['id']);
        }

        $db->groupBy('t_penjualan_det.m_produk_id');
        $db->orderBy('SUM(t_penjualan_det.jumlah) DESC');
        $db->limit(10);

        $laporanPenjualan = $db->findAll();

        $analisaPenjualan = ['label' => [], 'data' => []];
        foreach ($laporanPenjualan as $key => $value) {
            $analisaPenjualan['label'][] = limit_text($value->m_produk_nama, 3);
            $analisaPenjualan['data'][] = $value->jumlah;
        }
        // end ~ Laporan 10 Produk terlaris

        return successResponse($response, [
            'listSQPA' => $listSQPA,
            'listMQPA' => $listMQPA,
            'listStatusQPA' => $listStatusQPA,
            'listNewQPA' => $listNewQPA,
            'listKonfirmasiBayar' => $listKonfirmasiBayar,
            'lapPenjualan' => $lapPenjualan,
            'lapAPJ' => $analisaPenjualan,
        ]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server" . $e]);
    }
});

$app->get('/site/updateStatusQPA', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    try {
        // Penentuan perubahan status QPA dan SQPA
        $thisMonth = date("Y-m-d");

        // Tanggal 3 bulan ke belakang
        $lastMonth = date("Y-m-t", strtotime("-1 Month now"));
        $startingMonth = date("Y-m-01", strtotime("-3 Month now"));

        $listBulan = [
            (int) date("m", strtotime("-1 Month now")),
            (int) date("m", strtotime("-2 Month now")),
            (int) date("m", strtotime("-3 Month now")),
        ];
        // print_r($listBulan);
        // die;

        $db->select("
        m_member.id,
        m_member.kode,
        m_member.nama,
        m_member.no_hp,
        m_member.reff_id,
        m_level.nama as level,
        m_histori_level.status_qpa,
        m_histori_level.m_level_id
      ")
                ->from("m_member")
                ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
                ->andWhere("m_member.status", "=", "Aktif")
                ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
                ->customWhere('m_histori_level.m_level_id IN(1,2)', "AND")
                ->andWhere("m_member.tipe_member", "=", "Member");

        $listMember = $db->findAll();

        $db->select("
          m_member.id,
          m_member.kode,
          m_member.nama,
          m_member.no_hp,
          m_member.reff_id,
          m_level.nama as level,
          m_histori_level.status_qpa,
          m_histori_level.m_level_id,
          SUM(t_penjualan.poin_penjualan) as poin_penjualan,
          SUM(t_penjualan.total) total,
          MONTH(t_penjualan.tanggal_kirim) as bulan_transaksi
        ")
                ->from("m_member")
                ->join("LEFT JOIN", "t_penjualan", "m_member.id = t_penjualan.m_member_id
          AND t_penjualan.status IN('Selesai', 'Proses Pengiriman')
          AND t_penjualan.is_deleted = 0
          ")
                ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
                ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
                ->andWhere("m_member.status", "=", "Aktif")
                ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
                ->andWhere("m_member.tipe_member", "=", "Member");

        $db->andWhere("t_penjualan.tanggal_kirim", ">=", $startingMonth);
        $db->andWhere("t_penjualan.tanggal_kirim", "<=", $lastMonth);

        $db->groupBy("m_member.id, MONTH(t_penjualan.tanggal_kirim)");
        $laporan = $db->findAll();

        // Urutkan data berdasarkan id member dan bulan transaksi
        $transaksi = [];
        foreach ($laporan as $key => $value) {
            $transaksi[$value->id][$value->bulan_transaksi] = isset($value->poin_penjualan) ? $value->poin_penjualan : 0;
        }

        foreach ($listMember as $key => $value) {
            $penilaian_sqpa = true;
            $penilaian_qpa = 0;
            $listMember[$key]->is_promosi = 0;

            // Inisialisasi poin member
            $poin_member = $transaksi[$value->id];

            if (isset($poin_member)) {
                $listMember[$key]->detail_poin = $poin_member;

                foreach ($listBulan as $bkey) {
                    if ($value->m_level_id == 1) {
                        /*
                         * Jika SQPA maka pastikan bahwa total poinnya > 25
                         * selama tiga bulan
                         */
                        $penilaian_qpa += $poin_member[$bkey];
                    } else if ($value->m_level_id == 2) {
                        /*
                         * Jika SQPA maka pastikan bahwa dia tidak mengalami penurunan poin < 75
                         * di setiap bulan, selama tiga bulan.
                         */
                        if ($poin_member[$bkey] < 75) {
                            $penilaian_sqpa = false;
                        }
                    }
                }
            } else {
                $listMember[$key]->promosi = 'Beginner';
            }

            // Penentuan Promosi Qualified / Beginner
            if ($value->m_level_id == 1 && !isset($listMember[$key]->promosi)) {
                $listMember[$key]->promosi = $penilaian_qpa >= 25 ? "Qualified" : "Beginner";
            } else if ($value->m_level_id == 2 && !isset($listMember[$key]->promosi)) {
                $listMember[$key]->promosi = $penilaian_sqpa == true ? "Qualified" : "Beginner";
            }

            // Jika terjadi perubahan status Beginner / Qualified maka field is_promosi TRUE
            $listMember[$key]->is_promosi = $listMember[$key]->promosi != $value->status_qpa ? 1 : 0;
        }
        // End of Penentuan perubahan status QPA dan SQPA --
        // Simpan perubahan status QPA dan SQPA ke table histori
        $listMemberPromoted = [];
        foreach ($listMember as $key => $value) {
            if ($value->is_promosi == 1 && $value->promosi == 'Beginner') {
                $listMemberPromoted['Beginner'][] = $value->id;
            } else if ($value->is_promosi == 1 && $value->promosi == 'Qualified') {
                $listMemberPromoted['Qualified'][] = $value->id;
            }
        }

        // Update Beginner
        if (isset($listMemberPromoted['Beginner'])) {
            $listID = implode(",", $listMemberPromoted['Beginner']);
            $updateB = $db->run("UPDATE m_histori_level SET status_qpa='Beginner' WHERE m_member_id IN($listID) AND tgl_selesai IS NULL");
        }

        // Update Qualified
        if (isset($listMemberPromoted['Qualified'])) {
            $listID = implode(",", $listMemberPromoted['Qualified']);
            $updateB = $db->run("UPDATE m_histori_level SET status_qpa='Qualified' WHERE m_member_id IN($listID) AND tgl_selesai IS NULL");
        }

        // Simpan perubahan status QPA dan SQPA ke table notifikasi
        foreach ($listMember as $key => $value) {
            if ($value->is_promosi == 1 && $value->promosi == 'Beginner') {
                $param = [
                    'm_member_id' => $value->id,
                    'tipe_perubahan' => 'status_qpa',
                    'status_qpa_lama' => "Qualified",
                    'status_qpa_baru' => "Beginner",
                    'm_level_id_lama' => $value->m_level_id,
                    'm_level_id_baru' => $value->m_level_id,
                    'tanggal' => date("Y-m-d"),
                    'is_deleted' => 0,
                ];

                $db->insert("notif_level", $param);
            } else if ($value->is_promosi == 1 && $value->promosi == 'Qualified') {
                $param = [
                    'm_member_id' => $value->id,
                    'tipe_perubahan' => 'status_qpa',
                    'status_qpa_lama' => "Beginner",
                    'status_qpa_baru' => "Qualified",
                    'm_level_id_lama' => $value->m_level_id,
                    'm_level_id_baru' => $value->m_level_id,
                    'tanggal' => date("Y-m-d"),
                    'is_deleted' => 0,
                ];

                $db->insert("notif_level", $param);
            }
        }

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server" . $e]);
    }
});

$app->get('/site/testKodeUnik', function ($request, $response) {
    $db = $this->db;


    return successResponse($response, [
        'nominal unik DP = ' => gen_kode_unik('dp', 200000),
        'nominal unik Pesanan = ' => gen_kode_unik('pesanan', 1300000),
    ]);
});

$app->get('/site/importSC', function ($request, $response) {
    $db = $this->db;

    $inputFileName = __DIR__;
    $inputFileName = substr($inputFileName, 0, strpos($inputFileName, "routes")) . "file/kecamatan-sc.xlsx";
    // pd($inputFileName);
    if (file_exists($inputFileName)) {
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $data = [];
        $raw = "INSERT INTO sentral_cargo (kode_distrik, provinsi, kode_kota, kota, distrik) VALUES ";
        for ($row = 2; $row <= $highestRow; $row++) {
            $data = [
                'kode_distrik' => (string) $objPHPExcel->getSheet(0)->getCell('A' . $row)->getValue(),
                'provinsi' => (string) $objPHPExcel->getSheet(0)->getCell('B' . $row)->getValue(),
                'kode_kota' => (string) $objPHPExcel->getSheet(0)->getCell('C' . $row)->getValue(),
                'kota' => (string) $objPHPExcel->getSheet(0)->getCell('D' . $row)->getValue(),
                'distrik' => (string) $objPHPExcel->getSheet(0)->getCell('E' . $row)->getValue(),
            ];

            $raw .= ' ("' . implode('","', $data) . '"),';
        }

        $raw = substr_replace($raw, ";", -1);
        $db->run("TRUNCATE TABLE sentral_cargo");
        $log = $db->run($raw);

        return successResponse($response, 'data berhasil di import');
    } else {
        return unprocessResponse($response, 'data gagal di import');
    }
});


$app->get('/site/listSC', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    try {

        $db->select("
    sentral_cargo.*,
    w_kecamatan.id as w_kecamatan_id,
    w_kecamatan.kecamatan,
    w_kecamatan.kota_id,
    w_kota.kota as w_kota_nama,
    w_kota.ro_kota_id
    ")
                ->from("sentral_cargo")
                ->join("LEFT JOIN", "w_kecamatan", "w_kecamatan.kecamatan = sentral_cargo.distrik")
                ->join("LEFT JOIN", "w_kota", "w_kota.kota = sentral_cargo.kode_kota AND w_kecamatan.kota_id = w_kota.id");

        if (!empty($params['is_kosong']) && $params['is_kosong'] == 1) {
            $db->customWhere("w_kota.ro_kota_id IS NULL", "AND");
        }

        $sentral = $db->findAll();

        if (!empty($params['kode_distrik_sc']) && $params['kode_distrik_sc'] == 1) {
            $raw = "INSERT INTO w_kecamatan (id, kecamatan, kota_id, kode_distrik_sc, is_deleted) VALUES ";
            foreach ($sentral as $key => $value) {
                $data = [
                    'id' => $value->w_kecamatan_id,
                    'kota_id' => $value->w_kecamatan_id,
                    'kecamatan' => $value->kecamatan,
                    'kode_distrik_sc' => $value->kode_distrik,
                    'is_deleted' => 0,
                ];

                $raw .= ' ("' . implode('","', $data) . '"),';
            }
            $raw = substr_replace($raw, "", -1);
            $raw .= " ON DUPLICATE KEY UPDATE id=VALUES(id), kode_distrik_sc=VALUES(kode_distrik_sc)";

            $db->run($raw);
            return successResponse($response, ['Berhasil UPDATE kode distrik']);
        }

        return successResponse($response, $sentral);
    } catch (Exception $e) {
        return unprocessResponse($response, $e->getMessage());
    }
});

$app->get('/site/rajaBadut', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    try {

        // contoh tanpa params
        $client = new \GuzzleHttp\Client();
        $arr = "";

        $id = "jfd7d3ZS";
        $key = "c286eeee2d983dd593dd30dcbc138122";
        $token = "66ac04606b96a5daac7ec9c37bc05ac26cec222a3a4631b8258b0edc9e420349";

        $url = "https://api.trello.com/1/boards/$id/cards/?key=$key&token=$token";
        // $url    = "https://api.trello.com/1/boards/$id/checklists?fields=name&key=$key&token=$token";

        $cost = $client->request('GET', $url);
        $model = $cost->getBody()->getContents();
        $models = json_decode($model, true);
        foreach ($models as $key => $val) {
            if ($val['badges']['checkItemsChecked'] != 0 && $val['badges']['checkItems'] != 0) {
                $models[$key]['persentase'] = $val['badges']['checkItemsChecked'] / $val['badges']['checkItems'] * 100;  // code...
            }
        }

        pd($models);

        // return $model;
        // contoh tanpa params - END
        // COntoh dg params
        // $client   = new \GuzzleHttp\Client(['headers' => [
        //   'key'           => 'b3830bd7b7f1aadeaa07f89ac4892bfc',
        //   "content-type"  => "multipart/form-data",
        //   ]]);
        // $province = $client->request('GET', 'https://api.rajaongkir.com/starter/province?id=12');
        // // $response = $client->request('GET', 'https://api.rajaongkir.com/starter/city');
        // $cost = $client->request('POST', 'https://api.rajaongkir.com/starter/cost', [
        //   "form_params" => [
        //     "origin" => 501,
        //     "destination" => 114,
        //     "weight" => 1700,
        //     "courier" => "jne",
        //   ]
        // ]);
        // $model    = $cost->getBody()->getContents();
        //
    // echo $model;die;
        // $prov  = json_decode($model, FALSE);
        // COntoh dg params - END



        return successResponse($response, $sentral);
    } catch (Exception $e) {
        return unprocessResponse($response, $e->getMessage());
    }
});
