<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
       "kode"  => "required",
       "nama"  => "required",
     );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua m kategori produk
 */
$app->get("/t_histori_pesanan/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    // Jika ada hak akses produk
    $listProdukID = $listPenjualanProdukID = [];
    if(!empty($_SESSION['user']['akses_produk'])){
      foreach ($_SESSION['user']['akses_produk'] as $key => $value) {
        if( !empty($value->id) ){
          $listProdukID[] = $value->id;
        }
      }
      $listProdukID = implode(",", $listProdukID);
      $db->select("t_penjualan_det.*, t_penjualan_det.t_penjualan_id")
      ->from("t_penjualan_det")
      // ->join("LEFT JOIN", "t_penjualan", "t_penjualan.id = t_penjualan_det.t_penjualan_id")
      ->customWhere("t_penjualan_det.m_produk_id IN(". $listProdukID .")")
      ->groupBy("t_penjualan_id");
      $listPenjualanProduk = $db->findAll();

      if( !empty($listPenjualanProduk) ){
        foreach ($listPenjualanProduk as $key => $value) {
          $listPenjualanProdukID[] = $value->t_penjualan_id;
        }
      }
      // pd([$listPenjualanProduk, $listProdukID, $listPenjualanProduk]);
    }
    // Jika ada hak akses produk - END

    $db->select("
        t_penjualan.*,
        m_member.nama m_member_nama,
        m_member.no_hp m_member_no_hp,
        m_reseller.nama reseller_nama,
        m_member_edit.nama as editted_by
        ")
        ->from("t_penjualan")
        ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id")
        ->join("LEFT JOIN", "m_member as m_reseller", "m_reseller.id = t_penjualan.reseller_id")
        ->join("LEFT JOIN", "m_member as m_member_edit", "m_member_edit.id = t_penjualan.modified_by")
        ->orderBy("t_penjualan.id DESC");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }

    if (isset($params['sort'])) {
      $sort = $params['sort'];
      if (isset($params['order'])) {
          if ($params['order'] == "false") {
              $sort .= " ASC";
          } else {
              $sort .= " DESC";
          }
      }
      $db->orderBy($sort);
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    if ($_SESSION['user']['tipe_member'] == 'Member') {
        $db->andWhere("m_member_id", "=", $_SESSION['user']['id']);
    }

    if( !empty($listPenjualanProdukID) ){
      $db->customWhere("t_penjualan.id IN(". implode(",",$listPenjualanProdukID) .")", "AND");
    }

    $models    = $db->findAll();
    $totalItem = $db->count();

    if(empty($models))
      return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);

    // Menyiapkan Data
    // Get All Provinsi, Kota, Kec, Desa
    $listDesa      = dataSortID('w_desa');
    $listKecamatan = dataSortID('w_kecamatan');
    $listKota      = dataSortID('w_kota');
    $listProvinsi  = dataSortID('w_provinsi');

    $listID = [];
    foreach ($models as $key => $value) {
      $listID[] = $value->id;
      $models[$key]->m_member = ['id'=>$value->m_member_id, 'nama'=>$value->m_member_nama, 'no_hp'=>$value->m_member_no_hp];
      $models[$key]->reseller = ['id'=>$value->reseller_id, 'nama'=>$value->reseller_nama,];

      $models[$key]->w_desa      = isset($listDesa[$value->w_desa_id]) ? $listDesa[$value->w_desa_id] : null;
      $models[$key]->w_kecamatan = isset($listKecamatan[$value->w_desa['kecamatan_id']]) ? $listKecamatan[$value->w_desa['kecamatan_id']] : null;
      $models[$key]->w_kota      = isset($listKota[$value->w_kecamatan['kota_id']]) ? $listKota[$value->w_kecamatan['kota_id']] : null;
      $models[$key]->w_provinsi  = isset($listProvinsi[$value->w_kota['provinsi_id']]) ? $listProvinsi[$value->w_kota['provinsi_id']] : null;
    }

    if( !empty($listID) ){
      $listID = implode(",", $listID);
      $getProd = $db->select("m_produk.*, t_penjualan_det.jumlah, t_penjualan_det.t_penjualan_id")
        ->from("t_penjualan_det")
        ->join("LEFT JOIN", "m_produk", "t_penjualan_det.m_produk_id = m_produk.id")
        ->where("t_penjualan_det.jenis", "=", 'detail')
        ->customWhere("t_penjualan_id IN (". $listID .")", "AND")
        ->findAll();

      $listProduk=[];
      if( !empty($getProd) ){
        foreach ($getProd as $key => $value) {
          $listProduk[$value->t_penjualan_id][] = $value;
        }

        foreach ($models as $key => $value) {
          if( !empty($listProduk[$value->id]) ){
            $models[$key]->detailProduk = $listProduk[$value->id];
          }
        }
      }
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m kategori produk
 */
$app->post("/t_histori_pesanan/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {

        $param = [
          'poin_penjualan'    => $data['poin_penjualan'],
          'reseller_id'       => $data['reseller']['id'],
          'm_member_id'       => $data['m_member']['id'],
          'nama_penerima'     => $data['nama_penerima'],
          'telepon_penerima'  => $data['telepon_penerima'],
          'w_desa_id'         => $data['w_desa']['id'],
          'jalan_penerima'    => $data['jalan_penerima'],
          'rt_penerima'       => $data['rt_penerima'],
          'rw_penerima'       => $data['rw_penerima'],
          'kode_pos_penerima' => $data['kode_pos_penerima'],
          'catatan'           => $data['catatan'],
          'catatan_pesanan'   => $data['catatan_pesanan'],
          'ekspedisi_ket'     => $data['ekspedisi_ket'],
          'ongkir'            => $data['ongkir'],
          'status'            => $data['status'],
          'no_resi'           => $data['no_resi'],
          'total'             => $data['total'],
          'total_diskon'      => $data['total_diskon'],
        ];

        if($param['status'] == 'Selesai'){
          $param['tanggal_terima'] = date("Y-m-d");
          if( empty($param['tanggal_kirim']) ){
            $param['tanggal_kirim'] = $param['tanggal_terima'];
          }
          if( empty($param['tanggal_lunas']) ){
            $param['tanggal_lunas'] = $param['tanggal_terima'];
          }

        } else if($param['status'] == 'Proses Pengiriman') {
          $param['tanggal_kirim'] = date("Y-m-d");
          if( empty($param['tanggal_lunas']) ){
            $param['tanggal_lunas'] = $param['tanggal_kirim'];
          }

        } else if($param['status'] == 'Lunas') {
          if( empty($param['tanggal_lunas']) ){
            $param['tanggal_lunas'] = $param['tanggal_lunas'];
          }
        }

        $model = $db->update("t_penjualan", $param, ["id" => $data["id"]]);

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});
/**
 * Save status m kategori produk
 */
$app->post("/t_histori_pesanan/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $model = $db->update("t_penjualan", ['is_deleted' => $data["is_deleted"]], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }

});

$app->get("/t_histori_pesanan/getDetailPesanan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $list = $db->select("
        t_penjualan_det.*,
        m_produk.kode m_produk_kode,
        m_produk.nama m_produk_nama,
        m_produk.berat m_produk_berat,
        m_produk.stok as stok_asli,
        m_produk.poin,
        t_penjualan_det.jumlah * m_produk.berat total_berat
        ")
          ->from("t_penjualan_det")
          ->join("LEFT JOIN", "m_produk", "m_produk.id = t_penjualan_det.m_produk_id")
          ->where("t_penjualan_det.t_penjualan_id", "=", $data['id'])
          ->findAll();

        $total_berat = 0;
        foreach ($list as $key => $value) {
          $list[$key]->sub_total = ($value->harga * $value->jumlah) - $value->diskon;
          $total_berat += $value->total_berat;

          // stok temp
          $list[$key]->stok_temp = $value->stok_asli + $value->jumlah;
        }
      return successResponse($response, ['list'=>$list, 'total_berat'=>$total_berat]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_histori_pesanan/updateStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $model = $db->update("t_penjualan", ['status'=>$data['value']], ["id" => $data['form']["id"]]);

        if($model->status == 'Selesai'){
          $param['tanggal_terima'] = date("Y-m-d");
          if( empty($model->tanggal_kirim) ){
            $param['tanggal_kirim'] = date("Y-m-d");
          }
          if( empty($model->tanggal_lunas) ){
            $param['tanggal_lunas'] = date("Y-m-d");
          }

          $setTglTerima = $db->update("t_penjualan", $param, ["id" => $data['form']["id"]]);

        } else if($model->status == 'Proses Pengiriman'){
          $updateParam = [
            'tanggal_kirim' => date("Y-m-d"),
          ];
          if( empty($model->tanggal_lunas) ){
            $updateParam['tanggal_lunas'] = date("Y-m-d");
          }
          $setTglKirim = $db->update("t_penjualan", $updateParam, ["id" => $data['form']["id"]]);

        } else if($model->status == 'Lunas') {
          $param['tanggal_lunas'] = date("Y-m-d");
          $setTglTerima = $db->update("t_penjualan", $param, ["id" => $data['form']["id"]]);
        }

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_histori_pesanan/updateStok", function ($request, $response) {
    $params   = $request->getParams();
    $db       = $this->db;

    try {
      if($params['jenis_pesanan'] == 'non-po'){
        $data_stok = [
          'stok'  => $params['stok_temp'] - $params['jumlah'],
        ];

        $update_stok  = $db->update("m_produk", $data_stok, [ 'id' => $params['m_produk_id'] ]);
      }

      $update_det   = $db->update("t_penjualan_det", $params, [
        'id' => $params['id']
      ]);

      return successResponse($response, $update_stok);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

$app->post("/t_histori_pesanan/updateDetail", function ($request, $response) {
    $params   = $request->getParams();
    $db       = $this->db;

    try {
      if($params['jenis_pesanan'] == 'non-po'){
        $data_stok = [
          'stok'  => $params['stok_temp'],
        ];

        $update_stok  = $db->update("m_produk", $data_stok, [ 'id' => $params['m_produk_id'] ]);
      }

      $update_det   = $db->delete("t_penjualan_det", [
        'id' => $params['id']
      ]);

      return successResponse($response, $update_stok);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

$app->get("/t_histori_pesanan/getProdukTambahan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $list = $db->select("
        m_produk.id m_produk_id,
        m_produk.kode m_produk_kode,
        m_produk.nama m_produk_nama,
        m_produk.berat m_produk_berat,
        m_produk.harga_jual as harga,
        m_produk.stok as stok_asli,
        m_produk.stok as stok_temp,
        m_produk.poin
        ")
          ->from("m_produk")
          ->customWhere("m_produk.nama LIKE '%". $data['nama'] ."%'")
          ->andWhere("m_produk.jenis", "=", $data['jenis'])
          ->findAll();

      return successResponse($response, $list);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_histori_pesanan/saveProdukTambahan", function ($request, $response) {
    $params   = $request->getParams();
    $db       = $this->db;

    try {
      if($params['jenis_pesanan'] == 'non-po'){
        $data_stok = [
          'stok'  => $params['stok_temp'] - $params['jumlah'],
        ];

        $update_stok  = $db->update("m_produk", $data_stok, [ 'id' => $params['m_produk_id'] ]);
      }

      $insert_det   = $db->insert("t_penjualan_det", $params);

      return successResponse($response, $update_stok);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});
