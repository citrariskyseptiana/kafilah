<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    if( in_array( $data['m_level_id_baru']['id'], [1,2] )  ){
      $validasi['status_qpa_baru'] = 'required';
    }

    GUMP::set_field_name("status_qpa_baru", "Status Member");
    GUMP::set_field_name("m_level_id_baru", "Level Member");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/testing/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;

    try {
        $model = $db->update("m_member", ['status' => $data['status']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

function GetROSD($params) {
    if (empty($params)) {
        return [];
    }

    $client   = new \GuzzleHttp\Client();
    $headers  = [
      'key'       => '917c5e2481d806185b444d32366bf782',
      'Accept'    => 'application/json',
    ];

    $cost = $client->request(
      'GET',
      'https://pro.rajaongkir.com/api/subdistrict?city=' . $params['city'],
      ['headers' => $headers]
    );
    $model = $cost->getBody()->getContents();
    return json_decode($model, true);
}

$app->get("/testing/getSubdistrictRO", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
die;
    try {
      if(empty($data['index'])){
        pd('isikan index!');
      }
      $start  = $data['index'];
      $end    = $start + 9;

      for($start; $start <= $end; $start++ ){
        $params = ["city" => $start];
        $getSubdistrict = GetROSD($params);
        // pd([$start, $getSubdistrict]);

        if( !empty($getSubdistrict['rajaongkir']['results']) ){
          $getSubdistrict = !empty($getSubdistrict['rajaongkir']['results']) ? $getSubdistrict['rajaongkir']['results'] : [];

          // Insert DB
          $query = "INSERT INTO ro_subdistrict (city_id, province_id, subdistrict_id, province, city, type, subdistrict_name) VALUES ";
          foreach ($getSubdistrict as $key => $value) {
            // code...
            $DBparams = [
              'city_id'           => $value['city_id'],
              'province_id'       => $value['province_id'],
              'subdistrict_id'    => $value['subdistrict_id'],
              'province'          => $value['province'],
              'city'              => $value['city'],
              'type'              => $value['type'],
              'subdistrict_name'  => $value['subdistrict_name']
            ];

            $query .= ' ("' . implode('","', $DBparams) . '"),';
          }
          $query = substr_replace($query, ";", -1);
        }
        $db->run($query);
        // pd($query);
      }

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
