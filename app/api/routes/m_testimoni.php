<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "nama"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m promo
 */
$app->get("/m_testimoni/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
            $db->select("*")
        ->from("m_testimoni")
        ->where("is_deleted", "=", 0);
            /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m promo
 */
$app->post("/m_testimoni/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            if (isset($data["data"]["id"])) {
                $model = $db->update("m_testimoni", $data["data"], ["id" => $data["data"]["id"]]);

            } else {
                $model = $db->insert("m_testimoni", $data["data"]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m promo
 */
$app->post("/m_testimoni/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_testimoni", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_testimoni/setAktif", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      if($data['is_used'] == 1){
        $model = $db->update("m_testimoni", ['is_used'=>1], ["id" => $data["id"]]);
      } else {
        $model = $db->update("m_testimoni", ['is_used'=>0], ["id" => $data["id"]]);
      }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e ]);
    }

});
