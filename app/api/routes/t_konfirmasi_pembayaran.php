<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "kode"             => "required",
        "tanggal_transfer" => "required",
        "t_penjualan_id"   => "required",
        "jenis_pembayaran" => "required",
        "bank"             => "required",
        "bank_atas_nama"   => "required",
        "nominal_transfer" => "required",
    );

    GUMP::set_field_name("t_penjualan_id", "Kode Surat Pesanan");
    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_konfirmasi_pembayaran/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("
              t_konfirmasi_pembayaran.*,
              t_penjualan.terbayar,
              t_penjualan.kode as t_penjualan_kode,
              t_penjualan.m_member_id as m_member_id,
              t_penjualan.tanggal as t_penjualan_tanggal,
              t_penjualan.no_resi,
              (t_penjualan.total+t_penjualan.ongkir) as grand_total
              ")
        ->from("t_konfirmasi_pembayaran")
        ->join("LEFT JOIN", "t_penjualan", "t_konfirmasi_pembayaran.t_penjualan_id = t_penjualan.id")
        ->where("t_konfirmasi_pembayaran.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if ($_SESSION['user']['tipe_member'] == 'Member') {
        $db->andWhere("t_konfirmasi_pembayaran.created_by", "=", $_SESSION['user']['id']);
    }

    $db->orderBy("id DESC");

    $models    = $db->findAll();
    $totalItem = $db->count();

    $banks = getAllBank();

    foreach ($models as $key => $value) {
        $models[$key]->t_penjualan_id = [
            'id'          => $value->t_penjualan_id,
            'kode'        => $value->t_penjualan_kode,
            'tanggal'     => $value->t_penjualan_tanggal,
            'm_member_id' => $value->m_member_id,
        ];

        $models[$key]->bank = isset($banks[$models[$key]->bank]) ? $banks[$models[$key]->bank] : null;

        $models[$key]->link_transfer = isset($value->foto_transfer) ? $value->foto_transfer : null;
        unset($models[$key]->foto_transfer);
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m promo
 */
$app->post("/t_konfirmasi_pembayaran/save", function ($request, $response) {
    $data      = $request->getParams();
    $db        = $this->db;
    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "routes")) . "file/pembayaran/";
    $validasi  = validasi($data);

    if ($validasi === true) {
        try {
            // Tampung foto transfer
            $foto = null;
            if (isset($data['foto_transfer']) && is_array($data['foto_transfer'])) {
                $foto = $data['foto_transfer'];
            }

            // Menyiapkan data
            $file_path                = $file_path . $data['t_penjualan_id']['m_member_id'] . "/";
            $data['tanggal_transfer'] = date("Y-m-d", strtotime($data['tanggal_transfer']));
            $data['t_penjualan_id']   = $data['t_penjualan_id']['id'];
            $data['bank']             = $data['bank']['code'];
            $data['status']           = "Pending";

            if(isset($data['foto_transfer'])){
              unset($data['foto_transfer']);
            }
            if (isset($data["id"])) {
                $model = $db->update("t_konfirmasi_pembayaran", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("t_konfirmasi_pembayaran", $data);
            }

            // Simpan Bukti transfer
            if (!empty($foto)) {
                if (!is_dir($file_path)) {
                    mkdir($file_path, 0777, true);
                }

                $simpan_file = base64ToFile($foto, $file_path);
                $temp        = explode(".", $simpan_file["fileName"]);
                $newfilename = 'foto_transfer_' . $model->kode . '.' . end($temp);
                rename($simpan_file['filePath'], $file_path . $newfilename);

                $update_foto = $db->update("t_konfirmasi_pembayaran",
                    ['foto_transfer' => $newfilename], ["id" => $model->id]);
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m promo
 */
$app->post("/t_konfirmasi_pembayaran/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("t_konfirmasi_pembayaran", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/t_konfirmasi_pembayaran/kode", function ($request, $response) {
    $db = $this->db;

    try {
        $cekKode = $db->select("kode")
            ->from("t_konfirmasi_pembayaran")
            ->orderBy("id DESC")
            ->find();

        if ($cekKode) {
            $kode_terakhir = $cekKode->kode;
        } else {
            $kode_terakhir = 0;
        }
        $kode_cust = (substr($kode_terakhir, -5) + 1);
        $kodeCust  = substr('00000' . $kode_cust, strlen($kode_cust));
        $kodeCust  = 'BYR' . date("Ym") . $kodeCust;

        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_konfirmasi_pembayaran/getPesanan", function ($request, $response) {
    $db   = $this->db;
    $data = $request->getParams();

    try {
        $db->select("
          t_penjualan.*
          ")
            ->from("t_penjualan")
            ->where("is_deleted", "=", "0")
            ->customWhere("status IN('Menunggu Pembayaran','Pembayaran DP')", "AND");
        if( isset($data['kode']) && !empty($data['kode']) ){
          $db->andWhere("kode", "LIKE", $data['kode']);
        }
        $db->limit(15);

      $pesanan = $db->orderBy("id DESC")->findAll();

        return successResponse($response, $pesanan);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_konfirmasi_pembayaran/getBank", function ($request, $response) {
    $db   = $this->db;
    $data = $request->getParams();

    try {
        $banks = json_decode(config("BANKS"), true);

        return successResponse($response, $banks);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
