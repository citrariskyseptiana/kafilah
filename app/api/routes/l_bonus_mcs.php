<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "periode" => "required",
            );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/l_bonus_mcs/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi($data);
      if($validasi !== true)
        return unprocessResponse($response, $validasi);

      $mcs = memberSupervisorCommission( $data );

      return successResponse($response, [
        'laporan'     => $mcs['laporan'],
        'totalKomisi' => $mcs['totalKomisi'],
        ]);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
