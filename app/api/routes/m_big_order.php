<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = [
      'jenis_bulk'  => 'required',
    ];

    if($data['jenis_diskon'] == 'diskon'){
      $validasi['diskon'] = "required";
    }

    if($data['jenis_diskon'] == 'gratis barang'){
      $validasi['m_produk_gratis'] = "required";
    }

    GUMP::set_field_name("jenis_bulk", "Jenis Big Order");
    GUMP::set_field_name("m_produk_gratis", "Produk Gratis");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil detail m promo
 */
$app->get("/m_big_order/getBulk", function ($request, $response) {
    $params = $request->getParams();
    $db          = $this->db;

    try {
        $listBulk = $db->select("*")
            ->from("m_big_order")
            ->orderBy("id DESC")
            ->findAll();

        if (!empty($listBulk)) {
            foreach ($listBulk as $key => $value) {
                if ($value->jenis_diskon == 'gratis barang') {
                    $listBulk[$key]->m_produk_gratis = $db->find("SELECT * FROM m_produk WHERE id = {$value->m_produk_gratis_id}");
                }
            }
        }

        return successResponse($response, $listBulk);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/m_big_order/save", function ($request, $response) {
    $data       = $request->getParams();
    $db         = $this->db;

    try {
      foreach ($data as $key => $value) {
        $validasi = validasi($value);
        if ($validasi !== true)
        return unprocessResponse($response, $validasi);
      }

      /** Save list Bulk Order Bonus */
      $bulk_id_aktif = [];
      if (isset($data) && !empty($data)) {

          foreach ($data as $key => $value) {

              if ($value['jenis_diskon'] == 'gratis barang') {
                  $value['m_produk_gratis_id'] = isset($value['m_produk_gratis']['id']) ? $value['m_produk_gratis']['id'] : null;
              }

              if (isset($value['id'])) {
                  $detail_bulk = $db->update("m_big_order", $value, [ 'id' => $value['id'] ]);
              } else {
                  $detail_bulk = $db->insert("m_big_order", $value);
              }

              $bulk_id_aktif[] = $detail_bulk->id;
          }
      }

      /** Update bulk yg tidak aktif */
      if (!empty($bulk_id_aktif)) {
          $bulk_non_aktif = $db->run("DELETE FROM m_big_order WHERE id NOT IN(" . implode(",", $bulk_id_aktif) . ")");
      }

        return successResponse($response, $detail_bulk);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
