<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        "kode" => "required",
        "isi" => "required",
        "kategori" => "required",
    );

    if (isset($data['tipe']) && $data['tipe'] == 'post') {
        $validasi['judul'] = 'required';
    }

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

function slugify($text) {
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

$app->get('/m_forum/getUrl', function ($request, $response) {
    $site_url = config("SITE_URL");
    $site_url = substr($site_url, 0, -4);

    return successResponse($response, $site_url);
});

$app->get('/m_forum/isiurl', function ($request, $response) {
    $db = $this->db;

    $cek = $db->select("id, judul, url")
        ->from("m_forum")
        ->where("jenis", "=", "post")
        ->findAll();

    foreach ($cek as $key => $value) {
        $slug = slugify($value->judul);

        $arr = explode("-",$slug);
        $cut = array_slice($arr,0,5);
        $url = implode("-",$cut);

        $isiurl = $db->update("m_forum", ["url" => $url], ["id" => $value->id]);
    }

    return successResponse($response, $isiurl);
});

$app->get('/m_forum/index-first', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $kategori = $db->select("DISTINCT(m_forum.kategori) as kategori")->from("m_forum")->findAll();

    $kategori = [
        "Testimoni" => [],
        "News" => [],
        "Tanya Jawab" => [],
        "Success Story" => [],
        "Bahan Iklan" => [],
        "Materi" => [],
        "" => [],
    ];

    $arr = $kategori;
    foreach ($kategori as $key => $value) {
        $db->select("
          m_forum.*,
          m_member.nama as pembuat,
          m_histori_level.status_qpa,
          m_level.id level_id,
          m_level.no_urut level_urut,
          m_level.nama level_nama")
            ->from("m_forum")
            ->join("LEFT JOIN", "m_member", "m_member.id = m_forum.created_by")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
            ->where("m_forum.is_deleted", "=", 0)
            ->andWhere("m_forum.jenis", "=", "post")
            ->andWhere("m_forum.kategori", "=", $key);

        if (isset($params['url'])) {
            $db->andWhere("m_forum.url", "=", $params['url']);
            $data = $db->findAll();
        } else {
            $data = $db->limit(3)->orderBy("m_forum.id DESC")->findAll();
        }



//        // Hitung reply
//        $listID = valueConcate($data, "id");
//        if (!empty($listID)) {
//            $repliesCount = $db->select("COUNT(id) as jumlah, m_forum_id")
//                    ->from("m_forum")
//                    ->customWhere("m_forum_id IN($listID)")
//                    ->andWhere("jenis", "=", 'reply')
//                    ->groupBy("m_forum_id")
//                    ->findAll();
//            $repliesCount = sort_array_by_key($repliesCount, 'm_forum_id');
//        }
//
//        $repliesCount = sort_array_by_key($repliesCount, 'm_forum_id');

        $defaultImg = config("SITE_IMG") . "no-image.png";
        foreach ($data as $keys => $values) {
            if (isset($params['url'])) {
                $kategori = $values->kategori;
            }
            $firstImage = get_images($values->isi);
            $values->firstImage = isset($firstImage[0]) ? $firstImage[0] : $defaultImg;
//            $value->jumlah_reply = isset($repliesCount[$value->id]) ? $repliesCount[$value->id]['jumlah'] : NULL;
        }

        $arr[$key] = $data;
    }

    if (isset($params['url'])) {
        return successResponse($response, ['list' => $arr, 'kategori' => $kategori]);
    } else {
        return successResponse($response, $arr);
    }
});

$app->get("/m_forum/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    // Pengurutan berdasarkan komentar
    if (isset($params["sort"])) {
      $params['sort'] = json_decode($params["sort"], true);
    }

    $default_order = "id DESC";
    if( !empty($params["sort"]['komentar']) ){
      $sql = $this->db;
      $sql->select("m_forum_id as id, COUNT(id) as komentar")
      ->from("m_forum")
      ->where("jenis", "=", 'reply')
      ->groupBy("m_forum_id")
      ->orderBy("komentar " . $params["sort"]['komentar']);
      $urutan = $sql->findAll();

      if( !empty($urutan) ){
        $listUrutan = [];
        foreach ($urutan as $key => $value) {
          $listUrutan[] = $value->id;
        }

        $listUrutan = implode(',', $listUrutan);
        $default_order = "FIELD(m_forum.id, ". $listUrutan .") " . $params["sort"]['komentar'];
      }
    }
    // Pengurutan berdasarkan komentar - END

    $db->select("
            m_forum.*,
            m_member.nama as pembuat,
            m_histori_level.status_qpa,
            m_level.id level_id,
            m_level.no_urut level_urut,
            m_level.nama level_nama")
            ->from("m_forum")
            ->join("LEFT JOIN", "m_member", "m_member.id = m_forum.created_by")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
            ->where("m_forum.is_deleted", "=", 0)
            ->andWhere("m_forum.jenis", "=", "post");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if( !empty($params["sort"]['tanggal']) ){
      if( $default_order != 'id DESC'){
        $default_order .= " ,m_forum.created_at " . $params["sort"]['tanggal'];
      } else {
        $default_order = " m_forum.created_at " . $params["sort"]['tanggal'];
      }
    }

    $db->orderBy("$default_order");
    $models = $db->findAll();
    $totalItem = $db->count();

    // Hitung reply
    $listID = valueConcate($models, "id");
    if (!empty($listID)) {
        $repliesCount = $db->select("COUNT(id) as jumlah, m_forum_id")
                ->from("m_forum")
                ->customWhere("m_forum_id IN($listID)")
                ->andWhere("jenis", "=", 'reply')
                ->groupBy("m_forum_id")
                ->findAll();
        $repliesCount = sort_array_by_key($repliesCount, 'm_forum_id');
    }

    $defaultImg = config("SITE_IMG") . "no-image.png";
    foreach ($models as $key => $value) {
        $firstImage = get_images($value->isi);
        $models[$key]->firstImage = isset($firstImage[0]) ? $firstImage[0] : $defaultImg;
        $models[$key]->jumlah_reply = isset($repliesCount[$value->id]) ? $repliesCount[$value->id]['jumlah'] : NULL;
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m promo
 */
$app->post("/m_forum/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $cek_url = $db->select("url")
        ->from("m_forum")
        ->where("url", "=", $data['data']['url'])
        ->andWhere("is_deleted", "=", 0)
        ->findAll();

    if (empty($cek_url)) {
        $file_path = __DIR__ . "";
        $file_path = substr($file_path, 0, strpos($file_path, "api")) . "img/forum/";

        $validasi = validasi($data["data"]);
        if ($validasi !== true)
            return unprocessResponse($response, $validasi);

        try {
            // Proses upload gambar ke server
            $images = get_images($data["data"]['isi']);

            foreach ($images as $key => $value) {
                if (is_base64($value)) {
                    $img_file_name = base64toImg($value, $file_path);
                    $img_url = config("SITE_IMG") . 'forum/' . $img_file_name['data'];
                    $data['data']['isi'] = str_replace($value, $img_url, $data['data']['isi']);
                }
            }
            // End Proses upload gambar ke server

            if (isset($data["data"]["id"])) {
                $model = $db->update("m_forum", $data["data"], ["id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("m_forum", $data["data"]);
            }

            //insert notif
            $model_member = $db->select("id")->from("m_member")->where("status", "=", "aktif")->findAll();
            $arr_member = [];
            foreach ($model_member as $key => $value) {
                $arr_member[] = $value->id;
            }
            insertNotif('m_forum', 'Ada diskusi baru', $arr_member);
            //end insert notif

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    } else {
        return unprocessResponse($response, ["URL sudah ada"]);
    }

});
/**
 * Save status m promo
 */
$app->post("/m_forum/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $model = $db->update("m_forum", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/m_forum/kode", function ($request, $response) {
    $db = $this->db;

    try {
        $cekKode = $db->select("kode")
                ->from("m_forum")
                ->where("jenis", "=", "post")
                ->orderBy("id DESC")
                ->find();

        if ($cekKode) {
            $kode_terakhir = $cekKode->kode;
        } else {
            $kode_terakhir = 0;
        }
        $kode_cust = (substr($kode_terakhir, -5) + 1);
        $kodeCust = substr('00000' . $kode_cust, strlen($kode_cust));
        $kodeCust = 'FRM' . date("Y") . $kodeCust;

        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/m_forum/saveReply", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $get_signature = $db->find("SELECT signature FROM m_forum WHERE m_forum_id={$data['m_forum_id']} ORDER BY id DESC");

        $param = [
            'signature' => empty($get_signature) ? 1 : $get_signature->signature + 1,
            'jenis' => "reply",
            'm_forum_id' => $data['m_forum_id'],
            'm_forum_id_sub' => ($data['m_forum_id'] != $data['id']) ? $data['id'] : NULL,
            'isi' => $data['reply'],
            'created_by' => $_SESSION['user']['id'],
        ];

        $model = $db->insert("m_forum", $param);

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/m_forum/listReply", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
            m_forum.*,
            m_member.nama as pembuat,
            m_histori_level.status_qpa,
            m_level.id level_id,
            m_level.no_urut level_urut,
            m_level.nama level_nama,
            original_member.nama as original_member,
            original.isi as original_reply
            ")
            ->from("m_forum")
            ->join("LEFT JOIN", "m_member", "m_member.id = m_forum.created_by")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
            // Join m_forum untuk mendapatkan original posted pada reply
            ->join("LEFT JOIN", "m_forum as original", "m_forum.m_forum_id_sub = original.id")
            ->join("LEFT JOIN", "m_member as original_member", "original_member.id = original.created_by")
            ->where("m_forum.is_deleted", "=", 0)
            ->andWhere("m_forum.m_forum_id", "=", $params['m_forum_id'])
            ->andWhere("m_forum.jenis", "=", "reply");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
