<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "periode" => "required",
            );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

function isMasukKomisi($tipePenjualan, $status){
  $is_komisi = false;

  if($tipePenjualan == 'po'){
    if( in_array($status, ['Selesai', 'Proses Pengiriman']) ){
      $is_komisi = true;
    }
  } else if($tipePenjualan == 'non-po'){
    if( in_array($status, ['Selesai', 'Proses Pengiriman', 'Lunas']) ){
      $is_komisi = true;
    }

  }

  return $is_komisi;
}

$app->get("/l_detail_komisi/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi($data);
      if($validasi !== true)
        return unprocessResponse($response, $validasi);

      // $db->select("
      //     t_penjualan.*,
      //     (t_penjualan.total+t_penjualan.ongkir) as grand_total,
      //     m_member.nama as member_nama,
      //     m_member.no_hp as member_no_hp,
      //     m_reseller.nama as reseller_nama,
      //     m_reseller.no_hp as reseller_no_hp
      //   ")
      //   ->from("t_penjualan")
      //   ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id")
      //   ->join("LEFT JOIN", "m_member as m_reseller", "m_reseller.id = t_penjualan.reseller_id")
      //   ->where("t_penjualan.is_deleted", "=", 0);
      //
      // if(isset($data["periode"]) && !empty($data["periode"])){
      //   // if($data['export'] == 0) {
      //   //   $data["periode"] = json_decode($data["periode"], true);
      //   // }
      //
      //   $start  = date("Y-m-01", strtotime($data["periode"]));
      //   $end    = date("Y-m-t", strtotime($data["periode"]));
      //
      //   $db->andWhere("t_penjualan.tanggal", ">=", $start);
      //   $db->andWhere("t_penjualan.tanggal", "<=", $end);
      // }
      //
      // if(isset($data["member"]) && !empty($data["member"])){
      //   $data["member"] = json_decode($data["member"], true);
      //
      //   $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
      // }
      //
      // if(isset($data["reseller"]) && !empty($data["reseller"])){
      //   $db->andWhere("t_penjualan.reseller_id", "=", $data["reseller"]["id"]);
      // }
      //
      // if(isset($data["status"]) && !empty($data["status"])){
      //   $db->andWhere("t_penjualan.status", "=", $data["status"]);
      // }
      //
      // if($_SESSION['user']['tipe_member'] == "Member"){
      //   $db->andWhere("t_penjualan.m_member_id", "=", $_SESSION['user']['id']);
      // }
      //
      // $db->orderBy("m_member.nama");
      // $laporan = $db->findAll();
      //
      // $grand_total = 0;
      //
      // $listID = [];
      // if( !empty($laporan) ){
      //   foreach ($laporan as $key => $value) {
      //     $listID[] = $value->id;
      //     $grand_total += $value->grand_total;
      //   }
      // }
      //
      // if( !empty($listID) ){
      //   $listID = implode(",", $listID);
      //   $getProd = $db->select("
      //     m_produk.poin,
      //     m_produk.nama,
      //     t_penjualan_det.jumlah,
      //     t_penjualan_det.t_penjualan_id,
      //     t_penjualan.m_member_id
      //   ")
      //     ->from("t_penjualan_det")
      //     ->join("LEFT JOIN", "m_produk", "t_penjualan_det.m_produk_id = m_produk.id")
      //     ->join("LEFT JOIN", "t_penjualan", "t_penjualan_det.t_penjualan_id = t_penjualan.id")
      //     ->where("t_penjualan_det.jenis", "=", 'detail')
      //     ->customWhere("t_penjualan_id IN (". $listID .")", "AND")
      //     ->findAll();
      //
      //   $listProduk     = [];
      //   $poinPenjualan = $poinPenjualanMember  = $totalPenjualan = [];
      //
      //   if( !empty($getProd) ){
      //     foreach ($getProd as $key => $value) {
      //       $listProduk[$value->t_penjualan_id][] = $value;
      //
      //       if( empty($poinPenjualanMember[$value->m_member_id]) ){
      //         $poinPenjualanMember[$value->m_member_id] = 0;
      //       }
      //       $poinPenjualanMember[$value->m_member_id] += $value->poin;
      //
      //       $poinPenjualan[$value->t_penjualan_id] = $value->poin;
      //     }
      //   }
      //   // pd($poinPenjualanMember);
      //
      //   $urutan = $totalTemp = 0;
      //   foreach ($laporan as $key => $value) {
      //     if( !empty($listProduk[$value->id]) ){
      //       $laporan[$urutan]->detailProduk  = $listProduk[$value->id];
      //     }
      //     $laporan[$urutan]->poin          = $poinPenjualan[$value->id];
      //     $totalTemp                      += $value->total;
      //
      //     if($laporan[$urutan]->m_member_id != $laporan[$key+1]->m_member_id){
      //       $laporan[$urutan]->pc_persen   = isset($pc[$value->m_member_id]['komisi_persen']) ? $pc[$value->m_member_id]['komisi_persen'] : 0;
      //       $laporan[$urutan]->pc_nominal  = isset($pc[$value->m_member_id]['komisi_nominal']) ? $pc[$value->m_member_id]['komisi_nominal'] : 0;
      //       $laporan[$urutan]->pc_total    = isset($pc[$value->m_member_id]['total']) ? $pc[$value->m_member_id]['total'] : 0;
      //       $laporan[$urutan]->poin_total  = $poinPenjualanMember[$value->m_member_id];
      //       $laporan[$urutan]->grand_total = $totalTemp==0 ? $value->total : $totalTemp;
      //       $laporan[$urutan]->is_last     = 1;
      //       $totalTemp                     = 0;
      //     } else {
      //       $laporan[$urutan]->pc_persen   = "";
      //       $laporan[$urutan]->pc_nominal  = "";
      //       $laporan[$urutan]->is_last     = 0;
      //     }
      //
      //     $urutan++;
      //   }
      //   // pd($laporan);
      //
      // }

      /* FORMAT BARU */
      if(!empty($data['member'])){
        $data['member'] = json_decode($data['member'], true);
      }
      // pd($data);
      $res  = personalCommission($data);
      $pc   = dataSortID2($res['laporan']);

      // KUMPULKAN LIST PRODUK YG DIJUAL
      $listIDPenjualan = [];
      foreach ($pc as $key => $value) {
        if( !empty($value['list_t_penjualan']) ){
          foreach ($value['list_t_penjualan'] as $key2 => $value2) {
            $listIDPenjualan[] = $value2['id'];
          }
        }
      }

      $listIDPenjualan = implode(",", $listIDPenjualan);
      $getProd = $db->select("
        m_produk.poin,
        m_produk.nama,
        t_penjualan_det.jumlah,
        t_penjualan_det.t_penjualan_id,
        t_penjualan.m_member_id
      ")
        ->from("t_penjualan_det")
        ->join("LEFT JOIN", "m_produk", "t_penjualan_det.m_produk_id = m_produk.id")
        ->join("LEFT JOIN", "t_penjualan", "t_penjualan_det.t_penjualan_id = t_penjualan.id")
        ->where("t_penjualan_det.jenis", "=", 'detail')
        ->customWhere("t_penjualan_id IN (". $listIDPenjualan .")", "AND")
        ->findAll();

      $listProduk = [];
      if(!empty($getProd)){
        foreach ($getProd as $key => $value) {
          $listProduk[$value->t_penjualan_id][] = $value;
        }
      }

      foreach ($pc as $key => $value) {
        if( !empty($value['list_t_penjualan']) ){
          foreach ($value['list_t_penjualan'] as $key2 => $value2) {
            $pc[$key]['list_t_penjualan'][$key2]['detail'] = isset($listProduk[$value2['id']]) ? $listProduk[$value2['id']]: [];
          }
        }
      }

      // pd($pc);
      /* FORMAT BARU - END */

      if($data['export'] == 1) {

        $view = $this->view->fetch('laporan/l_pesanan.html', [
            "filter"      => $data,
            'start'       => $start,
            'end'       => $end,
            "laporan"     => $pc,
            // 'grand_total' => $grand_total,
            'session'     => $_SESSION['user']
        ]);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=Laporan-Surat-Pesanan.xls");
        echo $view;
      } else {
        return successResponse($response, [
          'laporan'     => $pc,
          // 'grand_total' => $grand_total
        ]);
      }

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }

});
