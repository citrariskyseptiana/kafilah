<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        "pertanyaan" => "required",
        "jawaban" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_faq/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("*")
            ->from("m_faq")
            ->where("is_deleted", "=", 0);

    if (isset($params['filter']) && !empty($params['filter'])) {
        $db->customWhere("pertanyaan LIKE '%" . $params['filter'] . "%'", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, $models);
});

$app->post("/m_faq/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data["data"]);
    if ($validasi !== true)
        return unprocessResponse($response, $validasi);

    try {
        if (isset($data["data"]["id"])) {
            $model = $db->update("m_faq", $data["data"], ["id" => $data["data"]["id"]]);
        } else {
            $model = $db->insert("m_faq", $data["data"]);
        }

        //insert notif
        $model_member = $db->select("id")->from("m_member")->where("status", "=", "aktif")->findAll();
        $arr_member = [];
        foreach ($model_member as $key => $value) {
            $arr_member[] = $value->id;
        }
        insertNotif('m_faq', 'Ada pertanyaan baru', $arr_member);
        //end insert notif

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
/**
 * Save status m promo
 */
$app->post("/m_faq/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_faq", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
