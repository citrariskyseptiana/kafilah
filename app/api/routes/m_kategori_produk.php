<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
       "kode"  => "required",
       "nama"  => "required",
     );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua m kategori produk
 */
$app->get("/m_kategori_produk/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_kategori_produk");
            /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m kategori produk
 */
$app->post("/m_kategori_produk/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {
                $model = $db->update("m_kategori_produk", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_kategori_produk", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m kategori produk
 */
$app->post("/m_kategori_produk/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_kategori_produk", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_kategori_produk/kode", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $cekKode = $db->select("kode")
          ->from("m_kategori_produk")
          ->orderBy("id DESC")
          ->find();

      if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
      } else {
        $kode_terakhir = 0;
      }
      $kode_cust = (substr($kode_terakhir, -3) + 1);
      $kodeCust  = substr('000' . $kode_cust, strlen($kode_cust));
      $kodeCust  = 'KGP' . $kodeCust;

      return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/m_kategori_produk/kategori", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $list = $db->select("*")
          ->from("m_kategori_produk")
          ->where("is_deleted", "=", 0)
          ->findAll();
      $result=[];
      foreach ($list as $key => $value) {
        $result[$key] = array_map("strval", (array) $value);
      }
      return successResponse($response, $list);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
