<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
                     "nik"  => "required",
                     "kode"  => "required",
                     "nama"  => "required",
                     "jenkel"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua m member
 */
$app->get("/m_pengajuan/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("
    m_member.*,
    m_level.id level_id,
    m_level.no_urut level_urut,
    m_level.nama level_nama
    ")
->from("m_member")
->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
->where("tipe_member", "=", "Member")
->andWhere("m_member.status", "=", 'menunggu persetujuan')
->andWhere("m_member.is_deleted", "=", 0);
            /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();

    if( !empty($models) ){
      $banks         = getAllBank();
      // Get All Provinsi, Kota, Kec, Desa
      $listDesa = dataSortID('w_desa');
      $listKecamatan = dataSortID('w_kecamatan');
      $listKota = dataSortID('w_kota');
      $listProvinsi = dataSortID('w_provinsi');


      foreach ($models as $key => $value) {
        $models[$key]->bank        = isset( $banks[ $models[$key]->bank ] ) ? $banks[ $models[$key]->bank ] : NULL;
        $models[$key]->reff_id = $db->find("SELECT id, nama FROM m_member WHERE id ={$value->reff_id}");
        $models[$key]->w_desa = isset($listDesa[$value->w_desa_id]) ? $listDesa[$value->w_desa_id] : NULL;
        $models[$key]->w_kecamatan = isset($listKecamatan[ $value->w_desa['kecamatan_id'] ]) ? $listKecamatan[ $value->w_desa['kecamatan_id'] ] : NULL;
        $models[$key]->w_kota = isset($listKota[ $value->w_kecamatan['kota_id'] ]) ? $listKota[ $value->w_kecamatan['kota_id'] ] : NULL;
        $models[$key]->w_provinsi = isset($listProvinsi[ $value->w_kota['provinsi_id'] ]) ? $listProvinsi[ $value->w_kota['provinsi_id'] ] : NULL;

        // Inisilisasi link
        $models[$key]->link_ktp      = isset($value->file_ktp) ? $value->file_ktp : null;
        $models[$key]->link_npwp     = isset($value->file_npwp) ? $value->file_npwp : null;
        $models[$key]->link_tabungan = isset($value->file_tabungan) ? $value->file_tabungan : null;
        unset($models[$key]->file_ktp);
        unset($models[$key]->file_npwp);
        unset($models[$key]->file_tabungan);
        unset($models[$key]->password);
      }
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m member
 */
$app->post("/m_pengajuan/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {
                $model = $db->update("m_member", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_member", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m member
 */
$app->post("/m_pengajuan/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_member", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
