<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "kode"          => "required",
        "nama"          => "required",
        "url"         => "required",
        "m_kategori_id" => "required",
        "jenis"         => "required",
        "berat"         => "required",
        "harga_jual"    => "required",
        "satuan"        => "required",
        "poin"          => "required",
    );

    GUMP::set_field_name("url", "URL Alias");
    GUMP::set_field_name("m_kategori_id", "Kategori Produk");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semua m produk
 */
$app->get("/m_produk/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("
      m_kategori_produk.id as kategori_id,
      m_kategori_produk.kode as kategori_kode,
      m_kategori_produk.nama as kategori,
      w_kecamatan.*,
      w_kecamatan.id as kecamatan_id,
      w_kecamatan.is_deleted as kecamatan_is_deleted,
      m_produk.*,
      w_kota.id as kota_asal_id,
      w_kota.kota as kota_asal_kota,
      w_kota.ro_kota_id as kota_asal_ro_kota_id,
      w_kota.provinsi_id as kota_asal_provinsi_id,
      w_provinsi.provinsi as kota_asal_provinsi
    ")
    ->from("m_produk")
    ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
    ->leftJoin("w_kota", "w_kota.id = m_produk.kota_id")
    ->leftJoin("w_kecamatan", "w_kecamatan.ro_subdistrict_id = m_produk.ro_subdistrict_id")
    ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id");
        // ->where("m_produk.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
        $models[$key]->m_kategori_id = [
            "id"   => $value->kategori_id,
            "kode" => $value->kategori_kode,
            "nama" => $value->kategori,
        ];

        $models[$key]->kota_asal = [
            "id"          => $value->kota_asal_id,
            "provinsi_id" => $value->kota_asal_provinsi_id,
            "kota"        => $value->kota_asal_kota,
            "ro_kota_id"  => $value->kota_asal_ro_kota_id,
            "provinsi"    => $value->kota_asal_provinsi,
        ];

        $models[$key]->kecamatan_asal = [
            "id"                => $value->kecamatan_id,
            "kota_id"           => $value->kota_id,
            "kecamatan"         => $value->kecamatan,
            "kode_distrik_sc"   => $value->kode_distrik_sc,
            "ro_subdistrict_id" => $value->ro_subdistrict_id,
            "is_deleted"        => $value->is_deleted,
        ];
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m produk
 */
$app->post("/m_produk/save", function ($request, $response) {
    $db = $this->db;

    $data                          = $request->getParams();
    $data['form']["m_kategori_id"] = isset($data['form']["m_kategori_id"]["id"]) ? $data['form']["m_kategori_id"]["id"] : null;

    $validasi = validasi($data['form']);
    if ($validasi !== true)
      return unprocessResponse($response, $validasi);

    try {
        $data['form']['ro_kota_id']         = !empty($data['form']['kota_asal']) ? $data['form']['kota_asal']['ro_kota_id'] : NULL;
        $data['form']['ro_subdistrict_id']  = !empty($data['form']['kecamatan_asal']) ? $data['form']['kecamatan_asal']['ro_subdistrict_id'] : NULL;
        $data['form']['kota_id']            = !empty($data['form']['kota_asal']) ? $data['form']['kota_asal']['id'] : NULL;

        if (isset($data['form']["id"])) {
            $model = $db->update("m_produk", $data['form'], ["id" => $data['form']["id"]]);
        } else {
            $data['form']['is_deleted'] = 0;
            $model                      = $db->insert("m_produk", $data['form']);
        }

        $kategori = $db->find("SELECT * FROM m_kategori_produk WHERE id = {$model->m_kategori_id}");
        $model->m_kategori_id   = !empty($kategori) ? $kategori : [];
        $model->kota_asal       = !empty($data['form']['kota_asal']) ? $data['form']['kota_asal'] : NULL;
        $model->kecamatan_asal  = !empty($data['form']['kecamatan_asal']) ? $data['form']['kecamatan_asal'] : NULL;

        return successResponse($response, ['form' => $model]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
/**
 * Save status m produk
 */
$app->post("/m_produk/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $model = $db->update("m_produk", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/m_produk/kode", function ($request, $response) {
    $db = $this->db;

    try {
        $cekKode = $db->select("kode")
            ->from("m_produk")
            ->orderBy("id DESC")
            ->find();

        if ($cekKode) {
            $kode_terakhir = $cekKode->kode;
        } else {
            $kode_terakhir = 0;
        }
        $kode_cust = (substr($kode_terakhir, -5) + 1);
        $kodeCust  = substr('00000' . $kode_cust, strlen($kode_cust));
        $kodeCust  = 'PRD' . $kodeCust;

        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/m_produk/getBulk/{id}", function ($request, $response) {
    $db          = $this->db;
    $m_produk_id = $request->getAttribute('id');

    try {
        $listBulk = $db->select("*")
            ->from("m_big_order")
            ->where("m_produk_id", "=", $m_produk_id)
            ->andWhere("is_deleted", "=", "0")
            ->orderBy("id DESC")
            ->findAll();

        if (!empty($listBulk)) {
            foreach ($listBulk as $key => $value) {
                if ($value->jenis_diskon == 'gratis barang') {
                    $listBulk[$key]->m_produk_gratis = $db->find("SELECT * FROM m_produk WHERE id = {$value->m_produk_gratis_id}");
                }
            }
        }

        return successResponse($response, $listBulk);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/m_produk/getProduk", function ($request, $response) {
    $db     = $this->db;
    $params = $request->getParams();

    try {
      if( !empty($params['promo']) && $params['promo'] == 'aktif' ){
        $db->select("m_produk.*,  m_promo.nama as promo, m_promo_det.diskon")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0);

      } else {
        $db->select("
          m_produk.id,
          m_produk.kode,
          m_produk.nama,
          m_produk.harga_jual,
          m_produk.harga_jual as harga_asli
        ")
        ->from("m_produk");

        if( !empty($params['is_deleted']) && $params['is_deleted'] == 'all'){

        } else {
          $db->where("m_produk.is_deleted", "=", 0);
        }
      }

        if( isset($params['nama']) && !empty($params['nama']) ){
          $db->customWhere('m_produk.nama LIKE "%' . $params['nama'] . '%" OR m_produk.kode LIKE "%' . $params['nama'] . '%"', 'AND');
        }

        if( isset($params['jenis_pesanan']) && !empty($params['jenis_pesanan']) ){
          $db->andWhere("m_produk.jenis", "=", $params['jenis_pesanan']);
        }

        $db->orderBy("m_produk.nama DESC")
            ->limit(15);

        $listProduk = $db->findAll();

        foreach ($listProduk as $key => $value) {
          if( !empty($value->diskon) ){
            $listProduk[$key]->diskon_persen  = $value->diskon;
            $listProduk[$key]->diskon         = round(($value->diskon/100) * $value->harga_jual);
          }
        }

        return successResponse($response, $listProduk);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post('/m_produk/upload', function ($request, $response) {
    $sql          = $this->db;
    $data         = $request->getParams();
    print_r($data);die;
    $data['file'] = isset($_FILES['file']) ? $_FILES['file'] : '';
    $img_path     = __DIR__;
    $img_path     = substr($img_path, 0, strpos($img_path, "api")) . "img/produk/";


    if (!empty($data['file'])) {
        $tempPath = $data['file']['tmp_name'];
        $riwfile  = $sql->find("SELECT id FROM m_produk_img ORDER BY id DESC");
        $gid      = (isset($riwfile->id)) ? $riwfile->id + 1 : 1;
        $newName  = $gid . "_" . urlParsing($data['file']['name']);

        $folder     = $img_path . $data['id'];
        $uploadPath = $folder . DIRECTORY_SEPARATOR . $newName;

        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }

        move_uploaded_file($tempPath, $uploadPath);
        $file = $uploadPath;
        if (file_exists($file)) {
            $check  = $sql->find("select * from m_produk_img where m_produk_id = '" . $data['id'] . "'");
            $answer = [
                'answer'      => 'File transfer completed',
                'foto'        => $newName,
                'id'          => $gid,
                'm_produk_id' => $data['id'],
                'is_primary'  => (empty($check)) ? 1 : 0,
            ];

            $data = [
                'id'          => $gid,
                'm_produk_id' => $data['id'],
                'foto'        => $newName,
                'is_primary'  => (empty($check)) ? 1 : 0,
            ];
            $create_file = $sql->insert('m_produk_img', $data);

            return successResponse($response, $answer);
        }
    } else {
        echo 'No files';
    }
});

$app->get("/m_produk/getFoto", function ($request, $response) {
    $db     = $this->db;
    $params = $request->getParams();

    try {
        $listFoto = $db->select("*")
            ->from("m_produk_img")
            ->where("m_produk_id", "=", $params['id'])
            ->findAll();

        return successResponse($response, $listFoto);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post('/m_produk/hapusFoto', function ($request, $response) {
    $db   = $this->db;
    $data = $request->getParams();

    try {
        $hapusFoto = $db->delete("m_produk_img", ['id' => $data['id']]);

        // Hapus file
        $img_path = __DIR__;
        $img_path = substr($img_path, 0, strpos($img_path, "api")) . "img/produk/";
        unlink($img_path . $data["m_produk_id"] . "/" . $data["foto"]);

        return successResponse($response, ['Berhasil menghapus foto!']);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post('/m_produk/setPrimary', function ($request, $response) {
    $db   = $this->db;
    $data = $request->getParams();

    try {
        $hapusFoto  = $db->update("m_produk_img", ['is_primary' => 1], ['id' => $data['id']]);
        $updateFoto = $db->run("UPDATE m_produk_img SET is_primary=0 WHERE m_produk_id ={$data['m_produk_id']} AND id != {$data['id']}");

        return successResponse($response, ['Berhasil diset sebagai primary!']);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post('/m_produk/setBestSeller', function ($request, $response) {
    $db   = $this->db;
    $data = $request->getParams();

    try {
      $value = $data['is_best_seller'] == 1 ? 0 : 1;
      $setBestSeller  = $db->update("m_produk", ['is_best_seller' => $value], ['id' => $data['id']]);

      return successResponse($response, $setBestSeller);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
$app->get("/m_produk/getKota", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $listKota = $db->select("w_kota.*, w_provinsi.provinsi")
            ->from("w_kota")
            ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id")
            ->findAll();

        return successResponse($response, $listKota);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/m_produk/getKotaSC", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $client = new \GuzzleHttp\Client();
        $url    = "https://api.sentralcargo.co.id/api/origin";
        $cost   = $client->request('GET', $url);
        $model  = $cost->getBody()->getContents();
        $models = json_decode($model, true);

        return successResponse($response, $models);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
