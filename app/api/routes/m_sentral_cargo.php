<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "kode_distrik_sc"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m promo
 */
$app->get("/m_sentral_cargo/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("
      provinsi, kota, w_kecamatan.*
    ")
      ->from("w_kecamatan")
      ->join("LEFT JOIN", "w_kota", "w_kota.id = w_kecamatan.kota_id")
      ->join("LEFT JOIN", "w_provinsi", "w_kota.provinsi_id = w_provinsi.id")
      ->where("w_kecamatan.is_deleted", "=", 0);

            /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $db->orderBy("w_kecamatan.kode_distrik_sc ASC, kecamatan ASC");

    $models    = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

$app->post("/m_sentral_cargo/save", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    try{
      $valid = validasi($params);
      if( $valid !== true ){
        return unprocessResponse($response, $valid);
      }

      $update = $db->update("w_kecamatan", ['kode_distrik_sc' => $params['kode_distrik_sc'] ], ['id' => $params['id'] ]);
      return successResponse($response, $models);
    } catch(Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }

});

function validasiTarif($data, $custom = array()){
  $validasi = array(
   "kode_distrik_sc"  => "required",
  );
  $cek = validate($data, $validasi, $custom);
  return $cek;
}

$app->get("/m_sentral_cargo/cekTarif", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    try{
      // UPG20700
      // PKU21203
      
      $models = json_decode(getTarifSentralCargo('Jakarta', 'UPG20700', 10), true);

      return successResponse($response, $models);
    } catch(Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . getErrorDesc($e)]);
    }

});
