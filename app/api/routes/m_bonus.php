<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "kode"  => "required",
             "nama"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil detail m promo
 */
$app->get("/m_bonus/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from("m_bonus");
    $models     = $db->findAll();

    $m_level = dataSortID("m_level");

    foreach ($models as $key => $value) {
      $models[$key]->m_level_id         = isset($m_level[$value->m_level_id]) ? $m_level[$value->m_level_id] : NULL;
      $models[$key]->m_level_id_member  = isset($m_level[$value->m_level_id_member]) ? $m_level[$value->m_level_id_member] : NULL;
    }

    $listDetail = [];
    foreach ($m_level as $key => $value) {
      foreach ($models as $lk => $lv) {
        if($lv->m_level_id['id'] == $key){
          $listDetail[$key][] = $lv;
        }
      }
    }

    return successResponse($response, $listDetail);
});

$app->get("/m_bonus/getLevel", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

        $db->select("*")
        ->from("m_level");
    $models    = $db->findAll();
    return successResponse($response, ['list'=>$models, 'totalItems'=> sizeof($models)]);
});
/**
 * Save m promo
 */
$app->post("/m_bonus/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $delete_old_setting = $db->run("DELETE FROM m_bonus WHERE 1");

        if(isset($data) && !empty($data)){
          // Increment berdasarkan level
          foreach ($data as $mkey => $mvalue) {

            if(!empty($mvalue)){
              // Increment berdasarkan list detail
              foreach($mvalue as $key => $val){
                $val["m_level_id"]        = $val['m_level_id']['id'];
                $val["m_level_id_member"] = isset($val['m_level_id_member']['id']) ? $val['m_level_id_member']['id'] : NULL;

                $db->insert("m_bonus", $val);
              }
            }

          }
        }

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
