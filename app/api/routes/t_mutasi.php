<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    // $validasi = [
    //     "m_level_id_baru" => "required",
    // ];

    if( in_array( $data['m_level_id_baru']['id'], [1,2] )  ){
      $validasi['status_qpa_baru'] = 'required';
    }

    GUMP::set_field_name("status_qpa_baru", "Status Member");
    GUMP::set_field_name("m_level_id_baru", "Level Member");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_mutasi/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("
            m_member.*,
            m_histori_level.status_qpa,
            m_level.id level_id,
            m_level.no_urut level_urut,
            m_level.nama level_nama
            ")
        ->from("m_member")
        ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
        ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
        ->where("tipe_member", "=", "Member")
        ->where("m_member.status", "=", "aktif")
        ->andWhere("m_member.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    if ($_SESSION['user']['tipe_member'] == 'Member') {
        $db->andWhere("reff_id", "=", $_SESSION['user']['id']);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    if (!empty($models)) {
        // Get All Provinsi, Kota, Kec, Desa
        foreach ($models as $key => $value) {
            $models[$key]->reff_id     = (!empty($value->reff_id)) ? $db->find("SELECT id, nama FROM m_member WHERE id ={$value->reff_id}") : [];
            unset($models[$key]->file_ktp);
            unset($models[$key]->file_npwp);
            unset($models[$key]->file_tabungan);
            unset($models[$key]->password);
        }
    }

    $listLevel = $db->select("*")
    ->from("m_level")
    ->findAll();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem, "listLevel"=>$listLevel]);
});

$app->post("/t_mutasi/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;

    try {
        $model = $db->update("m_member", ['status' => $data['status']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_mutasi/save", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;

    try {
      // Jika mengganti referal id
      $reff_id_lama = $db->select("
                        m_member.id as m_member_id,
                        m_member.reff_id,
                        m_jaringan.id as m_jaringan_id,
                        m_jaringan.parent_id
                        ")
                      ->from("m_member")
                      ->join("LEFT JOIN", "m_jaringan", "m_jaringan.m_member_id = m_member.id")
                      ->where("m_member.id", "=", $data['id'])
                      ->find();

      if( !empty($reff_id_lama) && $reff_id_lama->reff_id != $data['reff_id']['id'] ){
        // Temukan jaringan parent lama
        $jaringan_parent_lama = $db->select("*")
                            ->from("m_jaringan")
                            ->where("id", "=", $reff_id_lama->parent_id)
                            ->find();

        // Temukan jaringan parent baru
        $jaringan_parent_baru = $db->select("*")
                            ->from("m_jaringan")
                            ->where("m_member_id", "=", $data['reff_id']['id'])
                            ->find();

        // Update Reff ID member
        $update_reff_id  = $db->update("m_member",
          [
            'reff_id'   => $data['reff_id']['id']
          ],
          [ 'id'        => $data['id'] ]
        );
        // Update jaringan member
        $update_jaringan = $db->update("m_jaringan",
          [
            'parent_id' => $jaringan_parent_baru->id,
            'tree_id'   => $jaringan_parent_baru->tree_id,
          ],
          [ 'id'        => $reff_id_lama->m_jaringan_id ]
        );
      }
      // end of - Jika mengganti referal id

      // Jika mengganti Level Member / Status
      if( !empty($data['m_level_id_baru']) || !empty($data['status_qpa_baru']) ){
        // validasi
        $validasi = validasi($data);
        if ($validasi !== true)
          return unprocessResponse($response, $validasi);

        // Update history lama jadi non aktif
        $tanggal          = date("Y-m-d");
        $level_lama       = $db->update("m_histori_level", ['tgl_selesai' => $tanggal], ["id" => $data["m_histori_level_id"] ]);

        // Insert ke history
        $dataBaru = [
          "m_member_id"   => $data['id'],
          "m_level_id"    => $data['m_level_id_baru']['id'],
          "status_qpa"    => isset($data['status_qpa_baru']) ? $data['status_qpa_baru'] : "Beginner",
          "tgl_mulai"     => $tanggal,
        ];
        $level_baru       = $db->insert("m_histori_level", $dataBaru);

        // penentuan roles id
        if($level_baru->m_level_id == 1){
          $m_roles_id = 2;
        } else if($level_baru->m_level_id == 2) {
          $m_roles_id = 3;
        } else if($level_baru->m_level_id == 3) {
          $m_roles_id = 4;
        }

        // Update m_member
        $model = $db->update("m_member", [
          'm_histori_level_id' => $level_baru->id,
          'm_roles_id'         => $m_roles_id,
        ], ["id" => $level_baru->m_member_id ]);

        // print_r($level_lama);
        // print_r($level_baru);
        // die;

        /*
        * Update pohon jaringan
        **/
        if( $level_lama->m_level_id == 2 && $level_baru->m_level_id == 3 ){
          /* jika SQPA menjadi MQPA
          * - Buat tree baru
          * - set parent id = 0
          * - set tree_Id anaknya
          **/
          $tree_id = $db->select("(tree_id + 1) as new")->from("m_jaringan")
          ->orderBy("tree_id DESC")->find();

          $update_tree = $db->update("m_jaringan", [
            'parent_id' => 0,
            'tree_id'   => $tree_id->new,
          ], [ 'm_member_id'=>$data["id"] ]);

          $update_child = $db->update("m_jaringan", [
            'tree_id'   => $tree_id->new,
          ], [ 'parent_id' => $data["id"] ]);
        }

        if( $level_lama->m_level_id == 1 && $level_baru->m_level_id == 2 ){
          /* jika QPA menjadi SQPA dan Jabatan Parentnya adalah QPA, maka
          * - ambil parent_id dari parentnya
          * - set parent_id member ini dg parent_id yg baru
          **/

          $parent_member = $db->find("SELECT parent_id FROM m_jaringan WHERE m_member_id = {$data['id']}");

          if( $parent_member ){
            $level_parent = $db->select("
              m_histori_level.m_level_id,
              m_jaringan.parent_id
            ")
            ->from("m_jaringan")
            ->join("LEFT JOIN", "m_member", "m_member.id = m_jaringan.m_member_id")
            ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
            ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
            ->where("m_jaringan.id", "=", $parent_member->parent_id)
            ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
            ->find();

            if( isset($level_parent->m_level_id) && $level_parent->m_level_id == 1 ){

              $update_parent = $db->update("m_jaringan", [
                'parent_id' => $level_parent->parent_id,
              ], [ 'm_member_id'=>$data["id"] ]);

            }
          }
        }

      }
      return successResponse($response, $model);
      // end of - Jika mengganti Level Member / Status

    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
