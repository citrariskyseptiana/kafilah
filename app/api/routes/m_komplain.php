<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "kode"     => "required",
             "isi"      => "required",
             // "kategori" => "required",
            );

    if( isset($data['tipe']) && $data['tipe'] == 'post' ){
      $validasi['judul'] = 'required';
    }

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_komplain/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("
            m_komplain.*,
            m_member.nama,
            m_member.kode,
            m_member.email
          ")
          ->from("m_komplain")
          ->join("LEFT JOIN", "m_member", "m_member.id = m_komplain.m_member_id")
          ->where("tipe", "=", "post")
          ->andWhere("m_komplain.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $db->orderBy("id DESC");

    $models    = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {
      $models[$key]->is_closed = (int)$value->is_closed;
      $models[$key]->image = !empty($value->image) ? config("SITE_IMG") . "komplain/" . $value->image : NULL;
      // code...
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m promo
 */
$app->post("/m_komplain/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $file_path        = __DIR__ . "";
    $file_path        = substr($file_path, 0, strpos($file_path, "api")) . "img/forum/";

    $validasi = validasi($data["data"]);
    if ($validasi !== true)
      return unprocessResponse($response, $validasi);

    try {
      // Proses upload gambar ke server
      $images = get_images($data["data"]['isi']);

      foreach ($images as $key => $value) {
        if ( is_base64($value) ) {
          $img_file_name        = base64toImg( $value, $file_path );
          $img_url              = config("SITE_IMG") . 'forum/' . $img_file_name['data'];
          $data['data']['isi']  = str_replace($value, $img_url, $data['data']['isi']);
        }
      }
      // End Proses upload gambar ke server

        if (isset($data["data"]["id"])) {
            $model = $db->update("m_forum", $data["data"], ["id" => $data["data"]["id"]]);
        } else {
            $model = $db->insert("m_forum", $data["data"]);
        }

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }

});

$app->post("/m_komplain/saveReply", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {

      $param = [
        'tipe'         => "reply",
        'tanggal'      => date("Y-m-d"),
        'm_komplain_id'=> $data['m_komplain_id'],
        'isi'          => $data['reply'],
        'm_member_id'  => $_SESSION['user']['id'],
      ];

      $model = $db->insert("m_komplain", $param);

      return successResponse($response, $model);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e ]);
    }

});

$app->get("/m_komplain/listReply", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("
          m_komplain.*,
          m_member.nama,
          m_member.kode,
          m_member.email
        ")
        ->from("m_komplain")
        ->join("LEFT JOIN", "m_member", "m_member.id = m_komplain.m_member_id")
        ->where("tipe", "=", "reply")
        ->andWhere("m_komplain.m_komplain_id", "=", $params["m_komplain_id"])
        ->andWhere("m_komplain.is_deleted", "=", 0);
      /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $models    = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

$app->post("/m_komplain/close", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
        $model = $db->update("m_komplain", ['is_closed' => 1 ], ["id" => $data["id"] ]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
