<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "kode"  => "required",
             "nama"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil detail m promo
 */
$app->get("/m_afiliasi/member_url", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from("m_member")
        ->where("id", "=", $_SESSION['user']['id'] );

    $models     = $db->findAll();

    foreach ($models as $key => $value) {
      $models[$key]->url = config("SITE_URL") . "public/home?url=" . $value->kode;
    }

    return successResponse($response, $models);
});
/**
 * Ambil semua m promo
 */
$app->get("/m_afiliasi/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("m_produk.*,
          m_produk_img.foto,
          m_kategori_produk.id as kategori_id,
          m_kategori_produk.kode as kategori_kode,
          m_kategori_produk.nama as kategori,
          m_promo.nama as promo,
          m_promo_det.diskon
    ")
      ->from("m_produk")
      ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
      ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
      ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
      ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
      ->where("m_produk.is_deleted", "=", 0);

            /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();

    $models    = siapkan_produk($models);

    $kode_member = $db->find("SELECT kode FROM m_member WHERE id = ".$_SESSION['user']['id']);
    foreach ($models as $key => $value) {
      $models[$key]->url = config("SITE_URL") . "public/produk/" . $value->url ."?url=" . $kode_member->kode;
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
