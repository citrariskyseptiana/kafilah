<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "kode"  => "required",
             "nama"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil detail m promo
 */
$app->get("/m_promo/view", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
    $db->select("*")
        ->from("m_promo_det")
        ->where("m_promo_id", "=", $params["m_promo_id"]);
    $models     = $db->findAll();

    foreach ($models as $key => $value) {
      if( isset($value->m_produk_id) ){
        $models[$key]->m_produk_id = $db->find("SELECT * FROM m_produk WHERE id={$value->m_produk_id}");
      }
    }

    return successResponse($response, $models);
});
/**
 * Ambil semua m promo
 */
$app->get("/m_promo/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
            $db->select("*")
        ->from("m_promo")
        ->where("is_deleted", "=", 0);
            /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m promo
 */
$app->post("/m_promo/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "api")) . "img/promo";

    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            if (isset($data["data"]["id"])) {
                $model = $db->update("m_promo", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("m_promo_det", ["m_promo_id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("m_promo", $data["data"]);
            }
            /**
             * Simpan detail
             */
            if(isset($data["detail"]) && !empty($data["detail"])){
                foreach($data["detail"] as $key => $val){
                  $detail = $simpan_file = [];
                  $detail["m_produk_id"]  = $val['m_produk_id']['id'];
                  $detail["diskon"]       = isset($val["diskon"]) ? $val["diskon"] : '';
                  $detail["m_promo_id"]   = $model->id;
                  $detail['foto']         = $val['foto'];

                  if (isset($val['banner']) && is_array($val['banner'])) {
                    $namaFile     = $model->id .'-'. strtotime("now") . $key;
                    $simpan_file  = saveBase64($val['banner'], $file_path, $namaFile);
                    $detail['foto'] = $simpan_file['fileName'];
                  }
                  $insertDetail = $db->insert("m_promo_det", $detail);

                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m promo
 */
$app->post("/m_promo/saveStatus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_promo", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_promo/kode", function ($request, $response) {
    $db       = $this->db;

    try {
      $cekKode = $db->select("kode")
          ->from("m_promo")
          ->orderBy("id DESC")
          ->find();

      if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
      } else {
        $kode_terakhir = 0;
      }
      $kode_cust = (substr($kode_terakhir, -5) + 1);
      $kodeCust  = substr('00000' . $kode_cust, strlen($kode_cust));
      $kodeCust  = 'PMO'. date("Y") . $kodeCust;

      return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/m_promo/setPromo", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      if($data['is_used'] == 1){
        $model = $db->update("m_promo", ['is_used'=>1], ["id" => $data["id"]]);
        $update = $db->run('UPDATE m_promo SET is_used = 0 WHERE id !='.$data["id"]);
      } else {
        $model = $db->update("m_promo", ['is_used'=>0], ["id" => $data["id"]]);
      }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e ]);
    }

});
