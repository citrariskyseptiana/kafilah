<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = [
        "nik" => "required",
        "kode" => "required",
        "nama" => "required",
        "email" => "required|valid_email",
        // "jenkel"        => "required",
        "tanggal_lahir" => "required",
        "bank" => "required",
        "bank_an" => "required",
        "bank_rekening" => "required",
    ];
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m member
 */
$app->get("/m_member/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
            m_member.*,
            m_histori_level.status_qpa,
            m_level.id level_id,
            m_level.no_urut level_urut,
            m_level.nama level_nama
            ")
        ->from("m_member")
        ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
        ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
        ->where("tipe_member", "=", "Member")
        ->andWhere("m_member.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    if ($_SESSION['user']['tipe_member'] == 'Member') {
        $db->andWhere("reff_id", "=", $_SESSION['user']['id']);
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    if (!empty($models)) {
        $banks = getAllBank();
        // Get All Provinsi, Kota, Kec, Desa
        $listDesa = dataSortID('w_desa');
        $listKecamatan = dataSortID('w_kecamatan');
        $listKota = dataSortID('w_kota');
        $listProvinsi = dataSortID('w_provinsi');

        foreach ($models as $key => $value) {
            $models[$key]->bank = isset($banks[$models[$key]->bank]) ? $banks[$models[$key]->bank] : NULL;

            $models[$key]->reff_id = (!empty($value->reff_id)) ? $db->find("SELECT id, nama FROM m_member WHERE id ={$value->reff_id}") : [];
            $models[$key]->w_desa = isset($listDesa[$value->w_desa_id]) ? $listDesa[$value->w_desa_id] : null;
            $models[$key]->w_kecamatan = isset($listKecamatan[$value->w_desa['kecamatan_id']]) ? $listKecamatan[$value->w_desa['kecamatan_id']] : null;
            $models[$key]->w_kota = isset($listKota[$value->w_kecamatan['kota_id']]) ? $listKota[$value->w_kecamatan['kota_id']] : null;
            $models[$key]->w_provinsi = isset($listProvinsi[$value->w_kota['provinsi_id']]) ? $listProvinsi[$value->w_kota['provinsi_id']] : null;
            // Inisilisasi link
            $models[$key]->link_ktp = isset($value->file_ktp) ? $value->file_ktp : null;
            $models[$key]->link_npwp = isset($value->file_npwp) ? $value->file_npwp : null;
            $models[$key]->link_tabungan = isset($value->file_tabungan) ? $value->file_tabungan : null;
            unset($models[$key]->file_ktp);
            unset($models[$key]->file_npwp);
            unset($models[$key]->file_tabungan);
            unset($models[$key]->password);
            $models[$key]->m_roles_id = (string)$models[$key]->m_roles_id;
        }
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m member
 */
$app->post("/m_member/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

//    pd($data);

    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "routes")) . "file/member/";
    $data["username"] = isset($data["username"]) ? $data["username"] : '';
    $data["email"] = isset($data["email"]) ? $data["email"] : '';
    $data["password"] = isset($data["password"]) ? $data["password"] : '';
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            /** Cek email, nik, username apakah duplikat? */
            $db->select("id, nama, kode")
                ->from("m_member")
                ->customWhere("email ='" . $data["email"] . "' OR " . "nik ='" . $data["nik"] . "' OR " . "username ='" . $data["username"] . "'")
                ->where("tipe_member", "=", "Member")
                ->andWhere("is_deleted", "=", 0);
            if (isset($data["id"])) {
                $db->andWhere("m_member.id", "!=", $data["id"]);
            }
            $duplikat = $db->find();
            if (!empty($duplikat)) {
                return unprocessResponse($response, ["NIK / Email / Username sudah terdaftar! Gunakan NIK / Email / Username lain! " . $duplikat->nama . "($duplikat->kode)"]);
            }

            // Menyiapkan data bank
            $data['bank_nama'] = $data['bank']['name'];
            $data['bank'] = $data['bank']['code'];

            // Menyiapkan data alamat
            $data['w_desa_id'] = isset($data['w_desa']['id']) ? $data['w_desa']['id'] : null;
            $data['tanggal_lahir'] = isset($data['tanggal_lahir']) ? date("Y-m-d", strtotime($data['tanggal_lahir'])) : null;
            $tahun_lahir = isset($data['tanggal_lahir']) ? date("Y", strtotime($data['tanggal_lahir'])) : null;
            $data['tanggal_blokir'] = isset($data['tanggal_blokir']) ? date("Y-m-d", strtotime($data['tanggal_blokir'])) : null;
            $data['tipe_member'] = "Member";

            // File data ditampung di var temp
            $temp_file = [];
            if (isset($data['file_ktp']) && is_array($data['file_ktp'])) {
                $temp_file['file_ktp'] = $data['file_ktp'];
                unset($data['file_ktp']);
            }
            if (isset($data['file_npwp']) && is_array($data['file_npwp'])) {
                $temp_file['file_npwp'] = $data['file_npwp'];
                unset($data['file_npwp']);
            }
            if (isset($data['file_tabungan']) && is_array($data['file_tabungan'])) {
                $temp_file['file_tabungan'] = $data['file_tabungan'];
                unset($data['file_tabungan']);
            }
            /**
             * Menyiapkan Reff ID
             * Jika admin maka reff id sesuai pilihan Admin
             * jika member maka reff = id member
             */
            if (isset($data["id"])) {
                if (!empty($data["password"])) {
                    $data["password"] = sha1($data["password"]);
                } else {
                    unset($data["password"]);
                }
                unset($data['reff_id']);
                $model = $db->update("m_member", $data, ["id" => $data["id"]]);
            } else {
                // Create
                $data['kode'] = gen_kode_member();

                if ($_SESSION['user']['tipe_member'] == "Admin") {
                    $data['reff_id'] = isset($data['reff_id']['id']) ? $data['reff_id']['id'] : null;
                    $data['status'] = 'aktif';
                    /** Default password adalah "kode{tahun lahir}" */
                    $data['username'] = $data['kode'];
                    $data["password"] = sha1($data['kode'] . $tahun_lahir);
                    $model = $db->insert("m_member", $data);
                    // Send Email Konfirmasi
                    $data['password'] = $data['kode'] . date("Y", strtotime($data['tanggal_lahir']));
                    $view = $this->view->fetch('template_email/konfirmasi.html', [
                        'SITE_IMG' => config("SITE_IMG"),
                        'model' => $data,
                    ]);
                    $email_send = sendMail("Konfirmasi Pendaftaran Member", $data['nama'], $data['email'], $view);

                    //insert notif
                    $model_member = $db->select("id")->from("m_member")->where("tipe_member", "=", "Admin")->where("status", "=", "aktif")->findAll();
                    $arr_member = [];
                    foreach ($model_member as $key => $value) {
                        $arr_member[] = $value->id;
                    }
                    if (isset($data['reff_id']) && !empty($data['reff_id'])) {
                        $arr_member[] = $data['reff_id'];
                    }
                    insertNotif('m_member', 'Ada member baru', $arr_member);
                    //end insert notif
                } else {
                    $data['reff_id'] = $_SESSION['user']['id'];
                    $data['username'] = $data['kode'];
                    $data['status'] = 'menunggu persetujuan';
                    $model = $db->insert("m_member", $data);
                }
                /** Simpan jaringan */
                createNode($model->id, $data["reff_id"]);
                /** End simpan jaringan */
                $histori_data = [
                    "m_level_id" => 1,
                    "m_member_id" => $model->id,
                    "status_qpa" => "Beginner",
                    "tgl_mulai" => date("Y-m-d"),
                ];
                $histori = $db->insert("m_histori_level", $histori_data);
                // Update m_histori_lvl dan m_roles_id
                $upd_model = $db->update("m_member", ["m_histori_level_id" => $histori->id, "m_roles_id" => 2], ["id" => $model->id]);
            }
            /** Simpan file Member */
            $data_file = [];
            foreach ($temp_file as $key => $val) {
                // Cek apakah member mng-upload file?
                if (strpos($key, 'file') !== false && !empty($val)) {
                    $simpan_file = base64ToFile($val, $file_path);
                    $temp = explode(".", $simpan_file["fileName"]);
                    $newfilename = $key . '_' . $model->id . '.' . end($temp);
                    rename($simpan_file['filePath'], $file_path . $newfilename);
                    $data_file[$key] = $newfilename;
                }
            }
            // Update data file member
            if (!empty($data_file)) {
                $model = $db->update("m_member", $data_file, ["id" => $model->id]);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m member
 */
$app->post("/m_member/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->update("m_member", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
$app->get('/m_member/getMember', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();
    try {
        $db->select("*")
            ->from("m_member")
            ->where("tipe_member", "=", $params['tipe_member'])
            ->andWhere("is_deleted", "=", 0);
        if ($params['tipe_member'] == "Member") {
            $db->andWhere("status", "=", "aktif");
        }
        if ($params['tipe_member'] == "Reseller") {
            if (isset($params['m_member_id']) && !empty($params['m_member_id'])) {
                $db->andWhere("reff_id", "=", $params['m_member_id']);
            } else {
                $db->andWhere("reff_id", "=", $_SESSION['user']['id']);
            }
        }
        $db->customWhere("nama LIKE '%" . $params["nama"] . "%'", "AND");
        $listMember = $db->limit(15)->findAll();
        return successResponse($response, $listMember);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
$app->get("/m_member/profile", function ($request, $response) {
    $params = $request->getParams();
    $member_id = $_SESSION['user']['id'];
    $db = $this->db;
    $db->select("
            m_member.*,
            m_histori_level.status_qpa,
            m_level.id level_id,
            m_level.no_urut level_urut,
            m_level.nama level_nama
            ")
        ->from("m_member")
        ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")
        ->join("LEFT JOIN", "m_level", "m_level.id = m_histori_level.m_level_id")
        ->where("tipe_member", "=", "Member")
        ->andWhere("m_member.is_deleted", "=", 0)
        ->andWhere("m_member.id", "=", $member_id);
    /**
     * Filter
     */
    $models = $db->find();
    $totalItem = $db->count();

    if (!empty($models)) {
        $models->reff_id = (!empty($models->reff_id)) ? $db->find("SELECT id, nama FROM m_member WHERE id ={$models->reff_id}") : [];
        $models->w_desa = $db->find("SELECT * FROM w_desa WHERE id ='" . (isset($models->w_desa_id) ? $models->w_desa_id : '') . "'");
        $models->w_kecamatan = $db->find("SELECT * FROM w_kecamatan WHERE id ='" . (isset($models->w_desa->kecamatan_id) ? $models->w_desa->kecamatan_id : '') . "'");
        $models->w_kota = $db->find("SELECT * FROM w_kota WHERE id ='" . (isset($models->w_kecamatan->kota_id) ? $models->w_kecamatan->kota_id : '') . "'");
        $models->w_provinsi = $db->find("SELECT * FROM w_provinsi WHERE id ='" . (isset($models->w_kota->provinsi_id) ? $models->w_kota->provinsi_id : '') . "'");
        unset($models->password);

        // Inisilisasi link
        $models->link_ktp = isset($models->file_ktp) ? $models->file_ktp : null;
        $models->link_npwp = isset($models->file_npwp) ? $models->file_npwp : null;
        $models->link_tabungan = isset($models->file_tabungan) ? $models->file_tabungan : null;

        $models->bank = [
            'code' => $models->bank,
            'name' => $models->bank_nama,
        ];

        unset($models->file_ktp);
        unset($models->file_npwp);
        unset($models->file_tabungan);
        return successResponse($response, $models);
    } else {
        return successResponse($response, []);
    }
});
$app->post("/m_member/konfirmasi", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

//    pd($params);

    try {
        if ($params['status'] == 'terima') {
            $data = [
                "status" => "aktif",
                "username" => $params['form']['kode'],
                "password" => sha1($params['form']['kode'] . date("Y", strtotime($params['form']['tanggal_lahir']))),
                "approved_at" => strtotime("now"),
                "approved_by" => $_SESSION['user']['id'],
            ];
            $model = $db->update("m_member", $data, ["id" => $params['form']["id"]]);
            $data['password'] = $params['form']['kode'] . date("Y", strtotime($params['form']['tanggal_lahir']));
            $view = $this->view->fetch('template_email/konfirmasi.html', [
                'SITE_IMG' => config("SITE_IMG"),
                'model' => $data,
            ]);
            $email_send = sendMail("Konfirmasi Pendaftaran Member", $params['form']['nama'], $params['form']['email'], $view);

            //insert notif
            $model_member = $db->select("id")->from("m_member")->where("tipe_member", "=", "Admin")->where("status", "=", "aktif")->findAll();
            $arr_member = [];
            foreach ($model_member as $key => $value) {
                $arr_member[] = $value->id;
            }
            if (isset($data['form']['reff_id']) && !empty($data['form']['reff_id'])) {
                $arr_member[] = $data['form']['reff_id'];
            }
            insertNotif('m_member', 'Ada member baru', $arr_member);
            //end insert notif
        } else {
            $data = [
                "status" => "ditolak",
            ];
            $model = $db->update("m_member", $data, ["id" => $params['form']["id"]]);
        }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
$app->get("/m_member/kode", function ($request, $response) {
    $db = $this->db;
    try {
        $kodeCust = gen_kode_member();
        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
