<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "periode" => "required",
            );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/l_mutasi_moota/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi($data);
      if($validasi !== true)
        return unprocessResponse($response, $validasi);

        $db->select("t_histori_moota.*")
          ->from("t_histori_moota");

      if(isset($data["periode"]) && !empty($data["periode"])){
        $start  = date("Y-m-d", strtotime($data["periode"]["startDate"]));
        $end    = date("Y-m-d", strtotime($data["periode"]["endDate"]));

        $db->andWhere("t_histori_moota.date", ">=", $start);
        $db->andWhere("t_histori_moota.date", "<=", $end);
      }

      if(isset($data["jenis_mutasi"]) && !empty($data["jenis_mutasi"])){
        $db->andWhere("t_histori_moota.type", "=", $data["jenis_mutasi"]);
      }

      $laporan = $db->findAll();

      // Get List Bank yg terdaftar di Moota
      $getToken         = $db->find("SELECT moota_token FROM m_setting");
      $mootaToken       = !empty($getToken->moota_token) ? $getToken->moota_token: '';
      $client = new \GuzzleHttp\Client(['headers' => [
          'Authorization' => 'Bearer ' . $mootaToken,
          "Accept"        => 'application/json',
      ]]);

      $getBank   = $client->request('GET', 'https://app.moota.co/api/v1/bank');

      $getBank   = $getBank->getBody()->getContents();
      $getBank   = json_decode($getBank, true);

      $listBank = [];
      if( !empty($getBank) ){
        foreach ($getBank['data'] as $key => $value) {
          $listBank[ $value['bank_id'] ] = $value;
        }
      }
      // Get List Bank yg terdaftar di Moota - END

      foreach ($laporan as $key => $value) {
        $laporan[$key]->atas_nama = isset($listBank[ $value->bank_id ]) ? $listBank[ $value->bank_id ]['atas_nama'] : $value->atas_nama;
      }

      return successResponse($response, ['laporan'=>$laporan]);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server." . $e]);
    }
});
