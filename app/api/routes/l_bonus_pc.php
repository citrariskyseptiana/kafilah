<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "periode" => "required",
            );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/l_bonus_pc/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $validasi = validasi($data);
      if($validasi !== true)
        return unprocessResponse($response, $validasi);
        
      $res = personalCommission($data);

      return successResponse($response, [
        'laporan'     => $res['laporan'],
        'totalKomisi' => $res['totalKomisi'],
        ]);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
