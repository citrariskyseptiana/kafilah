<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "periode" => "required",
            );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/l_pesanan/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi($data);
      if($validasi !== true)
        return unprocessResponse($response, $validasi);

      $db->select("
          t_penjualan.*,
          (t_penjualan.total+t_penjualan.ongkir) as grand_total,
          m_member.nama as member_nama,
          m_member.no_hp as member_no_hp,
          m_reseller.nama as reseller_nama,
          m_reseller.no_hp as reseller_no_hp
        ")
        ->from("t_penjualan")
        ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id")
        ->join("LEFT JOIN", "m_member as m_reseller", "m_reseller.id = t_penjualan.reseller_id")
        ->join("LEFT JOIN", "t_penjualan_det", "t_penjualan.id = t_penjualan_det.t_penjualan_id")
        ->where("t_penjualan.is_deleted", "=", 0);

      if(isset($data["periode"]) && !empty($data["periode"])){
        if($data['export'] == 0) {
          $data["periode"] = json_decode($data["periode"], true);
        }

        $start  = date("Y-m-d", strtotime($data["periode"]["startDate"]));
        $end    = date("Y-m-d", strtotime($data["periode"]["endDate"]));

        $db->andWhere("t_penjualan.tanggal", ">=", $start);
        $db->andWhere("t_penjualan.tanggal", "<=", $end);
      }

      if(isset($data["member"]) && !empty($data["member"])){
        $data["member"] = json_decode($data["member"], true);

        $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
      }

      if(isset($data["reseller"]) && !empty($data["reseller"])){
        $db->andWhere("t_penjualan.reseller_id", "=", $data["reseller"]["id"]);
      }

      if(isset($data["status"]) && !empty($data["status"])){
        $db->andWhere("t_penjualan.status", "=", $data["status"]);
      }

      if(isset($data["produk"]) && !empty($data["produk"])){
        $db->andWhere("t_penjualan_det.m_produk_id", "=", $data["produk"]);
      }

      if($_SESSION['user']['tipe_member'] == "Member"){
        $db->andWhere("t_penjualan.m_member_id", "=", $_SESSION['user']['id']);
      }

      $db->groupBy("t_penjualan.id");

      $laporan = $db->findAll();

      $grand_total = 0;

      $listID = [];
      if( !empty($laporan) ){
        foreach ($laporan as $key => $value) {
          $listID[] = $value->id;
          $grand_total += $value->grand_total;
        }
      }

      if( !empty($listID) ){
        $listID = implode(",", $listID);
        $getProd = $db->select("m_produk.*, t_penjualan_det.jumlah, t_penjualan_det.t_penjualan_id")
          ->from("t_penjualan_det")
          ->join("LEFT JOIN", "m_produk", "t_penjualan_det.m_produk_id = m_produk.id")
          ->where("t_penjualan_det.jenis", "=", 'detail')
          ->customWhere("t_penjualan_id IN (". $listID .")", "AND")
          ->findAll();

        $listProduk=[];
        if( !empty($getProd) ){
          foreach ($getProd as $key => $value) {
            $listProduk[$value->t_penjualan_id][] = $value;
          }

          foreach ($laporan as $key => $value) {
            if( !empty($listProduk[$value->id]) ){
              $laporan[$key]->detailProduk = $listProduk[$value->id];
            }
          }
        }
      }

      if($data['export'] == 1) {

        $view = $this->view->fetch('laporan/l_pesanan.html', [
            "filter"      => $data,
            'start'       => $start,
            'end'       => $end,
            "laporan"     => $laporan,
            'grand_total' => $grand_total,
            'session'     => $_SESSION['user']
        ]);
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=Laporan-Surat-Pesanan.xls");
        echo $view;
      } else {

        return successResponse($response, ['laporan'=>$laporan, 'grand_total'=>$grand_total]);
      }

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
