<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "kode"  => "required",
             "nama"  => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m promo
 */
$app->get("/m_slider/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;
            $db->select("*")
        ->from("m_slider")
        ->where("is_deleted", "=", 0);
    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m promo
 */
$app->post("/m_slider/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    $file_path = __DIR__ . "";
    $file_path = substr($file_path, 0, strpos($file_path, "api")) . "img/slider";

    // $validasi = validasi($data["data"]);
    // if ($validasi === true) {
        try {
            /**
             * Simpan detail
             */

           $db->run("DELETE FROM m_slider WHERE true");
            if(isset($data["detail"]) && !empty($data["detail"])){

                foreach($data["detail"] as $key => $val){
                  $detail = $simpan_file = [];
                  $detail["keterangan"]   = $val['keterangan'];

                  if (isset($val['banner']) && is_array($val['banner'])) {
                    $namaFile     = strtotime("now") . $key;
                    $simpan_file  = saveBase64($val['banner'], $file_path, $namaFile);
                    $detail['foto'] = $simpan_file['fileName'];
                    // pd($detail);
                  } else {
                    $detail['foto']         = $val['foto'];
                  }
                  $insertSlider = $db->insert("m_slider", $detail);

                }
            }
            return successResponse($response, ['Berhasil']);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    // }
    // return unprocessResponse($response, $validasi);
});

$app->post("/m_slider/setPromo", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      if($data['is_used'] == 1){
        $model = $db->update("m_promo", ['is_used'=>1], ["id" => $data["id"]]);
        $update = $db->run('UPDATE m_promo SET is_used = 0 WHERE id !='.$data["id"]);
      } else {
        $model = $db->update("m_promo", ['is_used'=>0], ["id" => $data["id"]]);
      }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e ]);
    }

});
