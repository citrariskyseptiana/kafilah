<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = [
        "nik" => "required",
        "kode" => "required",
        "nama" => "required",
        "email" => "valid_email",
        "jenkel" => "required",
        "tanggal_lahir" => "required",
    ];
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m member
 */
$app->get("/m_reseller/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
            m_member.*
            ")
            ->from("m_member")
            ->where("tipe_member", "=", "Reseller")
            ->andWhere("m_member.is_deleted", "=", 0);
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if ($_SESSION['user']['tipe_member'] == 'Member') {
        $db->andWhere("reff_id", "=", $_SESSION['user']['id']);
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    if (!empty($models)) {
        // Get All Provinsi, Kota, Kec, Desa
        $listDesa = dataSortID('w_desa');
        $listKecamatan = dataSortID('w_kecamatan');
        $listKota = dataSortID('w_kota');
        $listProvinsi = dataSortID('w_provinsi');

        foreach ($models as $key => $value) {
            $models[$key]->reff_id = (!empty($value->reff_id)) ? $db->find("SELECT id, nama FROM m_member WHERE id ={$value->reff_id}") : [];
            $models[$key]->w_desa = isset($listDesa[$value->w_desa_id]) ? $listDesa[$value->w_desa_id] : NULL;
            $models[$key]->w_kecamatan = isset($listKecamatan[$value->w_desa['kecamatan_id']]) ? $listKecamatan[$value->w_desa['kecamatan_id']] : NULL;
            $models[$key]->w_kota = isset($listKota[$value->w_kecamatan['kota_id']]) ? $listKota[$value->w_kecamatan['kota_id']] : NULL;
            $models[$key]->w_provinsi = isset($listProvinsi[$value->w_kota['provinsi_id']]) ? $listProvinsi[$value->w_kota['provinsi_id']] : NULL;
        }
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m member
 */
$app->post("/m_reseller/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            /** Cek email, nik, username apakah duplikat? */
            $db->select("id")
                    ->from("m_member")
                    ->customWhere("email ='" . $data["email"] . "' OR " . "nik ='" . $data["nik"] . "'");

            if (isset($data["id"])) {
                $db->andWhere("m_member.id", "!=", $data["id"]);
            }

            $duplikat = $db->find();

            if (!empty($duplikat)) {
                return unprocessResponse($response, ["NIK / Email sudah terdaftar! Gunakan NIK / Email lain!"]);
            }

            // Menyiapkan data alamat
            $data['w_desa_id'] = isset($data['w_desa']['id']) ? $data['w_desa']['id'] : NULL;
            $data['reff_id'] = isset($data['reff_id']['id']) ? $data['reff_id']['id'] : NULL;

            $data['tanggal_lahir'] = isset($data['tanggal_lahir']) ? date("Y-m-d", strtotime($data['tanggal_lahir'])) : NULL;
            $data['tipe_member'] = "Reseller";

            if (isset($data["id"])) {
                $model = $db->update("m_member", $data, ["id" => $data["id"]]);
            } else {
                // Reff ID dari login session
                $data["reff_id"] = $_SESSION['user']['id'];
                $model = $db->insert("m_member", $data);

                //insert notif
                $model_member = $db->select("id")->from("m_member")->where("tipe_member", "=", "Admin")->where("status", "=", "aktif")->findAll();
                $arr_member = [];
                foreach ($model_member as $key => $value) {
                    $arr_member[] = $value->id;
                }
                $arr_member[] = $_SESSION['user']['id'];
                insertNotif('m_reseller', 'Ada reseller baru', $arr_member);
                //end insert notif
            }

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m member
 */
$app->post("/m_reseller/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_member", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_reseller/kode", function ($request, $response) {
    $db = $this->db;

    try {
        $cekKode = $db->select("kode")
                ->from("m_member")
                ->where("tipe_member", "=", "Reseller")
                ->orderBy("kode DESC")
                ->find();

        if ($cekKode) {
            $kode_terakhir = $cekKode->kode;
        } else {
            $kode_terakhir = 0;
        }
        $kode_cust = (substr($kode_terakhir, -5) + 1);
        $kodeCust = substr('00000' . $kode_cust, strlen($kode_cust));
        $kodeCust = 'RSL' . $kodeCust;

        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
