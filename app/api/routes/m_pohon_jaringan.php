<?php
$app->get("/m_pohon_jaringan/index", function ($request, $response) {
    $data       = $request->getParams();

    try {
      $pohonJaringan = get_pohon_jaringan();

      return successResponse($response, $pohonJaringan);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server."]);
    }
});
