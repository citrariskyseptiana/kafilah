<?php

$app->get("/l_tutup_bulan/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from("l_tutup_bulan");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
          if($key == 'periode'){
            $periode = date("Y", strtotime($val));
            $db->where('YEAR(bulan)', "=", $periode);
          } else {
            $db->where($key, "LIKE", $val);
          }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $val) {
      $val->is_special = in_array($val->id, [1,2,3,4]) ? true : false;
      $val->akses = json_decode($val->akses);
    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{

    $validasi = array(
      "periode" => "required",
    );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/l_tutup_bulan/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      if($data['tipe_laporan'] == 'laporan_manual'){
        $validasi = validasi($data);
        if($validasi !== true){
          return unprocessResponse($response, $validasi);
        }
      }

      if($data['tipe_laporan'] == 'laporan_otomatis') {
        $newDate = strtotime(date("Y-m-01"));
        $newDate = date("Y-m-d", strtotime("-1 month ", $newDate));

        $data['periode'] = $newDate;
      }

      /** Get Personal Commission */
      $pc = personalCommission($data);
      $pc = dataSortID2($pc['laporan']);

      /** Get Recruitment Commission */
      $rc = recruitmentCommission($data);
      $rc = dataSortID2( $rc['laporan'] );

      /** Get Monthly Reward */
      $mr = monthlyRewardCommission($data);
      $mr = dataSortID2( $mr['laporan'] );

      /** Get Member Commission Manager */
      $mcm_all      = memberManagerCommission($data);
      $mcm_res      = [];
      foreach ( $mcm_all['laporan']['listMQPA'] as $key => $value) {
        $mcm_res[ $value['data']['m_member_id'] ] = $value;
      }

      /** Get Member Commission Supervisor */
      $mcs = memberSupervisorCommission($data);
      $mcs = dataSortID2( $mcs['laporan'] );

      /** Get Supervisor poin */
      $poin_supervisor = getSupervisorPoin($data);

      // Filter tanggal
      $query_tgl = "";
      if(isset($data["periode"]) && !empty($data["periode"])){
        $start  = date("Y-m-01", strtotime($data["periode"]));
        $end    = date("Y-m-t", strtotime($data["periode"]));

        $query_tgl = "AND t_penjualan.tanggal_lunas >= '$start' AND t_penjualan.tanggal_lunas <= '$end'";
      }

      $db->select("
          m_member.id,
          m_member.kode,
          m_member.nama,
          m_member.bank_nama,
          m_member.bank_an,
          m_member.bank_rekening,
          m_level.nama as level,
          m_histori_level.status_qpa,
          m_histori_level.m_level_id,
          SUM(t_penjualan.poin_penjualan) as poin_penjualan,
          SUM(t_penjualan.total) total
        ")
        ->from("m_member")
        ->join("LEFT JOIN", "t_penjualan",
            "m_member.id = t_penjualan.m_member_id
            AND t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas')
            AND t_penjualan.is_deleted =0 " . $query_tgl)
        ->join("LEFT JOIN", "m_histori_level", "m_histori_level.id = m_member.m_histori_level_id")

        ->join("LEFT JOIN", "m_level", "m_histori_level.m_level_id = m_level.id")
        ->andWhere("m_member.status", "=", "Aktif")
        ->customWhere('m_histori_level.tgl_selesai IS NULL', "AND")
        ->andWhere("m_member.tipe_member", "=", "Member");

      if($_SESSION['user']['tipe_member'] == "Member"){
        $db->andWhere("t_penjualan.m_member_id", "=", $_SESSION['user']['id']);
      }

      if(isset($data["member"]) && !empty($data["member"])){
        $db->andWhere("t_penjualan.m_member_id", "=", $data["member"]["id"]);
      }

      $db->groupBy("m_member.id");
      $laporan = $db->findAll();

      $comSetting       = getCommissionSetting();
      $totalPenjualan   = 0;

      $totalKomisi      = [
        "QPA"   => 0,
        "SQPA"  => 0,
        "MQPA"  => 0,
      ];
      $lap_result =[
        "QPA"   => [],
        "SQPA"  => [],
        "MQPA"  => [],
      ];

      foreach ($laporan as $key => $value) {
        // + Personal Commission
        $value->total_komisi = 0;
        $value->total_komisi += isset($pc[$value->id]['komisi_nominal']) ? $pc[$value->id]['komisi_nominal'] : 0;
        $value->komisi['pc']        = isset($pc[$value->id]['komisi_nominal']) ? $pc[$value->id]['komisi_nominal'] : 0;
        $value->komisi_persen['pc'] = isset($pc[$value->id]['komisi_persen']) ? $pc[$value->id]['komisi_persen'] : 0;

        // + Poin Supervisor
        $value->poin['mcs'] = $poin_supervisor[$value->id];

        // + Monthly Reward
        $value->total_komisi += isset($mr[$value->id]['monthly_reward']) ? $mr[$value->id]['monthly_reward'] : 0;
        $value->komisi['mr'] = isset($mr[$value->id]['monthly_reward']) ? $mr[$value->id]['monthly_reward'] : 0;

        // + Member Commission Manager
        $value->total_komisi += isset($mcm_res[$value->id]['komisi_nominal']) ? $mcm_res[$value->id]['komisi_nominal'] : 0;
        $value->komisi['mcm'] = isset($mcm_res[$value->id]['komisi_nominal']) ? $mcm_res[$value->id]['komisi_nominal'] : 0;
        if( $value->m_level_id == 3 ){
          $value->poin['mcm']   = isset($mcm_res[$value->id]['total_poin']) ? $mcm_res[$value->id]['total_poin'] : 0;
        }
        if( $value->m_level_id != 3 ){
          // + Recruitment Commission Reward
          $value->total_komisi += isset($rc[$value->id]['komisi_qpa_b']) || isset($rc[$value->id]['komisi_qpa_q']) ? ($rc[$value->id]['komisi_qpa_b']+$rc[$value->id]['komisi_qpa_q']) : 0;
          $value->komisi['rc'] = isset($rc[$value->id]['komisi_qpa_b']) || isset($rc[$value->id]['komisi_qpa_q']) ? ($rc[$value->id]['komisi_qpa_b']+$rc[$value->id]['komisi_qpa_q']) : 0;

        }
        // + Member Commission Supervisor
        $value->total_komisi += isset($mcs[$value->id]['mcs_reward_nominal']) ? $mcs[$value->id]['mcs_reward_nominal'] : 0;
        $value->komisi['mcs'] = isset($mcs[$value->id]['mcs_reward_nominal']) ? $mcs[$value->id]['mcs_reward_nominal'] : 0;

        $lap_result[$value->level][]  = $value;
        $totalPenjualan               += $value->total;
        $totalKomisi[$value->level]   += $value->total_komisi;
      }

      // Update t_penjualan.is_tutup_bulan = true
      $is_tutup_bulan = $db->run("UPDATE t_penjualan
              SET is_tutup_bulan = 1
              WHERE status='Selesai'
              AND tanggal_lunas <= '$end'
              AND is_tutup_bulan = 0
        ");

      // Save detail to table l_tutup_bulan
      $data_tutup_bulan = simpan_tutup_bulan($data, $lap_result, $totalPenjualan, $totalKomisi);
      // End of Save detail to table l_tutup_bulan

      return successResponse($response, [
        'laporan'         => $data_tutup_bulan['laporan'],
        'totalPenjualan'  => $data_tutup_bulan['totalPenjualan'],
        'totalKomisi'     => $totalKomisi
        ]);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
})->setName('publicFrontend');

function simpan_tutup_bulan($filter, $laporan, $total, $komisi){
  $db     = config('DB');
  $db     = new Cahkampung\Landadb($db['db']);
  date_default_timezone_set("Asia/Jakarta");

  // - Periksa apakah sudah pernah tutup bulan pada bulan ini?
  $is_saved = $db->select("id")
    ->from("l_tutup_bulan")
    ->where("bulan", "=", date("Y-m-01", strtotime($filter['periode']) ) )
    ->find();

  if( !empty($is_saved) ){
    return get_tutup_bulan_data( $is_saved->id );
  }

  // Create utup Bulan
  $save_tb = $db->insert('l_tutup_bulan', [
    'bulan'             => date("Y-m-01", strtotime($filter['periode']) ),
    'total_penjualan'   => $total,
    'total_komisi_qpa'  => $komisi['QPA'],
    'total_komisi_sqpa' => $komisi['SQPA'],
    'total_komisi_mqpa' => $komisi['MQPA'],
  ]);

  $list_level = ['QPA', "SQPA", 'MQPA'];

  // Simpan TUtup Bulan
  foreach ($list_level as $level) {
    if(isset($laporan[$level])){
      // Simpan Det Tutup Bulan
      foreach ($laporan[$level] as $key => $value) {
        $save_tb_det = $db->insert('l_tutup_bulan_det', [
          'l_tutup_bulan_id'  => $save_tb->id,
          'm_member_id'       => $value->id,
          'nama'              => $value->nama,
          'bank_nama'         => $value->bank_nama,
          'bank_an'           => $value->bank_an,
          'bank_rekening'     => $value->bank_rekening,
          'm_level_id'        => $value->m_level_id,
          'level'             => $value->level,
          'status_qpa'        => $value->status_qpa,
          'total_penjualan'   => !empty($value->total) ? $value->total : 0,
          'poin_penjualan'    => !empty($value->poin_penjualan) ? $value->poin_penjualan : 0,
        ]);

        // Simpan Det Komisi
        foreach ($value->komisi as $kkom => $vkom) {
          $save_td_det_kom = $db->insert('l_tutup_bulan_det_kom', [
            'l_tutup_bulan_det_id'  => $save_tb_det->id,
            'jenis_komisi'          => $kkom,
            'nominal_komisi'        => $vkom,
          ]);


          if( $kkom=='pc' && !empty($value->komisi_persen['pc']) ){
            $update_komisi_pc = $db->run("UPDATE
              l_tutup_bulan_det_kom
              SET persen_komisi = {$value->komisi_persen['pc']}
              WHERE l_tutup_bulan_det_id = {$save_tb_det->id}
              AND jenis_komisi = 'pc'
              ");
          }
        }
        // End of - Simpan Det Komisi

        // Simpan Det Poin
        foreach ($value->poin as $kpoin => $vpoin) {
          $save_td_det_poin = $db->insert('l_tutup_bulan_det_poin', [
            'l_tutup_bulan_det_id'  => $save_tb_det->id,
            'jenis_poin'            => $kpoin,
            'poin'                  => $vpoin,
          ]);
        }
        // End of - Simpan Det Poin
      }
      // End of - Simpan Det Tutup Bulan
    }
  }
  // End of - Simpan TUtup Bulan

  return get_tutup_bulan_data( $save_tb->id );
}

function get_tutup_bulan_data($l_tutup_bulan_id){
  $db     = config('DB');
  $db     = new Cahkampung\Landadb($db['db']);
  date_default_timezone_set("Asia/Jakarta");

  $l_tutup_bulan = $db->select("*")->from("l_tutup_bulan")
    ->where("id", "=", $l_tutup_bulan_id)
    ->find();

  $l_tutup_bulan_det = $db->select("*")
    ->from("l_tutup_bulan_det")
    ->where("l_tutup_bulan_id", "=", $l_tutup_bulan_id)
    ->findAll();

  // Mengumpulkan list id detail tb
  $listIDKom = valueConcate($l_tutup_bulan_det, 'id');

  // Get detail komisi
  $l_tutup_bulan_det_kom = $db->select("*")->from("l_tutup_bulan_det_kom")
    ->customWhere("l_tutup_bulan_det_id IN(". $listIDKom .")")
    ->findAll();


  $detKomisi = [];
  foreach ($l_tutup_bulan_det_kom as $key => $value) {
    $detKomisi[$value->l_tutup_bulan_det_id][$value->jenis_komisi] = $value->nominal_komisi;
  }


  // Memformat susunan array
  $result = [];
  foreach ($l_tutup_bulan_det as $key => $value) {
    $param                = $value;
    $param->komisi        = isset($detKomisi[$value->id]) ? $detKomisi[$value->id] : [];
    $param->total_komisi  = isset($detKomisi[$value->id]) ? array_sum($detKomisi[$value->id]) : [];
    $result[$value->level][] = $param;
  }

  return [
    'laporan'         => $result,
    'totalPenjualan'  => $l_tutup_bulan->total_penjualan,
    'totalKomisi'     => [
      'QPA'   => $l_tutup_bulan->total_komisi_qpa,
      'SQPA'  => $l_tutup_bulan->total_komisi_sqpa,
      'MQPA'  => $l_tutup_bulan->total_komisi_mqpa,
    ],
  ];
}
