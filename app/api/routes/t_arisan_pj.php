<?php

function validasi($data, $custom = array()) {
    $validasi = array(
        "kode" => "required",
        "nama" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_arisan_pj/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
      t_arisan_peserta.*,
      m_member.id as m_member_id,
      m_member.nama as m_member_nama
    ")
    ->from("t_arisan_peserta")
    ->join("LEFT JOIN", "m_member", "m_member.id = t_arisan_peserta.m_member_id")
    ->where("t_arisan_id", "=", $params["t_arisan_id"]);
    $models = $db->findAll();

    if (!empty($models)) {
        $listIDpeserta = valueConcate($models, 'id');

        $listDet = $db->select("
          t_arisan_det.*,
          m_produk.id as produk_id,
          m_produk.nama as produk_nama,
          m_produk.kode as produk_kode,
          m_produk.harga_jual as harga
        ")
        ->from("t_arisan_det")
        ->join("LEFT JOIN", "m_produk", "m_produk.id = t_arisan_det.m_produk_id")
        ->customWhere("t_arisan_det.t_arisan_peserta_id IN($listIDpeserta)")
        ->findAll();

        foreach ($listDet as $key => $value) {
            $listDet[$key]->m_produk_id = [
                'id' => $value->produk_id,
                'nama' => $value->produk_nama,
                'kode' => $value->produk_kode,
            ];
        }

        foreach ($models as $key => $value) {
          if( !empty($value->m_member_id) ){
            $models[$key]->m_member_id = [
              'id'      => $value->m_member_id,
              'nama'    => $value->m_member_nama,
            ];
          }

            foreach ($listDet as $kdey => $dval) {
                if ($dval->t_arisan_peserta_id == $value->id) {
                    $models[$key]->detail[] = $dval;
                }
            }
        }
    }

    return successResponse($response, $models);
});

$app->get("/t_arisan_pj/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
              t_arisan.*,
              m_member.nama as pembuat_nama,
              m_member.no_hp as pembuat_no_hp,
              COUNT(t_arisan_peserta.id) as jumlah_peserta
              ")
            ->from("t_arisan")
            ->join("LEFT JOIN", "m_member", "t_arisan.created_by = m_member.id")
            ->join("LEFT JOIN", "t_arisan_peserta", "t_arisan_peserta.t_arisan_id = t_arisan.id")
            ->where("t_arisan.is_deleted", "=", 0)
            ->andWhere("t_arisan.tipe", "=", 'arisan');
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if ($_SESSION['user']['tipe_member'] != 'Admin') {
        $db->andWhere("t_arisan.created_by", "=", $_SESSION['user']['id']);
    }

    $db->groupBy("t_arisan_id, t_arisan.id");
    $db->orderBy("id DESC");
    $models = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

function validasi_pesanan($data, $custom = array()) {
    $validasi = array(
        "jenis_pesanan" => "required",
        "nama" => "required",
    );

    GUMP::set_field_name("nama", "Nama Pelanggan");
    GUMP::set_field_name("jenis_pesanan", "Jenis Barang (PO / Non-PO)");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/t_arisan_pj/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        // $listPeserta  = $data['detail'];
        $form = $data['data'];

        // Validasi list peserta
        if (!empty($listPeserta)) {
            foreach ($listPeserta as $key => $value) {
                $validasi_pesanan = validasi_pesanan($value);
                if ($validasi_pesanan !== true) {
                    return unprocessResponse($response, $validasi_pesanan);
                }
            }
        }

        // Menyiapkan data Form
        $form['tanggal_mulai']    = isset($form['tanggal_mulai']) ? date("Y-m-d", strtotime($form['tanggal_mulai'])) : NULL;
        $form['tanggal_selesai']  = isset($form['tanggal_selesai']) ? date("Y-m-d", strtotime($form['tanggal_selesai'])) : NULL;
        $form['tipe']             = 'arisan';

        if (isset($form["id"])) {
            $model = $db->update("t_arisan", $form, ["id" => $form["id"]]);
        } else {
            $model = $db->insert("t_arisan", $form);

            //insert notif
            $model_member = $db->select("id")->from("m_member")->where("tipe_member", "=", "Admin")->where("status", "=", "aktif")->findAll();
            $arr_member = [];
            foreach ($model_member as $key => $value) {
                $arr_member[] = $value->id;
            }
            $arr_member[] = $_SESSION['user']['id'];
            insertNotif('t_arisan', 'Ada arisan baru', $arr_member);
            //end insert notif
        }

        // Simpan detail peserta
        if (isset($listPeserta) && !empty($listPeserta)) {
            $listIDpeserta = [];
            foreach ($listPeserta as $key => $value) {
                $value['tanggal_selesai'] = isset($value['tanggal_selesai']) ? date("Y-m-d", $value['tanggal_selesai']) : NULL;

                // Jika update
                if (isset($value['id'])) {
                    $update_peserta = $db->update("t_arisan_peserta", $value, ['id' => $temp_id]);
                } else {
                    // Jika data baru
                    $value['t_arisan_id'] = $model->id;
                    $value['status_arisan'] = isset($value['status_arisan']) ? $value['status_arisan'] : 'Pending';

                    $update_peserta = $db->insert("t_arisan_peserta", $value);
                }

                $listIDpeserta[] = $update_peserta->id;

                // Update Data Detail Barang
                if (isset($value['detail']) && !empty($value['detail'])) {
                    $listIDproduk = [];
                    foreach ($value['detail'] as $prkey => $prval) {
                        $prval['m_produk_id'] = isset($prval['m_produk_id']) ? $prval['m_produk_id']['id'] : NULL;
                        $prval['tanggal_selesai'] = isset($prval['tanggal_selesai']) ? date("Y-m-d", $prval['tanggal_selesai']) : NULL;

                        if (isset($prval['id'])) {
                            $update_prd = $db->update("t_arisan_det", $prval, ['id' => $prval['id']]);
                        } else {
                            $prval['t_arisan_id'] = $model->id;
                            $prval['t_arisan_peserta_id'] = $update_peserta->id;

                            $update_prd = $db->insert("t_arisan_det", $prval);
                        }

                        $listIDproduk[] = $update_prd->id;
                    }

                    // Delete yg tidak ada di dftr produk terbaru
                    $delete_prd = $db->run("DELETE
              FROM t_arisan_det
              WHERE t_arisan_id=" . $model->id
                            . " AND t_arisan_peserta_id=" . $update_peserta->id
                            . " AND id NOT IN( " . implode(",", $listIDproduk) . " )");
                }
                // End of Update Data Detail Barang
            }
            // Delete Peserta yg tidak ada di dftr terbaru
            $delete_peserta = $db->run("DELETE
          FROM t_arisan_peserta
          WHERE t_arisan_id=" . $model->id
                    . " AND id NOT IN( " . implode(",", $listIDpeserta) . " )");
            // End of Update Data Peserta
            // Delete barang yg tidak ada di dftr terbaru
            $delete_prd = $db->run("DELETE
          FROM t_arisan_det
          WHERE t_arisan_id=" . $model->id
                    . " AND t_arisan_peserta_id NOT IN( " . implode(",", $listIDpeserta) . " )");
        }

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_arisan_pj/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $model = $db->update("t_arisan", ['is_deleted' => $data['is_deleted']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

$app->get("/t_arisan_pj/kode", function ($request, $response) {
    $db = $this->db;

    try {
        $cekKode = $db->select("kode")
                ->from("t_arisan")
                ->where("t_arisan.tipe", "=", "arisan")
                ->orderBy("id DESC")
                ->find();

        if ($cekKode) {
            $kode_terakhir = $cekKode->kode;
        } else {
            $kode_terakhir = 0;
        }
        $kode_cust = (substr($kode_terakhir, -5) + 1);
        $kodeCust = substr('00000' . $kode_cust, strlen($kode_cust));
        $kodeCust = 'ARI' . date("Y") . $kodeCust;

        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_arisan_pj/setPemenang", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $cek_maintenance = $db->select("is_maintenance")
                ->from("m_setting")
                ->find();

        if ($cek_maintenance->is_maintenance == 1) {
            return unprocessResponse($response, ["Sistem sedang dalam maintenance, pesanan tidak dapat diproses!"]);
        }

        // Set SESSION
        // $jenis_pesanan = $data['jenis_pesanan'];
        // $_SESSION['user']['keranjang'][$jenis_pesanan] = [];
        //
      // $arr_temp = [];
        // if( isset($data['detail']) && !empty($data['detail']) ){
        //   foreach ($data['detail'] as $key => $value) {
        //     $arr_temp[ $value['m_produk_id']['id'] ] = $value['jumlah'];
        //   }
        //
      // }
        $_SESSION['user']['keranjang_arisan']['t_arisan_id'] = $data['id'];

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_arisan_pj/batalPesan", function ($request, $response) {
    try {
        // Set SESSION
        unset($_SESSION['user']['keranjang']);

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

function validasiPeserta($data, $custom = array()) {
    $validasi = array(
        "nama"        => "required",
        "no_hp"       => "required",
        "m_member_id" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/t_arisan_pj/savePeserta", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $validasiPeserta = validasiPeserta($data['peserta']);
        if ($validasiPeserta !== true) {
            return unprocessResponse($response, $validasiPeserta);
        }

        $dataPeserta = [
            't_arisan_id'   => $data['form']['id'],
            'nama'          => $data['peserta']['nama'],
            'no_hp'         => $data['peserta']['no_hp'],
            'status_arisan' => $data['peserta']['status_arisan'],
            'm_member_id'   => $data['peserta']['m_member_id']['id'],
        ];

        if (!empty($data['peserta']['id'])) {
            $dataPeserta['id'] = $data['peserta']['id'];
            $model = $db->update("t_arisan_peserta", $dataPeserta, ["id" => $data['peserta']["id"]]);
        } else {
            $model = $db->insert("t_arisan_peserta", $dataPeserta);
        }

        // Detail Produk
        $detail = $db->select("
        m_produk.nama,
        m_produk.kode,
        t_arisan_det.*
        ")
                ->from("t_arisan_det")
                ->leftJoin("m_produk", "m_produk.id = t_arisan_det.m_produk_id")
                ->where("t_arisan_det.t_arisan_peserta_id", "=", $model->id)
                ->findAll();

        if (!empty($detail)) {
            foreach ($detail as $key => $value) {
                $detail[$key]->m_produk_id = [
                    'id' => $value->m_produk_id,
                    'nama' => $value->nama,
                    'kode' => $value->kode,
                ];
            }

            $model->detail = $detail;
        }

        // Inisiasi QPA
        if( !empty($data['peserta']['m_member_id']) ){
          $model->m_member_id = $data['peserta']['m_member_id'];
        }

        return successResponse($response, ['peserta' => $model, 'detail' => $detail]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

function validasiProduk($data, $custom = array()) {
    $validasi = array(
        "m_produk_id" => "required",
        "jumlah" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/t_arisan_pj/saveProduk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    try {
        $validasiProduk = validasiProduk($params['produk']);
        if ($validasiProduk !== true) {
            return unprocessResponse($response, $validasiProduk);
        }

        $dataProduk = [
            'm_produk_id' => $params['produk']['m_produk_id']['id'],
            'jumlah' => $params['produk']['jumlah'],
            'harga' => $params['produk']['harga'],
            'diskon' => $params['produk']['diskon'],
            't_arisan_id' => $params['form']['id'],
            't_arisan_peserta_id' => $params['peserta']['id'],
        ];

        if (!empty($params['produk']['id'])) {
            // Get Detail Produk Arisan Milik Peserta Ini
            $listProduk = $db->select("*")
                    ->from("t_arisan_det")
                    ->where("t_arisan_peserta_id", "=", $params['peserta']['id'])
                    ->find();

            // Kembalikan stoknya
            if (!empty($listProduk)) {
                $kembalikanStok = $db->run("UPDATE m_produk SET stok = stok + $listProduk->jumlah
            WHERE id = $listProduk->m_produk_id
          ");
            }

            $cekStok = cekKetersediaan($params['produk']['m_produk_id']['id'], $params['produk']['jumlah']);
            if ($cekStok['allow'] == false) {
                $kembalikanStok = $db->run("UPDATE m_produk SET stok = stok - $listProduk->jumlah
            WHERE id = $listProduk->m_produk_id
          ");

                return unprocessResponse($response, ['Stok produk ' . $params['produk']['m_produk_id'] . ' tidak mencukupi. Sisa stok saat ini : ' . $cekStok['produk']['stok']]);
            }

            // Pengurangan stok
            $kurangiStok = $db->run("UPDATE m_produk SET stok = stok - " . $params['produk']['jumlah'] . "
          WHERE id = " . $params['produk']['m_produk_id']['id']);

            $produk = $db->update("t_arisan_det", $dataProduk, ["id" => $params['produk']['id']]);
        } else {
            // Pengurangan stok
            $kurangiStok = $db->run("UPDATE m_produk SET stok = stok - {$params['produk']['jumlah']}
          WHERE id = {$params['produk']['m_produk_id']['id']}
          ");
            $produk = $db->insert("t_arisan_det", $dataProduk);
        }

        return successResponse($response, ['formProduk' => $produk]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

function cekKetersediaan($m_produk_id, $jumlah) {
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);

    $produk = $db->select("*")
            ->from("m_produk")
            ->where("id", "=", $m_produk_id)
            ->find();

    if ($produk->stok < $jumlah) {
        $res = [
            'allow' => false,
            'detail' => (array) $produk
        ];
    } else {
        $res = [
            'allow' => true,
            'detail' => (array) $produk
        ];
    }

    return $res;
}

$app->post("/t_arisan_pj/hapusProduk", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    try {
        $jumlah = !empty($params['produk']['jumlah']) ? $params['produk']['jumlah'] : 0;
        $m_produk_id = !empty($params['produk']['m_produk_id']) ? $params['produk']['m_produk_id']['id'] : 0;

        $kembalikanStok = $db->run("UPDATE m_produk SET stok = stok + $jumlah
        WHERE id = $m_produk_id
      ");
        $hapusProduk = $db->delete("t_arisan_det", ['id' => $params['produk']['id']]);

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

$app->post("/t_arisan_pj/hapusPeserta", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    try {
        // Get Detail Produk Arisan Milik Peserta Ini
        $listProduk = $db->select("*")
                ->from("t_arisan_det")
                ->where("t_arisan_peserta_id", "=", $params['peserta']['id'])
                ->findAll();

        // Kembalikan stoknya
        if (!empty($listProduk)) {
            foreach ($listProduk as $key => $value) {
                $kembalikanStok = $db->run("UPDATE m_produk SET stok = stok + $value->jumlah
            WHERE id = $value->m_produk_id
          ");
            }
        }

        $hapusPeserta = $db->delete("t_arisan_peserta", ['id' => $params['peserta']['id']]);
        $hapusProduk = $db->delete("t_arisan_det", ['t_arisan_peserta_id' => $params['peserta']['id']]);

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

$app->post("/t_arisan_pj/prosesArisan", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    try {
      $prosesArisan = $db->update("t_arisan_peserta", ['status_arisan' => 'Proses'], ['id' => $params['id']]);

      return successResponse($response, $prosesArisan);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }
});

// List arisan
$app->get("/t_arisan_pj/indexList", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("
      t_arisan.kode,
      t_arisan_peserta.*,
      m_member.id as m_member_id,
      m_member.nama as m_member_nama
    ")
    ->from("t_arisan_peserta")
    ->join("LEFT JOIN", "m_member", "m_member.id = t_arisan_peserta.m_member_id")
    ->join("LEFT JOIN", "t_arisan", "t_arisan.id = t_arisan_peserta.t_arisan_id")
    ->where("t_arisan_peserta.status_arisan", "!=", "Pending")
    ->andWhere("t_arisan.is_deleted", "=", "0")
    ->andWhere("t_arisan.tipe", "=", "arisan");

    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }

    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    if ($_SESSION['user']['tipe_member'] != 'Admin') {
        $db->andWhere("t_arisan_peserta.m_member_id", "=", $_SESSION['user']['id']);
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    if (!empty($models)) {
        $listIDpeserta = valueConcate($models, 'id');

        $listDet = $db->select("
          t_arisan_det.*,
          m_produk.id as produk_id,
          m_produk.nama as produk_nama,
          m_produk.kode as produk_kode,
          m_produk.harga_jual as harga
        ")
        ->from("t_arisan_det")
        ->join("LEFT JOIN", "m_produk", "m_produk.id = t_arisan_det.m_produk_id")
        ->customWhere("t_arisan_det.t_arisan_peserta_id IN($listIDpeserta)")
        ->findAll();

        foreach ($listDet as $key => $value) {
            $listDet[$key]->m_produk_id = [
                'id' => $value->produk_id,
                'nama' => $value->produk_nama,
                'kode' => $value->produk_kode,
            ];
        }

        foreach ($models as $key => $value) {
          if( !empty($value->m_member_id) ){
            $models[$key]->m_member_id = [
              'id'      => $value->m_member_id,
              'nama'    => $value->m_member_nama,
            ];
          }

            foreach ($listDet as $kdey => $dval) {
                if ($dval->t_arisan_peserta_id == $value->id) {
                    $models[$key]->detail[] = $dval;
                }
            }
        }
    }

    return successResponse($response, ['list' => $models, "totalItems" => $totalItem]);
});
