<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "periode" => "required",
    );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/l_poin_reseller/laporan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
        $validasi = validasi($data);
        if ($validasi !== true)
            return unprocessResponse($response, $validasi);

        //get laporan non-po
        // $db->select("
        //   m_member.kode,
        //   m_member.nama,
        //   m_member.no_hp,
        //   SUM(t_penjualan.poin_penjualan) as poin_penjualan,
        //   SUM(t_penjualan.total) total
        // ")
        //     ->from("m_member")
        //     ->join("LEFT JOIN", "t_penjualan",
        //         "m_member.id = t_penjualan.reseller_id AND
        //   t_penjualan.status IN('Selesai', 'Proses Pengiriman', 'Lunas')")
        //     ->where("t_penjualan.is_deleted", "=", 0)
        //     ->andWhere("m_member.status", "=", 0)
        //     ->andWhere("t_penjualan.jenis_pesanan", "=", "non-po")
        //     ->andWhere("m_member.tipe_member", "=", "Reseller");
        //
        // if (isset($data["periode"]) && !empty($data["periode"])) {
        //     $start = date("Y-m-d", strtotime($data["periode"]["startDate"]));
        //     $end = date("Y-m-d", strtotime($data["periode"]["endDate"]));
        //
        //     $db->andWhere("t_penjualan.tanggal_lunas", ">=", $start);
        //     $db->andWhere("t_penjualan.tanggal_lunas", "<=", $end);
        // }
        //
        // if ($_SESSION['user']['tipe_member'] == 'Member') {
        //     $db->andWhere("m_member.reff_id", "=", $_SESSION['user']['id']);
        // }
        //
        // $db->groupBy("m_member.id");
        // $laporan = $db->findAll();
        //get laporan non-po -end
        $laporan=[];

        //get laporan po
        $db->select("
          m_member.kode,
          m_member.nama,
          m_member.no_hp,
          SUM(t_penjualan.poin_penjualan) as poin_penjualan,
          SUM(t_penjualan.total) total
        ")
            ->from("m_member")
            ->join("LEFT JOIN", "t_penjualan",
                "m_member.id = t_penjualan.reseller_id AND
          t_penjualan.status IN('Selesai', 'Proses Pengiriman')")
            ->where("t_penjualan.is_deleted", "=", 0)
            ->andWhere("m_member.status", "=", 0)
            ->andWhere("t_penjualan.jenis_pesanan", "=", "po")
            ->andWhere("m_member.tipe_member", "=", "Reseller");

        if (isset($data["periode"]) && !empty($data["periode"])) {
            $start = date("Y-m-d", strtotime($data["periode"]["startDate"]));
            $end = date("Y-m-d", strtotime($data["periode"]["endDate"]));

            $db->andWhere("t_penjualan.tanggal_kirim", ">=", $start);
            $db->andWhere("t_penjualan.tanggal_kirim", "<=", $end);
        }

        if ($_SESSION['user']['tipe_member'] == 'Member') {
            $db->andWhere("m_member.reff_id", "=", $_SESSION['user']['id']);
        }

        $db->groupBy("m_member.id");
        $laporan2 = $db->findAll();
        //get laporan po -end

        $laporanMerge = [];
        foreach ($laporan as $key => $value) {
          if( empty($laporanMerge[$value->kode]) ){
            $laporanMerge[$value->kode] = $value;
          } else {
            $laporanMerge[$value->kode]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->kode]->total += $value->total;
          }
        }

        foreach ($laporan2 as $key => $value) {
          if( empty($laporanMerge[$value->kode]) ){
            $laporanMerge[$value->kode] = $value;
          } else {
            $laporanMerge[$value->kode]->poin_penjualan += $value->poin_penjualan;
            $laporanMerge[$value->kode]->total += $value->total;
          }
        }

        $laporan_poin = array_values($laporanMerge);
        // $laporan_poin = array_merge($laporan, $laporan2);
        // pd(['non-po', $laporan, 'po', $laporan2, 'campur', $laporan_poin]);

        $total_penjualan = 0;
        $total_poin = 0;
        foreach ($laporan_poin as $key => $value) {
            $total_penjualan += $value->total;
            $total_poin += $value->poin_penjualan;
        }

        return successResponse($response, [
            'laporan' => $laporan_poin,
            'total_poin' => $total_poin,
            'total_penjualan' => $total_penjualan,
        ]);

    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});


$app->post("/l_poin_reseller/laporan_per_tahun", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
        $validasi = validasi($data);
        if ($validasi !== true)
            return unprocessResponse($response, $validasi);

        $db->select("
          SUM(t_penjualan.poin_penjualan) as poin_penjualan,
          SUM(t_penjualan.total) total,
          MONTH(t_penjualan.tanggal_kirim) as bulan
        ")
            ->from("t_penjualan")
            ->leftJoin("m_member", "m_member.id = t_penjualan.reseller_id")
            ->where("t_penjualan.is_deleted", "=", 0)
            ->customWhere("t_penjualan.status IN('Selesai', 'Proses Pengiriman')", "AND")
            ->andWhere("m_member.status", "=", 0)
            ->andWhere("m_member.tipe_member", "=", "Reseller");

        if (isset($data["tahun"]) && !empty($data["tahun"])) {
            $start = date("Y-01-01", strtotime($data["tahun"]));
            $end = date("Y-12-31", strtotime($data["tahun"]));

            $db->andWhere("t_penjualan.tanggal_kirim", ">=", $start);
            $db->andWhere("t_penjualan.tanggal_kirim", "<=", $end);
        }

        if ($_SESSION['user']['tipe_member'] == 'Member') {
            $db->andWhere("m_member.reff_id", "=", $_SESSION['user']['id']);
        }

        $db->groupBy("MONTH(t_penjualan.tanggal_kirim)");
        $laporan = $db->findAll();

        $total_penjualan = 0;
        $total_poin = 0;
        foreach ($laporan as $key => $value) {
            $total_penjualan += $value->total;
            $total_poin += $value->poin_penjualan;
        }

        // Get Laporan komisi per-bulan
        $tutup_bulan = $db->select("
        l_tutup_bulan.bulan,
        l_tutup_bulan_det.total_penjualan,
        l_tutup_bulan_det.poin_penjualan,
        l_tutup_bulan_det_kom.jenis_komisi,
        l_tutup_bulan_det_kom.nominal_komisi,
        l_tutup_bulan_det_kom.persen_komisi
      ")
            ->from("l_tutup_bulan_det_kom")
            ->leftJoin("l_tutup_bulan_det",
                "l_tutup_bulan_det.id = l_tutup_bulan_det_kom.l_tutup_bulan_det_id")
            ->leftJoin("l_tutup_bulan",
                "l_tutup_bulan.id = l_tutup_bulan_det.l_tutup_bulan_id")
            ->where("l_tutup_bulan.bulan", ">=", $start)
            ->andWhere("l_tutup_bulan.bulan", "<=", $end)
            ->andWhere("l_tutup_bulan_det.m_member_id", "=", $_SESSION['user']['id'])
            ->findAll();

        // Format data tutup bulan
        $tb_res = [];
        foreach ($tutup_bulan as $key => $value) {
            $tb_res[$value->bulan][$value->jenis_komisi] = $value;
        }

        $lap_temp = sort_array_by_key($laporan, "bulan");
        // End of Get Laporan komisi per-bulan

        // Get Laporan Poin per-bulan
        $tutup_bulan_poin = $db->select("
        l_tutup_bulan.bulan,
        l_tutup_bulan_det.total_penjualan,
        l_tutup_bulan_det.poin_penjualan,
        l_tutup_bulan_det_poin.jenis_poin,
        l_tutup_bulan_det_poin.poin
      ")
            ->from("l_tutup_bulan_det_poin")
            ->leftJoin("l_tutup_bulan_det",
                "l_tutup_bulan_det.id = l_tutup_bulan_det_poin.l_tutup_bulan_det_id")
            ->leftJoin("l_tutup_bulan",
                "l_tutup_bulan.id = l_tutup_bulan_det.l_tutup_bulan_id")
            ->where("l_tutup_bulan.bulan", ">=", $start)
            ->andWhere("l_tutup_bulan.bulan", "<=", $end)
            ->andWhere("l_tutup_bulan_det.m_member_id", "=", $_SESSION['user']['id'])
            ->findAll();

        // Format data tutup bulan
        $tb_poin = [];
        foreach ($tutup_bulan_poin as $key => $value) {
            $tb_poin[$value->bulan][$value->jenis_poin] = $value;
        }
        // End of Get Laporan komisi per-bulan

        /** Get MR */
        if ($_SESSION['user']['m_roles_id'] != 2) {
            /** Get Monthly Reward */
            $this_mr = monthlyRewardCommission($data);
            $this_mr = $this_mr['laporan'];
            $this_mr = isset($this_mr[$_SESSION['user']['id']]) ? $this_mr[$_SESSION['user']['id']]->monthly_reward : 0;
        }


        /** Get Member Commission Supervisor */
        $filter = $data;
        $filter['periode'] = $data['periode']['startDate'];
        $this_mcs = memberSupervisorCommission($filter);
        $this_mcs = dataSortID2($this_mcs['laporan']);
        $this_mcs = isset($this_mcs[$_SESSION['user']['id']]) ? $this_mcs[$_SESSION['user']['id']] : [];

        /** Get Personal Commission Per Tahun */
        $poin_personal = personalCommission($filter);

        /** Get Member Commission Manager */
        $this_mcm = memberManagerCommission($filter);
        $this_mcm = isset($this_mcm['laporan']['listMQPA'][0]) ? $this_mcm['laporan']['listMQPA'][0] : [];

        // Menyiapkan Data
        $result = [];
        $idx = $total_komisi = 0;
        for ($i = 1; $i <= 12; $i++) {
            $month = $i < 10 ? '0' . $i : $i;
            $bulan = date("Y-$month-01", strtotime($data["tahun"]));

            // siapkan data -- Dari tabel tutup bulan
            $pc = isset($tb_res[$bulan]['pc']) ? $tb_res[$bulan]['pc']->nominal_komisi : 0;
            $poin = isset($tb_res[$bulan]['pc']) ? $tb_res[$bulan]['pc']->poin_penjualan : 0;

            $mr = isset($tb_res[$bulan]['mr']) ? $tb_res[$bulan]['mr']->nominal_komisi : 0;
            $mcs = isset($tb_res[$bulan]['mcs']) ? $tb_res[$bulan]['mcs']->nominal_komisi : 0;
            $mcm = isset($tb_res[$bulan]['mcm']) ? $tb_res[$bulan]['mcm']->nominal_komisi : 0;
            $rc = isset($tb_res[$bulan]['rc']) ? $tb_res[$bulan]['rc']->nominal_komisi : 0;
            // $penjualan   = isset($lap_temp[$i]) ? $lap_temp[$i]['total'] : 0;

            $penjualan = isset($poin_personal['laporan'][$idx]['total']) ? $poin_personal['laporan'][$idx]['total'] : 0;
            // $poin        = isset($lap_temp[$i]) ? $lap_temp[$i]['poin_penjualan'] : 0;

            // PC data
            if ($pc > 0) {
                $start = date("Y-m-01", strtotime($bulan));
                $end = date("Y-m-t", strtotime($bulan));
                $list_t_penjualan = $db->findAll("SELECT *
            FROM t_penjualan
            WHERE m_member_id={$filter['member']['id']}
            AND t_penjualan.tanggal_lunas >= '$start'
            AND t_penjualan.tanggal_lunas <= '$end'
          ");
            }

            // $pc_data     = [
            //   'komisi_persen'     => isset($tb_res[$bulan]['pc']) ? $tb_res[$bulan]['pc']->persen_komisi : 0,
            //   'list_t_penjualan'  => $list_t_penjualan,
            // ];
            $pc_data = $poin_personal['laporan'][$idx];

            // Poin Personal, Supervisor, Manager
            $poin_mcm = isset($tb_poin[$bulan]['mcm']) ? $tb_poin[$bulan]['mcm']->poin : 0;
            $poin_mcs = isset($tb_poin[$bulan]['mcs']) ? $tb_poin[$bulan]['mcs']->poin : 0;
            // $poin_p      = isset($tb_poin[$bulan]['pc']) ? $tb_poin[$bulan]['pc']->poin : 0;
            $poin_p = isset($poin_personal['laporan'][$idx]['poin_penjualan']) ? $poin_personal['laporan'][$idx]['poin_penjualan'] : 0;

            // Data pada bulan ini didapatkan dari hasil count data terbaru
            if ($bulan == date("Y-m-01")) {

                $penjualan = $poin_personal['laporan'][$idx]['total'];
                $poin_p = $poin_personal['laporan'][$idx]['poin_penjualan'];
                $pc = $poin_personal['laporan'][$idx]['komisi_nominal'];
                $pc_data = $poin_personal['laporan'][$idx];

                $mr = $this_mr;
                $poin_mcs = !empty($this_mcs['total_poin_penjualan']) ? $this_mcs['total_poin_penjualan'] : 0;
                $mcs = !empty($this_mcs['mcs_reward_nominal']) ? $this_mcs['mcs_reward_nominal'] : 0;
                $poin_mcm = !empty($this_mcm['total_poin']) ? $this_mcm['total_poin'] : 0;
                $mcm = !empty($this_mcm['komisi_nominal']) ? $this_mcm['komisi_nominal'] : 0;
            }

            $total_komisi += $pc + $mr + $mcs + $mcm + $rc;

            $result[$idx] = [
                'bulan' => $bulan,
                'penjualan' => $penjualan,
                'poin' => $poin,
                'pc' => $pc,
                'pc_data' => $pc_data,
                'mr' => $mr,
                'mcs' => $mcs,
                'mcm' => $mcm,
                'rc' => $rc,
                'komisi_nominal' => $pc + $mr + $mcs + $mcm + $rc,
                'poin_personal' => $poin_p,
                'poin_manager' => $poin_mcm,
                'poin_supervisor' => $poin_mcs,
            ];

            $idx++;
        }

        // Get setting pajak
        $pajak = $db->select("pajak")->from("m_setting")->find();
        $pajak = !empty($pajak) ? $pajak->pajak : 0;

        return successResponse($response, [
            'laporan' => $result,
            'total_poin' => $total_poin,
            'total_penjualan' => $total_penjualan,
            'total_komisi' => $total_komisi,
            'pajak' => $pajak,
        ]);

    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
