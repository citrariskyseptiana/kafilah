<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "periode" => "required",
            );

    GUMP::set_field_name("bank", "Nama Bank");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/l_arisan/laporan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi($data);
      if($validasi !== true)
        return unprocessResponse($response, $validasi);

        $db->select("t_arisan.*, COUNT(t_arisan_peserta.id) as jumlah_peserta")
          ->from("t_arisan")
          ->join("LEFT JOIN","t_arisan_peserta", "t_arisan_peserta.t_arisan_id = t_arisan.id")
          ->where("is_deleted", "=", 0)
          ->groupBy("t_arisan_id, t_arisan.id");

      if(isset($data["periode"]) && !empty($data["periode"])){
        $start  = date("Y-m-d", strtotime($data["periode"]["startDate"]));
        $end    = date("Y-m-d", strtotime($data["periode"]["endDate"]));

        $db->andWhere("t_arisan.tanggal_mulai", ">=", $start);
        $db->andWhere("t_arisan.tanggal_mulai", "<=", $end);
      }

      if(isset($data["progres"]) && !empty($data["progres"])){
        $db->andWhere("t_arisan.progres", "=", $data["progres"]);
      }

      $laporan = $db->findAll();

      return successResponse($response, ['laporan'=>$laporan]);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server." . $e]);
    }
});
