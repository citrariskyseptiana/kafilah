<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
             "no_telp"    => "required",
             "email"      => "required",
             "alamat"     => "required",
            );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil detail m promo
 */
$app->get("/m_setting/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from("m_setting")
        ->where("id", "=", 1);
    $models     = $db->find();
    $models->seo_image_link = $models->seo_image;
    unset($models->seo_image);

    $listBank     =  getAllBank();
    $listSettingBank = $db->select("*")->from("m_setting_bank")->findAll();

    if( !empty($listSettingBank) ){
      foreach ($listSettingBank as $key => $value) {
        $listSettingBank[$key]->bank = isset( $listBank[$value->bank] ) ? $listBank[$value->bank] : [];
      }
    }

    if(!empty($models->ro_kota_id)){
      $getKotaAsal = $db->select("w_kota.*, w_provinsi.provinsi")
      ->from("w_kota")
      ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id")
      ->where("w_kota.ro_kota_id", "=", $models->ro_kota_id)
      ->find();
    }
    $models->kota_asal = !empty($getKotaAsal) ? $getKotaAsal : [];

    if(!empty($models->ro_subdistrict_id)){
      $getKecAsal = $db->select("w_kecamatan.*")
      ->from("w_kecamatan")
      ->where("w_kecamatan.ro_subdistrict_id", "=", $models->ro_subdistrict_id)
      ->find();
    }
    $models->kecamatan_asal = !empty($getKecAsal) ? $getKecAsal : [];

    return successResponse($response, [
      'form'            =>  $models,
      'listSettingBank' =>  $listSettingBank
    ]);
});

$app->post("/m_setting/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    $file_path        = __DIR__ . "";
    $file_path        = substr($file_path, 0, strpos($file_path, "api")) . "img/";

    $validasi = validasi($data['form']);
    if ($validasi !== true)
      return unprocessResponse($response, $validasi);

    try {
      // Upload site image
      if (isset($data['form']['seo_image']) && is_array($data['form']['seo_image'])) {
        $simpan_file = base64ToFile($data['form']['seo_image'], $file_path);
        $temp        = explode(".", $simpan_file["fileName"]);
        $newfilename = 'site_image.' . end($temp);
        rename($simpan_file['filePath'], $file_path . $newfilename);
        $data['form']['seo_image'] = $newfilename;
      }

      $data['form']['ro_kota_id']         = !empty($data['form']['kota_asal']) ? $data['form']['kota_asal']['ro_kota_id'] : NULL;
      $data['form']['ro_subdistrict_id']  = !empty($data['form']['kecamatan_asal']) ? $data['form']['kecamatan_asal']['ro_subdistrict_id'] : NULL;

      if (isset($data['form']["id"])) {
          $model = $db->update("m_setting", $data['form'], ["id" => $data['form']["id"]]);
      } else {
          $model = $db->insert("m_setting", $data['form']);
      }

      if( isset($data['listSettingBank']) && !empty($data['listSettingBank']) ){
        $delete_bank = $db->run("DELETE FROM m_setting_bank WHERE 1");
          foreach ($data['listSettingBank'] as $key => $value) {
            $value['bank']  = $value['bank']['code'];
            $insert_bank    = $db->insert("m_setting_bank", $value);
          }
      }

      return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/m_setting/setMaintenance", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {

      if (isset($data['form']["id"])) {
          $model = $db->update("m_setting", ['is_maintenance'=>$data['form']['is_maintenance']], ["id" => $data['form']["id"]]);
      } else {
          $model = $db->insert("m_setting", ['is_maintenance'=>$data['form']['is_maintenance']]);
      }

      return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/m_setting/getKota", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $listKota = $db->select("w_kota.*, w_provinsi.provinsi")
        ->from("w_kota")
        ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id")
        ->findAll();

      return successResponse($response, $listKota);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/m_setting/getKotaSC", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $client = new \GuzzleHttp\Client();
      $url    = "https://api.sentralcargo.co.id/api/origin";
      $cost   = $client->request('GET', $url);
      $model  = $cost->getBody()->getContents();
      $models = json_decode($model, true);

      return successResponse($response, $models);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
