<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        // "kode"               => "required",
        "nama_penerima" => "required",
        "telepon_penerima" => "required",
        "w_desa" => "required",
        "ekspedisi" => "required",
        "ongkir" => "required",
        "w_kota" => "required",
        "kode_pos_penerima" => "required",
    );

    GUMP::set_field_name("w_desa", "Desa");
    GUMP::set_field_name("w_kota", "Kota");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m kategori produk
 */
$app->get("/t_keranjang/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_produk.*, m_produk_img.foto, m_promo.nama as promo, m_promo_det.diskon,
                  w_kota.id as kota_asal_id,
                  w_kota.kota as kota_asal_kota,
                  w_kota.ro_kota_id as kota_asal_ro_kota_id,
                  w_kota.provinsi_id as kota_asal_provinsi_id,
                  w_provinsi.provinsi as kota_asal_provinsi")
            ->from("m_produk")
            ->join("LEFT JOIN", "m_produk_img", "m_produk_img.m_produk_id = m_produk.id AND m_produk_img.is_primary=1")
            ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
            ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
            ->leftJoin("w_kota", "w_kota.id = m_produk.kota_id")
            ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id")
            ->where("m_produk.is_deleted", "=", 0);
    // ->customWhere("IF(m_produk.jenis='non-po', 'm_produk.stok>0', '')", "AND");
    /**
     * Filter
     */
    $_SESSION['user']['status_promo'] = 'non-aktif';
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == 'm_produk.jenis') {
                $db->where($key, "=", $val);
            } elseif ($key == 'is_promo') {
                if ($val == 'promo')
                    $db->customWhere("m_promo_det.m_promo_id IS NOT NULL", "AND");
                if ($val == 'non-promo') {
                    // $db->customWhere("m_promo_det.m_promo_id IS NULL", "AND");
                    $_SESSION['user']['status_promo'] = 'aktif';
                }
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $models = $db->findAll();
    $totalItem = $db->count();

    foreach ($models as $key => $value) {

        $models[$key]->jumlah_pesan = cekKeranjang((array) $value);
        $models[$key]->is_dipesan = $models[$key]->jumlah_pesan > 0 ? true : false;

        if (!empty($value->foto)) {
            $models[$key]->img_path = config("SITE_IMG") . "produk/$value->id/$value->foto";
        } else {
            $models[$key]->img_path = config("SITE_IMG") . "no-image.png";
        }

        // Diskon Promo
        if (!empty($value->diskon) && ( isset($_SESSION['user']['status_promo']) && $_SESSION['user']['status_promo'] == 'non-aktif' )) {
            $models[$key]->harga_coret = $value->harga_jual;
            $models[$key]->harga_jual = $value->harga_jual - ($value->harga_jual * ($value->diskon / 100));
        }

        if ($value->jenis == 'non-po' && $value->jenis < 0) {
            unset($models[$key]);
        }


    }

    // Inisilisasi jumlah pesanan PO/Non-PO
    $jumlahPesanan = jumlahPesanan();

    return successResponse($response, [
        "list" => $models,
        "totalItems" => $totalItem,
        "jumlah_po" => $jumlahPesanan["po"],
        "jumlah_non_po" => $jumlahPesanan["non_po"],
    ]);
});

/**
 * Save m kategori produk
 */
$app->post("/t_keranjang/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

//    pd($data);

    $validasi = validasi($data['form']);
    if ($validasi === true) {
        try {
            // Menyiapkan data
            $data['form']['reseller_id'] = isset($data['form']['reseller_id']['id']) ? $data['form']['reseller_id']['id'] : 0;
            // Cek jika formmember id kosong maka ambil dari session user id
            if (isset($data['form']['m_member_id']['id'])) {
                $data['form']['m_member_id'] = $data['form']['m_member_id']['id'];
            } else {
                $data['form']['m_member_id'] = $_SESSION['user']['id'];
            }
            $data['form']['tanggal'] = date("Y-m-d");
            $data['form']['w_desa_id'] = $data['form']['w_desa']['id'];
            $data['form']['status'] = "Menunggu Pembayaran";
            $data['form']['jenis_pesanan'] = isset($data['form']['tipe_produk']) ? $data['form']['tipe_produk'] : "non-po";
            $data['form']['kode'] = genKodePesanan();

            $model = $db->insert("t_penjualan", $data['form']);
            /* Menetapkan DP dan dilengkapi kode unik */
            if ($data['form']['tagihan_dp'] > 0) {
                $tempDP = gen_kode_unik('dp', $data['form']['tagihan_dp']);
                $insertDP = $db->update("t_penjualan", ['tagihan_dp' => $tempDP], ['id' => $model->id]);
            }

            /* Menetapkan Tagihan + kode Unik */
            $tempTagihan = $data['form']['grand_total'] - $data['form']['tagihan_dp'];
            $tempTagihan = gen_kode_unik('pesanan', $tempTagihan);
            $insertDP = $db->update("t_penjualan", ['tagihan_pesanan' => $tempTagihan], ['id' => $model->id]);

            // Save Detail
            $total_poin_penjualan = 0;
            if (!empty($data['detail'])) {
                foreach ($data['detail'] as $key => $value) {

                    $detail = [
                        "t_penjualan_id" => $model->id,
                        "m_promo_id" => isset($value['m_promo_id']) ? $value['m_promo_id'] : NULL,
                        "m_produk_id" => $value['id'],
                        "jumlah" => $value['jumlah_pesan'],
                        "jenis" => $value['jenis'],
                        "diskon" => $value['diskon_big_order_rp'],
                        "harga" => $value['harga_jual'],
                    ];

                    $insertDetail = $db->insert("t_penjualan_det", $detail);

                    if (empty($data['form']['t_arisan_id']) && isset($data['form']['tipe_produk']) && $data['form']['tipe_produk'] == 'non-po') {
                        $updateStok = $db->run("UPDATE m_produk SET stok = stok - {$value['jumlah_pesan']} WHERE id = {$value['id']}");
                    }

                    $total_poin_penjualan += ($value['poin'] * $value['jumlah_pesan']);
                }

                // Update Poin Penjualan
                $updatePoin = $db->run("UPDATE t_penjualan SET poin_penjualan = {$total_poin_penjualan} WHERE id = {$model->id}");
            }
            // End Save Detail

            /**
             * Setelah pesanan disimpan di DB, Destroy Session Pesanan tersebut
             */
            $tipe_produk = isset($data['form']['tipe_produk']) ? $data['form']['tipe_produk'] : "";
            unset($_SESSION['user']['keranjang'][$tipe_produk]);
            unset($_SESSION['user']['status_promo']);

            /**
             * Jika pemesanan ini dari arisan maka update status arisan
             */
            if (isset($data['form']['t_arisan_id'])) {
                // Update status arisan peserta ini = Selesai
                $db->update("t_arisan_peserta", [
                    'status_arisan' => 'Selesai',
                    'tanggal_selesai' => date("Y-m-d"),
                        ], ['id' => $data['form']['t_arisan_peserta_id']]);

                /*
                 * Cek apakah semua peserta sudah jadi pemenang?
                 * jika ya maka arisan = selesai
                 */
                $sisa = $db->select("id")->from("t_arisan_peserta")
                        ->where("status_arisan", "=", "Pending")
                        ->andWhere("t_arisan_id", "=", $data['form']['t_arisan_id'])
                        ->find();

                if (empty($sisa)) {
                    $db->update("t_arisan", [
                        'progres' => 'Selesai',
                        'tanggal_selesai' => date("Y-m-d"),
                            ], [
                        'id' => $data['form']['t_arisan_id']
                    ]);
                }

                // Unset arisan di session
                unset($data['form']['t_arisan_id']);
                unset($_SESSION['user']['keranjang_arisan']['t_arisan_id']);
            }

            //insert notif
            $model_member = $db->select("id")->from("m_member")->where("tipe_member", "=", "Admin")->where("status", "=", "aktif")->findAll();
            $arr_member = [];
            foreach ($model_member as $key => $value) {
                $arr_member[] = $value->id;
            }

            $arr_member[] = isset($data['form']['member_id']) && !empty($data['form']['member_id']) ? $data['form']['member_id'] : $_SESSION['user']['id'];
            insertNotif('t_penjualan', 'Ada orderan baru', $arr_member);
            //end insert notif

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m kategori produk
 */
$app->post("/t_keranjang/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_kategori_produk", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/t_keranjang/getKota", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $listKota = $db->select("w_kota.*, w_provinsi.provinsi")
        ->from("w_kota")
        ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id")
        ->findAll();

      return successResponse($response, $listKota);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/t_keranjang/getKotaSC", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      $client = new \GuzzleHttp\Client();
      $url    = "https://api.sentralcargo.co.id/api/origin";
      $cost   = $client->request('GET', $url);
      $model  = $cost->getBody()->getContents();
      $models = json_decode($model, true);

      return successResponse($response, $models);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});

$app->get("/t_keranjang/kode", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $kodeCust = genKodePesanan();

        return successResponse($response, $kodeCust);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/kategori", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $list = $db->select("*")
                ->from("m_kategori_produk")
                ->where("is_deleted", "=", 0)
                ->findAll();
        $result = [];
        foreach ($list as $key => $value) {
            $result[$key] = array_map("strval", (array) $value);
        }
        return successResponse($response, $list);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get('/t_keranjang/getReseller', function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    try {
        $listReseller = $db->select("*")
                        ->from("m_member")
                        ->where("tipe_member", "=", "Reseller")
                        ->andWhere("is_deleted", "=", 0)
                        ->customWhere("nama LIKE '%" . $params["nama"] . "%'", "AND")
                        ->limit(15)->findAll();

        return successResponse($response, $listReseller);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->post("/t_keranjang/isiKeranjang", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {

        $cek_maintenance = $db->select("is_maintenance")
                ->from("m_setting")
                ->find();

        if ($cek_maintenance->is_maintenance == 1) {
            return unprocessResponse($response, ["Sistem sedang dalam maintenance, pesanan tidak dapat diproses!"]);
        }

        // Create session keranjang belanja
        $jenis = $data['jenis'];
        $produk_id = $data['id'];
        $_SESSION['user']['keranjang'][$jenis][$produk_id] = $data['jumlah_pesan'];

        return successResponse($response, []);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});

$app->post("/t_keranjang/hapusKeranjang", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        // Create session keranjang belanja
        $jenis = $data['jenis'];
        $produk_id = $data['id'];
        unset($_SESSION['user']['keranjang'][$jenis][$produk_id]);


        return successResponse($response, $produk_id);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});

$app->get("/t_keranjang/cekPesanan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $list = $_SESSION['user']['keranjang'];
        return successResponse($response, $list);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/checkout", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $cek_maintenance = $db->select("is_maintenance")
                ->from("m_setting")
                ->find();

        if ($cek_maintenance->is_maintenance == 1) {
            return unprocessResponse($response, ["Sistem sedang dalam maintenance, pesanan tidak dapat diproses!"]);
        }

        $jenis = $data['tipe'];
        $list = isset($_SESSION['user']['keranjang'][$jenis]) ? $_SESSION['user']['keranjang'][$jenis] : NULL;

        if ($list == NULL) {
            return unprocessResponse($response, ["Keranjang Belanja kosong! Silakan pilih Produk yang akan dibeli terlebih dahulu!"]);
        }

        foreach ($list as $key => $val){
            $listProdukId[] = $key;
        }

        // Get kota asal produk
        $cek_kota_asal = $db->select("m_produk.ro_kota_id, m_produk.kota_asal_sc")
            ->from("m_produk")
            ->customWhere("m_produk.id IN(" . implode(",", $listProdukId) . ")", "AND")
            ->groupBy("m_produk.ro_kota_id")
            ->findAll();

        if (count($cek_kota_asal) > 1) {
            return unprocessResponse($response, ["Terdapat Lebih dari 1 Kota Asal Pengiriman !"]);
        }

        if(!empty($cek_kota_asal[0]->ro_kota_id)){
            $getKotaAsal = $db->select("w_kota.*, w_provinsi.provinsi")
                ->from("w_kota")
                ->leftJoin("w_provinsi", "w_provinsi.id = w_kota.provinsi_id")
                ->where("w_kota.ro_kota_id", "=", $cek_kota_asal[0]->ro_kota_id)
                ->find();
        }

        $kota_asal = !empty($getKotaAsal) ? $getKotaAsal : [];
        $kota_asal_sc = $cek_kota_asal[0]->kota_asal_sc;
        // Get kota asal produk - END

        // Get kecamatan
        $cek_kec_asal = $db->select("
          w_kecamatan.*
        ")
        ->from("m_produk")
        ->join("LEFT JOIN", "w_kecamatan", "w_kecamatan.ro_subdistrict_id = m_produk.ro_subdistrict_id")
        ->customWhere("m_produk.id IN(" . implode(",", $listProdukId) . ")", "AND")
        ->find();
        // Get kecamatan - END

        $listID = [];
        foreach ($list as $key => $value) {
            $listID[] = $key;
        }

        /*
         * Pengecekan Stok sistem dan Keranjang Belanja
         * - Jika jumlah pesanan melebihi sissa stok maka
         * dilakukan penyesuaian dan alert akan diisi dg nama produk.
         */
        $lowStokAlert = NULL;
        if ($jenis == 'non-po') {
            $lowStokAlert = "";
            $getStok = $db->select("m_produk.*")
                    ->from("m_produk")
                    ->customWhere("m_produk.id IN(" . implode(",", $listID) . ")", "AND")
                    ->findAll();

            foreach ($getStok as $key => $value) {
                if (($value->stok < $list[$value->id])) {
                    if ($value->stok == 0) {
                        unset($list[$value->id]);
                    } else {
                        $_SESSION['user']['keranjang'][$jenis][$value->id] = $value->stok;
                        $list[$value->id] = $value->stok;
                    }
                    $lowStokAlert .= "\n - $value->kode - $value->nama";
                }
            }
        }
        /* end alert */

        $listProduk = $db->select("
          m_produk.*,
          m_produk_img.foto,
          m_promo.nama as promo,
          m_promo.id as m_promo_id,
          m_promo_det.diskon")
                ->from("m_produk")
                ->join("LEFT JOIN", "m_produk_img", "m_produk_img.m_produk_id = m_produk.id AND m_produk_img.is_primary=1")
                ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
                ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
                ->where("m_produk.is_deleted", "=", 0)
                ->customWhere("m_produk.id IN(" . implode(",", $listID) . ")", "AND")
                ->findAll();

        // $listBigOrder = $db->select("m_big_order.*")
        //     ->from("m_big_order")
        //     ->where("m_big_order.is_deleted", "=", 0)
        //     ->customWhere("m_big_order.m_produk_id IN(".implode(",", $listID).")", "AND")
        //     ->findAll();
        //
      // $listBigOrder = dataSortID2($listBigOrder);

        $bonusProduk = [];
        foreach ($listProduk as $key => $value) {
            unset($listProduk[$key]->keterangan);
            $listProduk[$key]->jenis = 'detail';
            $listProduk[$key]->jumlah_pesan = $list[$value->id];
            $listProduk[$key]->diskon_big_order = 0;

            // Diskon Promo
            if (!empty($value->diskon) && ( isset($_SESSION['user']['status_promo']) && $_SESSION['user']['status_promo'] == 'non-aktif' )) {
                $listProduk[$key]->m_promo_id = $value->m_promo_id;
                $listProduk[$key]->harga_coret = $value->harga_jual;
                $listProduk[$key]->harga_jual = $value->harga_jual - round($value->harga_jual * ($value->diskon / 100));
                unset($listProduk[$key]->diskon);
            } else {
                unset($listProduk[$key]->m_promo_id);
            }

            // $bigOrder = isBigOrder($listBigOrder, $listProduk[$key]);
            // if( !empty($bigOrder) ){
            //   if($bigOrder['jenis_diskon'] == 'gratis barang'){
            //     $bonusProduk[] = $bigOrder['m_produk_gratis'];
            //   } else if($bigOrder['jenis_diskon'] == 'diskon'){
            //     $listProduk[$key]->diskon_big_order = $bigOrder['diskon'];
            //   }
            // }
            // UNSET IMG
            unset($listProduk[$key]->foto);
        }

        // if( !empty($bonusProduk) ){
        //   foreach ($bonusProduk as $key => $value) {
        //     $data = $value;
        //     $data->jenis = "gratis";
        //     $data->diskon_big_order = 0;
        //     unset($data->keterangan);
        //     $listProduk[] = $data;
        //   }
        // }

        // GET Daerah Asal

        return successResponse($response, [
            'listPesanan'     => $listProduk,
            'kode_unik'       => gen_kode_penjualan(),
            'alert'           => $lowStokAlert,
            'kota_asal'       => $kota_asal,
            'kec_asal'        => !empty($cek_kec_asal) ? $cek_kec_asal : NULL,
            'kota_asal_sc'    => $kota_asal_sc,
        ]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});


$app->get("/t_keranjang/checkout_arisan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $cek_maintenance = $db->select("is_maintenance")
                ->from("m_setting")
                ->find();

        if ($cek_maintenance->is_maintenance == 1) {
            return unprocessResponse($response, ["Sistem sedang dalam maintenance, pesanan tidak dapat diproses!"]);
        }

        $t_arisan_id = $_SESSION['user']['keranjang_arisan']['t_arisan_id'];
        $listProduk = $db->select("
          m_produk.*,
          t_arisan_det.jumlah,
          t_arisan_det.diskon,
          t_arisan_det.harga
        ")
                ->from("m_produk")
                ->join("LEFT JOIN", "t_arisan_det", "m_produk.id = t_arisan_det.m_produk_id")
                ->where("t_arisan_det.t_arisan_peserta_id", "=", $t_arisan_id)
                ->findAll();

        $bonusProduk = [];
        foreach ($listProduk as $key => $value) {
            unset($listProduk[$key]->keterangan);
            $listProduk[$key]->jenis              = 'detail';
            $listProduk[$key]->jumlah_pesan       = $value->jumlah;
            $listProduk[$key]->harga_jual         = $value->harga;
            $listProduk[$key]->diskon             = $value->diskon * $value->jumlah;
            $listProduk[$key]->diskon_big_order   = 0;
        }

        return successResponse($response, [
            'listPesanan' => $listProduk,
            'kode_unik' => gen_kode_penjualan()
        ]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/cetakPesanan", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $penjualan = $db->select("*")
                ->from("t_penjualan")
                ->where("id", "=", $data['id'])
                ->find();

        if (empty($penjualan)) {
            return unprocessResponse($response, ["ID Nota tidak ditemukan"]);
        } else {
            $penjualan->w_desa = $db->find("SELECT * FROM w_desa WHERE id ={$penjualan->w_desa_id}");
            $penjualan->w_kecamatan = $db->find("SELECT * FROM w_kecamatan WHERE id ={$penjualan->w_desa->kecamatan_id}");
            $penjualan->w_kota = $db->find("SELECT * FROM w_kota WHERE id ={$penjualan->w_kecamatan->kota_id}");
            $penjualan->w_provinsi = $db->find("SELECT * FROM w_provinsi WHERE id ={$penjualan->w_kota->provinsi_id}");
        }

        $penjualan_det = $db->select("
        t_penjualan_det.*,
        m_produk.nama produk_nama,
        m_produk.berat produk_berat
        ")
                ->from("t_penjualan_det")
                ->join("LEFT JOIN", "m_produk", "m_produk.id = t_penjualan_det.m_produk_id")
                ->where("t_penjualan_id", "=", $data['id'])
                ->findAll();

        $petugas = $db->select("*")
                ->from("m_member")
                ->customWhere("id IN({$penjualan->m_member_id}, {$penjualan->reseller_id})", "AND")
                ->findAll();

        $setting = $db->find("SELECT * FROM m_setting ");
        $listBank = getAllBank();
        $listSettingBank = $db->select("*")->from("m_setting_bank")->findAll();

        if (!empty($listSettingBank)) {
            foreach ($listSettingBank as $key => $value) {
                $listSettingBank[$key]->bank = isset($listBank[$value->bank]['name']) ? $listBank[$value->bank]['name'] : [];
            }
        }

        // $grand_total = substr($grand_total, 0, (strlen($grand_total)-3) ) . $penjualan->kode_unik;
        $grand_total = $penjualan->total + $penjualan->ongkir;

        $penjualan->grand_total = $grand_total;

        $view = $this->view->fetch('kwitansi.html', [
            'penjualan' => $penjualan,
            'penjualan_det' => $penjualan_det,
            'petugas' => $petugas,
            'setting' => $setting,
            'listSettingBank' => $listSettingBank,
            'terbilang' => terbilang($grand_total),
        ]);

        echo $view;
        die;

        return successResponse($response, $list);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

function validasi_ongkir($data, $custom = array()) {
    $validasi = array(
        "origin" => "required",
        "destination" => "required",
        "weight" => "required|min_numeric,1",
        "courier" => "required",
    );

    GUMP::set_field_name("weight", "Total Berat Pesanan");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_keranjang/cekOngkir", function ($request, $response) {
    $data = $request->getParams();
    // echo json_encode($data); die();
    $db = $this->db;

    try {
      if (empty($data['kota_asal'])) {
        $originCity = $db->find("SELECT ro_kota_id FROM m_setting");
        $originCity = $originCity->ro_kota_id;

      } else {
        $originCity = $data['kota_asal'];
      }

      $originSubdistrict = $db->find("SELECT ro_subdistrict_id FROM m_setting"); // Cek di setingan
      // Kecamatan ke Kecamatan
      if( !empty($data['subdistrict_tujuan']) && ( !empty($data['subdistrict_asal']) || !empty($originSubdistrict->ro_subdistrict_id) ) ){
        if(empty($data['subdistrict_asal'])){
          $data['subdistrict_asal'] = $originSubdistrict->ro_subdistrict_id;
        }
        $params = [
          "origin"            => $data['subdistrict_asal'],
          "originType"        => 'subdistrict',
          "destination"       => $data['subdistrict_tujuan'],
          "destinationType"   => 'subdistrict',
          "weight"            => $data['weight'],
          "courier"           => $data['courier'],
        ];
        $list = costRajaongkirPRO($params);
        $keteranganKirim = 'dari Kecamatan ke Kecamatan';
      // Kota ke Kecamatan
    } else if( empty($data['subdistrict_asal']) && !empty($data['subdistrict_tujuan']) ) {
        $params = [
          "origin"            => $originCity,
          "originType"        => 'city',
          "destination"       => $data['subdistrict_tujuan'],
          "destinationType"   => 'subdistrict',
          "weight"            => $data['weight'],
          "courier"           => $data['courier'],
        ];
        $list = costRajaongkirPRO($params);
        $keteranganKirim = 'dari Kota ke Kecamatan';

      // Kecamatan ke Kota
    } else if( (!empty($data['subdistrict_asal']) || !empty($originSubdistrict->ro_subdistrict_id)) && empty($data['subdistrict_tujuan']) ) {
        if(empty($data['subdistrict_asal'])){
          $data['subdistrict_asal'] = $originSubdistrict->ro_subdistrict_id;
        }
        $params = [
          "origin"            => $data['subdistrict_asal'],
          "originType"        => 'subdistrict',
          "destination"       => $data['destination'],
          "destinationType"   => 'city',
          "weight"            => $data['weight'],
          "courier"           => $data['courier'],
        ];
        $list = costRajaongkirPRO($params);
        $keteranganKirim = 'dari Kecamatan ke Kota';

      // Kota ke Kota
      } else {
        // Rajaongkir starter
        if (empty($data['kota_asal'])) {
          $origin = $db->find("SELECT ro_kota_id FROM m_setting");
          $params = [
            "originType"      => 'city',
            "origin"          => $origin->ro_kota_id,
            "destinationType" => 'city',
            "destination"     => $data['destination'],
            "weight"          => $data['weight'],
            "courier"         => $data['courier'],
          ];
        } else {
          $params = [
            "originType"      => 'city',
            "origin"          => $data['kota_asal'],
            "destinationType" => 'city',
            "destination"     => $data['destination'],
            "weight"          => $data['weight'],
            "courier"         => $data['courier'],
          ];
        }

        $validasi = validasi_ongkir($params);
        if ($validasi !== true)
        return unprocessResponse($response, $validasi);

        $list = costRajaongkirPRO($params);
        $keteranganKirim = 'dari Kota ke Kota';
        // Rajaongkir starter - END
      }

        return successResponse($response, [
          'keterangan'  => $keteranganKirim,
          'result'      => $list['rajaongkir']['results'],
        ]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/cekOngkirDakota", function ($request, $response) {
    $data = $request->getParams();
    $data = json_decode($data['form'], true);
    $db = $this->db;

    try {
        if (!isset($data['kota_asal']) && empty($data['kota_asal'])) {
            $origin = $db->select("w_kota.kota")
                ->from("m_setting")
                ->leftJoin("w_kota", "w_kota.ro_kota_id = m_setting.ro_kota_id")
                ->find();

            $asal_kota = $origin->kota;
            $asal_kota = str_replace("KAB. ", "", $asal_kota);
            $asal_kota = str_replace("KOTA ", "", $asal_kota);
            $asal_kota = str_replace("ADM. ", "", $asal_kota);
            $asal_kota = str_replace("KEP. ", "", $asal_kota);

            $asal_kota = $db->select("dakota_kota.kota")
                ->from("dakota_kota")
                ->customWhere("kota LIKE '%" . $asal_kota . "%'", "AND")
                ->find();

            $asal_kota = $asal_kota->kota;

            $tujuan = $db->select("
            dakota_provinsi.provinsi,
            dakota_kota.kota,
            dakota_kecamatan.kecamatan
            ")
                ->from("dakota_kecamatan")
                ->leftJoin("dakota_kota", "dakota_kecamatan.dakota_kota_id = dakota_kota.id")
                ->leftJoin("dakota_provinsi", "dakota_kota.dakota_provinsi_id = dakota_provinsi.id")
                ->customWhere("dakota_kecamatan.kecamatan LIKE '%" . $data['w_kecamatan']['kecamatan'] . "%'", "AND")
                ->find();

            $params = [
                "asal_kota" => $asal_kota,
                "tujuan_provinsi" => $tujuan->provinsi,
                "tujuan_kota" => $tujuan->kota,
                "tujuan_kecamatan" => $tujuan->kecamatan,
            ];
        } else {
            if (isset($data['kota_asal']['kota'])) {
                $asal_kota = $data['kota_asal']['kota'];
                $asal_kota = str_replace("KAB. ", "", $asal_kota);
                $asal_kota = str_replace("KOTA ", "", $asal_kota);
                $asal_kota = str_replace("ADM. ", "", $asal_kota);
                $asal_kota = str_replace("KEP. ", "", $asal_kota);

                $asal_kota = $db->select("dakota_kota.kota")
                    ->from("dakota_kota")
                    ->customWhere("kota LIKE '%" . $asal_kota . "%'", "AND")
                    ->find();

                $asal_kota = $asal_kota->kota;
            } else {
                $origin = $db->select("w_kota.kota")
                    ->from("m_setting")
                    ->leftJoin("w_kota", "w_kota.ro_kota_id = m_setting.ro_kota_id")
                    ->find();

                $asal_kota = $origin->kota;
                $asal_kota = str_replace("KAB. ", "", $asal_kota);
                $asal_kota = str_replace("KOTA ", "", $asal_kota);
                $asal_kota = str_replace("ADM. ", "", $asal_kota);
                $asal_kota = str_replace("KEP. ", "", $asal_kota);

                $asal_kota = $db->select("dakota_kota.kota")
                    ->from("dakota_kota")
                    ->customWhere("kota LIKE '%" . $asal_kota . "%'", "AND")
                    ->find();

                $asal_kota = $asal_kota->kota;
            }

            $tujuan = $db->select("
            dakota_provinsi.provinsi,
            dakota_kota.kota,
            dakota_kecamatan.kecamatan
            ")
                ->from("dakota_kecamatan")
                ->leftJoin("dakota_kota", "dakota_kecamatan.dakota_kota_id = dakota_kota.id")
                ->leftJoin("dakota_provinsi", "dakota_kota.dakota_provinsi_id = dakota_provinsi.id")
                ->customWhere("dakota_kecamatan.kecamatan LIKE '%" . $data['w_kecamatan']['kecamatan'] . "%'", "AND")
                ->find();

            $params = [
                "asal_kota" => $asal_kota,
                "tujuan_provinsi" => $tujuan->provinsi,
                "tujuan_kota" => $tujuan->kota,
                "tujuan_kecamatan" => $tujuan->kecamatan,
            ];
        }

        $ongkir = costDakota($params);
        $hitung_ongkir = 0;

        if ($data['total_berat'] <= ($ongkir['price']['0']['minkg'] * 1000)) {
            $hitung_ongkir = $ongkir['price']['0']['pokok'];
        } else {
            $pokok = $ongkir['price']['0']['pokok'];
            $minkg = $ongkir['price']['0']['minkg'] * 1000;
            $kgnext = $ongkir['price']['0']['kgnext'];
            $hitung_ongkir = $pokok + round(($data['total_berat'] - $minkg) / 1000) * $kgnext;
        }

        return successResponse($response, [
            'origin_price' => $ongkir['price']['0'],
            'hitung_ongkir' => $hitung_ongkir,
        ]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/cekOngkirSC", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    // echo json_encode($data); die();

    try {
        $data['destination'] = json_decode($data['destination'], true);
        $data['weight'] = ($data['weight'] / 1000) < 1 ? 1 : round($data['weight'] / 1000);

        if (!isset($data['kota_asal'])) {
            $origin = $db->find("SELECT kota_asal_sc FROM m_setting");

            $models = json_decode(getTarifSentralCargo(
                    $origin->kota_asal_sc, $data['destination']['kode_distrik_sc'], $data['weight']
                ), true);
        } elseif (isset($data['kota_asal'])) {
            $models = json_decode(getTarifSentralCargo(
                $data['kota_asal'], $data['destination']['kode_distrik_sc'], $data['weight']
            ), true);
        }

        return successResponse($response, $models);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/cetakAlamat", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        $penjualan = $db->select("*")
                ->from("t_penjualan")
                ->where("id", "=", $data['id'])
                ->find();

        if (empty($penjualan)) {
            return unprocessResponse($response, ["ID Nota tidak ditemukan"]);
        } else {
            if (!empty($penjualan->w_desa_id)) {
                $penjualan->w_desa = $db->find("SELECT * FROM w_desa WHERE id ={$penjualan->w_desa_id}");
                $penjualan->w_kecamatan = $db->find("SELECT * FROM w_kecamatan WHERE id ={$penjualan->w_desa->kecamatan_id}");
            } else {
                $penjualan->w_kecamatan = $db->find("SELECT * FROM w_kecamatan WHERE id ={$penjualan->w_kecamatan_id}");
            }
            $penjualan->w_kota = $db->find("SELECT * FROM w_kota WHERE id ={$penjualan->w_kecamatan->kota_id}");
            $penjualan->w_provinsi = $db->find("SELECT * FROM w_provinsi WHERE id ={$penjualan->w_kota->provinsi_id}");
        }

        $penjualan_det = $db->select("t_penjualan_det.*, m_produk.berat, m_produk.nama")
                ->from("t_penjualan_det")
                ->join("LEFT JOIN", "m_produk", "m_produk.id = t_penjualan_det.m_produk_id")
                ->where("t_penjualan_id", "=", $data['id'])
                ->findAll();

        $berat = 0;
        foreach ($penjualan_det as $key => $value) {
            $berat += ($value->berat * $value->jumlah);
        }
        $penjualan->total_berat = $berat;

        /* Data Pengirim */
        if (!empty($penjualan->reseller_id) && $penjualan->reseller_id > 0) {
            $penjual_id = $penjualan->reseller_id;
        } else {
            $penjual_id = $penjualan->m_member_id;
        }

        if (!empty($penjual_id)) {
            $penjual = $db->select("m_member.*")
                    ->from("m_member")
                    ->where("m_member.id", "=", $penjual_id)
                    ->find();

            if (!empty($penjual->w_desa_id)) {
                $penjual->w_kota = $db->find("SELECT w_kota.*
          FROM w_kota
          LEFT JOIN w_kecamatan ON w_kecamatan.kota_id = w_kota.id
          LEFT JOIN w_desa ON w_desa.kecamatan_id = w_kecamatan.id
          WHERE w_desa.id ={$penjual->w_desa_id}");
            }
        }

        /* Data Pengirim - END */

        $view = $this->view->fetch('alamat_penerima.html', [
            'penjualan' => $penjualan,
            'detail' => $penjualan_det,
            'alamat_kirim' => $penjual,
        ]);

        echo $view;
        die;
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});

$app->get("/t_keranjang/genKodeUnik", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        return successResponse($response, gen_kode_penjualan());
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server : " . $e]);
    }
});
