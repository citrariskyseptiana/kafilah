(function ($) {
    "use strict";

/*-- Variables --*/
var windows = $(window);
var screenSize = windows.width();




/*-- Product Hover Function --*/
$(window).on('load', function(){
    function productHover() {

        var productInner = $('.product-inner');
        var proImageHeight = productInner.find('img').outerHeight();

        productInner.hover(
            function() {
                var porContentHeight = $( this ).find('.content').innerHeight()-55;
                $( this ).find('.image-overlay').css({
                    "height": proImageHeight - porContentHeight,
                });
            }, function() {
                $( this ).find('.image-overlay').css({
                    "height": '100%',
                });
            }
        );

    }
    productHover();
    windows.resize(productHover);
});


/*--
    Menu Sticky
-----------------------------------*/
var sticky = $('.header-sticky');

windows.on('scroll', function() {
    var scroll = windows.scrollTop();
    if (scroll < 300) {
        sticky.removeClass('is-sticky');
    }else{
        sticky.addClass('is-sticky');
    }
});

/*--
    Mobile Menu
------------------------*/
var mainMenuNav = $('.main-menu nav');
mainMenuNav.meanmenu({
    meanScreenWidth: '991',
    meanMenuContainer: '.mobile-menu',
    meanMenuClose: '<span class="menu-close"></span>',
    meanMenuOpen: '<span class="menu-bar"></span>',
    meanRevealPosition: 'right',
    meanMenuCloseSize: '0',
});

/*--
    Header Search
------------------------*/
var searchToggle = $('.search-toggle');
var searchWrap = $('.header-search-wrap');

searchToggle.on('click', function(){

    if( !$(this).hasClass('active') ) {
        $(this).addClass('active');
        searchWrap.addClass('active');
    } else {
        $(this).removeClass('active');
        searchWrap.removeClass('active');
    }

});
/*--
    Header Cart
------------------------*/
var headerCart = $('.header-cart');
var closeCart = $('.close-cart, .cart-overlay');
var miniCartWrap = $('.mini-cart-wrap');

headerCart.on('click', function(e){
    e.preventDefault();
    $('.cart-overlay').addClass('visible');
    miniCartWrap.addClass('open');
});
closeCart.on('click', function(e){
    e.preventDefault();
    $('.cart-overlay').removeClass('visible');
    miniCartWrap.removeClass('open');
});

/*--
    Hero Slider
--------------------------------------------*/
var heroSlider = $('.hero-slider');
heroSlider.slick({
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    dots: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    fade: true,
    infinite: true,
    slidesToShow: 1,
    prevArrow: '<button type="button" class="slick-prev"><i class="icofont icofont-long-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="icofont icofont-long-arrow-right"></i></button>',
});

/*--
	Product Slider
-----------------------------------*/
$('.small-product-slider').slick({
    arrows: false,
    dots: false,
    autoplay: false,
    infinite: true,
    slidesToShow: 4,
    rows: 2,
    prevArrow: '<button type="button" class="slick-prev"><i class="icofont icofont-long-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="icofont icofont-long-arrow-right"></i></button>',
    responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 767,
            settings: {
                autoplay: true,
                slidesToShow: 2,
                arrows: false,
            }
        },
        {
            breakpoint: 479,
            settings: {
                autoplay: true,
                slidesToShow: 1,
                arrows: false,
            }
        }
    ]
});

$('.best-deal-slider').slick({
    arrows: false,
    dots: false,
    autoplay: false,
    infinite: true,
    slidesToShow: 1,
    prevArrow: '<button type="button" class="slick-prev"><i class="icofont icofont-long-arrow-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="icofont icofont-long-arrow-right"></i></button>',
});

/*-----
	Testimonial Slider
--------------------------------*/
$('.testimonial-slider').slick({
    arrows: false,
    dots: false,
    autoplay: false,
    infinite: true,
    slidesToShow: 2,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
            }
        },
    ]
});

/*--
	Brand Slider
-----------------------------------*/
$('.brand-slider').slick({
    arrows: false,
    dots: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 6,
    prevArrow: '<button type="button" class="slick-prev"><i class="icofont icofont-rounded-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="icofont icofont-rounded-right"></i></button>',
    responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 5,
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 4,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 479,
            settings: {
                slidesToShow: 2,
            }
        }
    ]
});

/*--
	Product Slider
-----------------------------------*/
$('.pro-thumb-img').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    infinite: true,
    slidesToShow: 4,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 4,
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 4,
            }
        },
        {
            breakpoint: 479,
            settings: {
                slidesToShow: 3,
            }
        }
    ]
});
$('.related-product-slider-1').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    infinite: true,
    slidesToShow: 4,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 767,
            settings: {
                autoplay: true,
                slidesToShow: 1,
                arrows: false,
            }
        }
    ]
});
$('.related-product-slider-2').slick({
    arrows: true,
    dots: false,
    autoplay: false,
    infinite: true,
    slidesToShow: 3,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 767,
            settings: {
                autoplay: true,
                slidesToShow: 1,
                arrows: false,
            }
        }
    ]
});

/*-----
	Product Zoom
--------------------------------*/
// Instantiate EasyZoom instances
var $easyzoom = $('.easyzoom').easyZoom();

// Setup thumbnails example
var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

$('.pro-thumb-img').on('click', 'a', function(e) {
    var $this = $(this);

    e.preventDefault();

    // Use EasyZoom's `swap` method
    api1.swap($this.data('standard'), $this.attr('href'));
});

/*--
	Count Down Timer
------------------------*/
$('[data-countdown]').each(function() {
	var $this = $(this), finalDate = $(this).data('countdown');
	$this.countdown(finalDate, function(event) {
		$this.html(event.strftime('<span class="cdown day"><span class="time-count">%-D</span> <p>Days</p></span> <span class="cdown hour"><span class="time-count">%-H</span> <p>Hours</p></span> <span class="cdown minutes"><span class="time-count">%M</span> <p>Mint</p></span> <span class="cdown second"><span class="time-count">%S</span> <p>Secs</p></span>'));
	});
});

/*--
	MailChimp
-----------------------------------*/
$('#mc-form').ajaxChimp({
	language: 'en',
	callback: mailChimpResponse,
	// ADD YOUR MAILCHIMP URL BELOW HERE!
	url: 'http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef'

});
function mailChimpResponse(resp) {

	if (resp.result === 'success') {
		$('.mailchimp-success').html('' + resp.msg).fadeIn(900);
		$('.mailchimp-error').fadeOut(400);

	} else if(resp.result === 'error') {
		$('.mailchimp-error').html('' + resp.msg).fadeIn(900);
	}
}

/*--
    Scroll Up
-----------------------------------*/
$.scrollUp({
    easingType: 'linear',
    scrollSpeed: 900,
    animation: 'fade',
    scrollText: '<i class="icofont icofont-swoosh-up"></i>',
});

/*--
    Nice Select
------------------------*/
$('.nice-select').niceSelect()

/*--
	Price Range Slider
------------------------*/
$('#price-range').slider({
   range: true,
   min: 0,
   max: 2000,
   values: [ 250, 1670 ],
   slide: function( event, ui ) {
	$('#price-amount').val( '$' + ui.values[ 0 ] + '  -  $' + ui.values[ 1 ] );
   }
  });
$('#price-amount').val( '$' + $('#price-range').slider( 'values', 0 ) +
   '  -  $' + $('#price-range').slider('values', 1 ) );

/*-----
	Quantity
--------------------------------*/
$('.pro-qty').prepend('<span class="dec qtybtn"><i class="ti-minus"></i></span>');
$('.pro-qty').append('<span class="inc qtybtn"><i class="ti-plus"></i></span>');
$('.qtybtn').on('click', function() {
	var $button   = $(this);
	var oldValue  = $button.parent().find('input').val();
  var max       = $button.parent().find('input').prop("max");

  if( is_maintenance == '1' ){
    showAlertKafilah('error', 'Mohon Maaf',
    `Sistem sedang dalam Mode Maintenance, <br>
    Transaksi tidak dapat dilakukan untuk <br>
    sementara waktu. Cobalah beberapa <br>
    saat lagi.`);
    return;
  }

	if ( $button.hasClass('inc') && parseFloat(oldValue) < max) {
	  var newVal = parseFloat(oldValue) + 1;
	} else {
   // Don't allow decrementing below zero
	  if (oldValue > 0) {
		    var newVal = parseFloat(oldValue) - 1;
		} else {
		    newVal = 0;
	  }
  }

	$button.parent().find('input').val(newVal);

  // + - di session
  console.log("class button ini : ", $button.parent().find('input').attr('class'));
  console.log("id button ini : ", $button.parent().find('input').attr('id'));

  var idProduk = $button.parent().find('input').attr('id');

  if($button.hasClass('inc')){
    idProduk += "1";
  } else {
    idProduk += "-1";
  }

    $.ajax({
      url : "tambah-keranjang",
      type: "POST",
      data: {
        m_produk_id: idProduk,
      },
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          showAlertKafilah('success', 'Berhasil', data.data);
          location.reload();
        } else {
          var pesan ="";
          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
        }
      },
    });

  // End +- i session

});

/*-----
	Shipping Form Toggle
--------------------------------*/
$('[data-shipping]').on('click', function(){
    if( $('[data-shipping]:checked').length > 0 ) {
        $('#shipping-form').slideDown();
    } else {
        $('#shipping-form').slideUp();
    }
})

/*-----
	Payment Method Select
--------------------------------*/
$('[name="payment-method"]').on('click', function(){

    var $value = $(this).attr('value');

    $('.single-method p').slideUp();
    $('[data-method="'+$value+'"]').slideDown();

})

// Tambahan Bintang
function resetAll() {
  if($("#form_register").length){
    $('#form_register')[0].reset(); // reset form
    $('#successAlert').hide();
    $('#errorAlert').hide();
  }

  if($("#form_lupa_password").length){
    $('#form_lupa_password')[0].reset(); // reset form
    $('#successAlert').hide();
    $('#errorAlert').hide();
  }
  if($("#form_reset").length){
    $('#form_reset')[0].reset(); // reset form
    $('#successAlert').hide();
    $('#errorAlert').hide();
  }

};

function showAlert(id, message){
  $('#'+id).html(message);
  $('#'+id).fadeIn();

  setTimeout(function(){
    $('#'+id).fadeOut();
    // resetAll();
  }, 5000);
};

function showAlertKafilah(type, header, message){
  var kelas = (type == 'success') ? "alert-kafilah alert alert-success" : "alert-kafilah alert alert-danger";
  var pesan = "<strong>"+header+'</strong><br>';
  pesan = pesan + message;

  $('.alert-kafilah').attr("class", kelas);
  $('.alert-kafilah').html(pesan);
  $('.alert-kafilah').fadeIn();

  setTimeout(function(){
    $('.alert-kafilah').fadeOut();
    // resetAll();
  }, 3200);
};

resetAll();

$("#form_register_submit").click(function() {

  $('#form_register_submit').text('Sedang Memproses'); //change button text
  $('#form_register_submit').attr('disabled',true); //set button disable

  // var url = "{{base_url ~ 'tukar-tagihan/save'}}";
  var url = "register";
  // console.log("data ", $('#form').serialize());
  var terpilih = false;

    $.ajax({
      url : url,
      type: "POST",
      data: $('#form_register').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          console.log("data : ", data);
          $('#form_register_submit').text('Register'); //change button text
          $('#form_register_submit').attr('disabled',false); //set button enable

          showAlert('successAlert', `<strong>Selamat!</strong> Pendaftaran berhasil, silakan`
          +` cek email Anda untuk mengonfirmasi pendaftaran.`);

          $('#form_register_submit').text('Register'); //change button text
          $('#form_register_submit').attr('disabled',false); //set button enable
          $('#form_register')[0].reset();
        } else {
          var pesan ="<strong>Peringatan!</strong>";

          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlert('errorAlert', pesan);
          $('#form_register_submit').text('Register'); //change button text
          $('#form_register_submit').attr('disabled',false); //set button enable
        }
      },
    });

});

/**
* Lupa Password
*/
$("#form_lupa_submit").click(function() {

  $('#form_lupa_submit').text('Sedang Memproses'); //change button text
  $('#form_lupa_submit').attr('disabled',true); //set button disable

  // var url = "{{base_url ~ 'tukar-tagihan/save'}}";
  var url = "reset-password";

    $.ajax({
      url : url,
      type: "POST",
      data: $('#form_lupa_password').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          console.log("data : ", data);
          $('#form_lupa_submit').text('Submit'); //change button text
          $('#form_lupa_submit').attr('disabled',false); //set button enable

          showAlert('successAlert', `<strong>Berhasil!</strong><br>Link Permintaan Reset Password`
          +` telah dikirim ke email Anda, selanjutnya ikuti prosedur yang tertera dalam email Anda.`);

          $('#form_lupa_submit').text('Submit'); //change button text
          $('#form_lupa_submit').attr('disabled',false); //set button enable
          $('#form_lupa_password')[0].reset();
        } else {
          var pesan ="<strong>Peringatan!</strong>";

          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlert('errorAlert', pesan);
          $('#form_lupa_submit').text('Submit'); //change button text
          $('#form_lupa_submit').attr('disabled',false); //set button enable
        }
      },
    });

});

/**
* Reset Password
*/
$("#form_reset_submit").click(function() {

  $('#form_reset_submit').text('Sedang Memproses'); //change button text
  $('#form_reset_submit').attr('disabled',true); //set button disable

  // var url = "{{base_url ~ 'tukar-tagihan/save'}}";
  var url = "konfirm-reset-password";

    $.ajax({
      url : url,
      type: "POST",
      data: $('#form_reset').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          console.log("data : ", data);
          $('#form_reset_submit').text('Submit'); //change button text
          $('#form_reset_submit').attr('disabled',false); //set button enable

          showAlert('successAlert', `<strong>Selamat!</strong><br>Password Anda telah direset,`
          +` silakan login dengan password Anda yang baru.`);

          $('#form_reset_submit').text('Submit'); //change button text
          $('#form_reset_submit').attr('disabled',false); //set button enable
          $('#form_reset')[0].reset();
        } else {
          var pesan ="<strong>Peringatan!</strong>";

          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlert('errorAlert', pesan);
          $('#form_reset_submit').text('Submit'); //change button text
          $('#form_reset_submit').attr('disabled',false); //set button enable
        }
      },
    });

});

/**
* Reset Password
*/
$("#form_login_submit").click(function() {

  $('#form_login_submit').text('Sedang Memproses'); //change button text
  $('#form_login_submit').attr('disabled',true); //set button disable

  // var url = "{{base_url ~ 'tukar-tagihan/save'}}";
  var url = "login";

    $.ajax({
      url : url,
      type: "POST",
      data: $('#form_login').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          $('#form_login_submit').text('Login'); //change button text
          $('#form_login_submit').attr('disabled',false); //set button enable

          showAlert('successAlert', `<strong>Selamat!</strong><br>Anda berhasil login.`);

          $('#form_login_submit').text('Login'); //change button text
          $('#form_login_submit').attr('disabled',false); //set button enable
          $('#form_login')[0].reset();
          window.location.replace(data.data.redirect);
        } else {
          var pesan ="<strong>Peringatan!</strong>";

          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlert('errorAlert', pesan);
          $('#form_login_submit').text('Login'); //change button text
          $('#form_login_submit').attr('disabled',false); //set button enable
        }
      },
    });

});


/**
* Tambah Pesanan
*/

$(".tambahKeranjang").click(function() {
  console.log("site_url : " + site_url);
  $(this).text('Sedang Memproses'); //change button text
  $(this).attr('disabled',true); //set button disable
  var idProduk = $(this).val();
  if( is_maintenance == '1' ){
    showAlertKafilah('error', 'Mohon Maaf',
    `Sistem sedang dalam Mode Maintenance, <br>
    Transaksi tidak dapat dilakukan untuk <br>
    sementara waktu. Cobalah beberapa <br>
    saat lagi.`);
    return;
  }

    $.ajax({
      url : site_url + "/tambah-keranjang",
      type: "POST",
      data: {
        m_produk_id: idProduk,
      },
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          $('.tambahKeranjang').text('add to cart'); //change button text
          $('.tambahKeranjang').attr('disabled',false); //set button enable

          showAlertKafilah('success', 'Berhasil', data.data);
        } else {
          var pesan ="";
          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
          $('.tambahKeranjang').text('add to cart'); //change button text
          $('.tambahKeranjang').attr('disabled',false); //set button enable
        }
      },
    });

});

$("#updateCart").click(function() {
  var table = document.getElementById('tabel_pesanan'),
    rows = table.getElementsByTagName('tr'),
    i, j, cells, customerId;

  for (i = 0, j = rows.length; i < j; ++i) {
      cells = rows[i].getElementsByTagName('td');
      if (!cells.length) {
          continue;
      }
      customerId = cells[0].innerHTML;
      $(this).find("input:text,select").each(function() {
        console.log(this.value);
        });

  }

});

/**
* remove row Pesanan
*/
$(".hapusRow").click(function() {
  var idProduk = $(this).val();
  console.log("idProduk", idProduk);
  if( !confirm("Apakah Anda ingin membatalkan produk ini dari daftar pesanan?") ){
    return;
  }

    $.ajax({
      url : "remove-item-keranjang",
      type: "POST",
      data: {
        m_produk_id: idProduk,
      },
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          $('#'+data.data.rowId).remove();
          showAlertKafilah('success', 'Berhasil', data.data.pesan);
          location.reload();

        } else {
          var pesan ="";
          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
        }
      },
    });
});

$("#proses-checkout").click(function() {
  var tipe_pesanan = $(this).val();
  console.log("tipe_pesanan", tipe_pesanan);
  if( !confirm("Apakah Anda ingin melanjutkan ke proses Checkout?") ){
    return;
  }

  // Cek apakah user sudah login ?
  $.ajax({
    url : "is_login",
    type: "POST",
    data: {},
    dataType: "JSON",
    success: function(data)
    {
      if( data.status_code == 200 ){
        if( data.data.is_login == false ){
          alert(`Sebelum melakukan Checkout Anda harus
            login terlebih dahulu, atau silakan membuat
            Akun terlebih dahulu jika Anda belum pernah mendaftar.`);
          return;
        } else {
          $.ajax({
            url : "proses-checkout",
            type: "POST",
            data: {
              tipe_pesanan: tipe_pesanan,
            },
            dataType: "JSON",
            success: function(data)
            {
              if( data.status_code == 200 ){
                /*
                * Jika pesanan tidak melebihi stok
                * simpan ke t_penjualan
                * redirect ke halaman checkout
                */

                if( data.data.lowStokAlert.length ){
                  alert("Jumlah pesanan untuk produk " + data.data.lowStokAlert +
                        `\n melebihi sisa stok yang tersedia, sehingga jumlah
                         pesanan untuk produk tersebut telah disesuaikan oleh sistem.`
                  );
                }

                window.location.replace(data.data.redirect);

              } else {
                var pesan ="";
                for(var i in data.errors){
                  pesan += "<br>" + data.errors[i];
                }

                showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
              }
            },
          });
        }
      } else {
        var pesan ="";
        for(var i in data.errors){
          pesan += "<br>" + data.errors[i];
        }

        showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
      }
    },
  });
  // End Cek apakah user sudah login ?
});

$(document).ready(function() {
    $('#kota').select2({
       placeholder: "Ketikkan Nama Kota / Kab.",
       // allowClear: true,
    });
    getKota();

    $('#kecamatan').select2({
       placeholder: "Ketikkan Nama Kecamatan",
    });

    if($("#form-komplain").length){
      $('#form-komplain')[0].reset();
    }

    $('#listRajaOngkir').hide();
    $('#detailDakota').hide();
});

// Fungsi ambil provinsi dan kota
function getKota(param=""){

  $.ajax({
    url : "getKota",
    type: "POST",
    data: {
      nama : param,
    },
    dataType: "JSON",
    success: function(data)
    {
      if( data.status_code == 200 ){
        var options = $('#kota');
        $.each(data.data.listKota, function() {
            options.append($("<option />").val(this.ro_kota_id).text(this.kota +" ("+ this.provinsi+")"));
        });
      } else {
        var pesan ="";
        for(var i in data.errors){
          pesan += "<br>" + data.errors[i];
        }

        showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
      }
    },
  });
};

$('#kota').on('select2:select', function (e) {
  console.log(e.params.data);
  getKecamatan(e.params.data.id);
});

$('#kecamatan').on('select2:select', function (e) {
  $('#kecamatan_text').text(e.params.data); //dapatkan nama kecamatan
});

// Fungsi ambil Kecamatan
function getKecamatan(id){

  $.ajax({
    url : "getKecamatan",
    type: "POST",
    data: {
      kota_id : id,
    },
    dataType: "JSON",
    success: function(data)
    {
      if( data.status_code == 200 ){
        var options = $('#kecamatan');
        $.each(data.data.listKecamatan, function() {
            options.append($("<option />").val(this.id).text(this.kecamatan));
        });
      } else {
        var pesan ="";
        for(var i in data.errors){
          pesan += "<br>" + data.errors[i];
        }

        showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
      }
    },
  });
};

$('#ekspedisi').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;

    $.ajax({
      url : "cek-ongkir",
      type: "POST",
      data: $('#form-checkout').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if( data.status_code == 200 ){
          console.log(data.data);

          if( data.data.ekspedisi == 'dakota' ){
            // Jika ekspedisi Dakota
            $('#listRajaOngkir').hide();
            $('#detailDakota').show();

            trHTML += `<tr>
                          <td class="text-center">Rp. `+ numCommas( data.data.origin_price.pokok ) +`</td>
                          <td class="text-center">`+ data.data.origin_price.minkg +`</td>
                          <td class="text-center">Rp. `+ numCommas( data.data.origin_price.kgnext ) +`</td>
                      </tr>`;

            $('#tableDetailDakota').append(trHTML);

            // Menampilkan ongkir
            $('#biaya_ongkir').val( data.data.hitung_ongkir );
            $('#biaya_ongkir').text( numCommas( data.data.hitung_ongkir ) );

            var sub_total = parseInt( $('#sub_totaln').val() );
            var total     = parseInt( data.data.hitung_ongkir ) + sub_total;

            // Penambahan kode Unik
            var kode_unik = parseInt( $('#kode_unik').val() );
            var str           = total.toString();
            var length        = str.length;
            var total         = str.slice(0, (length-3)) + kode_unik;
            console.log(kode_unik);
            console.log(length);
            console.log(total);
            // End of - Penambahan kode Unik

            $('#grand_total').text( numCommas(total) );

            $('#ongkir_dakota').val( `Rp. ` + numCommas( parseInt( data.data.hitung_ongkir ) ) );
            $('#detail_ongkir').val( `0,0,0,`+ parseInt( data.data.hitung_ongkir ) );

            showAlertKafilah('success', 'Berhasil', "Biaya kirim telah dipilih.");

          } else if(data.data.ekspedisi == 'rajaOngkir' && data.data.status == 'available'){
            // Jika ekspedisi JNE, POS, TIKI
            $('#detailDakota').hide();
            $('#listRajaOngkir').show();
            var trHTML = '';
            $.each(data.data.result, function (i, item) {
                trHTML += `<tr><td>
                                 <button class="ONGKIR btn btn-sm btn-default" type="button" name="ongkir[]" id="ongkir[]" value="`+item.cost+`">Pilih</button>
                                 <input type="hidden" name="ongkir_detail[]" id="ongkir_detail[]" value="`+item.service+','+ item.description+','+item.etd+`,`+item.cost+`">
                              </td>
                              <td>`
                        + item.service + '</td><td>' + item.description + `</td>`
                        + '<td>' + item.etd + `</td><td>` + item.cost + `</td></tr>`;
            });
            $('#list-ongkir').append(trHTML);
          } else if(data.data.ekspedisi == 'rajaOngkir' && data.data.status == 'unavailable') {
            var trHTML = `
              <td colspan="5"> Layanan Tidak Tersedia. </td>`;
            $('#list-ongkir').append(trHTML);
          }
        } else {
          var pesan ="";
          for(var i in data.errors){
            pesan += "<br>" + data.errors[i];
          }

          showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
        }
        // End Jika ekspedisi JNE, POS, TIKI
      },
    });
});

$(document).on( "click", "button.ONGKIR", function() {
  $('#biaya_ongkir').val( $( this ).val() );
  $('#biaya_ongkir').text( numCommas( $( this ).val() ) );

  var sub_total = parseInt( $('#sub_totaln').val() );
  var total = parseInt( $( this ).val() ) + sub_total;

  // Penambahan kode Unik
  var kode_unik = parseInt( $('#kode_unik').val() );
  var str           = total.toString();
  var length        = str.length;
  var total         = str.slice(0, (length-3)) + kode_unik;
  console.log(kode_unik);
  console.log(length);
  console.log(total);
  // End of - Penambahan kode Unik

  $('#grand_total').text( numCommas(total) );
  showAlertKafilah('success', 'Berhasil', "Biaya kirim telah dipilih.");

  $('button.ONGKIR').attr( 'class', 'ONGKIR btn btn-sm btn-default');
  $('button.ONGKIR').text(`Pilih`);
  $(this).attr( 'class', 'ONGKIR btn btn-sm btn-success');
  $(this).html(`<i class="fa fa-check-o"></i> Terpilih`);

  $('#detail_ongkir').val( $(this).next('input').val() );
});

function numCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
 }

 $("#checkout").click(function() {
   $(this).text('Sedang Memproses'); //change button text
   $(this).attr('disabled',true); //set button disable

   showAlertKafilah('success', 'Berhasil', 'produk telah tambagkan ke keranjang');

     $.ajax({
       url : "checkout",
       type: "POST",
       data: $('#form-checkout').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         if( data.status_code == 200 ){
           $('#checkout').text('Checkout'); //change button text
           $('#checkout').attr('disabled',false); //set button enable

           showAlertKafilah('success', 'Berhasil', "Data pesanan berhasil disimpan.");
           window.location.replace(data.data.redirect);
         } else {
           var pesan ="";
           for(var i in data.errors){
             pesan += "<br>" + data.errors[i];
           }

           showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
           $('#checkout').text('Checkout'); //change button text
           $('#checkout').attr('disabled',false); //set button enable
         }
       },
     });

 });

 $("#submit_komplain").click(function() {
   $(this).attr('disabled',true); //set button disable

   var form = $($('#form-komplain'));
    var formData = false;
    if (window.FormData){
        formData = new FormData(form[0]);
    }

   $.ajax({
     url : "submit-komplain",
     type: "POST",
     data: formData ? formData : form.serialize(),
     dataType: "JSON",
     cache       : false,
      contentType : false,
      processData : false,
     success: function(data)
     {
       if( data.status_code == 200 ){
         $('#submit_komplain').attr('disabled',false); //set button enable

         showAlertKafilah('success', 'Terimakasih', "Pengajuan komplain berhasil disimpan, Kami akan segera memberikan respon.");
         window.location.replace(data.data.redirect);
       } else {
         var pesan ="";
         for(var i in data.errors){
           pesan += "<br>" + data.errors[i];
         }

         showAlertKafilah('error', 'Terjadi Tesalahan', pesan);
         $('#submit_komplain').attr('disabled',false); //set button enable
       }
     },
   });
 });

})(jQuery);
