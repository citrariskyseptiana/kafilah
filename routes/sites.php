<?php

/**
 * LIST API
 * - /home                 : beranda
 * - /produk               : list produk
 * - /produk/{url}         : single produk
 * - /artikel              : list artikel
 * - /artikel/{alias}      : single artikel
 * - /kontak               : kontak
 * - /tentang-kami         : about
 * - /cart                 : keranjang belanja
 * - /checkout             : halaman pembayaran
 * - /login-register       : login dan register
 * - /register             : register customer baru
 * - /konfirmasi-register  : konfirmasi proses reg
 * - /lupa-password        : permintaan reset password
 * - /reset-password       : mengirim kode unik ke email customer untuk reset password
 * - /konfirm-reset-password GET : menampilkan form untuk password baru
 * - /konfirm-reset-password POST : menyimpan password baru (reset)
 * - /login                : login member
 * - /logout               : logout session
 */
$app->get("/", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      // jika user mengakses web melalui afiliasi maka simpan kode membernya
      if( isset($data['url']) && !empty(isset($data['url'])) ){
        $_SESSION['public']['afiliasi'] = $data['url'];
      }

      // Get 3 Produk Best seller
      $produkBestseller = $db->select("m_produk.*,
            m_produk_img.foto,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->where("m_produk.is_best_seller", "=", 1)
        ->limit(4)
        ->orderBy("RAND()")
        ->findAll();

      // Mempersiapkan foto, detail, dll
      $produkBestseller = siapkan_produk($produkBestseller);

      // Get beberapa produk
      $contohProduk = $db->select("m_produk.*,
            m_produk_img.foto,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->limit(8)
        ->orderBy("RAND()")
        ->findAll();

      // Mempersiapkan foto, detail, dll
      $contohProduk = siapkan_produk($contohProduk);

      // Get 3 Produk Promo
      // m_produk_img.foto,
      $produkPromo = $db->select("m_produk.*,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon,
            m_promo_det.foto
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->customWhere("m_promo_det.m_produk_id IS NOT NULL", "AND")
        // ->limit(3)
        ->findAll();

      $produkPromo = siapkan_produk($produkPromo, 'promo');

      // Get artikel terbaru
      $artikel = get_some_article("terbaru", 4);

      // Get Testimoni Pelanggan
      $listTestimoni = $db->select("m_testimoni.*")
        ->from("m_testimoni")
        ->where("m_testimoni.is_deleted", "=", 0)
        ->andWhere("m_testimoni.is_used", "=", 1)
        ->findAll();

      $listSlider = $db->select('*')
        ->from("m_slider")
        ->where("is_deleted", "=", 0)
        ->findAll();

      // pd($produkPromo);
      $view = $this->view->fetch('public/index.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "home",
          "contohProduk"  => $contohProduk,
          "produkPromo"   => $produkPromo,
          "bestSeller"    => $produkBestseller,
          "listArtikel"   => $artikel,
          "listTestimoni" => $listTestimoni,
          "listSlider"    => $listSlider,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);

      echo $view;

    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/home", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      // jika user mengakses web melalui afiliasi maka simpan kode membernya
      if( isset($data['url']) && !empty(isset($data['url'])) ){
        $_SESSION['public']['afiliasi'] = $data['url'];
      }

      // Get 3 Produk Best seller
      $produkBestseller = $db->select("m_produk.*,
            m_produk_img.foto,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->where("m_produk.is_best_seller", "=", 1)
        ->limit(4)
        ->orderBy("RAND()")
        ->findAll();

      // Mempersiapkan foto, detail, dll
      $produkBestseller = siapkan_produk($produkBestseller);

      // Get beberapa produk
      $contohProduk = $db->select("m_produk.*,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon,
            m_produk_img.foto
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->limit(8)
        ->orderBy("RAND()")
        ->findAll();

      // Mempersiapkan foto, detail, dll
      $contohProduk = siapkan_produk($contohProduk);

      //Get 3 Produk Promo
      // m_produk_img.foto,
      $produkPromo = $db->select("m_produk.*,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon,
            m_promo_det.foto
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->customWhere("m_promo_det.m_produk_id IS NOT NULL", "AND")
        ->limit(3)
        ->findAll();

      $produkPromo = siapkan_produk($produkPromo);

      // Get artikel terbaru
      $artikel = get_some_article("terbaru", 4);

      // Get Testimoni Pelanggan
      $listTestimoni = $db->select("m_testimoni.*")
        ->from("m_testimoni")
        ->where("m_testimoni.is_deleted", "=", 0)
        ->andWhere("m_testimoni.is_used", "=", 1)
        ->findAll();

      $view = $this->view->fetch('public/index.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "home",
          "contohProduk"  => $contohProduk,
          "produkPromo"   => $produkPromo,
          "bestSeller"    => $produkBestseller,
          "listArtikel"   => $artikel,
          "listTestimoni" => $listTestimoni,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);

      echo $view;

    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/produk", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {

      $page     = isset($_GET['page']) && $_GET['page'] != "" ? $_GET['page'] : 1;
      $hasil    = createPagination('m_produk', 2, 9, $page, config('SITE_URL') . "/produk" );

      $listProduk = [];
      $listProduk = siapkan_produk($hasil['resultData']);

      // Get produk Best Seller
      $beseSeller = $db->select("m_produk.*, m_produk_img.foto")
                    ->from("m_produk")
                    ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary = 1")
                    ->where("is_best_seller","=",1)
                    ->andWhere("is_deleted","=",0)
                    ->limit(4)
                    ->findAll();

      $beseSeller = siapkan_produk($beseSeller);

      $view = $this->view->fetch('public/produk-left-sidebar.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "produk",
          "pagination"    => $hasil['pagination'],
          "listProduk"    => $listProduk,
          "listProdukBestSeller"    => $beseSeller,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/produk/{url}", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {

      // jika user mengakses web melalui afiliasi maka simpan kode membernya
      if( isset($data['url']) && !empty(isset($data['url'])) ){
        $_SESSION['public']['afiliasi'] = $data['url'];
      }

      $produk_url    = $request->getAttribute("url");
      $produk   = $db->select("m_produk.*,
            m_produk_img.foto,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->where("m_produk.url", "=", $produk_url)
        ->find();

      $produk->m_kategori_id = [
        "id"   => $produk->kategori_id,
        "kode" => $produk->kategori_kode,
        "nama" => $produk->kategori,
      ];

      $defaultImg = config("SITE_IMG") . "no-image.png";
      // Inisialisasi gambar produk
      if( empty($produk->foto) ){
        $produk->foto = $defaultImg;
      } else {
        $produk->foto = config("SITE_IMG") . "/produk/$produk->id/$produk->foto";
      }

      // Diskon Promo
      if( !empty($produk->diskon) ){
        $produk->harga_coret = $produk->harga_jual;
        $produk->harga_jual = $produk->harga_jual - ($produk->harga_jual * ($produk->diskon/100));
      }

      // Get list foto
      $listFoto = $db->select("*")
      ->from("m_produk_img")
      ->where("m_produk_id", "=", $produk->id)
      ->orderBy("is_primary DESC")
      ->findAll();

      foreach ($listFoto as $key => $value) {
        $listFoto[$key]->foto_url = config("SITE_IMG") . "produk/$value->m_produk_id/$value->foto";
      }

      // Get related Product
      $relatedProduct   = $db->select("m_produk.*,
            m_produk_img.foto,
            m_kategori_produk.id as kategori_id,
            m_kategori_produk.kode as kategori_kode,
            m_kategori_produk.nama as kategori,
            m_promo.nama as promo,
            m_promo_det.diskon
      ")
        ->from("m_produk")
        ->join("LEFT JOIN", "m_kategori_produk", "m_kategori_produk.id = m_produk.m_kategori_id")
        ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary=1")
        ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
        ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
        ->where("m_produk.is_deleted", "=", 0)
        ->where("m_produk.m_kategori_id", "=", $produk->m_kategori_id['id'])
        ->findAll();

      $relatedProduct = siapkan_produk($relatedProduct);
      // pd($relatedProduct);
      $view = $this->view->fetch('public/produk-single.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "produk"        => $produk,
          "listFoto"      => $listFoto,
          "relatedProduct"=> $relatedProduct,
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/artikel", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

//    echo json_encode($data);
//    die();

    try {
      $page     = isset($_GET['page']) && $_GET['page'] != "" ? $_GET['page'] : 1;
      $hasil    = createPagination('m_artikel', 2, 6, $page, config('SITE_URL') . "artikel" );

      $newsList   = [];
      $defaultImg = config("SITE_IMG") . "no-image.png";
//      print_r($defaultImg);
//      die;

      if(!empty($hasil['resultData']) ){
        foreach($hasil['resultData'] as $key => $val){
          $newsList[$key]                 = (array)$val;
          $firstImg                       = get_images($val->artikel);
          $newsList[$key]['firstImg']     = isset($firstImg[0]) ? $firstImg[0] : $defaultImg;
          $newsList[$key]['url']          = config('SITE_URL') . "public/artikel/" . $val->url;
          $newsList[$key]['description']  = !empty($val->deskripsi) ? limit_text($val->deskripsi, 29) : limit_text($val->artikel, 29);
        }
//        print_r($hasil);
//        die;
      }

      $view = $this->view->fetch('public/artikel.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listArtikel"   => $newsList,
          "pagination"    => $hasil['pagination'],
          "uri"           => "artikel",
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;

    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/artikel/{alias}", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $artikel = $db->select("
        m_artikel.*,
        m_member.nama created_by
      ")
      ->from("m_artikel")
      ->join("LEFT JOIN", "m_member", "m_member.id = m_artikel.created_by")
      ->where("m_artikel.is_deleted", "=", 0)
      ->andWhere("m_artikel.is_publish", "=", 1)
      ->andWhere("m_artikel.url", "=", $request->getAttribute("alias"))
      ->find();

      // Cek apakah artikel tersedia
      if(!empty($artikel) ){
          $view = $this->view->fetch('public/404.html', [
            "base_url"      => config('SITE_URL') . "/views",
            "site_url"      => config('SITE_URL') ,
            "listKategori"  => get_kategori_produk(),
        ]);
        echo $view;
      }

      // Update jumlah view
      $update_view = $db->run("UPDATE m_artikel SET views = views+1 WHERE id = $artikel->id");

      $defaultImg         = config("SITE_IMG") . "no-image.png";
      $firstImg           = get_images($artikel->artikel);
      $artikel->firstImg  = isset($firstImg[0]) ? $firstImg[0] : $defaultImg;
      $artikel->url       = config("SITE_URL") . "public/artikel/" . $artikel->url;
      $artikel->deskripsi = isset($artikel->deskripsi) ? $artikel->deskripsi : limit_text($artikel->artikel, 20);
      $artikel->keyword_meta   = isset($artikel->keyword) ? $artikel->keyword : 'Kafilah, buku';
      $artikel->keyword   = isset($artikel->keyword) ? explode(",", $artikel->keyword) : ['Kafilah', "buku"];

      $artikel->artikel = custom_youtube_iframe($artikel->artikel);

      // pd($artikel);
      $view = $this->view->fetch('public/artikel-single.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "artikel",
          "artikel"       => $artikel,
          "listKategori"  => get_kategori_produk(),
          "artikelPopuler"  => get_some_article("populer", 3),
          "session"       => session_customer(),
      ]);

      echo $view;

    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/kontak", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {

      $view = $this->view->fetch('public/kontak.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "kontak",
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
          "about"         => $db->find("SELECT * FROM m_artikel WHERE url = 'tentang-kami'"),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/tentang-kami", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $about = $db->find("SELECT * FROM m_artikel WHERE url = 'tentang-kami'");

      $view = $this->view->fetch('public/about.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "about",
          "listKategori"  => get_kategori_produk(),
          "about"         => $about,
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {

      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/cart", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      // List Pesanan
      if( isset( $data['tipe'] ) && $data['tipe'] == 'nonpo'){
        $tipe_pesanan = 'non-po';
      } else {
        $tipe_pesanan = "po";
      }

      if( !empty($_SESSION['public']['keranjang'][$tipe_pesanan]) ){
        $list = $_SESSION['public']['keranjang'][$tipe_pesanan];
      } else {
        // Alert kosong
        $view = $this->view->fetch('public/keranjang.html', [
            "base_url"      => config('SITE_URL') . "/views",
            "site_url"      => config('SITE_URL') ,
            "uri"           => "cart",
            "listKategori"  => get_kategori_produk(),
            "session"       => session_customer(),
            "listPesanan"   => [],
        ]);
        echo $view;
        die;
      }

      $listID=[];
      foreach ($list as $key => $value) {
        $listID[]=$key;
      }

      /*
      * Pengecekan Stok sistem dan Keranjang Belanja
      * - Jika jumlah pesanan melebihi sissa stok maka
      * dilakukan penyesuaian dan alert akan diisi dg nama produk.
      */
      $lowStokAlert = NULL;
      if($jenis == 'non-po'){
        $lowStokAlert = "";
        $getStok = $db->select("m_produk.*")
                #!/histori-pesanan  ->from("m_produk")
                  ->customWhere("m_produk.id IN(".implode(",", $listID).")", "AND")
                  ->findAll();

        foreach ($getStok as $key => $value) {
          if( ($value->stok < $list[$value->id]) ){
            if($value->stok == 0){
              unset($list[$value->id]);
            } else {
              $_SESSION['public']['keranjang'][$tipe_pesanan][$value->id] = $value->stok;
              $list[$value->id] = $value->stok;
            }
            $lowStokAlert .= "\n - $value->kode - $value->nama";
          }
        }
      }
      /* end alert*/

      $listPesanan = $db->select("
          m_produk.*,
          m_produk_img.foto,
          m_promo.nama as promo,
          m_promo.id as m_promo_id,
          m_promo_det.diskon")
          ->from("m_produk")
          ->join("LEFT JOIN", "m_produk_img", "m_produk_img.m_produk_id = m_produk.id AND m_produk_img.is_primary=1")
          ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
          ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
          ->where("m_produk.is_deleted", "=", 0)
          ->customWhere("m_produk.id IN(".implode(",", $listID).")", "AND")
          ->findAll();

      $total = 0;
      $listPesanan = siapkan_produk($listPesanan);
      foreach ($listPesanan as $key => $value) {
        // $listPesanan[$key]->jenis = 'detail';
        $listPesanan[$key]->jumlah_pesan  = $list[$value->id];
        $listPesanan[$key]->sub_total     = $list[$value->id] * $value->harga_jual;

        if($tipe_pesanan == 'non-po'){
          $listPesanan[$key]->max = $list[$value->id];
        } else {
          $listPesanan[$key]->max = 1000;
        }

        $total += $listPesanan[$key]->sub_total;
      }

      $view = $this->view->fetch('public/keranjang.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "cart",
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
          'listPesanan'   => $listPesanan,
          'alert'         => $lowStokAlert,
          'totalPesanan'  => $total,
          'tipe_pesanan'  => $tipe_pesanan,
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->post("/proses-checkout", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    date_default_timezone_set("Asia/Jakarta");

    try {
      // Ambil produk dari session dan check stoknya
      $tipe_pesanan   = $data['tipe_pesanan'];
      $listPesan      = $_SESSION['public']['keranjang'][$tipe_pesanan];

      $listID=[];
      foreach ($listPesan as $key => $value) {
        $listID[] = $key;
      }

      /*
      * Pengecekan Stok sistem dan Keranjang Belanja
      * - Jika jumlah pesanan melebihi sissa stok maka
      * dilakukan penyesuaian dan alert akan diisi dg nama produk.
      */
      $lowStokAlert = "";
      if($tipe_pesanan == 'non-po'){

        $lowStokAlert = "";
        $getStok = $db->select("m_produk.*")
                  ->from("m_produk")
                  ->customWhere("m_produk.id IN(".implode(",", $listID).")", "AND")
                  ->findAll();

        foreach ($getStok as $key => $value) {
          // jika stok dibawah pesanan
          if( ($value->stok < $list[$value->id]) ){
            if($value->stok == 0){
              unset($listPesan[$value->id]);
            } else {
              // replace jumlah pesanan dengan sisa stok
              $_SESSION['public']['keranjang'][$tipe_pesanan][$value->id] = $value->stok;
              $listPesan[$value->id] = $value->stok;
            }
            // ini peringatan pemberitahuan pada customer
            $lowStokAlert .= "\n - $value->kode - $value->nama";
          }
        }
      }
      /* end alert*/

      // Tampilkan daftar produk dengan jumlah pesanan yg telah diperbarui
      $listProduk = $db->select("
          m_produk.*,
          m_produk_img.foto,
          m_promo.nama as promo,
          m_promo.id as m_promo_id,
          m_promo_det.diskon")
          ->from("m_produk")
          ->join("LEFT JOIN", "m_produk_img", "m_produk_img.m_produk_id = m_produk.id AND m_produk_img.is_primary=1")
          ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
          ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
          ->where("m_produk.is_deleted", "=", 0)
          ->customWhere("m_produk.id IN(".implode(",", $listID).")", "AND")
          ->findAll();

      $listProduk = siapkan_produk($listProduk);

      $total=0;
      foreach ($listProduk as $key => $value) {
        $listProduk[$key]->jumlah_pesan  = $listPesan[$value->id];
        $listProduk[$key]->sub_total     = $listPesan[$value->id] * $value->harga_jual;

        if($tipe_pesanan == 'non-po'){
          $listProduk[$key]->max = $listPesan[$value->id];
        } else {
          $listProduk[$key]->max = 1000;
        }

        $total += $listProduk[$key]->sub_total;
      }

      return successResponse($response, [
        'lowStokAlert'      => $lowStokAlert,
        'redirect'          => config("SITE_URL") .'checkout?tipe=' . $tipe_pesanan,
      ]);
    } catch (Exception $e) {

      return unprocessResponse($response, ['Terjadi Kesalahan.' . $e]);
    }
})->setName('publicFrontend');

$app->get("/checkout", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      /*
      * ALur checkout
      * - Jika isset session afiliasi maka pada penualan isi m_member_id
      * - Jika tidak maka, ambil reff_id dari tabel m_member
      * - jika tidak ada, maka tanpa m_member_id
      */

      // Ambil produk dari session dan check stoknya
      if( isset( $data['tipe'] ) && ($data['tipe'] == 'nonpo' || $data['tipe'] == 'non-po')){
        $tipe_pesanan = 'non-po';
      } else {
        $tipe_pesanan = "po";
      }

      $listPesan      = $_SESSION['public']['keranjang'][$tipe_pesanan];


      if( empty($listPesan) ){
        $view = $this->view->fetch('public/checkout.html', [
            "base_url"      => config('SITE_URL') . "/views",
            "site_url"      => config('SITE_URL') ,
            "uri"           => "checkout",
            "listKategori"  => get_kategori_produk(),
            "session"       => session_customer(),
            "listProduk"    => $listProduk,
            "subTotal"      => $total,
            "total_berat"   => $total_berat,
            "tipe_pesanan"  => $tipe_pesanan,
            "setting"       => $setting,
            "kode_unik"     => gen_kode_penjualan(),
        ]);

        echo $view;
        die;
      }
      $listID = $listProduk = [];
      foreach ($listPesan as $key => $value) {
        $listID[] = $key;
      }

      $listProduk = $db->select("
          m_produk.*,
          m_produk_img.foto,
          m_promo.nama as promo,
          m_promo.id as m_promo_id,
          m_promo_det.diskon")
          ->from("m_produk")
          ->join("LEFT JOIN", "m_produk_img", "m_produk_img.m_produk_id = m_produk.id AND m_produk_img.is_primary=1")
          ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
          ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
          ->where("m_produk.is_deleted", "=", 0)
          ->customWhere("m_produk.id IN(".implode(",", $listID).")", "AND")
          ->findAll();

      $listProduk = siapkan_produk($listProduk);

      $total= $total_berat = 0;
      foreach ($listProduk as $key => $value) {
        $listProduk[$key]->nama = limit_text($value->nama, 35);
        $listProduk[$key]->jumlah_pesan  = $listPesan[$value->id];
        $listProduk[$key]->sub_total     = $listPesan[$value->id] * $value->harga_jual;

        if($tipe_pesanan == 'non-po'){
          $listProduk[$key]->max = $listPesan[$value->id];
          // Cek jika jumlah stok memenuhi jumlah pesan, jika tidak maka isi dg stok yg tersisa
          if( $listProduk[$key]->jumlah_pesan <= $value->stok  ){
          } else {
            $listProduk[$key]->jumlah_pesan = $value->stok;
          }

        } else {
          $listProduk[$key]->max = 1000;
        }

        $total += $listProduk[$key]->sub_total;
        $total_berat += $listProduk[$key]->jumlah_pesan * $value->berat;
      }

      $setting = $db->find("SELECT * FROM m_setting ");

      $view = $this->view->fetch('public/checkout.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "checkout",
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
          "listProduk"    => $listProduk,
          "subTotal"      => $total,
          "total_berat"   => $total_berat,
          "tipe_pesanan"  => $tipe_pesanan,
          "setting"       => $setting,
          "kode_unik"     => gen_kode_penjualan(),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

function validasi_co($data, $custom = array())
{
    $validasi = array(
       "telp"           => "required",
       "kota"           => "required",
       "kode_pos"       => "required",
       "alamat"         => "required",
       "sub_totaln"     => "required|min_numeric,1",
       "detail_ongkir"  => "required",
       "tipe_pesanan"   => "required",
       "ekspedisi"      => "required",
     );

     GUMP::set_field_name("telp", "No. Telepon / HP");
     GUMP::set_field_name("kota", "Kota & Provinsi");
     GUMP::set_field_name("sub_totaln", "Daftar Produk");
     GUMP::set_field_name("detail_ongkir", "Paket Ekspedisi");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/checkout", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      /* Alur
      * ambil data pesanan sesuai session
      */
      $validasi = validasi_co($data);
      if( $validasi !== true ){
        return unprocessResponse($response, $validasi);
      }

      // Ambil produk dari session dan check stoknya
      $tipe_pesanan   = $data['tipe_pesanan'];
      $listPesan      = $_SESSION['public']['keranjang'][$tipe_pesanan];

      $listID=[];
      foreach ($listPesan as $key => $value) {
        $listID[] = $key;
      }

      $listProduk = $db->select("
          m_produk.*,
          m_produk_img.foto,
          m_promo.nama as promo,
          m_promo.id as m_promo_id,
          m_promo_det.diskon")
          ->from("m_produk")
          ->join("LEFT JOIN", "m_produk_img", "m_produk_img.m_produk_id = m_produk.id AND m_produk_img.is_primary=1")
          ->join("LEFT JOIN", "m_promo", "m_promo.is_used = 1")
          ->join("LEFT JOIN", "m_promo_det", "m_promo_det.m_promo_id = m_promo.id AND m_promo_det.m_produk_id = m_produk.id")
          ->where("m_produk.is_deleted", "=", 0)
          ->customWhere("m_produk.id IN(".implode(",", $listID).")", "AND")
          ->findAll();

      $listProduk = siapkan_produk($listProduk);

      $total= $total_berat = 0;
      foreach ($listProduk as $key => $value) {
        $listProduk[$key]->nama = limit_text($value->nama, 35);
        $listProduk[$key]->jumlah_pesan  = $listPesan[$value->id];
        $listProduk[$key]->sub_total     = $listPesan[$value->id] * $value->harga_jual;

        if($tipe_pesanan == 'non-po'){
          $listProduk[$key]->max = $listPesan[$value->id];
          // Cek jika jumlah stok memenuhi jumlah pesan, jika tidak maka isi dg stok yg tersisa
          if( $listProduk[$key]->jumlah_pesan <= $value->stok  ){
          } else {
            $listProduk[$key]->jumlah_pesan = $value->stok;
          }

        } else {
          $listProduk[$key]->max = 1000;
        }

        $total += $listProduk[$key]->sub_total;
        $total_berat += $listProduk[$key]->jumlah_pesan * $value->berat;
      }

      $setting = $db->find("SELECT * FROM m_setting ");

      // Menyiapkan data reff id
      if( isset($_SESSION['public']['afiliasi']) ){
        $member = $db->find("SELECT *
          FROM m_member
          WHERE kode = '".$_SESSION['public']['afiliasi']."'
        ");

        $m_member_id = !empty($member) ? $member->id : NULL;
      } else if( isset($_SESSION['public']['customer']['reff_id']) ){
        $m_member_id = $_SESSION['public']['customer']['reff_id'];
      } else {
        $m_member_id = NULL;
      }

      // Kode Penjualan
      $cekKode = $db->select("kode")
          ->from("t_penjualan")
          ->orderBy("id DESC")
          ->find();

      if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
      } else {
        $kode_terakhir = 0;
      }
      $kode_cust = (substr($kode_terakhir, -5) + 1);
      $kodeCust  = substr('00000' . $kode_cust, strlen($kode_cust));
      $kodeCust  = 'SPN'. date("Ym") . $kodeCust;

      // Catatan kirim
      $data["detail_ongkir"] = explode(",", $data["detail_ongkir"]);
      $catatanKirim = "";
      $catatanKirim .= "Dikirim dengan ekspedisi " . $data['ekspedisi'] ." ";
      if( $data['ekspedisi'] == 'dakota' ){
        $catatanKirim .= " - Biaya :Rp." . $data["detail_ongkir"][3];
      } else {
        $catatanKirim .= "paket " . $data["detail_ongkir"][0] . " - " . $data["detail_ongkir"][1]." ";
        $catatanKirim .= "estimasi " . $data["detail_ongkir"][2] . "hari - Biaya :Rp." . $data["detail_ongkir"][3];
      }

      $param_pj = [
        "m_member_id"     => $m_member_id,
        "m_member_id_customer" => $_SESSION['public']['customer']['id'],
        "kode"            => $kodeCust,
        "tanggal"         => date("Y-m-d"),
        "catatan"         => $catatanKirim,
        "nama_penerima"   => $_SESSION['public']['customer']['nama'],
        "telepon_penerima"=> $data['telp'],
        "jalan_penerima"  => $data['alamat'],
        "kode_pos_penerima"  => $data['kode_pos'],
        "ro_kota_id"      => $data['kota'],
        "w_kecamatan_id"  => $data['kecamatan'],
        "status"          => "Menunggu Pembayaran",
        "ekspedisi"       => $data['ekspedisi'],
        "total"           => $data['sub_totaln'],
        "kode_unik"       => $data['kode_unik'],
        "ongkir"          => $data["detail_ongkir"][3],
        "jenis_pesanan"   => $tipe_pesanan,
      ];

      $penjualan = $db->insert("t_penjualan", $param_pj);

      /* Menetapkan Tagihan + kode Unik */
      $tempTagihan    = $data['sub_totaln'] + $data["detail_ongkir"][3];
      $tempTagihan    = gen_kode_unik('pesanan', $tempTagihan);
      $insertTagihan  = $db->update("t_penjualan", ['tagihan_pesanan' => $tempTagihan], ['id' => $penjualan->id]);

      foreach ($listProduk as $key => $value) {
        $detail = [
          "t_penjualan_id"    => $penjualan->id,
          "m_produk_id"       => $value->id,
          "jumlah"            => $value->jumlah_pesan,
          "jenis"             => 'detail',
          "harga"             => $value->harga_jual,
        ];

        $insertDetail = $db->insert("t_penjualan_det", $detail);

        if( $tipe_pesanan == 'non-po'){
          $updateStok = $db->run("UPDATE m_produk SET stok = stok - {$value->jumlah_pesan} WHERE id = {$value->id}");
        }

        $total_poin_penjualan += ($value->poin * $value->jumlah_pesan);
      }

      // Update Poin Penjualan
      $updatePoin = $db->run("UPDATE t_penjualan SET poin_penjualan = {$total_poin_penjualan} WHERE id = {$penjualan->id}");

      /**
      * Setelah pesanan disimpan di DB, Destroy Session Pesanan tersebut
      */
      unset($_SESSION['public']['keranjang'][$tipe_pesanan]);

      return successResponse($response, [
        'penjualan'   => $penjualan,
        'redirect'    => config("SITE_URL") .'histori-pesanan-detail?id='.$penjualan->id,
      ]);
    } catch (Exception $e) {
      return unprocessResponse($response, ['Terjadi Kesalahan, cobalah beberapa saat lagi.' . $e]);
    }
})->setName('publicFrontend');

$app->post("/getKota", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $db->select("kota, provinsi, ro_kota_id")
      ->from("w_provinsi")
      ->join("LEFT JOIN", "w_kota", "w_kota.provinsi_id = w_provinsi.id")
      ->where("w_kota.is_deleted", "=", 0)
      ->andWhere("w_provinsi.is_deleted", "=", 0);

      if( isset($data['nama']) && $data['nama'] != '' ){
        $db->customWhere("w_kota.kota LIKE '%".$data["nama"]."%'", "AND");
      }

      $listKota = $db->findAll();

      return successResponse($response, [
        'listKota'  => $listKota,
      ]);
    } catch (Exception $e) {
      return unprocessResponse($response, ['Terjadi Kesalahan, cobalah beberapa saat lagi.']);
    }
})->setName('publicFrontend');

$app->post("/getKecamatan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $db->select("w_kota.kota, w_kecamatan.kecamatan, w_kecamatan.id")
      ->from("w_kota")
      ->leftJoin('w_kecamatan', 'w_kota.id = w_kecamatan.kota_id')
      ->where("w_kecamatan.is_deleted", "=", 0)
      ->andWhere("w_kota.ro_kota_id", "=", $data['kota_id']);

      $listKecamatan = $db->findAll();

      return successResponse($response, [
        'listKecamatan'  => $listKecamatan,
      ]);
    } catch (Exception $e) {
      return unprocessResponse($response, ['Terjadi Kesalahan, cobalah beberapa saat lagi.' . $e]);
    }
})->setName('publicFrontend');

function validasi_ongkir($data, $custom = array())
{
    $validasi = array(
       "origin"       => "required",
       "destination"  => "required",
       "weight"       => "required|min_numeric,1",
       "courier"      => "required",
     );

     GUMP::set_field_name("weight", "Total Berat Pesanan");
     GUMP::set_field_name("destination", "Kota Tujuan");

    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/cek-ongkir", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;

    try {
      if( $data['ekspedisi'] == 'dakota' ){
        $origin     = $db->select("w_kota.kota")
                      ->from("m_setting")
                      ->leftJoin("w_kota", "w_kota.ro_kota_id = m_setting.ro_kota_id")
                      ->find();

        $asal_kota  = $origin->kota;
        $asal_kota  = str_replace("KAB. ", "", $asal_kota);
        $asal_kota  = str_replace("KOTA ", "", $asal_kota);

        $asal_kota  = $db->select("dakota_kota.kota")
        ->from("dakota_kota")
        ->customWhere("kota LIKE '%".$asal_kota."%'", "AND")
        ->find();

        $asal_kota  = $asal_kota->kota;

        $tujuan = $db->select("
          dakota_provinsi.provinsi,
          dakota_kota.kota,
          dakota_kecamatan.kecamatan
          ")
        ->from("dakota_kecamatan")
        ->leftJoin("dakota_kota", "dakota_kecamatan.dakota_kota_id = dakota_kota.id")
        ->leftJoin("dakota_provinsi", "dakota_kota.dakota_provinsi_id = dakota_provinsi.id")
        ->customWhere("dakota_kecamatan.kecamatan LIKE '%".$data['kecamatan_text']."%'", "AND")
        ->find();

        $params = [
          "asal_kota"         => $asal_kota,
          "tujuan_provinsi"   => $tujuan->provinsi,
          "tujuan_kota"       => $tujuan->kota,
          "tujuan_kecamatan"  => $tujuan->kecamatan,
        ];

        $ongkir         = costDakota($params);
        $hitung_ongkir  = 0;

        if( $data['total_berat'] <= ($ongkir['price']['0']['minkg']*1000) ){
          $hitung_ongkir = $ongkir['price']['0']['pokok'];
        } else {
          $pokok      = $ongkir['price']['0']['pokok'];
          $minkg      = $ongkir['price']['0']['minkg']*1000;
          $kgnext     = $ongkir['price']['0']['kgnext'];
          $hitung_ongkir = $pokok + round( ($data['total_berat'] - $minkg) / 1000 ) * $kgnext;
        }

        return successResponse($response, [
          'origin_price'   => $ongkir['price']['0'],
          'hitung_ongkir'  => $hitung_ongkir,
          'ekspedisi'      => 'dakota',
        ]);
      } else {
        $origin = $db->find("SELECT ro_kota_id FROM m_setting");
        $params = [
          "origin"      => $origin->ro_kota_id,
          "destination" => $data['kota'],
          "weight"      => $data['berat'],
          "courier"     => $data['ekspedisi'],
        ];

        $validasi = validasi_ongkir($params);
        if( $validasi !== true )
          return unprocessResponse($response, $validasi);

        $list   = costRajaongkir($params);
        $hasil  = [];
        $list   = $list['rajaongkir']['results'][0]['costs'];

        foreach ($list as $key => $value) {
          $hasil[$key] = [
            'service'       => $value['service'],
            'description'   => $value['description'],
            'cost'          => $value['cost'][0]['value'],
            'note'          => $value['cost'][0]['note'],
            'etd'           => $value['cost'][0]['etd'],
          ];
        }

        return successResponse($response, [
          'status'    => empty($hasil) ? 'unavailable' : 'available',
          'ekspedisi' => 'rajaOngkir',
          'result'    => $hasil,
        ]);
      }

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server, cobalah beberapa saat lagi."]);
    }
})->setName('publicFrontend');

$app->get("/login-register", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      if( isset( $_SESSION['public']['customer']['id'] ) )
        return $response->withRedirect(config("SITE_URL") .'/home');

      $view = $this->view->fetch('public/login-register.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "login-register",
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;

    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

function validasi_reg_customer($data, $custom = array())
{
    $validasi = [
        "nama"          => "required",
        "username"      => "required",
        "password"      => "required",
        "password_confirm" => "required",
        "email"         => "required|valid_email",
    ];
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/register", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi_reg_customer($data);
      if( $validasi !== true)
        return unprocessResponse($response, $validasi);

      if( $data['password'] != $data['password_confirm'] ){
        $errorMessage[] = "Password yang Anda masukkan tidak sama.";
        return unprocessResponse($response, $errorMessage);
      }

      // Buat kode untuk Customer
      $cekKode = $db->select("kode")
          ->from("m_member")
          ->where("tipe_member", "=", "Customer")
          ->orderBy("id DESC")
          ->find();
      if ($cekKode) {
          $kode_terakhir = $cekKode->kode;
      } else {
          $kode_terakhir = '';
      }
      $kode_cust = (!empty($kode_terakhir)) ? (substr($kode_terakhir, -5) + 1) : 1;
      $kodeCust  = substr('00000' . $kode_cust, strlen($kode_cust));
      $kodeCust  = 'CUST' . date("Y") . $kodeCust;

      // Cek username dan email terdaftar?
      $errorMessage = [];
      $cekUsername = $db->select("*")
                    ->from("m_member")
                    ->where("email", "=", $data['email'])
                    ->orWhere("username", "=", $data['username'])
                    ->andWhere("status", "=", "Aktif")
                    ->find();

      if( isset($cekUsername->username) ){
        $errorMessage[] = "Username/email sudah digunakan, silakan pilih username/email yang baru.";
        return unprocessResponse($response, $errorMessage);

      } else {
        $params = [
          'kode'      => $kodeCust,
          'nama'      => $data['nama'],
          'username'  => $data['username'],
          'email'     => $data['email'],
          'password'  => sha1($data['password']),
          'reff_id'   => isset( $_SESSION['public']['reff_id'] ) ? $_SESSION['public']['reff_id'] : NULL,
          'status'    => 0,
          'tipe_member'    => "Customer",
        ];
        $new_customer = $db->insert("m_member", $params);

        // Kirim email konfirmasi
        $view             = $this->view->fetch('template_email/konfirmasi-register.html', [
            'SITE_IMG'        => config("SITE_IMG"),
            'konfirmasi_url'  => config('SITE_URL') . "/konfirmasi-register?kode=" . $new_customer->kode,
            'model'           => $new_customer,
        ]);

        $email_send = sendMail("Konfirmasi Pendaftaran Customer", $data['nama'], $data['email'], $view);
      }

      return successResponse($response, $data);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server."]);
    }
})->setName('publicFrontend');

$app->get("/konfirmasi-register", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {

      if( isset($data['kode']) && !empty($data['kode']) ){
        $is_registered = $db->select("*")
        ->from("m_member")
        ->where("tipe_member", "=", "Customer")
        ->andWhere("status", "=", 0)
        ->andWhere("kode", "=", $data['kode'])
        ->andWhere("is_deleted", "=", 0)
        ->find();
      }

      if( !empty($is_registered) ){
        $update_customer    = $db->update("m_member", ['status'=>'Aktif'], ['id'=>$is_registered->id]);
        $status_konfirmasi  = 'confirmed';
      } else {
        $status_konfirmasi  = 'false';
      }

      $view = $this->view->fetch('public/konfirmasi-register.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "konfirmasi-register",
          "listKategori"  => get_kategori_produk(),
          "status_konfirmasi" => $status_konfirmasi,
          "session"       => session_customer(),
      ]);
      echo $view;

    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/lupa-password", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {

      $view = $this->view->fetch('public/lupa-password.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "lupa-password",
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

function validasi_lupa($data, $custom = array())
{
    $validasi = [
        "email"         => "required|valid_email",
    ];
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->post("/reset-password", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi_lupa($data);
      if( $validasi !== true)
        return unprocessResponse($response, $validasi);

      $customer = $db->select("*")
      ->from("m_member")
      ->where("tipe_member", "=", "Customer")
      ->andWhere("status", "=", "Aktif")
      ->andWhere("email", "=", $data['email'])
      ->find();

      if( empty($customer) ) {
        $errorMessage[] = "Email yang Anda masukkan tidak terdaftar pada sistem.";
        return unprocessResponse($response, $errorMessage);
      }

      $unique_char  = sha1(rand() . $customer->kode . rand());
      $save_kode    = $db->update("m_member", ['password_reset'=> $unique_char], ['id'=>$customer->id]);

      // Kirim email konfirmasi
      $vars         = ['email' => $customer->email, 'kode' => $unique_char];
      $querystring  = http_build_query($vars);
      $url          = config('SITE_URL') . "/konfirm-reset-password?" . $querystring;

      $view             = $this->view->fetch('template_email/konfirmasi-lupa.html', [
          'SITE_IMG'        => config("SITE_IMG"),
          'konfirmasi_url'  => $url,
          'model'           => $new_customer,
      ]);

      $email_send = sendMail("Konfirmasi Reset Password Customer", $customer->nama, $customer->email, $view);
      return successResponse($response, $data);

    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server."]);
    }
})->setName('publicFrontend');

$app->get("/konfirm-reset-password", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    $not_found = $this->view->fetch('public/404.html', [
        "base_url"      => config('SITE_URL') . "/views",
        "site_url"      => config('SITE_URL') ,
        "listKategori"  => get_kategori_produk(),
        "session"       => session_customer(),
    ]);

    try {
      // Cek parameter email dan kode yang dikirim
      $customer = $db->select("*")
                ->from("m_member")
                ->where("email", "=", $data['email'])
                ->andWhere("password_reset", "=", $data['kode'])
                ->find();

      if( empty($customer) )
        echo $not_found; die;

      $view = $this->view->fetch('public/lupa-password.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "konfirm-reset-password",
          "customer"      => $customer,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    } catch (Exception $e) {
      echo $not_found;
    }
})->setName('publicFrontend');

function validasi_reset($data, $custom = array())
{
    $validasi = [
        "email"            => "required",
        "password"         => "required",
        "password_confirm" => "required",
    ];
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
$app->post("/konfirm-reset-password", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $validasi = validasi_reset($data);
      if( $validasi !== true)
        return unprocessResponse($response, $validasi);

      if( $data['password'] != $data['password_confirm'] ){
        $errorMessage[] = "Password yang Anda masukkan tidak sama.";
        return unprocessResponse($response, $errorMessage);
      }

      $customer = $db->select("*")
      ->from("m_member")
      ->where("tipe_member", "=", "Customer")
      ->andWhere("status", "=", "Aktif")
      ->andWhere("email", "=", $data['email'])
      ->andWhere("password_reset", "=", $data['kode_uniq'])
      ->find();

      if( empty($customer) ) {
        $errorMessage[] = "Customer tidak terdaftar pada sistem.";
        return unprocessResponse($response, $errorMessage);
      }

      $reset_pass    = $db->update("m_member", [
        'password'        => sha1($data['password']),
        'password_reset'  => NULL,
      ],
      [ 'id' => $customer->id]);

      return successResponse($response, $reset_pass);

    } catch (Exception $e) {
      return unprocessResponse($response, $e);
    }
})->setName('publicFrontend');

$app->post("/login", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      if( empty($data['username']) || empty($data['password']) )
        return unprocessResponse($response, ['Username / password harus diisi!']);

      $customer = $db->select("
        id, nama, email, reff_id
      ")
      ->from("m_member")
      ->where("username", "=", $data['username'])
      ->andWhere("password", "=", sha1($data['password']))
      ->andWhere("status", "=", 'aktif')
      ->find();

      if(empty($customer))
        return unprocessResponse($response, ['Username tidak terdaftar.']);

      $_SESSION['public']['customer'] = (array)$customer;
      return successResponse($response, ['redirect'=> config("SITE_URL") .'/home']);
      // return $response->withRedirect(config("SITE_URL") .'public/home');

    } catch (Exception $e) {
      return unprocessResponse($response, $e);
    }
})->setName('publicFrontend');

$app->get('/session', function ($request, $response) {
  return successResponse($response, session_customer());
})->setName('publicFrontend');

$app->get('/logout', function ($request, $response) {
  unset($_SESSION['public']['customer']);
  return $response->withRedirect(config("SITE_URL") .'/home');
})->setName('publicFrontend');

$app->post('/tambah-keranjang', function ($request, $response) {
  $data     = $request->getParams();
  $db       = $this->db;
  date_default_timezone_set("Asia/Jakarta");

  try {
    $produk       = explode(",", $data['m_produk_id']);
    $m_produk_id  = $produk[0];
    $jenis        = $produk[1]; // po/non-po
    $jumlah       = isset($produk[2]) ? $produk[2] : NULL; // jumlah increment atau decrement

    if( isset($_SESSION['public']['keranjang'][$jenis][$m_produk_id]) ){
      $_SESSION['public']['keranjang'][$jenis][$m_produk_id] += 1;
    } else {
      $_SESSION['public']['keranjang'][$jenis][$m_produk_id] = 1;
    }

    if( isset($jumlah) && $jumlah==(-1) ){
      $_SESSION['public']['keranjang'][$jenis][$m_produk_id] -= 2;
    }

    return successResponse($response, ['Produk berhasil ditambahkan ke keranjang belanja.']);
  } catch (Exception $e) {
    return unprocessResponse($response, ['Produk gagal ditambahkan. Silakan coba beberapa saat lagi.']);
  }
})->setName('publicFrontend');

$app->post('/remove-item-keranjang', function ($request, $response) {
  $data     = $request->getParams();
  $db       = $this->db;
  date_default_timezone_set("Asia/Jakarta");

  try {
    $produk       = explode(",", $data['m_produk_id']);
    $m_produk_id  = $produk[0];
    $jenis        = $produk[1]; // po/non-po

    if( isset($_SESSION['public']['keranjang'][$jenis][$m_produk_id]) ){
      unset($_SESSION['public']['keranjang'][$jenis][$m_produk_id]);
    }

    return successResponse($response, [
      'pesan'   => 'Produk berhasil dikeluarkan dari keranjang belanja.',
      'rowId'   => $m_produk_id,
    ]);
  } catch (Exception $e) {
    return unprocessResponse($response, ['Produk gagal dihapus. Silakan coba beberapa saat lagi.']);
  }
})->setName('publicFrontend');

$app->post('/is_login', function ($request, $response) {
  $data     = $request->getParams();
  $db       = $this->db;
  date_default_timezone_set("Asia/Jakarta");

  try {
    $is_login = NULL;

    if( isset( $_SESSION['public']['customer']['id'] ) ){
      $is_login= true;
    } else {
      $is_login= false;
    }
    return successResponse($response, [ 'is_login'   => $is_login ]);

  } catch (Exception $e) {
    return unprocessResponse($response, ['Produk gagal dihapus. Silakan coba beberapa saat lagi.']);
  }
})->setName('publicFrontend');

$app->get("/histori-pesanan", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {
      $listPesanan = $db->select("
          t_penjualan.*,
          m_member.nama customer_nama
          ")
          ->from("t_penjualan")
          ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id_customer")
          ->where("t_penjualan.m_member_id_customer", "=", $_SESSION['public']['customer']['id'])
          ->orderBy("t_penjualan.id DESC")
          ->findAll();

      $view = $this->view->fetch('public/histori-pesanan.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "histori-pesanan",
          "listKategori"  => get_kategori_produk(),
          "listPesanan"   => $listPesanan,
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->get("/histori-pesanan-detail", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    $_404 = $this->view->fetch('public/404.html', [
        "base_url"      => config('SITE_URL') . "/views",
        "site_url"      => config('SITE_URL') ,
        "listKategori"  => get_kategori_produk(),
        "session"       => session_customer(),
    ]);

    try {
      if( isset($data['id']) ){
        $t_pesanan_id = $data['id'];
      } else {
        echo $_404;
      }

      $pesanan = $db->select("
          t_penjualan.*,
          m_member.nama customer_nama,
          w_kota.kota,
          w_provinsi.provinsi
          ")
          ->from("t_penjualan")
          ->join("LEFT JOIN", "m_member", "m_member.id = t_penjualan.m_member_id_customer")
          ->join("LEFT JOIN", "w_kota", "w_kota.ro_kota_id = t_penjualan.ro_kota_id")
          ->join("LEFT JOIN", "w_provinsi", "w_kota.provinsi_id = w_provinsi.id")
          ->where("t_penjualan.id", "=", $t_pesanan_id)
          ->find();

      $detPesanan = $db->select("
        t_penjualan_det.*,
        m_produk.kode m_produk_kode,
        m_produk.nama m_produk_nama,
        m_produk.berat m_produk_berat,
        m_produk.stok as stok_asli,
        m_produk.poin,
        t_penjualan_det.jumlah * m_produk.berat total_berat,
        m_produk_img.foto
        ")
      ->from("t_penjualan_det")
      ->join("LEFT JOIN", "m_produk", "m_produk.id = t_penjualan_det.m_produk_id")
      ->join("LEFT JOIN", "m_produk_img", "m_produk.id = m_produk_img.m_produk_id AND m_produk_img.is_primary")
      ->where("t_penjualan_det.t_penjualan_id", "=", $t_pesanan_id)
      ->findAll();

      $total_berat  = $total = 0;
      $defaultImg   = config("SITE_IMG") . "no-image.png";
      foreach ($detPesanan as $key => $value) {
        $detPesanan[$key]->sub_total = ($value->harga * $value->jumlah) - $value->diskon;
        $total        += $detPesanan[$key]->sub_total;
        $total_berat  += $value->total_berat;

        if( empty($value->foto) ){
          $detPesanan[$key]->foto = $defaultImg;
        } else {
          $detPesanan[$key]->foto = config("SITE_IMG") . "/produk/$value->m_produk_id/$value->foto";
        }
      }

      $listBank       = getAllBank();
      $listSettingBank = $db->select("*")->from("m_setting_bank")->findAll();


      if( !empty($listSettingBank) ) {
        foreach ($listSettingBank as $key => $value) {
          $listSettingBank[$key]->bank = isset( $listBank[$value->bank]['name'] ) ? $listBank[$value->bank]['name'] : [];
        }
      }

      // Penambahan kode unik di grand total belanja
      // $grand_total          = $pesanan->total+$pesanan->ongkir;
      // $grand_total          = substr($grand_total, 0, (strlen($grand_total)-3) ) . $pesanan->kode_unik;
      $pesanan->grand_total = $pesanan->tagihan_pesanan;

      $view = $this->view->fetch('public/histori-pesanan-detail.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "histori-pesanan",
          "listKategori"  => get_kategori_produk(),
          "pesanan"       => $pesanan,
          "detPesanan"    => $detPesanan,
          "total"         => $total,
          "session"       => session_customer(),
          "listSettingBank" => $listSettingBank
      ]);

      echo $view;
    } catch (Exception $e) {
      echo $_404;
    }
})->setName('publicFrontend');

/*
* Komplain customer
*/
function getListKomplain(){
  $db         = config('DB');
  $db         = new Cahkampung\Landadb($db['db']);

  if( empty( $_SESSION['public']['customer']['id'] ) ){
    return [];
  }

  $m_member_id  = $_SESSION['public']['customer']['id'];
  $listKomplain = $db->select("
          m_komplain.*,
          m_member.nama,
          m_member.kode as kode_customer,
          m_member.email
        ")
        ->from("m_komplain")
        ->join("LEFT JOIN", "m_member", "m_member.id = m_komplain.m_member_id")
        ->where("tipe", "=", "post")
        ->andWhere("m_komplain.m_member_id", "=", $m_member_id)
        ->andWhere("m_komplain.is_deleted", "=", 0)
        ->orderBy("m_komplain.id DESC")
        ->findAll();

  $defaultImg = config("SITE_IMG") . "no-image.png";
  foreach ($listKomplain as $key => $value) {
    $listKomplain[$key]->image = isset($value->image) ? config("SITE_IMG") . "komplain/" . $value->image : $defaultImg;

  }

  return $listKomplain;
}

$app->get("/komplain", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    try {

      $view = $this->view->fetch('public/komplain.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "komplain",
          "listKategori"  => get_kategori_produk(),
          "listKomplain"  => getListKomplain(),
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {
      $view = $this->view->fetch('public/404.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "listKategori"  => get_kategori_produk(),
          "session"       => session_customer(),
      ]);
      echo $view;
    }
})->setName('publicFrontend');

$app->post('/submit-komplain', function ($request, $response) {
  $data     = $request->getParams();
  $db       = $this->db;
  date_default_timezone_set("Asia/Jakarta");

  try {

    if(isset($_FILES["image"])){
      $img_path     = __DIR__;
      $img_path     = substr($img_path, 0, strpos($img_path, "api")) . "img/komplain/";
      $allowedExts  = array("gif", "jpeg", "jpg", "png");
      $temp         = explode(".", $_FILES["image"]["name"]);
      $newfilename  = round(microtime(true)) . '.' . end($temp);
      $extension    = end($temp);

      if( ($_FILES["image"]["size"] < 500000) && in_array($extension, $allowedExts) ){
        move_uploaded_file($_FILES["image"]["tmp_name"], $img_path . $newfilename);
      }
    }

    // Kode Komplain
    if( $data['tipe'] == 'post' ){
      $cekKode = $db->select("kode")
          ->from("m_komplain")
          ->where('tipe', "=", 'post')
          ->orderBy("id DESC")
          ->find();

      if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
      } else {
        $kode_terakhir = 0;
      }
      $kode_cust = (substr($kode_terakhir, -5) + 1);
      $kodeCust  = substr('00000' . $kode_cust, strlen($kode_cust));
      $kodeCust  = 'KPL'. date("Ym") . $kodeCust;
    }

    $params = [
      'kode'      => isset($kodeCust) ? $kodeCust : NULL,
      'tanggal'   => date("Y-m-d"),
      'subjek'    => isset($data['subjek']) ? $data['subjek'] : NULL,
      'kategori'  => isset($data['kategori']) ? $data['kategori'] : NULL,
      'isi'       => isset($data['content']) ? $data['content'] : NULL,
      'tipe'      => $data['tipe'],
      'm_komplain_id' => isset($data['m_komplain_id']) ? $data['m_komplain_id'] : NULL,
      'image'     => isset($newfilename) ? $newfilename : NULL,
      'm_member_id' => isset($data['m_member_id']) ? $data['m_member_id'] : $_SESSION['user']['id'],
      'is_closed' => 0,
    ];

    $newKomplain = $db->insert("m_komplain", $params);
    if( $data['tipe'] == 'reply' ){
      return $response->withRedirect(config("SITE_URL") .'/komplain-detail?id=' . $newKomplain->m_komplain_id);
    } else {
      return $response->withRedirect(config("SITE_URL") .'/komplain');
    }

  } catch (Exception $e) {
    $view = $this->view->fetch('public/404.html', [
        "base_url"      => config('SITE_URL') . "/views",
        "site_url"      => config('SITE_URL') ,
        "listKategori"  => get_kategori_produk(),
        "session"       => session_customer(),
    ]);
    echo $view;
  }
})->setName('publicFrontend');

$app->get("/komplain-detail", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    date_default_timezone_set("Asia/Jakarta");

    $_404 = $this->view->fetch('public/404.html', [
        "base_url"      => config('SITE_URL') . "/views",
        "site_url"      => config('SITE_URL') ,
        "listKategori"  => get_kategori_produk(),
        "session"       => session_customer(),
    ]);

    try {
      if( empty($data['id']) ){
        echo $_404;
      }

      $komplain = $db->select("
          m_komplain.*,
          m_member.nama,
          m_member.kode,
          m_member.email
        ")
        ->from("m_komplain")
        ->join("LEFT JOIN", "m_member", "m_member.id = m_komplain.m_member_id")
        ->where("tipe", "=", "post")
        ->andWhere("m_komplain.id", "=", $data['id'])
        ->find();

      $komplain->image = isset($komplain->image) ? config("SITE_IMG") . "komplain/" . $komplain->image : NULL;

      $db->select("
            m_komplain.*,
            m_member.nama,
            m_member.kode,
            m_member.email
          ")
          ->from("m_komplain")
          ->join("LEFT JOIN", "m_member", "m_member.id = m_komplain.m_member_id")
          ->where("tipe", "=", "reply")
          ->andWhere("m_komplain.m_komplain_id", "=", $data['id'])
          ->andWhere("m_komplain.is_deleted", "=", 0);

      $komplain_det = $db->findAll();

      $view = $this->view->fetch('public/komplain-detail.html', [
          "base_url"      => config('SITE_URL') . "/views",
          "site_url"      => config('SITE_URL') ,
          "uri"           => "komplain-detail",
          "listKategori"  => get_kategori_produk(),
          "komplain"      => $komplain,
          "komplain_det"  => $komplain_det,
          "session"       => session_customer(),
      ]);

      echo $view;
    } catch (Exception $e) {

      echo $_404;
    }
})->setName('publicFrontend');


/*
* End of Komplain customer
*/
